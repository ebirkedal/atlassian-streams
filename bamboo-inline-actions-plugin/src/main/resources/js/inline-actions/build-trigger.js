/**
 * Registers a "Trigger build" action against any feed items with a "build" type.
 * 
 * Creates a link which triggers a build for the specified plan.
 */
(function() {
    
    /**
     * Adds a Bamboo build to the queue.
     * 
     * @method addBuildToQueue
     * @param {Event} e Event object
     */
    function addBuildToQueue(e) {
        var target = AJS.$(e.target),
            activityItem = target.closest('div.activity-item'),
            triggerUrl,
            feedItem = e.data && e.data.feedItem;
        
        if (feedItem) {
            triggerUrl = feedItem.links['http://streams.atlassian.com/syndication/build-trigger'];
        } else {
            ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.bamboo.action.trigger.failure.general'), 'error');
            return;
        }
        
        e.preventDefault();
        hideTriggerLink(activityItem);

        AJS.$.ajax({
            type : 'POST',
            contentType: 'application/json',
            url : ActivityStreams.InlineActions.proxy(triggerUrl, feedItem),
            global: false,
            beforeSend: function() {
                target.trigger('beginInlineAction');
            },
            complete: function() {
                target.trigger('completeInlineAction');
            },
            success : function() {
                ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.bamboo.action.trigger.success'), 'info', function() {
                    showTriggerLink(activityItem);
                });
            },
            error : function(request) {
                var msg;
                //check both request.status and request.rc for backwards compatibility
                if (request.status == 401 || request.rc == 401) {
                    // User triggering build likely does not exist on Bamboo instance, 
                    // or trusted apps configuration is not properly set up.
                    msg = AJS.I18n.getText('streams.bamboo.action.trigger.failure.authentication');
                } else {
                    msg = AJS.I18n.getText('streams.bamboo.action.trigger.failure.general');
                }
                ActivityStreams.InlineActions.statusMessage(activityItem, msg, 'error', function() {
                    showTriggerLink(activityItem);
                });
            }
        });
    }

    /**
     * Hide the trigger link, showing the non-hyperlinked label instead.
     * 
     * @method hideTriggerLink
     * @param {Object} activityItem the .activity-item div
     */
    function hideTriggerLink(activityItem) {
        activityItem.find('a.activity-item-build-trigger-link').addClass('hidden');
        activityItem.find('span.activity-item-build-trigger-label').removeClass('hidden');
    }

    /**
     * Show the trigger link, hiding the non-hyperlinked label in the process.
     * 
     * @method showTriggerLink
     * @param {Object} activityItem the .activity-item div
     */
    function showTriggerLink(activityItem) {
        activityItem.find('a.activity-item-build-trigger-link').removeClass('hidden');
        activityItem.find('span.activity-item-build-trigger-label').addClass('hidden');
    }

    /**
     * Builds a "Trigger" link that triggers the action
     * 
     * @method buildTriggerBuildLink
     * @param {Object} feedItem Object representing the activity item
     * @return {HTMLElement}
     */
    function buildTriggerBuildLink(feedItem) {
        //if no build-trigger link exists in the feed item, do not bind the entry to a trigger handler
        if (!feedItem.links['http://streams.atlassian.com/syndication/build-trigger']) {
            return;
        } 
        
        var link = AJS.$('<a href="#" class="activity-item-build-trigger-link"></a>')
                .text(AJS.I18n.getText('streams.bamboo.action.trigger.title'))
                .bind('click', {feedItem: feedItem}, addBuildToQueue),
            label = AJS.$('<span class="activity-item-build-trigger-label hidden"></span>')
                .text(AJS.I18n.getText('streams.bamboo.action.trigger.title'));
        
        return link.add(label);
    }

    // Registers the trigger action for any builds in the feed
    ActivityStreams.registerAction('job', buildTriggerBuildLink, 5);
})();