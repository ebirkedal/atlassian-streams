/**
 * Registers a "Create Review" action against any feed items with a "changeset" type.
 * 
 * Creates a link which creates a review from a changeset.
 */
(function() {

    /**
     * Builds a link to trigger the action.
     * 
     * @method buildLink
     * @param {Object} feedItem Object representing the activity item
     * @return {HTMLElement}
     */
    function buildLink(feedItem) {
        var url = feedItem.links['http://streams.atlassian.com/syndication/changeset-review'];

        //if no changeset-review link exists in the feed item, do not bind the entry to a trigger handler
        if (!url) {
            return;
        } 
        
        return AJS.$('<a class="activity-item-changeset-review-link"></a>')
                .attr('href', url)
                .text(AJS.I18n.getText('streams.fecru.action.changeset.review.title'));
    }

    // Registers the action for any changesets in the feed
    ActivityStreams.registerAction('changeset', buildLink, 5);
})();