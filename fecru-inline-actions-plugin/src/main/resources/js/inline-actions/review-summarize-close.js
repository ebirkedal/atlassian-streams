/**
 * Registers a "Summarise and Close" action against any feed items with a "review" type.
 *
 * Creates a link which summarizes and closes a review.
 */
(function() {

    /**
     * Summarizes and closes a review.
     *
     * @method summarizeAndCloseReview
     * @param {Event} e Event object
     */
    function summarizeAndCloseReview(e) {
        var activityItem = AJS.$(e.target).closest('div.activity-item'),
            url,
            feedItem = e.data && e.data.feedItem;


        if (feedItem) {
            url = feedItem.links['http://streams.atlassian.com/syndication/review-summarize-close'];
        } else {
            ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.fecru.action.review.summarize.close.failure.general'), 'error');
            return;
        }

        e.preventDefault();
        hideLink(activityItem);

        // hit the summarize resource, which will call the close resource if successful
        ActivityStreams.ReviewTransitions.summarizeAndClose(e, activityItem, url, feedItem);
    }

    /**
     * Hide the link, showing the non-hyperlinked label instead.
     *
     * @method hideLink
     * @param {Object} activityItem the .activity-item div
     */
    function hideLink(activityItem) {
        activityItem.find('a.activity-item-review-summarize-close-link').addClass('hidden');
        activityItem.find('span.activity-item-review-summarize-close-label').removeClass('hidden');
    }

    /**
     * Builds a link to trigger the action.
     *
     * @method buildLink
     * @param {Object} feedItem Object representing the activity item
     * @return {HTMLElement}
     */
    function buildLink(feedItem) {
        //if no such link exists in the feed item, do not bind the entry to a trigger handler
        if (!feedItem.links['http://streams.atlassian.com/syndication/review-summarize-close']) {
            return;
        }

        var link = AJS.$('<a href="#" class="activity-item-review-summarize-close-link"></a>')
                .text(AJS.I18n.getText('streams.fecru.action.review.summarize.close.title'))
                .bind('click', {feedItem: feedItem}, summarizeAndCloseReview),
            label = AJS.$('<span class="activity-item-review-summarize-close-label hidden"></span>')
                .text(AJS.I18n.getText('streams.fecru.action.review.summarize.close.title'));

        return link.add(label);
    }

    // Registers the action for any reviews in the feed
    ActivityStreams.registerAction('review', buildLink, 5);
})();