#!/bin/sh

milestone=$1

if [ $# -eq 0 ] 
  then 
    echo -e "STRM milestone release: \c"
    read milestone
fi

echo -e "\nDownloading STRMs $milestone \n"

script_dir=/opt/j2ee/domains/atlassian.com/idog/scripts
temp_dir=$script_dir/temp/$milestone

# create temp directory
mkdir -p $temp_dir

# download streams files
$script_dir/get-snapshot.sh $milestone

# move downloaded files to created directory
mv *.jar $temp_dir

echo -e "\nDeploying STRM $milestone to JIRA"
$script_dir/deploy_jira.sh $milestone $temp_dir

echo -e "\nDeploying STRM $milestone to Confluence"
$script_dir/deploy_confluence.sh $milestone $temp_dir

echo -e "\nDeploying STRM $milestone to Bamboo"
$script_dir/deploy_bamboo.sh $milestone $temp_dir

echo -e "\nDeploying STRM $milestone to Fe/Cru"
$script_dir/deploy_fecru.sh $milestone $temp_dir

# remove directory
rm -rf $temp_dir

