#!/bin/sh

script_dir=/opt/j2ee/domains/atlassian.com/idog/scripts
milestone=$1
artifact_dir=$2

if [ $# -eq 0 ] 
  then 
    echo -e "STRM milestone release: \c"
    read milestone
    echo -e "STRM artifact directory: \c"
    read artifact_dir
else
  if [ $# -eq 1 ]
    then
      echo -e "STRM artifact directory: \c"
      read artifact_dir
  fi
fi

temp_dir=$script_dir/temp/$milestone

# create temp directory
mkdir -p $temp_dir

echo -e "\nCopying files from $artifact_dir to $temp_dir \n"

# move downloaded files to created directory
cp $artifact_dir/*.jar $temp_dir

echo -e "\nDeploying STRM $milestone to JIRA"
$script_dir/deploy_jira.sh $milestone $temp_dir

echo -e "\nDeploying STRM $milestone to Confluence"
$script_dir/deploy_confluence.sh $milestone $temp_dir

echo -e "\nDeploying STRM $milestone to Bamboo"
$script_dir/deploy_bamboo.sh $milestone $temp_dir

echo -e "\nDeploying STRM $milestone to Fe/Cru"
$script_dir/deploy_fecru.sh $milestone $temp_dir

# remove directory
rm -rf $temp_dir

