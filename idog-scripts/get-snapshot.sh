#!/bin/sh

# $0 <milestone> 
milestone=$1

script_dir=/opt/j2ee/domains/atlassian.com/idog/scripts

PID=$$ # get PID of current process
LogUID=`cat /proc/"$PID"/loginuid` # get loginuid of current process
username=`getent passwd "$LogUID" | awk -F ":" '{print $1}'` # translate loginuid to username

streams_plugins="streams-aggregator-plugin                          \
                 streams-api                                        \
                 streams-bamboo-plugin                              \
                 streams-bamboo-inline-actions-plugin               \
                 streams-confluence-plugin                          \
                 streams-confluence-inline-actions-plugin           \
                 streams-core-plugin                                \
                 streams-crucible-plugin                            \
                 streams-fecru-activity-item-provider-plugin        \
                 streams-fecru-inline-actions-plugin                \
                 streams-fisheye-plugin                             \
                 streams-inline-actions-plugin                      \
                 streams-jira-plugin                                \
                 streams-jira-inline-actions-plugin                 \
                 streams-spi                                        \
                 streams-thirdparty-plugin"

jars=""

# download plugin files
$script_dir/get-plugins.sh

for plugin in $streams_plugins
do
  jars="$jars --remote-name https://maven.atlassian.com/service/local/repo_groups/internal/content/com/atlassian/streams/$plugin/$milestone/$plugin-$milestone.jar"
done

curl --user $username $jars

