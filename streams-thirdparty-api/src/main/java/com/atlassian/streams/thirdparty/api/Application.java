package com.atlassian.streams.thirdparty.api;

import java.net.URI;

import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;

import org.apache.commons.lang.StringUtils;

import static com.atlassian.streams.api.common.Option.some;

import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Either.right;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Identifies the application that generated an {@link Activity}.
 */
public class Application
{
    private final String displayName;
    private final URI id;

    /**
     * Attempts to construct an {@link Application} instance from string parameters which have not
     * been validated.  This overload is used internally by the activity REST API.
     * @param displayName  the human-readable name of the application (may not contain HTML)
     * @param idString  the application's identifying URI as a string
     * @return  an {@link Either} containing either an {@link Application} ({@link Either#right()}) if
     *   successful, or {@link ValidationErrors} ({@link Either#left()}) if not
     */
    public static Either<ValidationErrors, Application> application(Option<String> displayName,
                                                                    Option<String> idString)
    {
        ValidationErrors.Builder errors = new ValidationErrors.Builder();
        if (!displayName.isDefined())
        {
            errors.addError("displayName cannot be omitted");
        }
        else
        {
            errors.checkString(displayName, "displayName");
            if (StringUtils.isBlank(displayName.get()))
            {
                errors.addError("displayName cannot be blank");
            }
        }
        if (!idString.isDefined())
        {
            errors.addError("id cannot be omitted");
        }
        else
        {
            Option<URI> id = errors.checkAbsoluteUriString(idString, "id");
            if (errors.isEmpty() && id.isDefined())
            {
                return right(new Application(displayName.get(), id.get()));
            }
        }
        return left(errors.build());
    }
    
    /**
     * Constructs an {@link Application} instance.  Use this factory method only if the parameters
     * are known to be valid; it will throw an {@link IllegalArgumentException} otherwise.
     * @param displayName  the human-readable name of the application (may not contain HTML)
     * @param id  the application's identifying URI
     */
    public static Application application(String displayName, URI id)
    {
        checkNotNull(displayName, "displayName");
        checkNotNull(id, "id");
        Either<ValidationErrors, Application> ret = application(some(displayName), some(id.toASCIIString()));
        if (ret.isLeft())
        {
            throw new IllegalArgumentException(ret.left().get().toString());
        }
        return ret.right().get();
    }

    private Application(String displayName, URI id)
    {
        this.displayName = displayName;
        this.id = id;
    }
    
    public String getDisplayName()
    {
        return displayName;
    }

    public URI getId()
    {
        return id;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (other instanceof Application)
        {
            Application a = (Application) other;
            return displayName.equals(a.displayName) && id.equals(a.id);
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return displayName.hashCode() * 37 + id.hashCode();
    }
}
