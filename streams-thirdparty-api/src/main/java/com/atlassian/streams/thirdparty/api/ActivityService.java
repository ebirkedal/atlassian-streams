package com.atlassian.streams.thirdparty.api;

import com.atlassian.streams.api.common.Option;

/**
 * Programmatic interface to persistent storage for third-party activity items.
 * Activities can also be queried or modified via REST.
 */
public interface ActivityService
{
    /**
     * Adds a new activity item.
     * @param activity  an {@link Activity}
     * @return  an updated {@link Activity}, identical to the input but with an
     *   automatically generated primary key ({@code activityId})
     */
    Activity postActivity(Activity activity);

    /**
     * Queries an activity item by primary key
     * @param activityId  the primary key
     * @return  an {@link Option} containing an {@link Activity}, or {@link Option#none()} if not found
     */
    Option<Activity> getActivity(long activityId);

    /**
     * Queries multiple activity items.
     * @param query  a {@link ActivityQuery} with optional query criteria;
     *   use {@link ActivityQuery#all()} to get all existing items
     * @return  an Iterable containing all {@link Activity} objects that match the query
     */
    Iterable<Activity> activities(ActivityQuery query);

    /**
     * Attempts to delete an activity item.
     * @param activityId  the primary key of the activity to delete
     * @return  {@code true} if the activity was deleted; {@code false} if it could not be deleted
     *   (i.e. invalid primary key)
     */
    boolean delete(long activityId);

    /**
     * Returns all the unique application objects
     * @return an Iterable containing all unique {@link Application}s
     */
    Iterable<Application> applications();
}
