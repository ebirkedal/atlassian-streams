package com.atlassian.streams.confluence;

import java.util.Collection;
import java.util.Set;

import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.ContributorQuery;
import com.atlassian.confluence.search.v2.query.CreatorQuery;
import com.atlassian.confluence.search.v2.query.DateRangeQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.query.MultiTextFieldQuery;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.junit.Test;

import static com.atlassian.confluence.search.service.ContentTypeEnum.ATTACHMENT;
import static com.atlassian.confluence.search.service.ContentTypeEnum.BLOG;
import static com.atlassian.confluence.search.service.ContentTypeEnum.COMMENT;
import static com.atlassian.confluence.search.service.ContentTypeEnum.PAGE;
import static com.atlassian.confluence.search.service.ContentTypeEnum.PERSONAL_SPACE_DESCRIPTION;
import static com.atlassian.confluence.search.service.ContentTypeEnum.SPACE_DESCRIPTION;
import static com.atlassian.confluence.search.service.ContentTypeEnum.USER_STATUS;
import static com.atlassian.confluence.search.v2.BooleanOperator.OR;
import static com.atlassian.confluence.search.v2.query.BooleanQuery.composeOrQuery;
import static com.atlassian.confluence.search.v2.query.DateRangeQuery.DateRangeQueryType.MODIFIED;
import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityObjectTypes.file;
import static com.atlassian.streams.api.ActivityObjectTypes.status;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.page;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.personalSpace;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static com.atlassian.streams.confluence.ConfluenceSearchQueryBuilder.CONTENT_TYPES;
import static com.atlassian.streams.confluence.ConfluenceSearchQueryBuilder.TEXT_FIELDS;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.union;
import static org.junit.Assert.assertEquals;

public class ConfluenceSearchQueryBuilderTest
{
    private static final boolean INCLUDE_TO = true;
    private static final boolean INCLUDE_FROM = true;

    private static final DateTime OCT_10_2010_10_10_10 = new DateTime()
        .withYear(2010).withMonthOfYear(Months.TEN.getMonths()).withDayOfMonth(10)
        .withHourOfDay(10).withMinuteOfHour(10).withSecondOfMinute(10).withMillisOfSecond(0);

    private static final DateTime OCT_12_2010_05_00_00 = new DateTime()
        .withYear(2010).withMonthOfYear(Months.TEN.getMonths()).withDayOfMonth(12)
        .withHourOfDay(5).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);

    @Test
    public final void testSingleCreator()
    {
        assertEquals(newQuery(new CreatorQuery("bob")), new ConfluenceSearchQueryBuilder().createdBy("bob").build());
    }

    @Test
    public final void testMultipleCreators()
    {
        assertEquals(newQuery(composeOrQuery(new CreatorQuery("bob"), new CreatorQuery("mary"))),
                new ConfluenceSearchQueryBuilder().createdBy("bob", "mary").build());
    }

    @Test
    public final void testNullOrBlankCreators()
    {
        assertEquals(newQuery(new CreatorQuery("bob")), new ConfluenceSearchQueryBuilder().createdBy("bob", null, " ").build());
    }

    @Test
    public final void testDuplicateUntrimmedCreator()
    {
        assertEquals(newQuery(new CreatorQuery("bob")), new ConfluenceSearchQueryBuilder().createdBy(" bob ", "bob").build());
    }

    @Test
    public final void testSingleLastModifier()
    {
        assertEquals(newQuery(new ContributorQuery("bob")), new ConfluenceSearchQueryBuilder().lastModifiedBy("bob").build());
    }

    @Test
    public final void testMultipleLastModifiers()
    {
        assertEquals(newQuery(composeOrQuery(new ContributorQuery("bob"), new ContributorQuery("mary"))),
                new ConfluenceSearchQueryBuilder().lastModifiedBy("bob", "mary").build());
    }

    @Test
    public final void testNullOrBlankLastModifiers()
    {
        assertEquals(newQuery(new ContributorQuery("bob")),
                new ConfluenceSearchQueryBuilder().lastModifiedBy(null, " ", "bob").build());
    }

    @Test
    public final void testDuplicateUntrimmedLastModifier()
    {
        assertEquals(newQuery(new ContributorQuery("bob")),
                new ConfluenceSearchQueryBuilder().lastModifiedBy(" bob ", "bob").build());
    }

    @Test
    public final void testSingleSpaceKey()
    {
        assertEquals(newQuery(new InSpaceQuery("JST")), new ConfluenceSearchQueryBuilder().inSpace("JST").build());
    }

    @Test
    public final void testMultipleSpaceKeys()
    {
        assertEquals(newQuery(new InSpaceQuery(newHashSet("JST", "PLUG"))),
                new ConfluenceSearchQueryBuilder().inSpace("JST", "PLUG").build());
    }

    @Test
    public final void testNullOrBlankSpaceKeys()
    {
        assertEquals(newQuery(new InSpaceQuery("JST")), new ConfluenceSearchQueryBuilder().inSpace("JST", null, " ").build());
    }

    @Test
    public final void testDuplicateUntrimmedSpaceKeys()
    {
        assertEquals(newQuery(new InSpaceQuery("JST")), new ConfluenceSearchQueryBuilder().inSpace(" JST ", "JST").build());
    }

    @Test
    public final void testSingleSearchTerm()
    {
        assertEquals(newQuery(new MultiTextFieldQuery("confluence", TEXT_FIELDS, OR)),
                new ConfluenceSearchQueryBuilder().searchFor("confluence").build());
    }

    @Test
    public final void testMultipleSearchTerms()
    {
        assertEquals(newQuery(new MultiTextFieldQuery("jira confluence", TEXT_FIELDS, OR)),
                new ConfluenceSearchQueryBuilder().searchFor("jira", "confluence").build());
    }

    @Test
    public final void testNullOrBlankSearchTerms()
    {
        assertEquals(newQuery(new MultiTextFieldQuery("jira", TEXT_FIELDS, OR)),
                new ConfluenceSearchQueryBuilder().searchFor("jira", null, " ").build());
    }

    @Test
    public final void testDuplicateUntrimmedSearchTerm()
    {
        assertEquals(newQuery(new MultiTextFieldQuery("jira", TEXT_FIELDS, OR)),
                new ConfluenceSearchQueryBuilder().searchFor(" jira ", "jira").build());
    }

    @Test
    public final void testSingleCreatorOrLastModifier()
    {
        assertEquals(newQuery(composeOrQuery(new CreatorQuery("bob"), new ContributorQuery("bob"))),
                new ConfluenceSearchQueryBuilder().createdOrLastModifiedBy("bob").build());
    }

    @Test
    public final void testMultipleCreatorOrLastModifiers()
    {
        assertEquals(newQuery(composeOrQuery(newHashSet(
                    new CreatorQuery("bob"), new ContributorQuery("bob"),
                    new CreatorQuery("mary"), new ContributorQuery("mary")))),
                new ConfluenceSearchQueryBuilder().createdOrLastModifiedBy("bob", "mary").build());
    }

    @Test
    public final void testThatSpecifyingOnlyMinDateCreatesDateRangeWithMaxDateNull()
    {
        assertEquals(newQuery(new DateRangeQuery(OCT_10_2010_10_10_10.toDate(), null, INCLUDE_FROM, INCLUDE_TO, MODIFIED)),
                new ConfluenceSearchQueryBuilder().minDate(some(OCT_10_2010_10_10_10.toDate())).build());
    }

    @Test
    public final void testThatSpecifyingOnlyMaxDateCreatesDateRangeWithMinDateNull()
    {
        assertEquals(newQuery(new DateRangeQuery(null, OCT_12_2010_05_00_00.toDate(), INCLUDE_FROM, INCLUDE_TO, MODIFIED)),
                new ConfluenceSearchQueryBuilder().maxDate(some(OCT_12_2010_05_00_00.toDate())).build());
    }

    @Test
    public final void testThatSpecifyingBothMinAndMaxDateCreatesDateRangeWithBothDate()
    {
        assertEquals(newQuery(new DateRangeQuery(OCT_10_2010_10_10_10.toDate(), OCT_12_2010_05_00_00.toDate(), INCLUDE_FROM, INCLUDE_TO, MODIFIED)),
                new ConfluenceSearchQueryBuilder()
                    .minDate(some(OCT_10_2010_10_10_10.toDate()))
                    .maxDate(some(OCT_12_2010_05_00_00.toDate())).build());
    }

    @Test
    public final void testThatExcludedSearchTermsAreAddedAsMustNots()
    {
        assertEquals(newQueryWithNot(new MultiTextFieldQuery("confluence", TEXT_FIELDS, OR)),
                new ConfluenceSearchQueryBuilder()
                    .excludeTerms(ImmutableList.of("confluence")).build());
    }

    @Test
    public final void testThatArticleRequestSearchesForOnlyBlogContent()
    {
        assertEquals(newQuery(ImmutableSet.<SearchQuery>of(), ImmutableSet.<SearchQuery>of(), ImmutableList.of(BLOG)),
                     new ConfluenceSearchQueryBuilder()
                          .activityObjects(ImmutableList.of(article())).build());
    }

    @Test
    public final void testThatPageRequestSearchesForOnlyPageContent()
    {
        assertEquals(newQuery(ImmutableSet.<SearchQuery>of(), ImmutableSet.<SearchQuery>of(), ImmutableList.of(PAGE)),
                     new ConfluenceSearchQueryBuilder()
                          .activityObjects(ImmutableList.of(page())).build());
    }

    @Test
    public final void testThatAttachmentRequestSearchesForOnlyAttachments()
    {
        assertEquals(newQuery(ImmutableSet.<SearchQuery>of(), ImmutableSet.<SearchQuery>of(), ImmutableList.of(ATTACHMENT)),
                     new ConfluenceSearchQueryBuilder()
                          .activityObjects(ImmutableList.of(file())).build());
    }

    @Test
    public final void testThatCommentRequestSearchesForOnlyComments()
    {
        assertEquals(newQuery(ImmutableSet.<SearchQuery>of(), ImmutableSet.<SearchQuery>of(), ImmutableList.of(COMMENT)),
                     new ConfluenceSearchQueryBuilder()
                          .activityObjects(ImmutableList.of(comment())).build());
    }

    @Test
    public final void testThatPersonalSpaceRequestSearchesForOnlyPersonalSpaceDescriptions()
    {
        assertEquals(newQuery(ImmutableSet.<SearchQuery>of(), ImmutableSet.<SearchQuery>of(), ImmutableList.of(PERSONAL_SPACE_DESCRIPTION)),
                     new ConfluenceSearchQueryBuilder()
                          .activityObjects(ImmutableList.of(personalSpace())).build());
    }

    @Test
    public final void testThatSpaceRequestSearchesForOnlySpaceDescriptions()
    {
        assertEquals(newQuery(ImmutableSet.<SearchQuery>of(), ImmutableSet.<SearchQuery>of(), ImmutableList.of(SPACE_DESCRIPTION, PERSONAL_SPACE_DESCRIPTION)),
                     new ConfluenceSearchQueryBuilder()
                          .activityObjects(ImmutableList.of(space())).build());
    }

    @Test
    public final void testThatStatusRequestSearchesForOnlyUserStatus()
    {
        assertEquals(newQuery(ImmutableSet.<SearchQuery>of(), ImmutableSet.<SearchQuery>of(), ImmutableList.of(USER_STATUS)),
                     new ConfluenceSearchQueryBuilder()
                          .activityObjects(ImmutableList.of(status())).build());
    }

    @Test
    public final void testThatRequestForMultipleActivityObjectsSearchesForMultipleContentTypes()
    {
        assertEquals(newQuery(ImmutableSet.<SearchQuery>of(), ImmutableSet.<SearchQuery>of(), ImmutableList.of(PAGE, COMMENT)),
                     new ConfluenceSearchQueryBuilder()
                          .activityObjects(ImmutableList.of(page(), comment())).build());
    }

    @Test
    public final void testCombinationOfAllQueriesInMultitudeWithNullBlankAndUntrimmedDuplicates()
    {
        Set<SearchQuery> userSet = newHashSet(
                new CreatorQuery("bob"), new ContributorQuery("bob"), new CreatorQuery("mary"), new ContributorQuery("mary"));
        Set<SearchQuery> rootSet = newHashSet(
                new InSpaceQuery(newHashSet("JST", "PLUG")),
                composeOrQuery(userSet),
                new MultiTextFieldQuery("jira confluence", TEXT_FIELDS, OR));

        assertEquals(newQuery(rootSet),
                new ConfluenceSearchQueryBuilder()
                        .createdOrLastModifiedBy("bob", null, " ", "mary", " bob ")
                        .inSpace("JST", null, " ", "PLUG", " JST ")
                        .searchFor("jira", null, " ", "confluence", " jira ")
                        .build());
    }

    private SearchQuery newQuery(SearchQuery query)
    {
        return newQuery(ImmutableSet.of(query));
    }

    private SearchQuery newQuery(Set<SearchQuery> subQueries)
    {
        return newQuery(subQueries, ImmutableSet.<SearchQuery>of(), CONTENT_TYPES);
    }

    private SearchQuery newQueryWithNot(SearchQuery query)
    {
        return newQuery(ImmutableSet.<SearchQuery>of(), ImmutableSet.of(query), CONTENT_TYPES);
    }

    private SearchQuery newQuery(Set<SearchQuery> andTerms, Set<SearchQuery> notTerms, Collection<ContentTypeEnum> contentTypes)
    {
        return new BooleanQuery(
                union(ImmutableSet.of(new ContentTypeQuery(contentTypes)), andTerms),
                ImmutableSet.<SearchQuery>of(),
                notTerms);
    }
}
