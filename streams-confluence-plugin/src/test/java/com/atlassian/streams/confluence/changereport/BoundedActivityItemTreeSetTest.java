package com.atlassian.streams.confluence.changereport;

import java.util.Collection;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.streams.spi.Evictor;

import com.google.common.collect.ImmutableList;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.streams.confluence.changereport.ActivityItemFactory.activityItemSorter;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BoundedActivityItemTreeSetTest
{
    @Mock Evictor<ConfluenceEntityObject> evictor;
    @Mock ActivityItem item1;
    @Mock ActivityItem item2;
    @Mock ActivityItem item3;
    @Mock ActivityItem item4;
    @Mock ActivityItem item5;

    BoundedActivityItemTreeSet set;

    @Before
    public void setup()
    {
        when(item1.getModified()).thenReturn(new DateTime().plusMinutes(1).toDate());
        when(item2.getModified()).thenReturn(new DateTime().plusMinutes(2).toDate());
        when(item3.getModified()).thenReturn(new DateTime().plusMinutes(3).toDate());
        when(item4.getModified()).thenReturn(new DateTime().plusMinutes(4).toDate());
        when(item5.getModified()).thenReturn(new DateTime().plusMinutes(5).toDate());

        set = new BoundedActivityItemTreeSet(evictor, 3, activityItemSorter);
    }
    
    @Test
    public void testAddDuplicatePageRevisions()
    {
        set.addAll(ImmutableList.of(item2, item1));
        assertFalse(set.add(item1));
        assertSetEquals(set, item2, item1);
    }

    @Test
    public void testRemove()
    {
        assertTrue(set.addAll(ImmutableList.of(item4, item3, item1)));
        assertTrue(set.remove(item3));
        assertSetEquals(set, item4, item1);
        assertFalse(set.remove(item5));
        assertSetEquals(set, item4, item1);
    }

    @Test
    public void testRemoveAll()
    {
        assertTrue(set.addAll(ImmutableList.of(item3, item2, item1)));
        assertTrue(set.removeAll(ImmutableList.of(item1, item2)));
        assertSetEquals(set, item3);
        assertFalse(set.removeAll(ImmutableList.of(item4, item5)));
        assertSetEquals(set, item3);
    }

    @Test
    public void testClear()
    {
        assertTrue(set.addAll(ImmutableList.of(item3, item2, item1)));
        set.clear();
        assertSetEquals(set); //empty set
    }

    @Test
    public void testRetainAll()
    {
        assertTrue(set.addAll(ImmutableList.of(item3, item2, item1)));
        assertTrue(set.retainAll(ImmutableList.of(item1, item2)));
        assertSetEquals(set, item2, item1);
        assertFalse(set.retainAll(ImmutableList.of(item1, item2)));
        assertSetEquals(set, item2, item1);
        assertTrue(set.retainAll(ImmutableList.of(item5)));
        assertSetEquals(set); //empty set
    }

    private static <T> void assertSetEquals(Collection<T> set, T... expected)
    {
        assertEquals("Sets are not equal length", expected.length, set.size());
        int i = 0;
        for (T item : set)
        {
            assertEquals("Item " + i + " not equals", expected[i], item);
            i++;
        }
    }
}