package com.atlassian.streams.confluence;

import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.CaptchaManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.confluence.changereport.ActivityItem;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;
import com.atlassian.user.User;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.streams.api.StreamsEntry.ActivityObject.params;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfluenceEntryFactoryImplTest
{
    @Mock ApplicationProperties applicationProperties;
    @Mock NotificationManager notificationManager;
    @Mock PageManager pageManager;
    @Mock SpaceManager spaceManager;
    @Mock UserProfileAccessor userProfileAccessor;
    @Mock CaptchaManager captchaManager;
    @Mock ActivityItem activityItem;
    @Mock UserManager userManager;
    @Mock UriProvider uriProvider;
    @Mock StreamsI18nResolver i18nResolver;
    @Mock Space space;

    ActivityObject activityObject;
    ConfluenceEntryFactoryImpl confluenceEntryFactory;

    @Before
    public void createStreamsActivityProvider()
    {
        activityObject = new ActivityObject(params().id("ID").activityObjectType(space()));
        confluenceEntryFactory = new ConfluenceEntryFactoryImpl(
            applicationProperties, notificationManager, pageManager, spaceManager, userProfileAccessor, captchaManager, userManager, uriProvider, i18nResolver);

        when(applicationProperties.getBaseUrl()).thenReturn("http://example.com");

        when(activityItem.isAcceptingCommentsFromUser(anyString())).thenReturn(true);
        when(activityItem.getContentType()).thenReturn("text/html");
        when(activityItem.getId()).thenReturn(12345L);
        when(notificationManager.isUserWatchingPageOrSpace(any(User.class), any(Space.class), any(AbstractPage.class))).thenReturn(false);
    }

    @Test
    public void assertThatReplyToLinkDoesNotExistWhenCaptchaIsOn()
    {
        when(captchaManager.showCaptchaForCurrentUser()).thenReturn(true);
        assertFalse(confluenceEntryFactory.buildReplyTo(activityItem).isDefined());
    }

    @Test
    public void assertThatReplyToLinkExistsWhenCaptchaIsOff()
    {
        when(captchaManager.showCaptchaForCurrentUser()).thenReturn(false);
        assertTrue(confluenceEntryFactory.buildReplyTo(activityItem).isDefined());
    }

    @Test
    public void assertThatWatchUrlExistsWhenActivityTypeIsSpace()
    {
        ConfluenceUser user = mock(ConfluenceUser.class);
        AuthenticatedUserThreadLocal.set(user);
        when(spaceManager.getSpace("DS")).thenReturn(space);
        when(activityItem.getActivityObjects()).thenReturn(ImmutableList.of(activityObject));
        when(activityItem.getSpaceKey()).thenReturn(some("DS"));
        assertTrue(confluenceEntryFactory.getWatchLink(activityItem).isDefined());
    }

    @Test
    public void assertThatPersonalSpaceWithSpaceReturnsEncodedWatchUrl()
    {
        ConfluenceUser user = mock(ConfluenceUser.class);
        AuthenticatedUserThreadLocal.set(user);
        when(spaceManager.getSpace("MY SPACE")).thenReturn(space);
        when(activityItem.getActivityObjects()).thenReturn(ImmutableList.of(activityObject));
        when(activityItem.getSpaceKey()).thenReturn(some("MY SPACE"));
        assertThat(confluenceEntryFactory.getWatchLink(activityItem).get().getHref().toASCIIString(),
                   equalTo("http://example.com/rest/confluence-activity-stream/1.0/actions/space-watch/MY+SPACE"));
    }
}
