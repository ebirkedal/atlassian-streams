package com.atlassian.streams.confluence.changereport;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.follow.FollowManager;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.confluence.ConfluenceFilterOptionProvider;
import com.atlassian.streams.spi.Evictor;
import com.atlassian.user.User;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Test the ActivityItemFactory of Confluence.
 */
@RunWith(MockitoJUnitRunner.class)
public class ActivityItemFactoryTest
{
    @Mock AttachmentActivityItemFactory attachmentActivityItemFactory;
    @Mock PageManager pageManager;
    @Mock Evictor<ConfluenceEntityObject> evictor;
    @Mock FollowManager followManager;
    @Mock UserManager userManager;
    @Mock UserAccessor userAccessor;

    private List<ConfluenceEntityObject> searchables;
    private String commentTitleByAFollowedUser;
    private ConfluenceUser followedUser;
    private ConfluenceUser loggedInUser;
    private ConfluenceUser followingUser;
    private ConfluenceUser independentUser;
    private ActivityItemFactory activityItemFactory;
    private final static String URL = "http://my-confluence/";

    @Before
    public void setUp()
    {
        ContentEntityActivityItemFactory contentEntityActivityItemFactory = Mockito.mock(ContentEntityActivityItemFactory.class);
        activityItemFactory = new ActivityItemFactory(contentEntityActivityItemFactory,
                attachmentActivityItemFactory,
                pageManager,
                evictor,
                followManager,
                userManager,
                userAccessor);

        // People follow each other in chain: victoria -> depardieu (current logged-in user) -> richard
        // One user is independent: "che"
        followedUser = mockUser("richard").getUser();
        loggedInUser = mockUser("depardieu")
                .isLoggedIn()
                .follows(followedUser)
                .getUser();
        followingUser = mockUser("victoria")
                .follows(loggedInUser)
                .getUser();
        independentUser = mockUser("che").getUser();

        // Puts comments in the stream
        searchables = Lists.newArrayList();

        // Add a comment by a followed user
        commentTitleByAFollowedUser = "Comment by the followed user";
        searchables.add(createComment(contentEntityActivityItemFactory, followedUser, commentTitleByAFollowedUser));

        // Add comments by other users
        searchables.add(createComment(contentEntityActivityItemFactory, independentUser, "Comment by an independent user"));
        searchables.add(createComment(contentEntityActivityItemFactory, loggedInUser, "Comment by the logged-in user"));
        searchables.add(createComment(contentEntityActivityItemFactory, followingUser, "Comment by a user following the logged in user"));
    }

    private static List<String> usernamesOfComments(Iterable<ActivityItem> comments)
    {
        List<String> names = new ArrayList<String>();

        for (ActivityItem c : comments)
        {
            names.add(c.getChangedBy());
        }

        return names;
    }

    @Test
    public void testFilterMyNetwork()
    {
        // Launch the request
        List<ActivityItem> activityItems = Lists.newArrayList(activityItemFactory.getActivityItems(ImmutableList.<ActivityItem> of(), searchables,
                createActivityRequest(true, false)));

        assertEquals("The comment from the followed user should be returned",
                ImmutableList.of(followedUser.getName()), usernamesOfComments(activityItems));
        assertThat("The returned comment should have the title given by the user.", titles(activityItems), hasItem(commentTitleByAFollowedUser));
    }

    @Test
    public void testExcludeMyNetwork()
    {
        // Launch the request
        List<ActivityItem> activityItems = Lists.newArrayList(activityItemFactory.getActivityItems(ImmutableList.<ActivityItem> of(), searchables,
                createActivityRequest(false, true)));

        assertEquals("All comments should be returned except the followed user",
                ImmutableList.of(followingUser.getName(), loggedInUser.getName(), independentUser.getName()),
                usernamesOfComments(activityItems));

        assertThat("The returned comment should have the title given by the user.", titles(activityItems), not(hasItem(commentTitleByAFollowedUser)));
    }

    private static List<String> titles(Iterable<ActivityItem> commentReports)
    {
        List<String> titles = Lists.newArrayList();
        for (ActivityItem commentReport : commentReports)
        {
            for (ActivityObject ao : commentReport.getActivityObjects())
            {
                Option<String> title = ao.getTitle();
                if (title.isDefined())
                {
                    titles.add(title.get());
                }
            }
        }
        return titles;
    }

    /* Keep created comments monotonic */
    private long nextDate = 0;

    private Comment createComment(ContentEntityActivityItemFactory contentEntityActivityItemFactory, ConfluenceUser author, String title)
    {
        final String type = "comment";

        Comment comment = new Comment();
        comment.setTitle(title);
        comment.setCreator(author);
        comment.setCreationDate(new Date(nextDate++));

        // Mock ActivityObjectType
        ActivityObjectType activityObjectType = Mockito.mock(ActivityObjectType.class);
        when(activityObjectType.key()).thenReturn(type);
        when(activityObjectType.iri()).thenReturn(URI.create(URL));

        // Create the activity
        ActivityObject commentActivity = new ActivityObject(ActivityObject.params()
                .title(some(title))
                .activityObjectType(activityObjectType));

        // Create the activity report
        CommentActivityItem commentReport = new CommentActivityItem(comment,
                Lists.newArrayList(commentActivity),
                none(ActivityObject.class),
                null,
                Predicates.<String> alwaysFalse());

        when(contentEntityActivityItemFactory.newActivityItem(any(URI.class), eq(comment))).thenReturn(commentReport);
        return comment;
    }

    /**
     * Creates an activity request. It mocks the request that is usually created by the gadget.
     *
     * @param onlyFollowedUsers
     *            if true, will add a provider filter, to include only people followed by the logged in user.
     * @param excludeFollowedUsers
     *            if true, will add a provider filter, to exclude people followed by the logged in user. If onlyFollowedUsers is true,
     *            excludeFollowedUsers will not be read.
     */
    private static ActivityRequest createActivityRequest(boolean onlyFollowedUsers, boolean excludeFollowedUsers)
    {
        ActivityRequest activityRequest = Mockito.mock(ActivityRequest.class);
        when(activityRequest.getMaxResults()).thenReturn(10);
        when(activityRequest.getTimeout()).thenReturn(1000);
        when(activityRequest.getUri()).thenReturn(Uri.parse(URL));
        when(activityRequest.getStandardFilters()).thenReturn(ArrayListMultimap.<String, Pair<StreamsFilterType.Operator, Iterable<String>>> create());

        Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> providerFilters = ArrayListMultimap.create();
        // Add filters if necessary
        if (onlyFollowedUsers)
        {
            providerFilters.put(
                    ConfluenceFilterOptionProvider.NETWORK_FILTER,
                    Pair.pair(
                            StreamsFilterType.Operator.IS,
                            (Iterable<String>) Lists.newArrayList(ConfluenceFilterOptionProvider.NETWORK_FILTER_OPTION_FOLLOWED)));
        }
        else if (excludeFollowedUsers)
        {
            providerFilters.put(
                    ConfluenceFilterOptionProvider.NETWORK_FILTER,
                    Pair.pair(
                            StreamsFilterType.Operator.NOT,
                            (Iterable<String>) Lists.newArrayList(ConfluenceFilterOptionProvider.NETWORK_FILTER_OPTION_FOLLOWED)));
        }
        when(activityRequest.getProviderFilters()).thenReturn(providerFilters);
        return activityRequest;
    }

    /**
     * Mocks a user and allows you to define more attributes
     */
    private UserMocker mockUser(String username)
    {
        return new UserMocker(userAccessor, userManager, followManager, username);
    }

    /**
     * Class which provides methods to mock a user
     */
    private static class UserMocker
    {
        private final String username;
        private final ConfluenceUser user;
        private final UserManager userManager;
        private final FollowManager followManager;

        public UserMocker(UserAccessor userAccessor, UserManager userManager, FollowManager followManager, String username)
        {
            this.userManager = userManager;
            this.followManager = followManager;
            this.username = username;
            this.user = Mockito.mock(ConfluenceUser.class);
            when(user.getName()).thenReturn(username);
            when(user.toString()).thenReturn(username);
            when(userAccessor.getUserByName(username)).thenReturn(user);
            // deprecated version
            when(userAccessor.getUser(username)).thenReturn(user);
        }

        public UserMocker isLoggedIn()
        {
            when(userManager.getRemoteUsername()).thenReturn(username);
            return this;
        }

        public UserMocker follows(ConfluenceUser... followedUsers)
        {
            when(followManager.getFollowing(user)).thenReturn(Sets.<ConfluenceUser>newHashSet(followedUsers));
            // deprecated version
            List<String> usernames = Lists.newArrayList(com.google.common.collect.Iterables.transform(Sets.<ConfluenceUser>newHashSet(followedUsers), new Function<ConfluenceUser, String>()
            {
                @Override
                public String apply(@Nullable ConfluenceUser input)
                {
                    return input.getName();
                }
            }));
            when(followManager.getFollowing((User)user)).thenReturn(usernames);
            return this;
        }

        public String getUsername()
        {
            return username;
        }

        public ConfluenceUser getUser()
        {
            return user;
        }
    }
}
