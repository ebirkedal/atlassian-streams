package com.atlassian.streams.confluence;

import java.util.Date;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.userstatus.StatusTextRenderer;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.confluence.changereport.ActivityObjectFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;

import org.hamcrest.Matcher;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.confluence.ConfluenceFilters.getSearchTerms;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfluenceFiltersTest
{
    private static final int PAGE_VERSION = 1;
    private static final Date BEFORE_TODAY = new DateTime().minusYears(1).toDate();

    @Mock ApplicationProperties applicationProperties;
    @Mock I18nResolver i18nResolver;
    @Mock PageManager pageManager;
    @Mock ThumbnailManager thumbnailManager;
    @Mock StatusTextRenderer textRenderer;
    @Mock ActivityObjectFactory activityObjectFactory;
    @Mock Page page;

    @Mock ActivityRequest request;
    @Mock Multimap<String, Pair<Operator, Iterable<String>>> providerFilters;
    @Mock Multimap<String, Pair<Operator, Iterable<String>>> standardFilters;

    @Before
    public void setup()
    {
        when(request.getProviderFilters()).thenReturn(providerFilters);
        when(request.getStandardFilters()).thenReturn(standardFilters);

        setupPageMock();
    }

    private void setupPageMock()
    {
        when(page.isLatestVersion()).thenReturn(true);
        when(page.isNew()).thenReturn(true);
        when(page.getCreatorName()).thenReturn("pete");
        when(page.getLastModifierName()).thenReturn("pete");
        when(page.getTitle()).thenReturn("A great page.");
        when(page.getBodyAsStringWithoutMarkup()).thenReturn("Content here.");
        when(page.getCreationDate()).thenReturn(BEFORE_TODAY);
        when(page.getLastModificationDate()).thenReturn(BEFORE_TODAY);
        when(page.getType()).thenReturn("page");
        when(page.getVersion()).thenReturn(PAGE_VERSION);
    }

    @Test
    public void testLabelsOfIssueKeys()
    {
        String[] isIssueKey = new String[] { "ONE-1" };
        ImmutableList<Pair<Operator, Iterable<String>>> issueKeys =
            ImmutableList.of(pair(IS, iterable(isIssueKey)));
        when(standardFilters.get(ISSUE_KEY.getKey())).thenReturn(issueKeys);

        assertThat(getSearchTerms(request), onlyHasItems("ONE-1"));
    }

    /**
     * Verifies that the specified elements exist and that no other elements do.
     */
    private static <T> Matcher<Iterable<T>> onlyHasItems(T... ts)
    {
        return allOf(size(ts), hasItems(ts));
    }

    /**
     * Need to break this out into separate function in order to properly cast generics type
     */
    private static <T> Matcher<Iterable<T>> size(T... ts)
    {
        return iterableWithSize(ts.length);
    }

    /**
     * Another workaround for generics/casting compilation problem
     */
    private static <T> Iterable<T> iterable(T[] array)
    {
        return ImmutableList.<T>copyOf(array);
    }
}
