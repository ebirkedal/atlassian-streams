package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.userstatus.UserStatus;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;

import static com.atlassian.streams.api.ActivityVerbs.update;

public class UserStatusActivityItem extends ContentEntityActivityItem
{
    public UserStatusActivityItem(UserStatus userStatus,
            Iterable<ActivityObject> activityObjects,
            Option<ActivityObject> target,
            Renderer renderer)
    {
        super(userStatus, activityObjects, target, renderer);
    }

    @Override
    public String getIconPath()
    {
        return "/images/icons/quotes.png";
    }

    @Override
    public ActivityVerb getVerb()
    {
        return update();
    }
}
