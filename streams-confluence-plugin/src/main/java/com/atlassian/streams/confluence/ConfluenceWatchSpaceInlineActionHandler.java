package com.atlassian.streams.confluence;

import java.util.List;

import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.user.User;

import com.google.common.base.Function;

import static com.google.common.base.Preconditions.checkNotNull;

public class ConfluenceWatchSpaceInlineActionHandler implements ConfluenceWatchInlineActionHandler<String>
{
    private final NotificationManager notificationManager;
    private final SpaceManager spaceManager;
    private final ConfluenceWatchHelper<Space, Long> watchHelper =
        new ConfluenceWatchHelper<Space, Long>();
    
    public ConfluenceWatchSpaceInlineActionHandler(NotificationManager notificationManager, SpaceManager spaceManager)
    {
        this.notificationManager = checkNotNull(notificationManager, "notificationManager");
        this.spaceManager = checkNotNull(spaceManager, "spaceManager");
    }

    public boolean startWatching(String key)
    {
        Space space = spaceManager.getSpace(key);
        User user = AuthenticatedUserThreadLocal.getUser();
        if (user != null && notificationManager.isUserWatchingPageOrSpace(user, space, null))
        {
            //already watching the space. return true
            return true;
        }

        return watchHelper.startWatching(space, addSpaceNotification, getSpaceNotifications);
    }
    
    private AddSpaceNotification addSpaceNotification = new AddSpaceNotification();
    
    private class AddSpaceNotification implements Function<Pair<User, Space>, Void>
    {
        public Void apply(Pair<User, Space> params)
        {
            notificationManager.addSpaceNotification(params.first(), params.second(), null);
            return null;
        }
    }
    
    private GetSpaceNotifications getSpaceNotifications = new GetSpaceNotifications();
    
    private class GetSpaceNotifications implements Function<Space, List<Notification>>
    {
        public List<Notification> apply(Space entity)
        {
            return notificationManager.getNotificationsBySpaceAndType(entity, null);
        }
    }
}
