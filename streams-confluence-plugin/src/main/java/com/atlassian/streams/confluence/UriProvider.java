package com.atlassian.streams.confluence;

import java.net.URI;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

public class UriProvider
{
    private static final Iterable<String> contentStyles = ImmutableList.of(
            "master.css", "wiki-content.css", "tables.css", "panels.css", "renderer-macros.css", "icons.css", "information-macros.css", "layout-macros.css");

    private final WebResourceManager webResourceManager;

    public UriProvider(WebResourceManager webResourceManager)
    {
        this.webResourceManager = checkNotNull(webResourceManager, "webResourceManager");
    }

    public URI getEntityUri(final URI baseUri, final ContentEntityObject entity)
    {
        //STRM-1633 always link the latest page versions
        if (entity instanceof AbstractPage && !entity.isLatestVersion() && entity.getLatestVersion() instanceof AbstractPage)
        {
            return URI.create(baseUri.toASCIIString() + ((AbstractPage)entity.getLatestVersion()).getUrlPath());
        }
        else
        {
            return URI.create(baseUri.toASCIIString() + entity.getUrlPath());
        }
    }

    public URI getPageDiffUri(final URI baseUri,
                              final ContentEntityObject entity,
                              final int originalVersion,
                              final int newerVersion)
    {
        checkNotNull(entity, "entity");
        final StringBuilder diffUrl = new StringBuilder();
        diffUrl.append(baseUri.toASCIIString());
        diffUrl.append("/pages/diffpagesbyversion.action?pageId=").append(entity.getId());
        diffUrl.append("&originalVersion=").append(originalVersion);
        diffUrl.append("&revisedVersion=").append(newerVersion);
        return URI.create(diffUrl.toString()).normalize();
    }

    public Iterable<URI> getContentCssUris()
    {
        return transform(contentStyles, getStaticPluginResource("confluence.web.resources:content-styles"));
    }

    private Function<String, URI> getStaticPluginResource(final String moduleKey)
    {
        return new Function<String, URI>()
        {
            public URI apply(String s)
            {
                return URI.create(webResourceManager.getStaticPluginResource(moduleKey, s,
                        com.atlassian.plugin.webresource.UrlMode.ABSOLUTE)).normalize();
            }
        };
    }
}
