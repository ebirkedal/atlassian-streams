package com.atlassian.streams.confluence.renderer;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.confluence.changereport.AttachmentActivityItem;
import com.atlassian.streams.confluence.changereport.AttachmentActivityItem.Entry;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.renderer.Renderers.render;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.size;

public final class AttachmentRendererFactory
{
    private final StreamsEntryRendererFactory rendererFactory;
    private final I18nResolver i18nResolver;
    private final TemplateRenderer templateRenderer;
    private final ApplicationProperties applicationProperties;

    public AttachmentRendererFactory(StreamsEntryRendererFactory rendererFactory,
            I18nResolver i18nResolver,
            TemplateRenderer templateRenderer,
            ApplicationProperties applicationProperties)
    {
        this.rendererFactory = checkNotNull(rendererFactory, "rendererFactory");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    public Renderer newInstance(Iterable<Entry> entries)
    {
        return new AttachmentRenderer(entries);
    }

    private final class AttachmentRenderer implements Renderer
    {
        private final Function<Iterable<UserProfile>, Html> authorsRenderer = rendererFactory.newAuthorsRenderer();
        private final Function<ActivityObject, Option<Html>> targetRenderer = rendererFactory.newActivityObjectRendererWithSummary();
        private final Iterable<Entry> entries;

        public AttachmentRenderer(Iterable<Entry> entries)
        {
            this.entries = entries;
        }

        @Override
        public Html renderTitleAsHtml(StreamsEntry entry)
        {
            return entry.getTarget().flatMap(targetRenderer).map(renderAttachedTo(entry)).getOrElse(renderAttached(entry));
        }

        private Supplier<Html> renderAttached(final StreamsEntry entry)
        {
            return new Supplier<Html>()
            {
                public Html get()
                {
                  return new Html(i18nResolver.getText("streams.confluence.attached",
                      authorsRenderer.apply(entry.getAuthors()),
                      size(entry.getActivityObjects())));
                }
            };
        }

        private Function<Html, Html> renderAttachedTo(final StreamsEntry entry)
        {
            return new Function<Html, Html>()
            {
                public Html apply(Html target)
                {
                  return new Html(i18nResolver.getText("streams.confluence.attached.to",
                      authorsRenderer.apply(entry.getAuthors()),
                      size(entry.getActivityObjects()),
                      target));
                }
            };
        }

        @Override
        public Option<Html> renderSummaryAsHtml(StreamsEntry entry)
        {
            return none();
        }

        @Override
        public Option<Html> renderContentAsHtml(StreamsEntry entry)
        {
            ImmutableMap<String, Object> context = ImmutableMap.<String, Object>of(
                    "previewable", ImmutableList.copyOf(filter(entries, previewable())),
                    "nonpreviewable", ImmutableList.copyOf(filter(entries, not(previewable()))),
                    "applicationProperties", applicationProperties);
            return some(new Html(render(templateRenderer, "attachment-content.vm", context)));
        }
    }

    private final Predicate<AttachmentActivityItem.Entry> previewable()
    {
        return Previewable.INSTANCE;
    }

    private enum Previewable implements Predicate<AttachmentActivityItem.Entry>
    {
        INSTANCE;

        public boolean apply(AttachmentActivityItem.Entry attachment)
        {
            return attachment.getPreview().isDefined();
        }
    };
}
