package com.atlassian.streams.confluence;

import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.confluence.changereport.ActivityItem;

import java.net.URI;

public interface ConfluenceEntryFactory
{
    /**
     * Converts a {@code ActivityItem} to a {@code StreamsEntry}.
     *
     *
     * @param baseUri the baseUri to use for links in the entries.
     * @param activityItem The activity item to convert into an entry.
     * @return The converted entry.
     */
    StreamsEntry buildStreamsEntry(URI baseUri, ActivityItem activityItem);
}
