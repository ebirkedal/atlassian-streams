package com.atlassian.streams.confluence.changereport;

import java.util.Date;

import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;

final class SpaceActivityItem extends ContentEntityActivityItem
{
    private final boolean isCreationEvent;
    
    public SpaceActivityItem(SpaceDescription space,
            boolean isCreationEvent,
            Iterable<ActivityObject> activityObjects,
            Option<ActivityObject> target,
            Renderer renderer)
    {
        super(space, activityObjects, target, renderer);
        this.isCreationEvent = isCreationEvent;
    }

    @Override
    public boolean isNew()
    {
        return isCreationEvent;
    }
    
    @Override
    public Date getModified()
    {
        return isNew() ? getEntity().getCreationDate() : getEntity().getLastModificationDate();
    }
    
    @Override
    public String getIconPath()
    {
        return "/images/icons/web_16.gif";
    }
}
