package com.atlassian.streams.confluence;

import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;
import com.atlassian.user.User;

import com.google.common.collect.ImmutableList;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.uri.Uris.encode;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.google.common.base.Preconditions.checkNotNull;

public class ConfluenceEntityAssociationProvider implements StreamsEntityAssociationProvider
{
    private static final String URL_PREFIX = "/display/";
    private static final Pattern SPACE_PATTERN = Pattern.compile("([A-Za-z0-9]+)([/#?].*)?");
    
    private final ApplicationProperties applicationProperties;
    private final SpaceManager spaceManager;
    private final SpacePermissionManager spacePermissionManager;
    
    public ConfluenceEntityAssociationProvider(ApplicationProperties applicationProperties,
                                               SpaceManager spaceManager,
                                               SpacePermissionManager spacePermissionManager)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.spaceManager = checkNotNull(spaceManager, "spaceManager");
        this.spacePermissionManager = checkNotNull(spacePermissionManager, "spacePermissionManager");
    }
    
    @Override
    public Iterable<EntityIdentifier> getEntityIdentifiers(URI target)
    {
        String targetStr = target.toString();
        if (target.isAbsolute())
        {
            if (!targetStr.startsWith(applicationProperties.getBaseUrl() + URL_PREFIX))
            {
                return ImmutableList.of();
            }
            return matchEntities(targetStr.substring(applicationProperties.getBaseUrl().length() + URL_PREFIX.length()));
        }
        else
        {
            return matchEntities(targetStr);
        }
    }

    @Override
    public Option<URI> getEntityURI(EntityIdentifier identifier)
    {
        if (identifier.getType().equals(space().iri()))
        {
            return some(URI.create(applicationProperties.getBaseUrl() + URL_PREFIX + encode(identifier.getValue())));
        }
        return none();
    }

    @Override
    public Option<String> getFilterKey(EntityIdentifier identifier)
    {
        if (identifier.getType().equals(space().iri()))
        {
            return some(PROJECT_KEY);
        }
        return none();
    }
    
    @Override
    public Option<Boolean> getCurrentUserViewPermission(EntityIdentifier identifier)
    {
        return getCurrentUserPermission(identifier, SpacePermission.VIEWSPACE_PERMISSION);
    }

    @Override
    public Option<Boolean> getCurrentUserEditPermission(EntityIdentifier identifier)
    {
        return getCurrentUserPermission(identifier, SpacePermission.COMMENT_PERMISSION);
    }

    private Option<Boolean> getCurrentUserPermission(EntityIdentifier identifier, String permission)
    {
        User user = AuthenticatedUserThreadLocal.getUser();
        if (identifier.getType().equals(space().iri()))
        {
            Space space = spaceManager.getSpace(identifier.getValue());
            if (space != null)
            {
                return some(Boolean.valueOf(spacePermissionManager.hasPermission(permission, space, user)));
            }
        }
        return none();
    }
    
    private Iterable<EntityIdentifier> matchEntities(String input)
    {
        Matcher spaceMatcher = SPACE_PATTERN.matcher(input);
        if (spaceMatcher.matches())
        {
            String spaceKey = spaceMatcher.group(1);
            Space space = spaceManager.getSpace(spaceKey);
            if (space != null)
            {
                URI canonicalUri = URI.create(applicationProperties.getBaseUrl() + URL_PREFIX + encode(spaceKey));
                return ImmutableList.of(new EntityIdentifier(space().iri(),
                                                             spaceKey,
                                                             canonicalUri));
            }
        }

        return ImmutableList.of();
    }
}
