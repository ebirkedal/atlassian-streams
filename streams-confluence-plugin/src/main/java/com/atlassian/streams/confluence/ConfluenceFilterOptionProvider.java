package com.atlassian.streams.confluence;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.ActivityOptions;
import com.atlassian.streams.spi.StreamsFilterOption;
import com.atlassian.streams.spi.StreamsFilterOptionProvider;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityObjectTypes.file;
import static com.atlassian.streams.api.ActivityObjectTypes.status;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.api.StreamsFilterType.SELECT;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.page;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.personalSpace;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

public class ConfluenceFilterOptionProvider implements StreamsFilterOptionProvider
{
    public static final String NETWORK_FILTER  = "network";
    public static final String NETWORK_FILTER_OPTION_FOLLOWED  = "followedByMe";

    public static final Iterable<Pair<ActivityObjectType, ActivityVerb>> activities = ImmutableList.of(
            pair(article(), post()),
            pair(article(), update()),
            pair(page(), post()),
            pair(page(), update()),
            pair(comment(), post()),
            pair(file(), post()),
            pair(status(), update()),
            pair(space(), post()),
            pair(space(), update()),
            pair(personalSpace(), post()),
            pair(personalSpace(), update()));

    private final Function<Pair<ActivityObjectType, ActivityVerb>, ActivityOption> toActivityOption;
    private final I18nResolver i18nResolver;

    public ConfluenceFilterOptionProvider(final I18nResolver i18nResolver)
    {
        this.toActivityOption = ActivityOptions.toActivityOption(checkNotNull(i18nResolver, "i18nResolver"), "streams.filter.confluence");
        this.i18nResolver = i18nResolver;
    }

    public Iterable<StreamsFilterOption> getFilterOptions()
    {
        Map<String, String> networkFilterOptions =
                ImmutableMap.of(NETWORK_FILTER_OPTION_FOLLOWED, i18nResolver.getText("streams.filter.confluence.network.followed"));

        StreamsFilterOption filterOption = new StreamsFilterOption.Builder(NETWORK_FILTER, SELECT)
            .displayName(i18nResolver.getText("streams.filter.confluence.network"))
            .helpTextI18nKey("streams.filter.confluence.network.help")
            .i18nKey("streams.filter.confluence.network")
            .unique(true)
            .values(networkFilterOptions)
            .build();

        return ImmutableList.of(filterOption);
    }

    public Iterable<ActivityOption> getActivities()
    {
        return transform(activities, toActivityOption);
    }
}
