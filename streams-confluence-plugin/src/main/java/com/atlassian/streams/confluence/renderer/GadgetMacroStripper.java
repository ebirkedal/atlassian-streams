package com.atlassian.streams.confluence.renderer;

import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactory;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Iterator;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

class GadgetMacroStripper
{
    private XmlEventReaderFactory xmlEventReaderFactory;
    private XmlOutputFactory xmlOutputFactory;

    GadgetMacroStripper(final XmlEventReaderFactory xmlEventReaderFactory, final XmlOutputFactory xmlOutputFactory)
    {
        this.xmlEventReaderFactory = xmlEventReaderFactory;
        this.xmlOutputFactory = xmlOutputFactory;
    }

    String stripGadgetMacros(String content) throws Exception
    {
        // we need a root node for the confluence xhtml fragment.
        String xml = "<div>" + content + "</div>";
        XMLEventReader xmlEventReader = xmlEventReaderFactory.createStorageXmlEventReader(new StringReader(xml), false);
        StringWriter result = new StringWriter();
        XMLEventWriter xmlEventWriter = xmlOutputFactory.createXMLEventWriter(result);

        try
        {
            while (xmlEventReader.hasNext()) {
                XMLEvent event = xmlEventReader.peek();

                if (event.isStartElement() && isGadgetMacro(event.asStartElement()))
                {
                    //Consume the GadgetMacro element
                    xmlEventReaderFactory.createXmlFragmentEventReader(xmlEventReader).close();
                }
                else
                {
                    xmlEventWriter.add(xmlEventReader.nextEvent());
                }
            }

            xmlEventWriter.flush();
        }
        finally
        {
            xmlEventWriter.close();
        }

        return result.toString();
    }

    private boolean isGadgetMacro(StartElement element)
    {
        if("macro".equals(element.getName().getLocalPart()))
        {
            Iterator<Attribute> attrs = element.getAttributes();
            while (attrs.hasNext())
            {
                Attribute attr = attrs.next();
                if("name".equals(attr.getName().getLocalPart()))
                {
                    return "gadget".equals(attr.getValue());
                }
            }
        }

        return false;
    }
}
