package com.atlassian.streams.confluence.renderer;

import com.atlassian.confluence.userstatus.StatusTextRenderer;
import com.atlassian.confluence.userstatus.UserStatus;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;

import com.google.common.base.Function;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

public class StatusRendererFactory
{
    private final Function<StreamsEntry, Html> titleRenderer;
    private final StatusTextRenderer statusRenderer;

    public StatusRendererFactory(StreamsEntryRendererFactory rendererFactory, StatusTextRenderer statusRenderer)
    {
        titleRenderer = checkNotNull(rendererFactory, "rendererFactory").
                newTitleRenderer("streams.item.confluence.userstatus.update");
        this.statusRenderer = checkNotNull(statusRenderer, "statusRenderer");
    }

    public Renderer newInstance(UserStatus userStatus)
    {
        return new StatusRenderer(userStatus);
    }

    private final class StatusRenderer implements Renderer
    {
        private final UserStatus userStatus;

        StatusRenderer(UserStatus userStatus)
        {
            this.userStatus = userStatus;
        }

        @Override
        public Option<Html> renderContentAsHtml(StreamsEntry entry)
        {
            return some(new Html(statusRenderer.render(userStatus.getStatus())));
        }

        @Override
        public Option<Html> renderSummaryAsHtml(StreamsEntry entry)
        {
            return none();
        }

        @Override
        public Html renderTitleAsHtml(StreamsEntry entry)
        {
            return titleRenderer.apply(entry);
        }
    }
}
