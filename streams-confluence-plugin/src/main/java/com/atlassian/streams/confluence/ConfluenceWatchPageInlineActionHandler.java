package com.atlassian.streams.confluence;

import java.util.List;

import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.user.User;

import com.google.common.base.Function;

import static com.google.common.base.Preconditions.checkNotNull;

public class ConfluenceWatchPageInlineActionHandler implements ConfluenceWatchInlineActionHandler<Long>
{
    private final NotificationManager notificationManager;
    private final PageManager pageManager;
    private final ConfluenceWatchHelper<AbstractPage, Long> watchHelper =
        new ConfluenceWatchHelper<AbstractPage, Long>();
    
    public ConfluenceWatchPageInlineActionHandler(NotificationManager notificationManager, PageManager pageManager)
    {
        this.notificationManager = checkNotNull(notificationManager, "notificationManager");
        this.pageManager = checkNotNull(pageManager, "pageManager");
    }

    public boolean startWatching(Long key)
    {
        AbstractPage page = pageManager.getAbstractPage(key);
        User user = AuthenticatedUserThreadLocal.getUser();
        if (user != null && notificationManager.isUserWatchingPageOrSpace(user, null, page))
        {
            //already watching the page. return true
            return true;
        }
        
        return watchHelper.startWatching(page, addPageNotification, getPageNotifications);
    }
    
    private AddPageNotification addPageNotification = new AddPageNotification();
    
    private class AddPageNotification implements Function<Pair<User, AbstractPage>, Void>
    {
        public Void apply(Pair<User, AbstractPage> params)
        {
            notificationManager.addPageNotification(params.first(), params.second());
            return null;
        }
    }
    
    private GetPageNotifications getPageNotifications = new GetPageNotifications();
    
    private class GetPageNotifications implements Function<AbstractPage, List<Notification>>
    {
        public List<Notification> apply(AbstractPage entity)
        {
            return notificationManager.getNotificationsByPage(entity);
        }
    }
}
