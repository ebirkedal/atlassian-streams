package com.atlassian.streams.confluence;

import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.streams.spi.FormatPreferenceProvider;
import com.atlassian.user.User;

import org.joda.time.DateTimeZone;

import static com.google.common.base.Preconditions.checkNotNull;

public class ConfluenceFormatPreferenceProvider implements FormatPreferenceProvider
{
    private final FormatSettingsManager formatSettingsManager;
    private final UserAccessor userAccessor;

    public ConfluenceFormatPreferenceProvider(FormatSettingsManager formatSettingsManager,
                                              UserAccessor userAccessor)
    {
        this.formatSettingsManager = checkNotNull(formatSettingsManager, "formatSettingsManager");
        this.userAccessor = checkNotNull(userAccessor, "userAccessor");
    }

    public String getTimeFormatPreference()
    {
        return formatSettingsManager.getTimeFormat();
    }

    public String getDateFormatPreference()
    {
        return formatSettingsManager.getDateFormat();
    }

    public String getDateTimeFormatPreference()
    {
        return formatSettingsManager.getDateTimeFormat();
    }

    public DateTimeZone getUserTimeZone()
    {
        User user = AuthenticatedUserThreadLocal.getUser();
        final ConfluenceUserPreferences userPreferences = userAccessor.getConfluenceUserPreferences(user);

        try
        {
            return DateTimeZone.forTimeZone(userPreferences.getTimeZone().getWrappedTimeZone());
        }
        catch (IllegalArgumentException e)
        {
            return DateTimeZone.getDefault();
        }
    }

    @Override
    public boolean getDateRelativizePreference()
    {
        return true;
    }


}
