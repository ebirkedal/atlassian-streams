package com.atlassian.streams.confluence;

import com.atlassian.confluence.core.ListQuery;

/**
 * @author Pete Aykroyd
 * @created Feb 4, 2008
 */
public class StreamsListQuery extends ListQuery
{
    private String queryString;

    public String getQueryString()
    {
        return queryString;
    }

    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    public String getDescription()
    {
        StringBuffer description = new StringBuffer(super.getDescription());
        description.append(queryString != null ? " containing text " + queryString : "");
        return description.toString();
    }
}
