package com.atlassian.streams.jira.renderer;

import java.util.Collections;

import javax.annotation.Nullable;

import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadata;
import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadataParticipant;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.NonEmptyIterable;
import com.atlassian.streams.api.common.NonEmptyIterables;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;

import static com.atlassian.streams.spi.renderer.Renderers.render;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

public class HistoryMetadataRendererFactory
{
    private final I18nResolver i18nResolver;
    private final StreamsEntryRendererFactory streamsEntryRendererFactory;
    private final Function<HistoryMetadataParticipant, Html> participantRenderer;

    public HistoryMetadataRendererFactory(final I18nResolver i18nResolver, final StreamsEntryRendererFactory streamsEntryRendererFactory,
            final TemplateRenderer templateRenderer)
    {
        this(i18nResolver, streamsEntryRendererFactory, newParticipantRenderer(templateRenderer, i18nResolver));
    }

    protected HistoryMetadataRendererFactory(final I18nResolver i18nResolver, final StreamsEntryRendererFactory streamsEntryRendererFactory,
            final Function<HistoryMetadataParticipant, Html> participantRenderer)
    {
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.streamsEntryRendererFactory = checkNotNull(streamsEntryRendererFactory, "streamsEntryRendererFactory");
        this.participantRenderer = checkNotNull(participantRenderer, "participantRenderer");
    }

    public StreamsEntry.Renderer newSuffixRenderer(final StreamsEntry originalEntry, final String suffix)
    {
        return new StreamsEntry.Renderer()
        {
            @Override
            public Html renderTitleAsHtml(final StreamsEntry entry)
            {
                return new Html(originalEntry.renderTitleAsHtml() + " " + escapeHtml(suffix));
            }

            @Override
            public Option<Html> renderSummaryAsHtml(final StreamsEntry entry)
            {
                return originalEntry.renderSummaryAsHtml();
            }

            @Override
            public Option<Html> renderContentAsHtml(final StreamsEntry entry)
            {
                return originalEntry.renderContentAsHtml();
            }
        };
    }

    private static Function<HistoryMetadataParticipant, Html> newParticipantRenderer(final TemplateRenderer templateRenderer, final I18nResolver i18nResolver)
    {
        checkNotNull(templateRenderer, "templateRenderer");
        checkNotNull(i18nResolver, "i18nResolver");
        return new Function<HistoryMetadataParticipant, Html>()
        {
            @Override
            public Html apply(@Nullable final HistoryMetadataParticipant participant)
            {
                if (participant != null)
                {
                    return new Html(render(templateRenderer, "jira-history-participant.vm", ImmutableMap.<String, Object>of(
                            "i18n", i18nResolver,
                            "participant", participant
                    )).trim());
                } else {
                    return new Html("");
                }
            }
        };
    }

    public StreamsEntry.Renderer newCustomKeyRenderer(final StreamsEntry originalEntry, final HistoryMetadata historyMetadata)
    {
        return new StreamsEntry.Renderer()
        {
            @Override
            public Html renderTitleAsHtml(final StreamsEntry entry)
            {
                String descriptionKey = historyMetadata.getActivityDescriptionKey();
                Html generatorDisplayName = participantRenderer.apply(historyMetadata.getGenerator());
                Html causeDisplayName = participantRenderer.apply(historyMetadata.getCause());
                final NonEmptyIterable<UserProfile> emptyAuthor = NonEmptyIterables.from(Collections.singleton(new UserProfile.Builder("").build())).get();
                return new Html(i18nResolver.getText(descriptionKey,
                        originalEntry.renderTitleAsHtml(),
                        streamsEntryRendererFactory.newAuthorsRenderer().apply(originalEntry.getAuthors()),
                        new StreamsEntry(StreamsEntry.params(originalEntry).authors(emptyAuthor), i18nResolver).renderTitleAsHtml(),
                        generatorDisplayName, causeDisplayName));
            }

            @Override
            public Option<Html> renderSummaryAsHtml(final StreamsEntry entry)
            {
                return originalEntry.renderSummaryAsHtml();
            }

            @Override
            public Option<Html> renderContentAsHtml(final StreamsEntry entry)
            {
                return originalEntry.renderContentAsHtml();
            }
        };
    }
}
