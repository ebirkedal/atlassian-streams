package com.atlassian.streams.jira;

import static com.google.common.base.Preconditions.checkNotNull;

import org.joda.time.DateTimeZone;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.sal.api.timezone.TimeZoneManager;
import com.atlassian.streams.spi.FormatPreferenceProvider;

public class JiraFormatPreferenceProvider implements FormatPreferenceProvider
{
    private final DateTimeFormatterFactory formatterFactory;
    private final TimeZoneManager timeZoneManager;
    private final ApplicationProperties applicationProperties;

    public JiraFormatPreferenceProvider(DateTimeFormatterFactory formatterFactory,
                                        TimeZoneManager timeZoneManager,
                                        ApplicationProperties applicationProperties)
    {
        this.formatterFactory = checkNotNull(formatterFactory, "formatterFactory");
        this.timeZoneManager = checkNotNull(timeZoneManager, "timeZoneManager");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    public String getTimeFormatPreference()
    {
        return formatterFactory.formatter().withStyle(DateTimeStyle.TIME).getFormatHint();
    }

    public String getDateFormatPreference()
    {
        return formatterFactory.formatter().withStyle(DateTimeStyle.DATE).getFormatHint();
    }

    public String getDateTimeFormatPreference()
    {
        return formatterFactory.formatter().withStyle(DateTimeStyle.COMPLETE).getFormatHint();
    }

    @Override
    public boolean getDateRelativizePreference()
    {
        return Boolean.parseBoolean(applicationProperties.getDefaultBackedText("jira.lf.date.relativize"));
    }

    public DateTimeZone getUserTimeZone()
    {
        try
        {
            return DateTimeZone.forTimeZone(timeZoneManager.getUserTimeZone());
        }
        catch (IllegalArgumentException e)
        {
            return DateTimeZone.getDefault();
        }
    }
}
