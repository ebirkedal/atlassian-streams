package com.atlassian.streams.jira;

import java.net.URI;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * JIRA implementation of building Atom author constructs
 */
public class JiraUserProfileAccessor implements UserProfileAccessor
{
    private static final int AVATAR_PIXELS = AvatarManager.ImageSize.LARGE.getPixels();

    private final ApplicationProperties applicationProperties;
    private final EmailFormatter emailFormatter;
    private final JiraAuthenticationContext authenticationContext;
    private final UserUtil userUtil;
    private final UserManager userManager;
    private final AvatarManager avatarManager;
    private final StreamsI18nResolver i18nResolver;

    public JiraUserProfileAccessor(UserUtil userUtil,
                                   ApplicationProperties applicationProperties,
                                   EmailFormatter emailFormatter,
                                   JiraAuthenticationContext authenticationContext,
                                   UserManager userManager,
                                   AvatarManager avatarManager,
                                   StreamsI18nResolver i18nResolver)
    {
        this.userUtil = checkNotNull(userUtil, "userUtil");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.emailFormatter = checkNotNull(emailFormatter, "emailFormatter");
        this.authenticationContext = checkNotNull(authenticationContext, "authenticationContext");
        this.userManager = checkNotNull(userManager, "userManager");
        this.avatarManager = checkNotNull(avatarManager, "avatarManager");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    private URI getUserProfileUri(URI baseUri, String username)
    {
        final String baseUriStr = baseUri == null ? applicationProperties.getBaseUrl() : baseUri.toASCIIString();
        return URI.create(baseUriStr
                + userManager.getUserProfile(username).getProfilePageUri().toASCIIString());
    }

    private URI getProfilePictureUri(URI baseUri, String username)
    {
        URI uri = userManager.getUserProfile(username).getProfilePictureUri(AVATAR_PIXELS, AVATAR_PIXELS);

        if (uri != null)
        {
            if (uri.isAbsolute())
            {
                return uri;
            }
            else
            {
                // XXX JIRA's relative URLs start with a slash that refers to the base URL, not the path
                final String baseUriStr = baseUri == null ? applicationProperties.getBaseUrl() : baseUri.toASCIIString();
                return URI.create(baseUriStr + uri.toString()).normalize();
            }
        }
        else
        {
            return getAnonymousProfilePictureUri(baseUri);
        }
    }

    public UserProfile getAnonymousUserProfile(URI baseUri)
    {
        return new UserProfile.Builder(i18nResolver.getText("streams.jira.authors.unknown.username"))
                .fullName(i18nResolver.getText("streams.jira.authors.unknown.fullname"))
                .email(none(String.class))
                .profilePageUri(none(URI.class))
                .profilePictureUri(some(getAnonymousProfilePictureUri(baseUri)))
                .build();
    }

    private URI getAnonymousProfilePictureUri(URI baseUri)
    {
        final String baseUriStr = baseUri == null ? applicationProperties.getBaseUrl() : baseUri.toASCIIString();
        return URI.create(baseUriStr + "/secure/useravatar?avatarId=" + avatarManager.getAnonymousAvatarId());
    }

    @Override
    public UserProfile getUserProfile(final String username)
    {
        return getUserProfile(URI.create(applicationProperties.getBaseUrl()), username);
    }

    @Override
    public UserProfile getAnonymousUserProfile()
    {
        return getAnonymousUserProfile(URI.create(applicationProperties.getBaseUrl()));
    }

    public UserProfile getUserProfile(URI baseUri, String username)
    {
        if (username == null)
        {
            return getAnonymousUserProfile(baseUri);
        }

        ApplicationUser user = userUtil.getUserByName(username);
        String email;
        if (user != null)
        {
            if(emailFormatter.emailVisible(authenticationContext.getLoggedInUser()))
            {
                email = user.getEmailAddress();
            }
            else
            {
                email = null;
            }

            return new UserProfile.Builder(username)
                    .fullName(user.getDisplayName())
                    .email(option(email))
                    .profilePageUri(some(getUserProfileUri(baseUri, username)))
                    .profilePictureUri(some(getProfilePictureUri(baseUri, username)))
                    .build();
        }
        else
        {
            return new UserProfile.Builder(username).profilePictureUri(some(getAnonymousProfilePictureUri(baseUri))).build();
        }
    }
}
