package com.atlassian.streams.jira;

import java.net.URI;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadata;
import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadataParticipant;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.link.RemoteIssueLinkManager;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.jira.builder.ActivityObjectBuilder;
import com.atlassian.streams.jira.builder.JiraEntryBuilderFactory;
import com.atlassian.streams.spi.UserProfileAccessor;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JiraHelperTest
{
    private static final String STATUS = "status";
    private static final String OLD_VALUE = "1";
    private static final String OLD_ENGLISH_TRANSLATION = "In Progress";
    private static final String OLD_FRENCH_TRANSLATION = "En Cours";
    private static final String NEW_VALUE = "2";
    private static final String NEW_ENGLISH_TRANSLATION = "Under Review";
    private static final String NEW_FRENCH_TRANSLATION = "En cours d'examen";

    private UserProfile jiraUser;
    private UserProfile anonUser;

    @Mock ConstantsManager constantsManager;
    @Mock JiraEntryBuilderFactory entryBuilderFactory;
    @Mock UriProvider uriProvider;
    @Mock ActivityObjectBuilder activityObjectBuilder;
    @Mock UserProfileAccessor userProfileAccessor;
    @Mock AttachmentManager attachmentManager;
    @Mock RemoteIssueLinkManager remoteIssueLinkManager;
    @Mock RendererManager rendererManager;
    @Mock FieldLayoutManager fieldLayoutManager;

    @Mock GenericValue changeItem;
    @Mock Status inProgress;
    @Mock Status underReview;

    private JiraHelper helper;

    @Before
    public void setup() throws Exception
    {
        anonUser = stubUser("anonymous").build();
        jiraUser = stubUser("jirauser").build();
        when(userProfileAccessor.getUserProfile(any(URI.class), eq(jiraUser.getUsername()))).thenReturn(jiraUser);
        when(userProfileAccessor.getAnonymousUserProfile(any(URI.class))).thenReturn(anonUser);

        when(inProgress.getNameTranslation()).thenReturn(OLD_FRENCH_TRANSLATION);
        when(underReview.getNameTranslation()).thenReturn(NEW_FRENCH_TRANSLATION);

        when(changeItem.getString("field")).thenReturn(STATUS);
        when(changeItem.getString("oldstring")).thenReturn(OLD_ENGLISH_TRANSLATION);
        when(changeItem.getString("oldvalue")).thenReturn(OLD_VALUE);
        when(changeItem.getString("newstring")).thenReturn(NEW_ENGLISH_TRANSLATION);
        when(changeItem.getString("newvalue")).thenReturn(NEW_VALUE);

        when(constantsManager.getConstantObject(STATUS, OLD_VALUE)).thenReturn(inProgress);
        when(constantsManager.getConstantObject(STATUS, NEW_VALUE)).thenReturn(underReview);

        helper = new JiraHelper(entryBuilderFactory, uriProvider, activityObjectBuilder,
                userProfileAccessor, attachmentManager, remoteIssueLinkManager, rendererManager,
                fieldLayoutManager, constantsManager);
    }

    @Test
    public void assertThatNewNameTranslationTranslatesWhenNewValueAndObjectExist()
    {
        assertThat(helper.getNewChangeItemNameTranslation(changeItem).get(), is(equalTo(NEW_FRENCH_TRANSLATION)));
    }

    @Test
    public void assertThatNewNameTranslationUsesNewstringValueWhenObjectDoesNotExist()
    {
        //this scenario could happen if the object was deleted after the change item was persisted
        when(constantsManager.getConstantObject(STATUS, NEW_VALUE)).thenReturn(null);
        assertThat(helper.getNewChangeItemNameTranslation(changeItem).get(), is(equalTo(NEW_ENGLISH_TRANSLATION)));
    }

    @Test
    public void assertThatNewNameTranslationUsesNewstringValueWhenNewvalueValueDoesNotExist()
    {
        //this scenario could happen as not all change items store an "newvalue"
        when(changeItem.getString("newvalue")).thenReturn(null);
        assertThat(helper.getNewChangeItemNameTranslation(changeItem).get(), is(equalTo(NEW_ENGLISH_TRANSLATION)));
    }

    @Test
    public void assertThatNewNameTranslationReturnsNoneWhenNewvalueAndNewstringDoNotExist()
    {
        when(changeItem.getString("newstring")).thenReturn(null);
        when(changeItem.getString("newvalue")).thenReturn(null);
        assertThat(helper.getNewChangeItemNameTranslation(changeItem).isDefined(), is(equalTo(false)));
    }

    @Test
    public void assertThatOldNameTranslationTranslatesWhenOldValueAndObjectExist()
    {
        assertThat(helper.getOldChangeItemNameTranslation(changeItem).get(), is(equalTo(OLD_FRENCH_TRANSLATION)));
    }

    @Test
    public void assertThatOldNameTranslationUsesOldstringValueWhenObjectDoesNotExist()
    {
        //this scenario could happen if the object was deleted after the change item was persisted
        when(constantsManager.getConstantObject(STATUS, OLD_VALUE)).thenReturn(null);
        assertThat(helper.getOldChangeItemNameTranslation(changeItem).get(), is(equalTo(OLD_ENGLISH_TRANSLATION)));
    }

    @Test
    public void assertThatOldNameTranslationUsesOldstringValueWhenOldvalueValueDoesNotExist()
    {
        //this scenario could happen as not all change items store an "oldvalue"
        when(changeItem.getString("oldvalue")).thenReturn(null);
        assertThat(helper.getOldChangeItemNameTranslation(changeItem).get(), is(equalTo(OLD_ENGLISH_TRANSLATION)));
    }

    @Test
    public void assertThatOldNameTranslationReturnsNoneWhenOldvalueAndOldstringDoNotExist()
    {
        when(changeItem.getString("oldstring")).thenReturn(null);
        when(changeItem.getString("oldvalue")).thenReturn(null);
        assertThat(helper.getOldChangeItemNameTranslation(changeItem).isDefined(), is(equalTo(false)));
    }

    @Test
    public void assertThatGetTransitionVerbHandlesNullChangeItemValues()
    {
        when(changeItem.getString("fieldtype")).thenReturn("jira");
        when(changeItem.getString("oldstring")).thenReturn(null);
        when(changeItem.getString("oldvalue")).thenReturn(null);

        ChangeHistory changeHistory = mock(ChangeHistory.class);
        when(changeHistory.getChangeItems()).thenReturn(Lists.newArrayList(changeItem));

        Option<Pair<ActivityObjectType, ActivityVerb>> pairOption = helper.jiraActivity(changeHistory);

        assertThat(pairOption.get().second(), is(equalTo(JiraActivityVerbs.transition())));
    }

    @Test
    public void assertThatGetTransitionVerbHandlesEmptyChangeItemValues()
    {
        when(changeItem.getString("fieldtype")).thenReturn("jira");
        when(changeItem.getString("oldstring")).thenReturn("");
        when(changeItem.getString("oldvalue")).thenReturn("");

        ChangeHistory changeHistory = mock(ChangeHistory.class);
        when(changeHistory.getChangeItems()).thenReturn(Lists.newArrayList(changeItem));

        Option<Pair<ActivityObjectType, ActivityVerb>> pairOption = helper.jiraActivity(changeHistory);

        assertThat(pairOption.get().second(), is(equalTo(JiraActivityVerbs.transition())));
    }

    @Test
    public void assertThatUserProfileWithoutMetadataIsReturned() throws Exception
    {
        // when
        final UserProfile userProfile = getOnlyElement(helper.getUserProfiles(new URI("http://localhost/jira"), stubActivity(jiraUser.getUsername(), null)));
        final UserProfile anonProfile = getOnlyElement(helper.getUserProfiles(new URI("http://localhost/jira"), stubActivity(null, null)));

        // then
        assertThat(userProfile, equalTo(jiraUser));
        assertThat(anonProfile, equalTo(anonUser));
    }

    @Test
    public void assertThatGeneratorAvatarReplacesUserAvatar() throws Exception
    {
        // having
        final String avatarUrl = "http://remotesystem/avatar";
        final HistoryMetadata historyMetadata = HistoryMetadata.builder("test")
                .generator(HistoryMetadataParticipant.builder("test", "user").avatarUrl(avatarUrl).build())
                .build();

        // when
        final UserProfile userProfile = getOnlyElement(helper.getUserProfiles(new URI("http://localhost/jira"), stubActivity(jiraUser.getUsername(), historyMetadata)));
        final UserProfile anonProfile = getOnlyElement(helper.getUserProfiles(new URI("http://localhost/jira"), stubActivity(null, historyMetadata)));

        // then
        assertThat(userProfile, equalTo(stubUser(jiraUser.getUsername()).profilePictureUri(Option.some(new URI(avatarUrl))).build()));
        assertThat(anonProfile, equalTo(stubUser(anonUser.getUsername()).profilePictureUri(Option.some(new URI(avatarUrl))).build()));
    }

    @Test
    public void assertThatActorReplacesAnonymousUserProfile() throws Exception
    {
        // having
        final HistoryMetadata.HistoryMetadataBuilder withActor = HistoryMetadata.builder("test")
                .actor(HistoryMetadataParticipant.builder("remoteuser", "user")
                        .displayName("Remote User")
                        .url("http://remotesystem/userprofile")
                        .avatarUrl("http://remotesystem/useravatar").build());
        final HistoryMetadata metadataWithActor = withActor.build();
        final HistoryMetadata metadataWithActorAndGenerator = withActor
                .generator(HistoryMetadataParticipant.builder("remote", "system").avatarUrl("http://remotesystem/systemavatar").build())
                .build();

        // when
        final UserProfile userProfile = getOnlyElement(helper.getUserProfiles(new URI("http://localhost/jira"), stubActivity(jiraUser.getUsername(), metadataWithActor)));
        final UserProfile anonProfile = getOnlyElement(helper.getUserProfiles(new URI("http://localhost/jira"), stubActivity(null, metadataWithActor)));
        final UserProfile userProfileWithGenerator = getOnlyElement(helper.getUserProfiles(new URI("http://localhost/jira"), stubActivity(jiraUser.getUsername(), metadataWithActorAndGenerator)));
        final UserProfile anonProfileWithGenerator = getOnlyElement(helper.getUserProfiles(new URI("http://localhost/jira"), stubActivity(null, metadataWithActorAndGenerator)));

        // then
        assertThat(userProfile, equalTo(jiraUser));
        assertAnonProfileFromMetadata(metadataWithActor, anonProfile, metadataWithActor.getActor().getAvatarUrl());
        assertThat(userProfileWithGenerator, equalTo(stubUser(jiraUser.getUsername()).profilePictureUri(Option.some(new URI("http://remotesystem/systemavatar"))).build()));
        assertAnonProfileFromMetadata(metadataWithActorAndGenerator, anonProfileWithGenerator, metadataWithActorAndGenerator.getGenerator().getAvatarUrl());
    }

    @Test
    public void assertThatParticipantIdIsUsedIfNoDisplayName() throws Exception
    {
        // having
        final HistoryMetadata participantWithId = HistoryMetadata.builder("test")
                .actor(HistoryMetadataParticipant.builder("remoteuser", "user").build())
                .build();

        // when
        final UserProfile profile = getOnlyElement(helper.getUserProfiles(new URI("http://localhost/jira"), stubActivity(null, participantWithId)));

        // then
        assertThat(profile.getFullName(), equalTo(participantWithId.getActor().getId()));
    }

    @Test
    public void assertRelativeAvatarUrlsArePrefixedWithBaseUri() throws Exception
    {
        // having
        final String jiraUrl = "https://foo.com/jira";
        final String jiraUrlWithSlash = "https://foo.com/jira/";
        final String relativeUrlWithoutSlash = "download/resources/foo:bar";
        final String relativeUrl = "/download/resources/foo:bar";
        final HistoryMetadata generatorAvatar = createMetadataWithAvatar(relativeUrl);
        final HistoryMetadata userAvatar = HistoryMetadata.builder("test")
                .actor(HistoryMetadataParticipant.builder("remoteuser", "user")
                        .avatarUrl(relativeUrl)
                        .build())
                .build();

        // when
        final UserProfile generatorAvatarProfile = getOnlyElement(helper.getUserProfiles(new URI(jiraUrl), stubActivity(null, generatorAvatar)));
        final UserProfile userAvatarProfile = getOnlyElement(helper.getUserProfiles(new URI(jiraUrl), stubActivity(null, userAvatar)));

        final UserProfile avatarSlashSlash = getOnlyElement(helper.getUserProfiles(new URI(jiraUrlWithSlash), stubActivity(null, createMetadataWithAvatar(relativeUrl))));
        final UserProfile avatarSlashNoSlash = getOnlyElement(helper.getUserProfiles(new URI(jiraUrlWithSlash), stubActivity(null, createMetadataWithAvatar(relativeUrlWithoutSlash))));
        final UserProfile avatarNoSlashSlash = getOnlyElement(helper.getUserProfiles(new URI(jiraUrl), stubActivity(null, createMetadataWithAvatar(relativeUrlWithoutSlash))));

        // then
        assertThat(generatorAvatarProfile.getProfilePictureUri().get().toString(), equalTo(jiraUrl + relativeUrl));
        assertThat(userAvatarProfile.getProfilePictureUri().get().toString(), equalTo(jiraUrl + relativeUrl));
        assertThat(avatarSlashSlash.getProfilePictureUri().get().toString(), equalTo(jiraUrl + relativeUrl));
        assertThat(avatarSlashNoSlash.getProfilePictureUri().get().toString(), equalTo(jiraUrl + relativeUrl));
        assertThat(avatarNoSlashSlash.getProfilePictureUri().get().toString(), equalTo(jiraUrl + relativeUrl));
    }

    private HistoryMetadata createMetadataWithAvatar(final String relativeUrl)
    {
        return HistoryMetadata.builder("test")
                .generator(HistoryMetadataParticipant.builder("generator", "generator")
                        .avatarUrl(relativeUrl)
                        .build())
                .build();
    }

    private void assertAnonProfileFromMetadata(final HistoryMetadata metadata, final UserProfile profile, String avatarUrl)
    {
        assertThat(profile.getUsername(), equalTo(metadata.getActor().getId()));
        assertThat(profile.getFullName(), equalTo(metadata.getActor().getDisplayName()));
        assertThat(profile.getProfilePageUri().get().toString(), equalTo(metadata.getActor().getUrl()));
        assertThat(profile.getProfilePictureUri().get().toString(), equalTo(avatarUrl));
        assertThat(profile.getEmail(), equalTo(anonUser.getEmail()));
    }

    private UserProfile.Builder stubUser(String username) throws Exception
    {
        return new UserProfile.Builder(username)
                .fullName(username + " JIRA User")
                .email(Option.some(username + "@jiradomain.com"))
                .profilePageUri(Option.some(new URI("http://jira/profile/" + username)))
                .profilePictureUri(Option.some(new URI("http://jira/avatar/" + username)));
    }

    private JiraActivityItem stubActivity(String jirauser, HistoryMetadata historyMetadata)
    {
        JiraActivityItem item = mock(JiraActivityItem.class);
        when(item.getChangeHistoryAuthors()).thenReturn(Option.option(jirauser));
        when(item.getHistoryMetadata()).thenReturn(Option.option(historyMetadata));
        return item;
    }
}
