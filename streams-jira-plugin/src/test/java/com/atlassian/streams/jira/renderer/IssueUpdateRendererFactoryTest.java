package com.atlassian.streams.jira.renderer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.when;

import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class IssueUpdateRendererFactoryTest
{
    @Mock
    IssueUpdateRendererFactory f;

    @Test
    public void getI18nKeyReturnsWorklogNormally()
    {
        when(f.getI18nKey(Mockito.<GenericValue>any(), Mockito.anyBoolean())).thenCallRealMethod();

        GenericValue changeItem = mock(GenericValue.class);
        when(changeItem.getString("field")).thenReturn("timespent");
        when(changeItem.getString("oldstring")).thenReturn("0");
        when(changeItem.getString("newstring")).thenReturn("1");

        assertEquals("streams.item.jira.updated.list.single.worklog",
                f.getI18nKey(changeItem, true));
    }

    @Test
    public void getI18nKeyShowsReducedTime()
    {
        when(f.getI18nKey(Mockito.<GenericValue>any(), Mockito.anyBoolean())).thenCallRealMethod();

        GenericValue changeItem = mock(GenericValue.class);
        when(changeItem.getString("field")).thenReturn("timespent");
        when(changeItem.getString("oldstring")).thenReturn("1");
        when(changeItem.getString("newstring")).thenReturn("0");

        assertEquals("streams.item.jira.updated.list.single.worklog.reduced",
                f.getI18nKey(changeItem, true));
    }
}
