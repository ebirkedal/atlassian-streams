package com.atlassian.streams.jira;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.testing.AbstractUserProfileAccessorTestSuite;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.jira.JiraTesting.mockUserProfile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JiraUserProfileAccessorTest extends AbstractUserProfileAccessorTestSuite
{
    @Mock EmailFormatter emailFormatter;
    @Mock JiraAuthenticationContext authenticationContext;
    @Mock UserManager userManager;
    @Mock AvatarService avatarService;
    @Mock AvatarManager avatarManager;
    @Mock StreamsI18nResolver i18nResolver;
    @Mock UserUtil userUtil;
    @Mock ApplicationUser user1;
    @Mock ApplicationUser user2;
    @Mock ApplicationUser user3;
    public static final URI BASE_URI = URI.create("http://localhost/streams");

    @Override
    public String getProfilePathTemplate()
    {
        return "/secure/ViewProfile.jspa?name={username}";
    }

    @Override
    protected String getProfilePicturePathTemplate()
    {
        return "/secure/useravatar?avatarId={profilePictureParameter}";
    }

    @Override
    protected String getProfilePicParameter(String username)
    {
        return "0";
    }

    @Before
    public void createUserProfileBuilder()
    {
        userProfileAccessor = new JiraUserProfileAccessor(
                userUtil, getApplicationProperties(), emailFormatter, authenticationContext, userManager,
                avatarManager, i18nResolver);
    }

    @Before
    public void prepareUserUtil()
    {
        URI avatarUri = URI.create("http://localhost/streams/secure/useravatar?avatarId=0");
        when(avatarService.getAvatarAbsoluteURL(any(ApplicationUser.class), anyString(), any(Avatar.Size.class))).thenReturn(avatarUri);

        when(emailFormatter.emailVisible(any(ApplicationUser.class))).thenReturn(true);

        when(user1.getKey()).thenReturn("user key");
        when(user1.getUsername()).thenReturn("user");
        when(user1.getDisplayName()).thenReturn("User");
        when(user1.getEmailAddress()).thenReturn("u@c.com");
        when(userUtil.getUserByKey("user key")).thenReturn(user1);
        when(userUtil.getUserByName("user")).thenReturn(user1);
        when(userManager.getUserProfile("user")).thenReturn(mockUserProfile("user"));

        when(user2.getKey()).thenReturn("user 2 key");
        when(user2.getDisplayName()).thenReturn("User 2");
        when(user2.getEmailAddress()).thenReturn("u2@c.com");
        when(userUtil.getUserByKey("user 2 key")).thenReturn(user2);
        when(userUtil.getUserByName("user 2")).thenReturn(user2);
        when(userManager.getUserProfile("user 2")).thenReturn(mockUserProfile("user 2"));

        when(user3.getKey()).thenReturn("user3 key");
        when(user3.getUsername()).thenReturn("user");
        when(user3.getDisplayName()).thenReturn("User <3&'>");
        when(user3.getEmailAddress()).thenReturn("u3@c.com");
        when(userUtil.getUserByKey("user3 key")).thenReturn(user3);
        when(userUtil.getUserByName("user3")).thenReturn(user3);
        when(userManager.getUserProfile("user3")).thenReturn(mockUserProfile("user3"));
    }

    @Test
    public void assertThatEmailIsOnlyAvailableIfItIsVisibleToTheLoggedInUser()
    {
        when(emailFormatter.emailVisible(any(ApplicationUser.class))).thenReturn(false);
        assertThat(userProfileAccessor.getUserProfile(BASE_URI, "user").getEmail(), is(equalTo(none(String.class))));
    }

    @Test
    @Override
    @Ignore("SAL is doing this for us in JIRA, so there's no reason to test it ourselves")
    public void assertThatUsernameIsUriEncodedInProfileUri()
    {
    }

    @Test
    @Override
    @Ignore("SAL is doing this for us in JIRA, so there's no reason to test it ourselves")
    public void assertThatUsernameIsUriEncodedInProfilePictureUri()
    {
    }

    @Test
    public void profilePictureUriIsTakenFromSal() throws Exception
    {
        UserProfile profile = mock(UserProfile.class);
        URI profileUri = new URI("http://example.test/profile-picture-provided-by-sal");

        when(profile.getProfilePageUri()).thenReturn(new URI(""));
        when(profile.getProfilePictureUri(anyInt(), anyInt())).thenReturn(profileUri);
        when(profile.getProfilePictureUri()).thenReturn(profileUri);

        when(userManager.getUserProfile("user")).thenReturn(profile);

        Option<URI> uri = userProfileAccessor.getUserProfile(BASE_URI, "user").getProfilePictureUri();

        assertEquals(Option.some(profileUri), uri);
    }

    @Test
    public void profilePictureUriIsTreatedRelativeToBaseUrl() throws Exception
    {
        UserProfile profile = mock(UserProfile.class);
        URI profileUri = new URI("/path-absolute-profile-picture-provided-by-sal");

        when(profile.getProfilePageUri()).thenReturn(new URI(""));
        when(profile.getProfilePictureUri(anyInt(), anyInt())).thenReturn(profileUri);
        when(profile.getProfilePictureUri()).thenReturn(profileUri);

        when(userManager.getUserProfile("user")).thenReturn(profile);

        Option<URI> uri = userProfileAccessor.getUserProfile(BASE_URI, "user").getProfilePictureUri();

        assertEquals(Option.some(new URI("http://localhost/streams/path-absolute-profile-picture-provided-by-sal")), uri);
    }

    @Test
    public void profilePictureUriIsNormalizedAfterConstructionFromSal() throws Exception
    {
        UserProfile profile = mock(UserProfile.class);
        URI profileUri = new URI("/path-absolute-profile-picture-provided-by-sal");

        when(profile.getProfilePageUri()).thenReturn(new URI(""));
        when(profile.getProfilePictureUri(anyInt(), anyInt())).thenReturn(profileUri);
        when(profile.getProfilePictureUri()).thenReturn(profileUri);

        when(userManager.getUserProfile("user")).thenReturn(profile);

        final URI baseUri = URI.create("http://example.test/streams/..");
        Option<URI> uri = userProfileAccessor.getUserProfile(baseUri, "user").getProfilePictureUri();

        assertEquals(Option.some(new URI("http://example.test/path-absolute-profile-picture-provided-by-sal")), uri);
    }

    @Test
    public void profilePictureUriIsConstructedWhenSalReturnsNull() throws Exception
    {
        UserProfile profile = mock(UserProfile.class);

        when(profile.getProfilePageUri()).thenReturn(new URI(""));
        when(profile.getProfilePictureUri(anyInt(), anyInt())).thenReturn(null);
        when(profile.getProfilePictureUri()).thenReturn(null);

        when(userManager.getUserProfile("user")).thenReturn(profile);

        when(avatarManager.getAnonymousAvatarId()).thenReturn(1L);

        Option<URI> uri = userProfileAccessor.getUserProfile(BASE_URI, "I don't exist").getProfilePictureUri();

        assertEquals(Option.some(new URI("http://localhost/streams/secure/useravatar?avatarId=1")), uri);
    }
}
