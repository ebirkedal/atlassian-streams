package com.atlassian.streams.testing.pageobjects.jira;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Function;
import com.google.common.base.Joiner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;
import static com.atlassian.streams.pageobjects.WebElements.getText;

public class XmlRestorePage implements Page
{
    @Inject
    private AtlassianWebDriver driver;
    
    @FindBy(name = "filename")
    private WebElement filename;
    
    @FindBy(id = "restore_submit")
    private WebElement submit;
    
    public String getUrl()
    {
        return "/secure/admin/XmlRestore!default.jspa";
    }

    public XmlRestorePage setFile(File file)
    {
        filename.sendKeys(file.getAbsolutePath());
        return this;
    }

    public XmlRestorePage setQuickImport(boolean enabled)
    {
        if (enabled)
        {
            driver.executeScript("AJS.$('#quickImport').attr('checked', 'checked')");
        }
        else
        {
            driver.executeScript("AJS.$('#quickImport').removeAttr('checked')");
        }
        return this;
    }

    public XmlRestorePage submit()
    {
        submit.click();

        driver.waitUntil(restoreIsDone);

        return this;
    }

    public boolean hasErrors()
    {
        return driver.elementExists(By.className("errorArea")) || driver.elementExists(By.className("errMsg"));
    }

    public String getErrors()
    {
        List<WebElement> errorAreas = driver.findElements(By.cssSelector(".errorArea ul li"));
        List<WebElement> errorMessages = driver.findElements(By.className("errMsg"));
        return Joiner.on('\n').join(transform(concat(errorAreas, errorMessages), getText()));
    }

    private final Function<WebDriver, Boolean> restoreIsDone = new Function<WebDriver, Boolean>()
    {
        public Boolean apply(WebDriver driver)
        {
            return driver.getCurrentUrl().contains("/secure/ImportResult.jspa");
        }
    };
}
