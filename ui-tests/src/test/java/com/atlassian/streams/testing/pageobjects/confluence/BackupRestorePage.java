package com.atlassian.streams.testing.pageobjects.confluence;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BackupRestorePage implements Page
{
    private static final String BACKUP_SELECT_XPATH = "//select[@name=\"localFileName\"]";

    @Inject
    private AtlassianWebDriver driver;

    @FindBy(xpath = BACKUP_SELECT_XPATH)
    private WebElement backupSelector;

    @FindBy(xpath = "//input[@value=\"Restore\"]")
    private WebElement restoreButton;

    public String getUrl()
    {
        return "/admin/backup.action";
    }

    @WaitUntil
    public void waitUntilBackupSelectorIsVisible()
    {
        driver.waitUntilElementIsVisible(By.xpath(BACKUP_SELECT_XPATH));
    }

    public BackupRestorePage selectFile(String filename)
    {
        String xpath = "//option[@value=\"" + filename + "\"]";
        backupSelector.findElement(By.xpath(xpath)).click();
        return this;
    }

    public void submit()
    {
        restoreButton.click();
        driver.waitUntil(restoreCompletes);
    }

    private static final Function<WebDriver, Boolean> restoreCompletes = new Function<WebDriver, Boolean>()
    {
        public Boolean apply(WebDriver driver)
        {
            //confluence will redirect to the login screen if the restore has completed.
            if (driver.getCurrentUrl().contains("login.action"))
            {
                return true;
            }
            else
            {
                try
                {
                    Thread.sleep(500);
                    driver.navigate().refresh();
                    return false;
                }
                catch (InterruptedException e)
                {
                    throw new RuntimeException(e);
                }
            }
        }
    };
}
