package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.streams.pageobjects.Gadget;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.atlassian.streams.pageobjects.Predicates.text;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;

public class ExtendedDashboardPage extends DashboardPage
{
    @FindBy(id = "dashboard")
    WebElement dashboard;

    @FindBy(id = "dashboard-tools-dropdown")
    WebElement toolsMenu;

    @FindBy(id = "add-gadget")
    WebElement addGadget;

    public DashboardToolsMenu openToolsMenu()
    {
        toolsMenu.findElement(By.tagName("a")).click();
        return pageBinder.bind(DashboardToolsMenu.class);
    }

    public AddGadgetDialog openAddGadgetDialog()
    {
        addGadget.click();
        return pageBinder.bind(AddGadgetDialog.class);
    }

    public Gadget getGadgetWithTitle(String gadgetTitle)
    {
        String gadgetId = find(dashboard.findElements(By.className("dashboard-item-title")), text(equalTo(gadgetTitle))).
            getAttribute("id").
            replace("-title", "");
        return pageBinder.bind(Gadget.class, gadgetId);
    }
}
