package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.1
 */
public class JiraProfileActivityStream {

    @Inject
    private AtlassianWebDriver driver;

    private final String AUTH_MESSAGES_CSS_CLASS = "applinks-auth-messages";
    private final String id;


    public JiraProfileActivityStream(String id) {
        this.id = id;
    }

    public String getAuthenticationMessage()
    {
        return getAuthenticationMessageContainer().getText();
    }

    public List<WebElement> getAllAuthenticationMessages() {
        return getAuthenticationMessageContainer().findElements(By.tagName("li"));
    }

    public String getAuthenticationMessageAtIndex(int index) {
        return getAllAuthenticationMessages().get(index).getText();
    }

    private WebElement getAuthenticationMessageContainer()
    {
        WebElement container = driver.findElement(By.id(id));
        driver.waitUntilElementIsLocated(By.className(AUTH_MESSAGES_CSS_CLASS));
        return container.findElement(By.className(AUTH_MESSAGES_CSS_CLASS));
    }
}
