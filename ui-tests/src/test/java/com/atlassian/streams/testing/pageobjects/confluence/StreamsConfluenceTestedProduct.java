package com.atlassian.streams.testing.pageobjects.confluence;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import com.atlassian.integrationtesting.ui.RestoreFromBackupException;
import com.atlassian.pageobjects.Defaults;
import com.atlassian.webdriver.confluence.ConfluenceTestedProduct;

import org.apache.commons.io.IOUtils;

import static com.atlassian.integrationtesting.ui.UiTesters.getHome;
import static org.apache.commons.io.FileUtils.toFile;
import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;

@Defaults(instanceId = "confluence", contextPath = "/streams", httpPort = 3990)
public class StreamsConfluenceTestedProduct /*extends ConfluenceTestedProduct implements RestorableTestedProduct<WebDriverTester>*/
{
    private final ConfluenceTestedProduct product;

    // There's a bug (SELENIUM-97) in pageobjects-confluence where a subclass of a subclass of TestedProduct won't get injected into
    // ConfluenceAbstractPage, so we'll just decorate ConfluenceTestedProduct for now.
    public StreamsConfluenceTestedProduct(ConfluenceTestedProduct product)
    {
        this.product = product;
    }

    public void restore(String path)
    {
        File backupFile = processBackupFile(path);

        product.gotoLoginPage()
                .loginAsSysAdmin(BackupRestorePage.class)
                .selectFile(backupFile.getName())
                .submit();

        product.gotoLoginPage().loginAsSysAdmin(BackupRestorePage.class);
    }

    private File processBackupFile(String filename)
    {
        URL url = getClass().getClassLoader().getResource(filename);
        if (url == null)
        {
            throw new RestoreFromBackupException("Backup file " + filename + " not found");
        }

        try
        {
            ZipFile backupZip = new java.util.zip.ZipFile(toFile(url));
            File outputFile = new File(getHome(), "restore/backup" + randomAlphanumeric(5) + ".zip");
            outputFile.getParentFile().mkdirs();
            ZipOutputStream testZip = new ZipOutputStream(new FileOutputStream(outputFile));

            try
            {
                Enumeration<? extends ZipEntry> entries = backupZip.entries();
                while (entries.hasMoreElements())
                {
                    ZipEntry entry = entries.nextElement();
                    InputStream inputEntryStream = backupZip.getInputStream(entry);
                    testZip.putNextEntry(new ZipEntry(entry.getName()));
                    if (entry.getName().equalsIgnoreCase("entities.xml"))
                    {
                        String data = IOUtils.toString(inputEntryStream);
                        data = data.replaceAll("@base-url@", product.getProductInstance().getBaseUrl());
                        IOUtils.write(data, testZip);
                    }
                    else
                    {
                        IOUtils.copy(inputEntryStream, testZip);
                    }

                    testZip.closeEntry();
                }
            }
            finally
            {
                closeQuietly(testZip);
            }

            return outputFile;
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
