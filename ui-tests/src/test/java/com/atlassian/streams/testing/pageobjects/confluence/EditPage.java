package com.atlassian.streams.testing.pageobjects.confluence;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditPage implements Page
{
    private static final String INSERT_BUTTON_ID = "rte-button-insert";
    private static final String OTHER_MACROS_ID = "rte-insert-macro";

    private final ConfluencePage pageUnderEdit;

    @Inject
    private PageBinder pageBinder;

    @Inject
    private AtlassianWebDriver driver;

    @FindBy(name = "confirm")
    private WebElement saveButton;

    @FindBy(name = "cancel")
    private WebElement cancelButton;

    @FindBy(id = INSERT_BUTTON_ID)
    private WebElement insertButton;

    public EditPage(ConfluencePage page)
    {
        this.pageUnderEdit = page;
    }

    public String getUrl()
    {
        return driver.getCurrentUrl();
    }

    @WaitUntil
    public void waitUntilInsertButtonIsVisible()
    {
        driver.waitUntilElementIsVisible(By.id(INSERT_BUTTON_ID));
    }

    public ConfluencePage save()
    {
        saveButton.click();
        return pageUnderEdit;
    }

    public ConfluencePage cancel()
    {
        cancelButton.click();
        return pageUnderEdit;
    }

    public InsertMacroDialog openMacroDialog()
    {
        insertButton.click();
        driver.findElement(By.id(OTHER_MACROS_ID)).click();
        return pageBinder.bind(InsertMacroDialog.class, this);
    }
}
