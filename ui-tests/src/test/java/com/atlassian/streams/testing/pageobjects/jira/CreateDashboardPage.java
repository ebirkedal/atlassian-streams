package com.atlassian.streams.testing.pageobjects.jira;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateDashboardPage implements Page
{
    private @Inject AtlassianWebDriver driver;
    private @Inject PageBinder pageBinder;

    @FindBy(name = "portalPageName")
    private WebElement name;

    @FindBy(id = "add_submit")
    private WebElement add;

    public String getUrl()
    {
        return "secure/AddPortalPage!default.jspa";
    }

    @WaitUntil
    public void waitUntilLoaded()
    {
        driver.waitUntilElementIsVisible(By.name("portalPageName"));
    }

    public CreateDashboardPage setName(String dashboardName)
    {
        name.sendKeys(dashboardName);
        return this;
    }

    public ExtendedDashboardPage addDashboard()
    {
        add.click();
        return pageBinder.bind(ExtendedDashboardPage.class);
    }
}
