package com.atlassian.streams.testing.pageobjects;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.Tester;

public interface RestorableTestedProduct<T extends Tester> extends TestedProduct<T>
{
    void restore(String filename);
}
