package com.atlassian.streams.testing.pageobjects.jira;

import java.net.URI;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class JiraIssuePage implements Page
{
    private static final String WATCHER_TOGGLE_ID = "watching-toggle";
    private static final String VOTE_TOGGLE_ID = "vote-toggle";
    
    private static final By WATCHER_DATA = By.id("watcher-data");

    private static final By VOTE_DATA = By.id("vote-data");
    
    private final String issueKey;
    
    @Inject
    private AtlassianWebDriver driver;
    
    @FindBy(id = "votes-val")
    private WebElement votes;
    
    @FindBy(id = "watchers-val")
    private WebElement watchers;
    
    @FindBy(id = WATCHER_TOGGLE_ID)
    private WebElement watcherToggle;
    
    @FindBy(id = VOTE_TOGGLE_ID)
    private WebElement voteToggle;
    
    public JiraIssuePage(String issueKey)
    {
        this.issueKey = issueKey;
    }
    
    public JiraIssuePage(URI uri)
    {
        String path = uri.getPath();
        this.issueKey = path.substring(path.lastIndexOf('/') + 1);
    }

    public String getUrl()
    {
        return "browse/" + issueKey;
    }

    @WaitUntil
    public void waitUntilWatcherDataIsVisible()
    {
        driver.waitUntilElementIsVisibleAt(WATCHER_DATA, watchers);
        driver.waitUntilElementIsVisibleAt(VOTE_DATA, votes);
    }
    
    public String getWatcherCount()
    {
        return watchers.findElement(WATCHER_DATA).getText();
    }
    
    public JiraIssuePage toggleWatching()
    {
        boolean wasWatching = isWatching();
        watcherToggle.click();
        if (wasWatching)
        {
            driver.waitUntilElementIsVisibleAt(By.className("icon-watch-off"), watcherToggle);
        }
        else
        {
            driver.waitUntilElementIsVisibleAt(By.className("icon-watch-on"), watcherToggle);
        }
        return this;
    }
    
    public boolean isWatching()
    {
        return watcherToggle.findElement(By.className("icon")).getAttribute("class").contains("icon-watch-on");
    }

    public String getVoterCount()
    {
        return votes.findElement(VOTE_DATA).getText();
    }
    
    public JiraIssuePage toggleVote()
    {
        boolean hadVoted = hasVotedForIssue();
        voteToggle.click();
        if (hadVoted)
        {
            driver.waitUntilElementIsVisibleAt(By.className("icon-vote-off"), voteToggle);
        }
        else
        {
            driver.waitUntilElementIsVisibleAt(By.className("icon-vote-on"), voteToggle);
        }
        return this;
    }

    public boolean hasVotedForIssue()
    {
        return voteToggle.findElement(By.className("icon")).getAttribute("class").contains("icon-vote-on");
    }
}
