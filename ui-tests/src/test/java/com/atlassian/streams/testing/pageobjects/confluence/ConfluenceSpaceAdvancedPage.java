package com.atlassian.streams.testing.pageobjects.confluence;

import java.net.URI;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConfluenceSpaceAdvancedPage implements Page
{
    private static final String SPACE_WATCHING_ID = "space-watching";

    private final URI uri;

    @Inject
    private AtlassianWebDriver driver;

    @FindBy(id = SPACE_WATCHING_ID)
    private WebElement spaceWatchingLink;

    public ConfluenceSpaceAdvancedPage(URI uri)
    {
        this.uri = uri;
    }

    public String getUrl()
    {
        return uri.toString();
    }

    @WaitUntil
    public void waitUntilLinksAreVisible()
    {
        driver.waitUntilElementIsLocated(By.id(SPACE_WATCHING_ID));
    }
    
    public boolean isWatching()
    {
        return spaceWatchingLink.getAttribute("title").startsWith("Stop");
    }

    public void toggleWatching()
    {
        boolean wasWatching = isWatching();
        
        spaceWatchingLink.click();
        
        driver.waitUntil(watching(!wasWatching));
    }

    public void watch()
    {
        if (!isWatching()) 
        {
            toggleWatching();
        }
    }

    public void unwatch()
    {
        if (isWatching()) 
        {
            toggleWatching();
        }
    }

    protected Function<WebDriver, Boolean> watching(boolean flag)
    {
        return new Watching(flag);
    }
    
    private class Watching implements Function<WebDriver, Boolean>
    {
        private final boolean flag;
        
        Watching(boolean flag)
        {
            this.flag = flag;
        }

        public Boolean apply(WebDriver from)
        {
            return (isWatching() == flag);
        }
    }
}
