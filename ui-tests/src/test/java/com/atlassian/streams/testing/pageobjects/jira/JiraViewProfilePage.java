package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.streams.pageobjects.ActivityStreamGadget;
import com.atlassian.streams.pageobjects.Gadget;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.inject.Inject;
import java.net.URI;
import java.util.List;

import static com.atlassian.streams.pageobjects.Predicates.text;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;

public class JiraViewProfilePage implements Page {

    private final URI uri;

    @Inject
    private AtlassianWebDriver driver;

    @Inject
    private PageBinder pageBinder;

    @FindBy(xpath = "//div[@id='activity-profile-fragment']//iframe")
    private WebElement activityStreamContainer;

    private final String ACTIVITY_STREAM_CONTAINER_ID = "1";

    public JiraViewProfilePage(URI uri)
    {
        this.uri = uri;
    }

    public String getUrl()
    {
        return uri.toString();
    }

    public JiraProfileActivityStream activityStreamView() {
        String frameId = activityStreamContainer.getAttribute("id");
        driver.switchTo().frame(frameId);
        return pageBinder.bind(JiraProfileActivityStream.class, ACTIVITY_STREAM_CONTAINER_ID);

    }


}
