package com.atlassian.streams.testing.pageobjects.jira;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddGadgetDialog
{
    @Inject
    private AtlassianWebDriver driver;

    @Inject
    private PageBinder pageBinder;

    @FindBy(id = "macro-browser-dialog")
    private WebElement dialog;

    @FindBy(id = "category-all")
    private final String currentCategory = "all";

    @FindBy(className = "finish")
    private WebElement finishButton;

    @WaitUntil
    public void waitForDialogToBeVisible()
    {
        driver.waitUntilElementIsVisibleAt(By.className("dialog-title"), dialog);
    }

    public AddGadgetDialog addGadget(String gadgetTitle)
    {
        final WebElement addButton = dialog.findElement(By.id(gadgetId(gadgetTitle))).findElement(By.className("macro-button-add"));
        addButton.click();
        driver.waitUntil(new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver input)
            {
                return addButton.isEnabled();
            }
        });
        return this;
    }

    private String gadgetId(String gadgetTitle)
    {
        String prefix = currentCategory.equals("all") ? "" : currentCategory + "-";
        return prefix + "macro-" + gadgetTitle.replaceAll(" ", "").replaceAll(":", "");
    }

    public ExtendedDashboardPage finished()
    {
        finishButton.click();
        driver.waitUntilElementIsNotVisibleAt(By.className("dialog-title"), dialog);
        return pageBinder.bind(ExtendedDashboardPage.class);
    }
}
