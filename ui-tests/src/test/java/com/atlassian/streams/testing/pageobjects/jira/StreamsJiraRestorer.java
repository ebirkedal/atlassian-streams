package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.integrationtesting.ui.RestoreFromBackupException;
import com.atlassian.jira.pageobjects.JiraTestedProduct;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static com.atlassian.integrationtesting.ui.UiTesters.getHome;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.io.FileUtils.toFile;
import static org.apache.commons.io.FileUtils.writeStringToFile;
import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;


public class StreamsJiraRestorer
{
    private final JiraTestedProduct product;

    public StreamsJiraRestorer(JiraTestedProduct product)
    {
        this.product = product;
    }

    public void restore(String filename)
    {
        product.gotoLoginPage().
            loginAsSysAdmin(WebSudoAuthenticatePage.class).
            loginAsSysAdmin();

        XmlRestorePage restore = product.visit(XmlRestorePage.class).
            setFile(processBackupFile(filename)).
            setQuickImport(true).
            submit();
        if (restore.hasErrors())
        {
            throw new RestoreFromBackupException(restore.getErrors());
        }
    }

    private File processBackupFile(String filename)
    {
        URL url = getClass().getClassLoader().getResource(filename);
        if (url == null)
        {
            throw new RestoreFromBackupException("Backup file " + filename + " not found");
        }

        try
        {
            String data = readFileToString(toFile(url));
            data = data.replaceAll("@jira-home@", getHome());
            data = data.replaceAll("@base-url@", product.getProductInstance().getBaseUrl());
            File dataFile = new File(getHome(), "import/backup" + randomAlphanumeric(5) + ".xml");
            writeStringToFile(dataFile, data);
            return dataFile;
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
