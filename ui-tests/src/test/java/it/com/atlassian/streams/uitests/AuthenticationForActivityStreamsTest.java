package it.com.atlassian.streams.uitests;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.streams.testing.UiTestsTesterRunner;
import com.atlassian.streams.testing.pageobjects.jira.JiraProfileActivityStream;
import com.atlassian.streams.testing.pageobjects.jira.JiraViewProfilePage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.URI;

import static com.atlassian.streams.testing.UiTestGroups.MULTI;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

@RunWith(UiTestsTesterRunner.class)
@TestGroups(MULTI)
public class AuthenticationForActivityStreamsTest
{

    private static final String USER_PROFILE_PAGE_URL = "/secure/ViewProfile.jspa";
    private final String AUTHENTICATION_MESSAGE = "Additional information may be available, please authenticate for more information";
    private JiraViewProfilePage pageUnderTest;

    @Before
    public void goToViewProfilePage()
    {
        JiraTestedProduct product = TestedProductFactory.create(JiraTestedProduct.class, "jira-applinks-oauth", null);
        product.gotoLoginPage().login("user", "user", DashboardPage.class);

        pageUnderTest = product.visit(JiraViewProfilePage.class, URI.create(USER_PROFILE_PAGE_URL));
    }

    @Test
    public void assertThatAuthenticationMessagesAreShown()
    {

        JiraProfileActivityStream activityStream = pageUnderTest.activityStreamView();
        assertThat(activityStream.getAuthenticationMessage(), containsString(AUTHENTICATION_MESSAGE));
        assertThat(activityStream.getAllAuthenticationMessages().size(), is(2));
        assertThat(activityStream.getAuthenticationMessageAtIndex(0), containsString("Authenticate with "));
        assertThat(activityStream.getAuthenticationMessageAtIndex(1), containsString("Authenticate with "));
    }

}
