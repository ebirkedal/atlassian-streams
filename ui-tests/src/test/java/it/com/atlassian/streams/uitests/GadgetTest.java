package it.com.atlassian.streams.uitests;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.streams.pageobjects.ActivityItem;
import com.atlassian.streams.pageobjects.ActivityStreamConfiguration;
import com.atlassian.streams.pageobjects.ActivityStreamGadget;
import com.atlassian.streams.testing.UiTestsTesterRunner;
import com.atlassian.streams.testing.pageobjects.jira.ExtendedDashboardPage;
import com.atlassian.streams.testing.pageobjects.jira.StreamsJiraRestorer;

import org.apache.commons.lang.math.RandomUtils;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.pageobjects.Matchers.hasSummary;
import static com.atlassian.streams.pageobjects.Matchers.withSummary;
import static com.atlassian.streams.testing.UiTestGroups.JIRA;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(UiTestsTesterRunner.class)
@TestGroups(JIRA)
public class GadgetTest
{
    private static JiraTestedProduct product = TestedProductFactory.create(JiraTestedProduct.class);
    private static StreamsJiraRestorer restorer = new StreamsJiraRestorer(product);

    ActivityStreamGadget gadget;

    @BeforeClass
    public static void restoreDataAndLogIn()
    {
        restorer.restore("jira/backups/all-entry-types.xml");
        product.gotoLoginPage().loginAsSysAdmin(DashboardPage.class);
    }

    @Before
    public void createNewDashboard()
    {
        String dashboardName = "Test Dashboard - " + RandomUtils.nextInt();
        gadget = product.visit(ExtendedDashboardPage.class).
            openToolsMenu().
            createDashboard().
            setName(dashboardName).
            addDashboard().
            openAddGadgetDialog().
            addGadget("ActivityStream").
            finished().
            getGadgetWithTitle("Activity Stream").
            viewAs(ActivityStreamGadget.class);
    }

    @Test
    public void assertThatFeedLinkUriContainsOsAuthBasic()
    {
        assertThat(gadget.getFeedLink().getHref(), containsString("os_authType=basic"));
    }

    @Test
    public void assertThatTitleIsProperlySet()
    {
        String title = "New Title";
        gadget.openConfiguration().setTitle(title).save();
        assertThat(gadget.getTitle(), is(equalTo(title)));
    }

    /**
     * Note: this will definitely fail if assertThatTitleIsProperlySet() is broken.
     */
    @Test
    public void assertThatTitleIsProperlyEscaped()
    {
        String title = "<script>alert('bwahahaha!')</script>";
        gadget.openConfiguration().setTitle(title).save();
        assertThat(gadget.getTitle(), is(equalTo(title)));
    }

    @Test
    public void assertThatOnlyEntriesRelatingToUserArePresentWhenUsernameFilterIsSpecified()
    {
        addUsernameFilter("admin");

        assertThat(gadget.getActivityItems(), allOf(
                everyItem(hasSummary(containsString("admin"))),
                not(is(emptyIterable(ActivityItem.class)))));
    }

    @Test
    public void assertThatOnlyIssueAddedEntriesArePresentWhenOnlyIssueAddedActivityFilterIsSelected()
    {
        selectActivity("Issue created");

        assertThat(gadget.getActivityItems(), allOf(
                everyItem(hasSummary(containsString("created"))),
                not(is(emptyIterable(ActivityItem.class)))));
    }

    @Test
    public void assertThatOnlyIssueUpdatedEntriesArePresentWhenOnlyIssueUpdatedActivityFilterIsSelected()
    {
        selectActivity("Issue edited");

        assertThat(gadget.getActivityItems(), allOf(
                everyItem(anyOf(hasSummary(containsString("updated")), hasSummary(containsString("changed")))),
                not(is(emptyIterable(ActivityItem.class)))));
    }

    @Test
    public void assertThatIssueTransitionEntriesWithNonSystemTransitionsArePresentWhenOnlyCustomIssueTransitionsActivityFilterIsSelected()
    {
        selectActivity("Issue transitioned");

        Matcher<? super Iterable<ActivityItem>> hasItemWithChangedStatusInSummary = hasItem(withSummary(containsString("changed the status")));
        assertThat(gadget.getActivityItems(), hasItemWithChangedStatusInSummary);
    }

    @Test
    public void assertThatOnlyIssueReopenedEntriesArePresentWhenOnlyIssueReopenedActivityFilterIsSelected()
    {
        selectActivity("Issue reopened");

        assertThat(gadget.getActivityItems(), allOf(
                everyItem(hasSummary(containsString("reopened"))),
                not(is(emptyIterable(ActivityItem.class)))));
    }

    @Test
    public void assertThatOnlyIssueClosedEntriesArePresentWhenOnlyIssueClosedActivityFilterIsSelected()
    {
        selectActivity("Issue closed");

        assertThat(gadget.getActivityItems(), allOf(
                everyItem(hasSummary(containsString("closed"))),
                not(is(emptyIterable(ActivityItem.class)))));
    }

    @Test
    public void assertThatOnlyIssueResolvedEntriesArePresentWhenOnlyIssueResolvedActivityFilterIsSelected()
    {
        selectActivity("Issue resolved");

        assertThat(gadget.getActivityItems(), allOf(
                everyItem(hasSummary(containsString("resolved"))),
                not(is(emptyIterable(ActivityItem.class)))));
    }

    @Test
    public void assertThatOnlyIssueProgressStartedEntriesArePresentWhenOnlyIssueProgressStartedActivityFilterIsSelected()
    {
        selectActivity("Issue progress started");

        assertThat(gadget.getActivityItems(), allOf(
                everyItem(hasSummary(containsString("started progress"))),
                not(is(emptyIterable(ActivityItem.class)))));
    }

    @Test
    public void assertThatOnlyIssueProgressStoppedEntriesArePresentWhenOnlyIssueProgressStoppedActivityFilterIsSelected()
    {
        selectActivity("Issue progress stopped");

        assertThat(gadget.getActivityItems(), allOf(
                everyItem(hasSummary(containsString("stopped progress"))),
                not(is(emptyIterable(ActivityItem.class)))));
    }

    @Test
    public void assertThatOnlyCommentedEntriesArePresentWhenOnlyCommentAddedActivityFilterIsSelected()
    {
        selectActivity("Comment added");

        assertThat(gadget.getActivityItems(), allOf(
                everyItem(hasSummary(containsString("commented"))),
                not(is(emptyIterable(ActivityItem.class)))));
    }

    @Test
    public void assertThatOnlyAttachmentAddedEntriesArePresentWhenOnlyAttachmentAddedActivityFilterIsSelected()
    {
        selectActivity("Attachment added");

        assertThat(gadget.getActivityItems(), allOf(
                everyItem(hasSummary(containsString("attached"))),
                not(is(emptyIterable(ActivityItem.class)))));
    }

    @Test
    public void assertThatTimeOutIsDisplayedWhenProvidedInTheFeed()
    {
        // We need to close the configuration panel first
        gadget.openConfiguration().save();

        ActivityStreamGadget gadgetWithCustomFeed = gadget.setServletPath("/plugins/servlet/jstest-resources/feed.xml");
        assertThat(gadgetWithCustomFeed.getTimeoutSources(), contains("Applink A", "Applink B"));
    }


    private void addUsernameFilter(String username)
    {
        addGlobalFilter("Username", username);
    }

    private void selectActivity(String activity)
    {
        addGlobalFilter("Activity", activity);
    }

    private void addGlobalFilter(String rule, String value) {
        addGlobalFilter(rule, "is", value);
    }

    private void addGlobalFilter(String rule, String operator, String value) {
        ActivityStreamConfiguration configuration = gadget.openConfiguration();
        configuration.addGlobalFilter().
            getFirstFilter().
            selectRule(rule).
            selectOperator(operator).
            setValue(value);
        configuration.save();
    }

    private <E> org.hamcrest.Matcher<java.lang.Iterable<? extends E>> emptyIterable(Class<E> type)
    {
        return Matchers.<E>emptyIterable();
    }
}
