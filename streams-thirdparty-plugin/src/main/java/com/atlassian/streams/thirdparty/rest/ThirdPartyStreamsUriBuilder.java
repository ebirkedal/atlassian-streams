package com.atlassian.streams.thirdparty.rest;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.thirdparty.rest.resources.ThirdPartyStreamsCollectionResource;
import com.atlassian.streams.thirdparty.rest.resources.ThirdPartyStreamsResource;

import com.sun.jersey.api.uri.UriBuilderImpl;

import static com.atlassian.streams.thirdparty.api.ActivityQuery.DEFAULT_MAX_RESULTS;
import static com.atlassian.streams.thirdparty.api.ActivityQuery.DEFAULT_START_INDEX;
import static com.google.common.base.Preconditions.checkNotNull;

public class ThirdPartyStreamsUriBuilder
{
    private final ApplicationProperties applicationProperties;

    public ThirdPartyStreamsUriBuilder(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    public final URI buildActivityUri(Long activityId)
    {
        return newBaseUriBuilder().path(ThirdPartyStreamsResource.class).build(activityId);
    }
    public final URI buildAbsoluteActivityUri(Long activityId)
    {
        return makeAbsolute(buildActivityUri(activityId));
    }

    public final URI buildActivityCollectionUri()
    {
        return buildActivityCollectionUri(DEFAULT_MAX_RESULTS,  DEFAULT_START_INDEX);
    }

    public final URI buildActivityCollectionUri(int maxResults, int startIndex)
    {
        UriBuilder uriBuilder = newBaseUriBuilder().path(ThirdPartyStreamsCollectionResource.class);
        //don't specify max results or start index parameters if they are the default values
        if (maxResults != DEFAULT_MAX_RESULTS)
        {
            uriBuilder = uriBuilder.queryParam("max-results", Integer.valueOf(maxResults));
        }
        if (startIndex != DEFAULT_START_INDEX)
        {
            uriBuilder = uriBuilder.queryParam("start-index", Integer.valueOf(startIndex));
        }
        return uriBuilder.build();
    }

    /**
     * Returns an absolute {@code URI}.
     * If the parameter is a relative URI, prepends the base url.
     * If the parameter is already absolute, does nothing.
     *
     * Note that this will NOT add the context path (i.e. "streams" in our case), given a relative URI.
     *
     * @param uri the uri to make absolute
     * @return the absolute uri
     */
    public final URI makeAbsolute(URI uri)
    {
        if (uri.isAbsolute())
        {
            return uri;
        }
        return URI.create(applicationProperties.getBaseUrl()).resolve(uri).normalize();
    }

    protected UriBuilder newBaseUriBuilder()
    {
        return newApplicationBaseUriBuilder().path("/rest/activities/1.0");
    }

    private UriBuilder newApplicationBaseUriBuilder()
    {
        URI base = URI.create(applicationProperties.getBaseUrl()).normalize();
        return new UriBuilderImpl().path(base.getPath());
    }
}
