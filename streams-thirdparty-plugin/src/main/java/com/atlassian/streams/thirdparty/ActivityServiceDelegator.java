package com.atlassian.streams.thirdparty;

import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityQuery;
import com.atlassian.streams.thirdparty.api.ActivityService;
import com.atlassian.streams.thirdparty.api.Application;

import static com.google.common.base.Preconditions.checkNotNull;

public class ActivityServiceDelegator implements ActivityService
{
    private ActivityServiceActiveObjects delegate;

    public ActivityServiceDelegator(ActivityServiceActiveObjects delegate)
    {
        this.delegate = checkNotNull(delegate);
    }
    
    @Override
    public Activity postActivity(Activity activity)
    {
        return delegate.postActivity(activity);
    }

    @Override
    public Option<Activity> getActivity(long activityId)
    {
        return delegate.getActivity(activityId);
    }

    @Override
    public Iterable<Activity> activities(ActivityQuery query)
    {
        return delegate.activities(query);
    }

    @Override
    public boolean delete(long activityId)
    {
        return delegate.delete(activityId);
    }

    @Override
    public Iterable<Application> applications()
    {
        return delegate.applications();
    }
}
