package com.atlassian.streams.thirdparty;

import java.util.Map;

import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.spi.StreamsFilterOption;
import com.atlassian.streams.spi.StreamsFilterOptionProvider;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.thirdparty.api.ActivityService;
import com.atlassian.streams.thirdparty.api.Application;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;

import static com.atlassian.streams.thirdparty.ThirdPartyStreamsEntryBuilder.getProviderIdAndName;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.transformValues;
import static com.google.common.collect.Maps.uniqueIndex;

public final class ThirdPartyStreamsFilterOptionProvider implements StreamsFilterOptionProvider
{
    public static final String PROVIDER_NAME = "provider_name";

    private final StreamsI18nResolver i18nResolver;
    private final ActivityService service;
    private final TransactionTemplate transactionTemplate;

    public ThirdPartyStreamsFilterOptionProvider(final ActivityService service,
                                                 final StreamsI18nResolver i18nResolver,
                                                 final TransactionTemplate transactionTemplate)
    {
        this.service = checkNotNull(service, "service");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.transactionTemplate = checkNotNull(transactionTemplate, "transactionTemplate");
    }

    @Override
    public Iterable<StreamsFilterOption> getFilterOptions()
    {
        return ImmutableList.of(getThirdPartyProviderFilter());
    }

    private StreamsFilterOption getThirdPartyProviderFilter()
    {
        Map<String,String> values = transactionTemplate.execute(new TransactionCallback<Map<String,String>>()
        {
            @Override
            public Map<String,String> doInTransaction()
            {
                return transformValues(uniqueIndex(applicationAlphaSorter.sortedCopy(service.applications()), appIdAndName), toDisplayName);
            }
        });

        return new StreamsFilterOption.Builder(PROVIDER_NAME, StreamsFilterType.SELECT)
            .displayName(i18nResolver.getText("streams.filter.thirdparty.provider.name"))
            .helpTextI18nKey("streams.filter.help.thirdparty.provider.name")
            .i18nKey("streams.filter.thirdparty.provider.name")
            .unique(true)
            .providerAlias(true)
            .values(values)
            .build();
    }

    @Override
    public Iterable<ActivityOption> getActivities()
    {
        return ImmutableList.<ActivityOption>of();
    }

    private static final Function<Application, String> appIdAndName = new Function<Application, String>()
    {
        @Override
        public String apply(Application app)
        {
            return getProviderIdAndName(app);
        }
    };

    private static final Function<Application, String> toDisplayName = new Function<Application, String>()
    {
        @Override
        public String apply(Application app)
        {
            return app.getDisplayName();
        }
    };

    private static final Ordering<Application> applicationAlphaSorter = new Ordering<Application>()
    {
        public int compare(Application app1, Application app2)
        {
            return app1.getDisplayName().compareTo(app2.getDisplayName());
        }
    };
}
