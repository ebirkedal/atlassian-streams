package com.atlassian.streams.thirdparty.ao;

import java.net.URI;

import net.java.ao.Entity;
import net.java.ao.Polymorphic;

@Polymorphic
public interface ActivityObjEntity extends Entity
{
    URI getObjectId();
    void setObjectId(URI objectId);

    String getContent();
    void setContent(String content);

    String getDisplayName();
    void setDisplayName(String displayName);

    MediaLinkEntity getImage();
    void setImage(MediaLinkEntity image);

    URI getObjectType();
    void setObjectType(URI objectType);

    String getSummary();
    void setSummary(String summary);

    URI getUrl();
    void setUrl(URI url);
}
