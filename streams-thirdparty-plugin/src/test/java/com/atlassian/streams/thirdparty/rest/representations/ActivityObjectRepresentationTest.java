package com.atlassian.streams.thirdparty.rest.representations;

import java.net.URI;

import com.atlassian.streams.api.Html;

import org.junit.Test;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.thirdparty.TestData.DEFAULT_ID;
import static com.atlassian.streams.thirdparty.TestData.INVALID_URI_STRING;
import static com.atlassian.streams.thirdparty.TestData.PICTURE_URI;
import static com.atlassian.streams.thirdparty.TestData.PROFILE_URI;
import static com.atlassian.streams.thirdparty.TestData.RELATIVE_URI;
import static com.atlassian.streams.thirdparty.TestData.USERNAME;
import static com.atlassian.streams.thirdparty.TestData.USER_FULL_NAME;
import static com.atlassian.streams.thirdparty.TestData.assertNotValidationError;
import static com.atlassian.streams.thirdparty.TestData.assertValidationError;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ActivityObjectRepresentationTest
{
    private ActivityObjectRepresentation rep;
    
    @Test
    public void toActivityObjectCopiesDisplayName()
    {
        String name = "my name";
        rep = ActivityObjectRepresentation.builder().displayName(some(name)).build();
        assertThat(assertNotValidationError(rep.toActivityObject()).getDisplayName(), equalTo(some(name)));
    }

    @Test
    public void toActivityObjectCopiesId()
    {
        rep = ActivityObjectRepresentation.builder().id(some(DEFAULT_ID)).build();
        assertThat(assertNotValidationError(rep.toActivityObject()).getId(), equalTo(some(DEFAULT_ID)));
    }

    @Test
    public void toActivityObjectRejectsInvalidId()
    {
        rep = ActivityObjectRepresentation.builder().idString(some(INVALID_URI_STRING)).build();
        assertValidationError(rep.toActivityObject());
    }
    
    @Test
    public void toActivityObjectCopiesObjectType()
    {
        URI type = URI.create("type");
        rep = ActivityObjectRepresentation.builder().objectType(some(type)).build();
        assertThat(assertNotValidationError(rep.toActivityObject()).getType(), equalTo(some(type)));
    }

    @Test
    public void toActivityObjectCopiesSummary()
    {
        Html summary = html("some Marie");
        rep = ActivityObjectRepresentation.builder().summary(some(summary)).build();
        assertThat(assertNotValidationError(rep.toActivityObject()).getSummary(), equalTo(some(summary)));
    }

    @Test
    public void toActivityObjectCopiesUrl()
    {
        rep = ActivityObjectRepresentation.builder().url(some(DEFAULT_ID)).build();
        assertThat(assertNotValidationError(rep.toActivityObject()).getUrl(), equalTo(some(DEFAULT_ID)));
    }

    @Test
    public void toActivityObjectRejectsInvalidUrl()
    {
        rep = ActivityObjectRepresentation.builder().urlString(some(INVALID_URI_STRING)).build();
        assertValidationError(rep.toActivityObject());
    }
    
    @Test
    public void toUserProfileCopiesUsername()
    {
        rep = ActivityObjectRepresentation.builder().idString(some(USERNAME)).displayName(some(USER_FULL_NAME)).build();
        assertThat(assertNotValidationError(rep.toUserProfile()).getUsername(), equalTo(USERNAME));
    }

    @Test
    public void toUserProfileCopiesFullName()
    {
        rep = ActivityObjectRepresentation.builder().idString(some(USERNAME)).displayName(some(USER_FULL_NAME)).build();
        assertThat(assertNotValidationError(rep.toUserProfile()).getFullName(), equalTo(USER_FULL_NAME));
    }

    @Test
    public void toUserProfileCopiesProfileUri()
    {
        rep = ActivityObjectRepresentation.builder().idString(some(USERNAME)).displayName(some(USER_FULL_NAME))
            .url(some(PROFILE_URI)).build();
        assertThat(assertNotValidationError(rep.toUserProfile()).getProfilePageUri(), equalTo(some(PROFILE_URI)));
    }

    @Test
    public void toUserProfileCopiesPictureUri()
    {
        rep = ActivityObjectRepresentation.builder().idString(some(USERNAME)).displayName(some(USER_FULL_NAME))
            .image(some(MediaLinkRepresentation.builder(PICTURE_URI).build())).build();
        assertThat(assertNotValidationError(rep.toUserProfile()).getProfilePictureUri(), equalTo(some(PICTURE_URI)));
    }

    @Test
    public void toUserProfileRejectsInvalidPictureUri()
    {
        rep = ActivityObjectRepresentation.builder().idString(some(USERNAME)).displayName(some(USER_FULL_NAME))
            .image(some(new MediaLinkRepresentation.Builder(INVALID_URI_STRING).build())).build();
        assertValidationError(rep.toUserProfile());
    }

    @Test
    public void toUserProfileRejectsInvalidProfileUri()
    {
        rep = ActivityObjectRepresentation.builder().idString(some(USERNAME)).displayName(some(USER_FULL_NAME))
            .urlString(some(INVALID_URI_STRING)).build();
        assertValidationError(rep.toUserProfile());
    }
    
    @Test
    public void toUserProfileRejectsRelativePictureUri()
    {
        rep = ActivityObjectRepresentation.builder().idString(some(USERNAME)).displayName(some(USER_FULL_NAME))
            .image(some(MediaLinkRepresentation.builder(RELATIVE_URI).build())).build();
        assertValidationError(rep.toUserProfile());
    }
    
    @Test
    public void toUserProfileRejectsRelativeProfileUri()
    {
        rep = ActivityObjectRepresentation.builder().idString(some(USERNAME)).displayName(some(USER_FULL_NAME))
            .url(some(RELATIVE_URI)).build();
        assertValidationError(rep.toUserProfile());
    }
}
