package com.atlassian.streams.thirdparty;

import java.net.URI;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.Link;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityObject;
import com.atlassian.streams.thirdparty.api.Application;
import com.atlassian.streams.thirdparty.api.Image;

import com.google.common.collect.Iterables;

import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.StreamsActivityProvider.ICON_LINK_REL;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.size;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ThirdPartyStreamsEntryBuilderTest
{
    private static final String ACTIVITY_ICON = "http://sweet/icon.png";
    private static final URI ACTIVITY_ID = URI.create("http://activity/id");
    private static final URI ACTIVITY_URI = URI.create("http://activity/uri");
    private static final URI ACTIVITY_OBJECT_ID = URI.create("http://object/id");
    private static final URI ACTIVITY_OBJECT_URI = URI.create("http://object/uri");
    private static final URI APPLICATION_ID = URI.create("http://app");
    private static final String APPLICATION_NAME = "AppName";
    private static final URI TARGET_ID = URI.create("http://target/id");
    private static final URI TARGET_URI = URI.create("http://target/uri");
    private static final String ACTIVITY_OBJECT_TYPE = "typo";
    private static final String ACTIVITY_OBJECT_TITLE = "title";
    private static final String TARGET_TYPE = "bog";
    private static final String TARGET_TITLE = "roll";
    private static final DateTime ACTIVITY_DATE = new DateTime();
    
    @Mock private ThirdPartyStreamsEntryBuilder entryBuilder;
    @Mock private StreamsI18nResolver i18nResolver;
    @Mock private WebResourceManager webResourceManager;

    @Mock private Activity activity;
    @Mock private UserProfile user;
    @Mock private Application application;
    @Mock private ActivityObject activityObject;
    @Mock private ActivityObject target;
    
    @Before
    public void setup()
    {
        entryBuilder = new ThirdPartyStreamsEntryBuilder(i18nResolver, webResourceManager);

        when(activity.getId()).thenReturn(some(ACTIVITY_ID));
        when(activity.getUrl()).thenReturn(some(ACTIVITY_URI));
        when(activity.getPostedDate()).thenReturn(ACTIVITY_DATE);
        when(activity.getTitle()).thenReturn(none(Html.class));
        when(activity.getContent()).thenReturn(none(Html.class));
        when(activity.getVerb()).thenReturn(none(URI.class));
        when(activity.getTarget()).thenReturn(none(ActivityObject.class));
        when(activity.getIcon()).thenReturn(none(Image.class));
        
        when(activity.getUser()).thenReturn(user);
        when(user.getUsername()).thenReturn("");
        when(user.getFullName()).thenReturn("");
        when(user.getProfilePageUri()).thenReturn(none(URI.class));
        when(user.getProfilePictureUri()).thenReturn(none(URI.class));
        
        when(activity.getApplication()).thenReturn(application);
        when(application.getId()).thenReturn(APPLICATION_ID);
        when(application.getDisplayName()).thenReturn(APPLICATION_NAME);
        
        when(activity.getObject()).thenReturn(some(activityObject));
        when(activityObject.getId()).thenReturn(some(ACTIVITY_OBJECT_ID));
        when(activityObject.getUrl()).thenReturn(some(ACTIVITY_OBJECT_URI));
        when(activityObject.getType()).thenReturn(some(URI.create(ACTIVITY_OBJECT_TYPE)));
        when(activityObject.getDisplayName()).thenReturn(some(ACTIVITY_OBJECT_TITLE));

        when(activity.getTarget()).thenReturn(some(target));
        when(target.getId()).thenReturn(some(TARGET_ID));
        when(target.getUrl()).thenReturn(some(TARGET_URI));
        when(target.getType()).thenReturn(some(URI.create(TARGET_TYPE)));
        when(target.getDisplayName()).thenReturn(some(TARGET_TITLE));
        
        when(webResourceManager.getStaticPluginResource(org.mockito.Matchers.<String>anyObject(),
                                                        org.mockito.Matchers.<String>anyObject(),
                                                        org.mockito.Matchers.<UrlMode>anyObject())).thenReturn(
            ACTIVITY_ICON);
    }
    
    @Test
    public void entryIdIsActivityId()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        assertThat(entry.getId(), equalTo(ACTIVITY_ID));
    }
    
    @Test
    public void postedDateIsActivityPublishedDate()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        assertThat(entry.getPostedDate(), equalTo(ACTIVITY_DATE));
    }
    
    @Test
    public void applicationTypeIsApplicationIdIfNotEmpty()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.getApplicationType(), equalTo(APPLICATION_ID.toASCIIString()));
    }
    
    @Test
    public void linkUriIsActivityUrlIfBothIdAndUrlAreGiven()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.getAlternateLink(), equalTo(ACTIVITY_URI));
    }
    
    @Test
    public void linkUriIsActivityUriIfActivityIdIsOmitted()
    {
        when(activity.getId()).thenReturn(none(URI.class));
        
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.getAlternateLink(), equalTo(ACTIVITY_URI));
    }
    
    @Test
    public void linkUriIsActivityIdIfActivityUrlIsOmitted()
    {
        when(activity.getUrl()).thenReturn(none(URI.class));
        
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.getAlternateLink(), equalTo(ACTIVITY_ID));
    }
    
    @Test
    public void categoriesAreEmpty()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.getCategories(), Matchers.<String>emptyIterable());  
    }
    
    @Test
    public void objectIdIsPresent()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        StreamsEntry.ActivityObject object = getOnlyElement(entry.getActivityObjects());
        
        assertThat(object.getId(), equalTo(some(ACTIVITY_OBJECT_ID.toString())));
    }

    @Test
    public void objectUrlIsPresent()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        StreamsEntry.ActivityObject object = getOnlyElement(entry.getActivityObjects());
        
        assertThat(object.getAlternateLinkUri(), equalTo(some(ACTIVITY_OBJECT_URI)));
    }
    
    @Test
    public void objectTypeIsPresentIfProvided()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        StreamsEntry.ActivityObject object = getOnlyElement(entry.getActivityObjects());
        ActivityObjectType type = object.getActivityObjectType().get();
        
        assertThat(type.key(), equalTo(ACTIVITY_OBJECT_TYPE));
        assertThat(type.iri().toString(), equalTo(ACTIVITY_OBJECT_TYPE));
    }

    @Test
    public void objectTypeIsAbsentIfNotProvided()
    {
        when(activityObject.getType()).thenReturn(none(URI.class));
        
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        StreamsEntry.ActivityObject object = getOnlyElement(entry.getActivityObjects());
        assertThat(object.getActivityObjectType(), equalTo(none(ActivityObjectType.class)));
    }

    @Test
    public void objectTitleIsPresent()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        StreamsEntry.ActivityObject object = getOnlyElement(entry.getActivityObjects());
        
        assertThat(object.getTitle(), equalTo(some(ACTIVITY_OBJECT_TITLE)));
    }

    @Test
    public void objectCanBeOmitted()
    {
    	when(activity.getObject()).thenReturn(none(ActivityObject.class));

        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        assertThat(size(entry.getActivityObjects()), equalTo(0));
    }
    
    @Test
    public void verbIsActivityVerbIfNotEmpty()
    {
        String verb = "eptify";
        when(activity.getVerb()).thenReturn(some(URI.create(verb)));

        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.getVerb().key(), equalTo(verb));
        assertThat(entry.getVerb().iri().toString(), equalTo(verb));
    }
    
    @Test
    public void verbIsPostIfEmpty()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.getVerb(), equalTo(post()));
    }

    @Test
    public void targetIdIsPresent()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        assertTrue(entry.getTarget().isDefined());
        assertThat(entry.getTarget().get().getId(), equalTo(some(TARGET_ID.toString())));
    }

    @Test
    public void targetUrlIsPresent()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        assertTrue(entry.getTarget().isDefined());
        assertThat(entry.getTarget().get().getAlternateLinkUri(), equalTo(some(TARGET_URI)));
    }
    
    @Test
    public void targetTypeIsPresentIfProvided()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        assertTrue(entry.getTarget().isDefined());
        ActivityObjectType type = entry.getTarget().get().getActivityObjectType().get();
        
        assertThat(type.key(), equalTo(TARGET_TYPE));
        assertThat(type.iri().toString(), equalTo(TARGET_TYPE));
    }
    
    @Test
    public void targetTypeIsAbsentIfNotProvided()
    {
        when(target.getType()).thenReturn(none(URI.class));
        
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);
        
        assertTrue(entry.getTarget().isDefined());
        assertThat(entry.getTarget().get().getActivityObjectType(), equalTo(none(ActivityObjectType.class)));
    }

    @Test
    public void targetTitleIsPresent()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertTrue(entry.getTarget().isDefined());
        assertThat(entry.getTarget().get().getTitle(), equalTo(some(TARGET_TITLE)));
    }

    @Test
    public void iconLinkIsDefaultActivityHasNoIcon()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(Iterables.getOnlyElement(entry.getLinks().get(ICON_LINK_REL)).getHref(), equalTo(URI.create(ACTIVITY_ICON)));
    }
    
    @Test
    public void iconLinkIsPresentIfActivityHasIcon()
    {
        URI iconUri = URI.create("http://icons/x");
        Image icon = Image.withUrl(iconUri);
        
        when(activity.getIcon()).thenReturn(some(icon));
        
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        Link link = Iterables.getOnlyElement(entry.getLinks().get(ICON_LINK_REL));
        assertThat(link.getHref(), equalTo(iconUri));
    }
    
    @Test
    public void titleHtmlIsRendered()
    {
        String title = "my <b>title</b>";
        
        when(activity.getTitle()).thenReturn(some(html(title)));
        
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.renderTitleAsHtml(), equalTo(html(title)));
    }
    
    @Test
    public void missingTitleIsRenderedAsEmptyString()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.renderTitleAsHtml(), equalTo(html("")));
    }
    
    @Test
    public void contentHtmlIsRendered()
    {
        String content = "my <b>content</b>";
        
        when(activity.getContent()).thenReturn(some(html(content)));
        
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.renderContentAsHtml(), equalTo(some(html(content))));
    }

    @Test
    public void missingContentIsRenderedAsNone()
    {
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.renderContentAsHtml(), equalTo(Option.<Html>none()));
    }
    
    @Test
    public void summaryIsRenderedAsNone()
    {
        // we aren't currently providing summaries
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        assertThat(entry.renderSummaryAsHtml(), equalTo(Option.<Html>none()));
    }
    
    @Test
    public void canSpecifyUserName()
    {
        String name = "user";
        when(user.getUsername()).thenReturn(name);

        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        UserProfile profile = getOnlyElement(entry.getAuthors());
        assertThat(profile.getUsername(), equalTo(name));
    }

    @Test
    public void canSpecifyUserFullName()
    {
        String name = "You Sir";
        when(user.getFullName()).thenReturn(name);

        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        UserProfile profile = getOnlyElement(entry.getAuthors());
        assertThat(profile.getFullName(), equalTo(name));
    }
    
    @Test
    public void canSpecifyUserImage()
    {
        URI iconUri = URI.create("http://my/face");
        when(user.getProfilePictureUri()).thenReturn(some(iconUri));
        
        StreamsEntry entry = entryBuilder.buildStreamsEntry(activity);

        UserProfile profile = getOnlyElement(entry.getAuthors());
        assertThat(profile.getProfilePictureUri(), equalTo(some(iconUri)));
    }
}
