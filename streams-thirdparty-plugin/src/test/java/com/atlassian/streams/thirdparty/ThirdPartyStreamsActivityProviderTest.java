package com.atlassian.streams.thirdparty;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityService;
import com.atlassian.streams.thirdparty.api.Application;
import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * Test for {@link ThirdPartyStreamsActivityProvider}
 *
 * @since v5.3.10
 */
@RunWith (MockitoJUnitRunner.class)
public class ThirdPartyStreamsActivityProviderTest
{
    @Mock
    StreamsI18nResolver streamsI18nResolver;

    @Mock
    ActivityService activityService;

    @Mock
    ThirdPartyStreamsEntryBuilder entryBuilder;

    @InjectMocks
    ThirdPartyStreamsActivityProvider thirdPartyStreamsActivityProvider;

    @Test
    public void testGetEntriesPaginatedRequests() throws Exception
    {
        ActivityRequest activityRequest = mock(ActivityRequest.class);
        when(activityRequest.getMaxResults()).thenReturn(100);

        ThirdPartyStreamsActivityProvider spy = spy(thirdPartyStreamsActivityProvider);

        URI jacURL = new URI("https://jira.atlassian.com");
        Activity activity = Activity.builder(Application.application("jira", jacURL), new DateTime(1234), "igrunert").id(Option.some(jacURL)).build().right().get();
        List<Activity> tenActivityItems = Arrays.asList(activity, activity, activity, activity, activity, activity, activity, activity, activity, activity);

        StreamsEntry streamsEntry = new StreamsEntry(newEntryParams(), streamsI18nResolver);
        when(entryBuilder.buildStreamsEntry(activity)).thenReturn(streamsEntry);

        doReturn(tenActivityItems).when(spy).getActivities(eq(activityRequest), eq(0));
        doReturn(tenActivityItems).when(spy).getActivities(eq(activityRequest), eq(10));
        doReturn(Collections.emptyList()).when(spy).getActivities(eq(activityRequest), eq(20));

        Iterable<StreamsEntry> entries = spy.getEntries(activityRequest);

        assertThat(entries, Matchers.<StreamsEntry>iterableWithSize(20));
    }

    @Test
    public void testGetEntriesPaginatedRequestsStopsWhenFulfilled() throws Exception
    {
        ActivityRequest activityRequest = mock(ActivityRequest.class);
        when(activityRequest.getMaxResults()).thenReturn(11);

        ThirdPartyStreamsActivityProvider spy = spy(thirdPartyStreamsActivityProvider);

        URI jacURL = new URI("https://jira.atlassian.com");
        Activity activity = Activity.builder(Application.application("jira", jacURL), new DateTime(1234), "igrunert").id(Option.some(jacURL)).build().right().get();
        List<Activity> tenActivityItems = Arrays.asList(activity, activity, activity, activity, activity, activity, activity, activity, activity, activity);

        StreamsEntry streamsEntry = new StreamsEntry(newEntryParams(), streamsI18nResolver);
        when(entryBuilder.buildStreamsEntry(activity)).thenReturn(streamsEntry);

        doReturn(tenActivityItems).when(spy).getActivities(eq(activityRequest), eq(0));
        doReturn(tenActivityItems).when(spy).getActivities(eq(activityRequest), eq(10));
        doReturn(Collections.emptyList()).when(spy).getActivities(eq(activityRequest), eq(20));

        Iterable<StreamsEntry> entries = spy.getEntries(activityRequest);

        assertThat(entries, Matchers.<StreamsEntry>iterableWithSize(11));
    }


    @Test
    public void testGetEntriesDoesNotOvershoot() throws Exception
    {
        ActivityRequest activityRequest = mock(ActivityRequest.class);
        when(activityRequest.getMaxResults()).thenReturn(10);

        ThirdPartyStreamsActivityProvider spy = spy(thirdPartyStreamsActivityProvider);

        URI jacURL = new URI("https://jira.atlassian.com");
        Activity activity = Activity.builder(Application.application("jira", jacURL), new DateTime(1234), "igrunert").id(Option.some(jacURL)).build().right().get();
        List<Activity> nineLotsOfActivityOnTheWall = Arrays.asList(activity, activity, activity, activity, activity, activity, activity, activity, activity);

        StreamsEntry streamsEntry = new StreamsEntry(newEntryParams(), streamsI18nResolver);
        when(entryBuilder.buildStreamsEntry(activity)).thenReturn(streamsEntry);

        doReturn(nineLotsOfActivityOnTheWall).when(spy).getActivities(eq(activityRequest), anyInt());

        Iterable<StreamsEntry> entries = spy.getEntries(activityRequest);

        assertThat(entries, Matchers.<StreamsEntry>iterableWithSize(10));
    }

    private StreamsEntry.Parameters<StreamsEntry.HasId, StreamsEntry.HasPostedDate, StreamsEntry.HasAlternateLinkUri, StreamsEntry.HasApplicationType, StreamsEntry.HasRenderer, StreamsEntry.HasVerb, StreamsEntry.HasAuthors> newEntryParams()
    {
        return StreamsEntry.params()
                .id(URI.create("http://example.com"))
                .postedDate(new DateTime())
                .alternateLinkUri(URI.create("http://example.com"))
                .applicationType("test")
                .authors(ImmutableNonEmptyList.of(new UserProfile.Builder("someone").fullName("Some One")
                        .build()))
                .addActivityObject(new StreamsEntry.ActivityObject(StreamsEntry.ActivityObject.params()
                        .id("activity")
                        .title(some("Some activity"))
                        .alternateLinkUri(URI.create("http://example.com"))
                        .activityObjectType(comment())))
                .verb(post()).renderer(newRenderer("title", "content"));
    }

    private StreamsEntry.Renderer newRenderer(String title, String content)
    {
        return newRenderer(title, some(content), none(String.class));
    }

    private StreamsEntry.Renderer newRenderer(String title, Option<String> content, Option<String> summary)
    {
        StreamsEntry.Renderer renderer = mock(StreamsEntry.Renderer.class);
        when(renderer.renderTitleAsHtml(any(StreamsEntry.class))).thenReturn(new Html(title));
        when(renderer.renderContentAsHtml(any(StreamsEntry.class))).thenReturn(content.map(html()));
        when(renderer.renderSummaryAsHtml(any(StreamsEntry.class))).thenReturn(summary.map(html()));
        return renderer;
    }
}
