var assertEquals = (function() {
    function getFailureMessage(expected, found) {
        return 'Expected "' + expected + '", found "' + found + '".';
    }
    
    function assertEquals(actual, expected) {
        equals(actual, expected, getFailureMessage(expected, actual));
    }

    return assertEquals;
})();

var assertNotEquals = (function() {
    function getFailureMessage(expected, found) {
        return 'Expected not "' + expected + '", found "' + found + '".';
    }
    
    function assertNotEquals(actual, expected) {
        notEqual(actual, expected, getFailureMessage(expected, actual));
    }

    return assertNotEquals;
})();