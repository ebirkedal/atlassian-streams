/* You can run the QUnit tests by starting the Aggregator plugin and going to
     http://localhost:3990/streams/plugins/servlet/jstests/all-tests (see atlassian-plugin.xml) */

var ActivityStreams = ActivityStreams || {};
// override default i18n method, which relies on gadget wizardry, to use AJS.params
ActivityStreams.getMsg = function(key) {
    return AJS.params[key];
};

// If you are doing amps:run from within a product (e.g., JIRA, you may access these tests via http://localhost:3990/streams/plugins/servlet/jstests/all-tests)
AJS.toInit(function() {

    var testObject = {
            jan1At12 : "Jan 1 2010 12:00:00",
            jan1At12Utc : "Jan 1 2010 12:00:00 -00:00",
            jan1At120002 : "Jan 1 2010 12:00:02",
            jan1At1201 : "Jan 1 2010 12:01:00",
            jan1At1202 : "Jan 1 2010 12:02:00",
            jan1At14 : "Jan 1 2010 14:00:00",
            jan1At13 : "Jan 1 2010 13:00:00",
            jan2AtFirstSec : "Jan 2 2010 00:00:01",
            jan2At12 : "Jan 2 2010 12:00:00",
            jan2AtLastSec: "Jan 2 2010 23:59:59",
            jan3At01 : "Jan 3 2010 01:00:00",
            jan3At12 : "Jan 3 2010 12:00:00",
            jan3At23 : "Jan 3 2010 23:00:00",
            jan7At23 : "Jan 7 2010 23:00:00",
            jan8At01 : "Jan 8 2010 01:00:00",
            jan8At12 : "Jan 8 2010 12:00:00",
            jan8At23 : "Jan 8 2010 23:00:00",
            jan15At12 : "Jan 15 2010 12:00:00",
            jan12011At12 : "Jan 1 2011 12:00:00",
            invalidDate : "definitelynotadate"
        };

    // we modified the date.js core file to not run automatically to accommodate loading in locales on the fly, so we have to invoke manually
    AJS.$(document).trigger('dateLocalizationLoaded.streams');

    module("Date QUnit Test");

    // Need to check if AJS.Date is being used. FECRU doesn't use pretty-date (temporary fix for FECRU) STRM-282
    if (AJS.Date && AJS.Date.parse) {
        test("Test that ISO string is properly parsed", function() {
            assertEquals(AJS.Date.parse('2010-01-01T12:00:00.000Z').getTime(), new Date(testObject.jan1At12Utc).getTime());
        });

        test("Test that correct fine-grained date string is produced for less than a minute ago", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan1At120002)), "Moments ago");
        });

        test("Test that correct coarse-grained date string is produced for less than a minute ago", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan1At120002)), "Today");
        });

        test("Test that correct fine-grained date string is produced for one minute ago", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan1At1201)), "1 minute ago");
        });

        test("Test that correct fine-grained date string is produced for minutes ago", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan1At1202)), "2 minutes ago");
        });

        test("Test that correct coarse-grained date string is produced for minutes ago", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan1At1202)), "Today");
        });

        test("Test that correct fine-grained date string is produced for one hour ago", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan1At13)), "1 hour ago");
        });

        test("Test that correct fine-grained date string is produced for hours ago", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan1At14)), "2 hours ago");
        });

        test("Test that correct coarse-grained date string is produced for hours ago", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan1At14)), "Today");
        });

        test("Test that correct fine-grained date string is produced for one day ago", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan2At12)), "Yesterday at 12:00 PM");
        });

        test("Test that the coarse-grained date string produced for previous day but less than 24 hours ago is yesterday", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan2AtFirstSec)), "Yesterday");
        });

        test("Test that correct coarse-grained date string is produced for one day ago", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan2At12)), "Yesterday");
        });

        test("Test that the coarse-grained date string produced for previous day but more than 24 hours ago is yesterday", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan2AtLastSec)), "Yesterday");
        });

        test("Test that correct fine-grained date string is produced for 2 days ago", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan3At12)), "Friday at 12:00 PM");
        });

        test("Test that the coarse-grained date string produced for 2 days ago but less than 48 hours ago is the day of the week", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan3At01)), "Friday");
        });

        test("Test that correct coarse-grained date string is produced for 2 days ago", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan3At12)), "Friday");
        });

        test("Test that the coarse-grained date string produced for 2 days ago but more than 48 hours ago is the day of the week", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan3At23)), "Friday");
        });

        test("Test that the coarse-grained date string produced for 6 days ago but more than 24*6 hours ago is the day of the week", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan7At23)), "Friday");
        });

        test("Test that the coarse-grained date string is produced for 7 days ago but less than 24*7 hours ago is not the day of the week", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan8At01)), "January 01");
        });

        test("Test that the coarse-grained date string is produced for 7 days ago but more than 24*7 hours ago is not the day of the week", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan8At23)), "January 01");
        });

        test("Test that correct fine-grained date string is produced for one week ago", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan8At12)), "January 01 at 12:00 PM");
        });

        test("Test that correct fine-grained date string is produced for 2 weeks ago", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan15At12)), "January 01 at 12:00 PM");
        });

        test("Test that correct coarse-grained date string is produced for 2 weeks ago", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan15At12)), "January 01");
        });

        test("Test that correct fine-grained date string is produced for one year ago", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan12011At12)), "Friday, January 01, 2010");
        });

        test("Test that correct coarse-grained date string is produced for one year ago", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan12011At12)), "January, 2010");
        });

        test("Test that correct date string is produced for one hour from now", function() {
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At13, new Date(testObject.jan1At12)), "Friday, January 01, 2010 at 1:00 PM");
        });

        test("Test that correct time is shown for local timezone offset of -0500 (New York)", function() {
            var offset = '-0500';
            assertEquals(AJS.Date.getFineRelativeDate(AJS.Date.parse(testObject.jan1At12Utc, offset), AJS.Date.handleTimezoneOffset(new Date(testObject.jan15At12), offset)), "January 01 at 7:00 AM");
        });

        test("Test that correct time is shown for local timezone offset of +0800 (Western Australia)", function() {
            var offset = '+0800';
            assertEquals(AJS.Date.getFineRelativeDate(AJS.Date.parse(testObject.jan1At12Utc, offset), AJS.Date.handleTimezoneOffset(new Date(testObject.jan15At12), offset)), "January 01 at 8:00 PM");
        });

        test("Test that correct time is shown for local timezone offset with non-zero minutes (-0345)", function() {
            var offset = '-0345';
            assertEquals(AJS.Date.getFineRelativeDate(AJS.Date.parse(testObject.jan1At12Utc, offset), AJS.Date.handleTimezoneOffset(new Date(testObject.jan15At12), offset)), "January 01 at 8:15 AM");
        });

        test("Test that both fine- and course-grained difference can handle zero difference", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.jan1At12, new Date(testObject.jan1At12)), "Today");
            assertEquals(AJS.Date.getFineRelativeDate(testObject.jan1At12, new Date(testObject.jan1At12)), "Moments ago");
        });

        test("Test that both fine- and course-grained difference can handle invalid dates", function() {
            assertEquals(AJS.Date.getCoarseRelativeDate(testObject.invalidDate, new Date(testObject.jan1At12)), null);
            assertEquals(AJS.Date.getFineRelativeDate(testObject.invalidDate, new Date(testObject.jan1At12)), null);
        });

        test("Test that military times are correctly escaped, e.g., 1400h shown as 1400h, rather than 1400'h' ", function() {
            var offset = '-0400';
            AJS.Date.setTimeFormat("HHmm'h'");
            assertEquals(AJS.Date.getFineRelativeDate(AJS.Date.parse(testObject.jan1At12Utc, offset), AJS.Date.handleTimezoneOffset(new Date(testObject.jan15At12), offset)), "January 01 at 0800h");
        });
    }

});
