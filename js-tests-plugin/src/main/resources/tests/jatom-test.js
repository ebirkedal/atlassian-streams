/* You can run the QUnit tests by starting the Aggregator plugin and going to
     http://localhost:3990/streams/plugins/servlet/jstests/all-tests (see atlassian-plugin.xml) */
// We have to wait for the dom to load so that we can get the base url with AJS.params
AJS.toInit(function() {
    var baseUrl = 'http://localhost:3990/streams';
    var responses = {};

    if (!jAtom) {
        test("running tests in the right plugin", function() {
            ok(false, "run the tests by running the aggregator plugin, not this one");
        });

        return;
    }

    function getFeedData(url) {
        if (!responses[url]) {
            var response = AJS.$.ajax({
                type: 'GET',
                url: url,
                async: false
            });
            responses[url] = jAtom(response.responseXML);
        }
        return responses[url];
    }

    module("jAtom QUnit Test", {
        setup: function() {
            this.feedObject = getFeedData(AJS.params.baseUrl + '/plugins/servlet/jstest-resources/feed.xml');
            this.author = this.feedObject.items[1].authors[0];
        }
    });

    test("Test feed has no authorise messages", function() {
        assertEquals(
            this.feedObject.authMessages.length,
            0
        )
    });

    test("Test that the correct number of feed items is returned", function() {
        assertEquals(
            4,
            this.feedObject.items.length
        );
    });

    test("Test that item titles are correctly parsed", function() {
        assertEquals(
            this.feedObject.items[0].title,
            '<a href="' + baseUrl + '/secure/ViewProfile.jspa?name=admin">admin</a> created <a href="' + baseUrl + '/browse/TST-2">TST-2</a> (My second bug) '
        );
    });

    test("Test that item summaries are correctly parsed", function() {
        assertEquals(
            this.feedObject.items[1].summary,
            '<a href="' + baseUrl + '/secure/ViewProfile.jspa?name=admin">admin</a> commented on <a href="' + baseUrl + '/browse/TST-1">TST-1</a> (My first bug) saying: <div class="activity-item-description">A comment.</div>'
        );
    });

    test("Test that item links are correctly parsed", function() {
        assertEquals(
            this.feedObject.items[0].links['alternate'],
            baseUrl + '/browse/TST-2'
        );
    });

    test("Test that item timestamps are correctly parsed", function() {
        assertEquals(
            this.feedObject.items[0].updated,
            '2010-10-14T21:39:34.000Z'
        );
    });

    test("Test that item categories are correctly parsed", function() {
        assertEquals(
            this.feedObject.items[2].category,
            'started'
        );
    });

    test("Test that item icons are correctly parsed", function() {
        assertEquals(
            this.feedObject.items[0].links['http://streams.atlassian.com/syndication/icon'],
            baseUrl + '/images/icons/bug.gif'
        );
    });

    test("Test that item reply-tos are correctly parsed", function() {
        assertEquals(
            this.feedObject.items[1].links['http://streams.atlassian.com/syndication/reply-to'],
            baseUrl + '/plugins/servlet/streamscomments/TST-1'
        );
    });

    test("Test that item ids are correctly parsed", function() {
        assertEquals(
            this.feedObject.items[3].id,
            'urn:uuid:3442e287-9b99-3fb1-9b1c-68b60a8bc75e'
        );
    });

    test("Test that correct number of authors is returned", function() {
        assertEquals(
            this.feedObject.items[2].authors.length,
            1
        );
    });

    test("Test that author names are correctly parsed", function() {
        assertEquals(
            this.author.name,
            'Ad Ministrator'
        );
    });

    test("Test that author urls are correctly parsed", function() {
        assertEquals(
            this.author.uri,
            baseUrl + '/secure/ViewProfile.jspa?name=admin'
        );
    });

    test("Test that author photo urls are correctly parsed", function() {
        assertEquals(
            this.author.photos[0].uri,
            baseUrl + '/secure/useravatar?ownerId=admin&s=16'
        );
        assertEquals(
            this.author.photos[1].uri,
            baseUrl + '/secure/useravatar?ownerId=admin&s=48'
        );
    });

    test("Test that summary in entry is blank", function() {
        assertEquals(
            this.feedObject.items[0].summary,
            ""
        );
    });

    test("Test that summary in activity:object is not returned", function() {
        assertNotEquals(
            this.feedObject.items[0].summary,
            'My second bug'
        );
    });

    test("Test that content in entry is returned", function() {
        assertEquals(
            this.feedObject.items[0].content,
            '<a href="' + baseUrl + '/secure/ViewProfile.jspa?name=admin">admin</a> created <a href="' + baseUrl + '/browse/TST-2">TST-2</a> (My second bug) '
        );
    });

    test("Test that author usernames are correctly parsed", function() {
        assertEquals(
            this.feedObject.items[2].authors[0].username,
            'admin'
        );
    });

    test("Test that item object types are correctly parsed", function() {
        assertEquals(
            this.feedObject.items[0].type,
            'issue'
        );
    });

    module("jAtom oauth", {
        setup: function() {
            this.feedObject = getFeedData(AJS.params.baseUrl + '/plugins/servlet/jstest-resources/oauth.xml');
        }
    });

    test("should have authentication message", function() {
        assertEquals(
            this.feedObject.authMessages.length,
            1
        );
    });

    test("should be auth for Confluence", function() {
        assertEquals(
            this.feedObject.authMessages[0].appName,
            'Confluence'
        )
    });

    test("should have auth link", function() {
        assertEquals(
            this.feedObject.authMessages[0].authUri,
            'some kind of valid url normally goes here'
        )
    });
});