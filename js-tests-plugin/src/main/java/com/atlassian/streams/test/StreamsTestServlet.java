package com.atlassian.streams.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;

import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

public class StreamsTestServlet extends HttpServlet
{
    private static final String VM_TEMPLATE_BASE = "templates/";
    private final TemplateRenderer renderer;

    public StreamsTestServlet(TemplateRenderer renderer)
    {
        this.renderer = renderer;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        try
        {
            renderer.render(VM_TEMPLATE_BASE + getUnitTestFromPath(request.getPathInfo()) + ".vm", response.getWriter());
        }
        catch (RenderingException ex)
        {
            response.sendError(SC_NOT_FOUND);
        }
    }

    private String getUnitTestFromPath(String path)
    {
        int lastIndex = path.lastIndexOf('/');
        if (lastIndex > -1)
        {
            if (lastIndex == path.length()-1)
            {
                path = path.substring(0, path.length()-1);
                lastIndex = path.lastIndexOf('/');
            }
            return path.substring(lastIndex + 1, path.length());
        }
        return path;
    }
}
