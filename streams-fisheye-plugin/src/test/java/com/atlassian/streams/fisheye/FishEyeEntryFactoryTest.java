package com.atlassian.streams.fisheye;

import java.net.URI;
import java.util.Date;

import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.fisheye.spi.data.FileRevisionData;
import com.atlassian.fisheye.spi.data.FileRevisionKeyData;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;

import com.cenqua.fisheye.RepositoryConfig;
import com.cenqua.fisheye.config1.RepositoryType;
import com.cenqua.fisheye.rep.RepositoryHandle;
import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.fisheye.spi.data.FileRevisionData.FileRevisionState.ADDED;
import static com.atlassian.streams.fisheye.FishEyeStreamsActivityProvider.CHANGESET_REVIEW_REL;
import static com.atlassian.streams.testing.matchers.Matchers.hasStreamsLink;
import static com.atlassian.streams.testing.matchers.Matchers.whereStreamsRel;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FishEyeEntryFactoryTest
{
    public static final URI BASE_URI = URI.create("http://localhost:3990/streams");
    @Mock ChangesetDataFE changeSet;
    @Mock RepositoryHandle repositoryHandle;
    @Mock RepositoryConfig cfg;
    @Mock RepositoryType type;
    @Mock FileRevisionKeyData fileRevisionKeyData;
    @Mock FileRevisionData fileRevisionData;
    @Mock UserProfileAccessor userProfileAccessor;
    @Mock ChangeSetRendererFactory rendererFactory;
    @Mock StreamsEntry.Renderer renderer;
    @Mock UriProvider uriProvider;
    @Mock FishEyePermissionAccessor permissionAccessor;
    @Mock StreamsI18nResolver i18nResolver;

    FishEyeEntryFactory fishEyeEntryFactory;

    @Before
    public void createFishEyeEntryFactory()
    {
        setUpRepositoryHandle();
        setUpFileRevisionData();
        setUpChangesetData();

        when(i18nResolver.getText("streams.item.fisheye.tooltip.changeset")).thenReturn("tooltip");

        when(userProfileAccessor.getUserProfile(org.mockito.Matchers.<URI>anyObject(),
                anyString())).thenReturn(new UserProfile.Builder("fred").build());

        when(uriProvider.getChangeSetUri(BASE_URI, changeSet, repositoryHandle))
                .thenReturn(URI.create("http://example.com/TST?cs=1234"));
        when(uriProvider.getRepositoryUri(BASE_URI, repositoryHandle)).thenReturn(URI.create("http://example.com/TST"));

        when(rendererFactory.newRenderer(Mockito.<ChangesetDataFE>any(), Mockito.<RepositoryHandle>any(),
                Mockito.<URI>any())).thenReturn(renderer);

        when(permissionAccessor.isCreateReviewAllowed()).thenReturn(true);

        fishEyeEntryFactory = new FishEyeEntryFactoryImpl(userProfileAccessor, uriProvider, rendererFactory, permissionAccessor, i18nResolver);
    }

    public void setUpRepositoryHandle()
    {
        when(type.getDescription()).thenReturn("Test");
        when(cfg.getRepositoryTypeConfig()).thenReturn(type);
        when(repositoryHandle.getCfg()).thenReturn(cfg);
        when(repositoryHandle.getName()).thenReturn("TST");
    }

    public void setUpFileRevisionData()
    {
        when(fileRevisionKeyData.getPath()).thenReturn("trunk/Test.java");
        when(fileRevisionKeyData.getRev()).thenReturn("2");

        when(fileRevisionData.getCsid()).thenReturn("2");
        when(fileRevisionData.getPath()).thenReturn("trunk/Test.java");
        when(fileRevisionData.getState()).thenReturn(ADDED);
        when(fileRevisionData.getLinesAdded()).thenReturn(1);
        when(fileRevisionData.getLinesRemoved()).thenReturn(1);
    }

    public void setUpChangesetData()
    {
        when(changeSet.getDate()).thenReturn(new Date(10L));
        when(changeSet.getAuthor()).thenReturn("someone");
        when(changeSet.getRevisionsIterable()).thenReturn(ImmutableList.<FileRevisionKeyData>of(fileRevisionKeyData));
        when(changeSet.getCsid()).thenReturn("1234");
    }

    @Test
    public void testGetEntryFromReviewReturnsEntry()
    {
        assertNotNull(fishEyeEntryFactory.getEntry(BASE_URI, changeSet, repositoryHandle));
    }

    @Test
    public void testChangesetReviewInlineActionLinkIsAddedWhenAllowedToCreateReviews()
    {
        assertThat(fishEyeEntryFactory.getEntry(BASE_URI, changeSet, repositoryHandle).getLinks().values(),
                   hasStreamsLink(whereStreamsRel(is(equalTo(CHANGESET_REVIEW_REL)))));
    }

    @Test
    public void testChangesetReviewInlineActionLinkIsNotAddedWhenNotAllowedToCreateReviews()
    {
        when(permissionAccessor.isCreateReviewAllowed()).thenReturn(false);
        assertThat(fishEyeEntryFactory.getEntry(BASE_URI, changeSet, repositoryHandle).getLinks().values(),
                   not(hasStreamsLink(whereStreamsRel(is(equalTo(CHANGESET_REVIEW_REL))))));
    }
}
