package com.atlassian.streams.fisheye;

import java.net.URI;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;

import com.cenqua.fisheye.rep.DbException;

import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

public class FishEyeUserProfileAccessor implements UserProfileAccessor
{
    private final ApplicationProperties applicationProperties;
    private final UserManager userManager;
    private final StreamsI18nResolver i18nResolver;

    public FishEyeUserProfileAccessor(ApplicationProperties applicationProperties, UserManager userManager, StreamsI18nResolver i18nResolver)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.userManager = checkNotNull(userManager, "userManager");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    private URI absoluteUri(URI uri)
    {
        if (uri == null || uri.isAbsolute())
        {
            return uri;
        }
        else
        {
            return URI.create(applicationProperties.getBaseUrl() + uri.toASCIIString());
        }
    }

    @Override
    public UserProfile getAnonymousUserProfile(URI baseUri)
    {
        return new UserProfile.Builder(i18nResolver.getText("streams.fecru.authors.unknown.username"))
                .fullName(i18nResolver.getText("streams.fecru.authors.unknown.fullname"))
                .profilePictureUri(some(getAnonymousProfilePictureUri(baseUri)))
                .build();
    }

    private URI getAnonymousProfilePictureUri(URI baseUri)
    {
        return URI.create(baseUri + "/avatar/");
    }

    @Override
    public UserProfile getUserProfile(final String username)
    {
        return getUserProfile(URI.create(applicationProperties.getBaseUrl()), username);
    }

    @Override
    public UserProfile getAnonymousUserProfile()
    {
        return getAnonymousUserProfile(URI.create(applicationProperties.getBaseUrl()));
    }

    @Override
    public UserProfile getUserProfile(URI baseUri, String username)
    {
        if (username == null)
        {
            return getAnonymousUserProfile(baseUri);
        }

        try
        {
            com.atlassian.sal.api.user.UserProfile userProfile = userManager.getUserProfile(username);
            if (userProfile != null)
            {
                return new UserProfile.Builder(username)
                    .fullName(userProfile.getFullName())
                    .email(option(userProfile.getEmail()))
                    .profilePageUri(option(absoluteUri(userProfile.getProfilePageUri())))
                    .profilePictureUri(option(absoluteUri(userProfile.getProfilePictureUri())))
                    .build();
            }
            else
            {
                return new UserProfile.Builder(username)
                        .profilePictureUri(some(getAnonymousProfilePictureUri(baseUri))).build();
            }
        }
        catch (DbException de)
        {
            return new UserProfile.Builder(username).build();
        }
    }
}
