package com.atlassian.streams.fisheye;

import com.atlassian.fisheye.spi.TxTemplate;
import com.atlassian.streams.spi.DefaultFormatPreferenceProvider;

import com.cenqua.crucible.model.Principal;
import com.cenqua.fisheye.AppConfig;
import com.cenqua.fisheye.rep.DbException;

import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.cenqua.crucible.model.Principal.Anonymous.isAnon;
import static com.google.common.base.Preconditions.checkNotNull;

public class FishEyeFormatPreferenceProvider extends DefaultFormatPreferenceProvider
{
    private static final Logger log = LoggerFactory.getLogger(FishEyeFormatPreferenceProvider.class);
    private final TxTemplate txTemplate;

    public FishEyeFormatPreferenceProvider(TxTemplate txTemplate)
    {
        this.txTemplate = checkNotNull(txTemplate, "txTemplate");
    }

    public DateTimeZone getUserTimeZone()
    {
        Principal user = txTemplate.getEffectivePrincipal();
        
        if (!isAnon(user))
        {
            try
            {
                return DateTimeZone.forTimeZone(AppConfig.getUserTimeZone(user.getUserName()));
            }
            catch (DbException e)
            {
                log.debug("Unable to get user timezone. Using system timezone.", e);
            }
        }
        
        try
        {
            return DateTimeZone.forTimeZone(AppConfig.getsConfig().getTimezone());
        }
        catch (IllegalArgumentException e)
        {
            return DateTimeZone.getDefault();
        }
    }
}
