package com.atlassian.streams.fisheye;

import com.atlassian.streams.spi.StreamsKeyProvider;

import static com.google.common.base.Preconditions.checkNotNull;

public class FishEyeKeyProvider implements StreamsKeyProvider
{
    private final ProjectKeys projectKeys;

    public FishEyeKeyProvider(ProjectKeys projectKeys)
    {
        this.projectKeys = checkNotNull(projectKeys, "projectKeys");
    }

    public Iterable<StreamsKey> getKeys()
    {
        return projectKeys.get();
    }
}
