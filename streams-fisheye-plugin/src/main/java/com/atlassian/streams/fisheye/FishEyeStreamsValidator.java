package com.atlassian.streams.fisheye;

import com.atlassian.streams.spi.StreamsValidator;

import com.cenqua.fisheye.config.RepositoryManager;

import static com.google.common.base.Preconditions.checkNotNull;

public class FishEyeStreamsValidator implements StreamsValidator
{
    private final RepositoryManager repositoryManager;
    
    private FishEyeStreamsValidator(RepositoryManager repositoryManager)
    {
        this.repositoryManager = checkNotNull(repositoryManager, "repositoryManager");
    }
    
    public boolean isValidKey(String key)
    {
        return repositoryManager.getRepository(key) != null;
    }
}
