package com.atlassian.streams.fisheye;

import com.atlassian.fisheye.spi.TxTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.spi.SessionManager;

import com.cenqua.crucible.hibernate.HibernateUtil;
import com.google.common.base.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

public final class FishEyeSessionManager implements SessionManager
{
    private static final Logger log = LoggerFactory.getLogger(FishEyeSessionManager.class);

    private final TxTemplate txTemplate;
    private final UserManager salUserManager;
    private final com.cenqua.fisheye.user.UserManager cenquaUserManager;

    public FishEyeSessionManager(TxTemplate txTemplate,
                                 UserManager salUserManager,
                                 com.cenqua.fisheye.user.UserManager cenquaUserManager)
    {
        this.txTemplate = checkNotNull(txTemplate, "txTemplate");
        this.salUserManager = checkNotNull(salUserManager, "salUserManager");
        this.cenquaUserManager = checkNotNull(cenquaUserManager, "cenquaUserManager");
    }

    @Override
    public <T> T withSession(Supplier<T> s)
    {
        // if there isn't a current session, one will be created for us when we do our querying.
        boolean sessionCreated = !HibernateUtil.isCurrentSession();

        try
        {
            return s.get();
        }
        finally
        {
            // If a new session was created for us, we need to explicitly close the Hibernate session indirectly
            // created since activity providers execute in a different thread than the request thread and hence are
            // not automatically cleaned up.
            if (sessionCreated && HibernateUtil.isCurrentSession())
            {
                try
                {
                    HibernateUtil.closeSession();
                }
                catch (Exception e)
                {
                    log.warn("Error closing Hibernate session", e);
                }
            }
        }
    }
}
