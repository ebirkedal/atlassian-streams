package com.atlassian.streams.fisheye;

import java.net.URI;

import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.StreamsUriBuilder;
import com.atlassian.streams.spi.UserProfileAccessor;

import com.cenqua.fisheye.rep.RepositoryHandle;
import com.google.common.collect.ImmutableList;

import org.joda.time.DateTime;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.fisheye.FishEyeActivityObjectTypes.changeset;
import static com.atlassian.streams.fisheye.FishEyeActivityObjectTypes.repository;
import static com.atlassian.streams.fisheye.FishEyeActivityVerbs.push;
import static com.atlassian.streams.fisheye.FishEyeStreamsActivityProvider.CHANGESET_REVIEW_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.ICON_LINK_REL;
import static com.google.common.base.Preconditions.checkNotNull;

public class FishEyeEntryFactoryImpl implements FishEyeEntryFactory
{
    public static final String FISHEYE_APPLICATION_TYPE = "com.atlassian.fisheye";
    private static final String COMMIT_CATEGORY = "commit";

    private final UserProfileAccessor userProfileAccessor;
    private final ChangeSetRendererFactory rendererFactory;
    private final UriProvider uriProvider;
    private final FishEyePermissionAccessor permissionAccessor;
    private final StreamsI18nResolver i18nResolver;

    public FishEyeEntryFactoryImpl(UserProfileAccessor userProfileAccessor,
            UriProvider uriProvider,
            ChangeSetRendererFactory rendererFactory,
            FishEyePermissionAccessor permissionAccessor,
            StreamsI18nResolver i18nResolver)
    {
        this.userProfileAccessor = checkNotNull(userProfileAccessor, "userProfileAccessor");
        this.uriProvider = checkNotNull(uriProvider, "uriProvider");
        this.rendererFactory = checkNotNull(rendererFactory, "rendererFactory");
        this.permissionAccessor = checkNotNull(permissionAccessor, "permissionAccessor");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    public StreamsEntry getEntry(final URI baseUri,
                                 final ChangesetDataFE changeSet,
                                 final RepositoryHandle repositoryHandle)
    {
        final StreamsUriBuilder streamsUriBuilder = new StreamsUriBuilder();
        final URI uri = uriProvider.getChangeSetUri(baseUri, changeSet, repositoryHandle);
        streamsUriBuilder.setUrl(uri.toASCIIString());

        StreamsEntry.Parameters params = StreamsEntry.params();

        if (permissionAccessor.isCreateReviewAllowed())
        {
            params = params.addLink(uriProvider.getChangesetReviewUri(baseUri, changeSet.getCsid(), repositoryHandle.getName()), CHANGESET_REVIEW_REL, none(String.class));
        }

        return new StreamsEntry(params
                .id(streamsUriBuilder.getUri())
                .postedDate(new DateTime(changeSet.getDate()))
                .applicationType(FISHEYE_APPLICATION_TYPE)
                .categories(ImmutableList.of(COMMIT_CATEGORY))
                .authors(ImmutableNonEmptyList.of(userProfileAccessor.getUserProfile(baseUri, changeSet.getAuthor())))
                .addLink(uriProvider.getCommitIconUri(baseUri), ICON_LINK_REL, some(i18nResolver.getText("streams.item.fisheye.tooltip.changeset")))
                .alternateLinkUri(uri)
                .addActivityObject(buildActivityObject(baseUri, changeSet, repositoryHandle))
                .verb(push())
                .target(some(buildActivityObject(baseUri, repositoryHandle)))
                .baseUri(baseUri)
                .renderer(rendererFactory.newRenderer(changeSet, repositoryHandle, baseUri)), i18nResolver);
    }

    private ActivityObject buildActivityObject(final URI baseUri,
                                               final ChangesetDataFE changeSet,
                                               final RepositoryHandle repositoryHandle)
    {
        URI changeSetUri = uriProvider.getChangeSetUri(baseUri, changeSet, repositoryHandle);
        return new ActivityObject(ActivityObject.params()
            .id(new StreamsUriBuilder().setUrl(changeSetUri.toASCIIString()).getUri().toASCIIString())
            .activityObjectType(changeset())
            .title(option(changeSet.getCsid()))
            .alternateLinkUri(changeSetUri));
    }

    private ActivityObject buildActivityObject(URI baseUri, RepositoryHandle repositoryHandle)
    {
        URI repositoryUri = uriProvider.getRepositoryUri(baseUri, repositoryHandle);
        return new ActivityObject(ActivityObject.params()
            .id(new StreamsUriBuilder().setUrl(repositoryUri.toASCIIString()).getUri().toASCIIString())
            .activityObjectType(repository())
            .title(option(repositoryHandle.getCfg().getRepositoryTypeConfig().getDescription()))
            .alternateLinkUri(repositoryUri));
    }
}
