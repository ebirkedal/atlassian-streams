package com.atlassian.streams.fisheye;

import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.streams.api.StreamsEntry;

import com.cenqua.fisheye.rep.RepositoryHandle;

import java.net.URI;

public interface FishEyeEntryFactory
{
    /**
     * Converts a log entry to a {@code StreamsEntry}.
     *
     * @param baseUri the baseUri to use for links in the entries.
     * @param changeSet the changeset to convert into an entry.
     * @param repository the repository handle.
     * @return The converted entry.
     */
    StreamsEntry getEntry(final URI baseUri, ChangesetDataFE changeSet, RepositoryHandle repository);
}
