package com.atlassian.streams.fisheye;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.fisheye.spi.data.FileRevisionData;

import com.cenqua.fisheye.rep.RepositoryHandle;

public class UriProvider
{
    static final String FISHEYE_COMMIT_ICON_PATH = "/download/resources/com.atlassian.streams.fisheye/images/changeset.gif";

    public UriProvider()
    {
    }

    public URI getChangeSetUri(final URI baseUri, final ChangesetDataFE changeSet, final RepositoryHandle repository)
    {
        final String csId = changeSet.getCsid();
        final URI repositoryUri = getRepositoryUri(baseUri, repository);
        return UriBuilder
                .fromUri(repositoryUri)
                .queryParam("cs", csId)
                .build();
    }

    public URI getRepositoryUri(final URI baseUri, final RepositoryHandle repository)
    {
        final String repositoryName = repository.getName();
        return UriBuilder
                .fromUri(baseUri)
                .path("changelog")
                .path(repositoryName)
                .build();
    }

    public URI getChangesetReviewUri(final URI baseUri, String csId, String projectKey)
    {
        return UriBuilder
                .fromUri(baseUri)
                .path("cru")
                .path("create")
                .queryParam("csid", csId)
                .queryParam("repo", projectKey)
                .build()
                .normalize();
    }

    public URI getChangelogUri(final URI baseUri, final FileRevisionData revision, final RepositoryHandle repository)
    {
        final String repositoryName = repository.getName();
        final String revisionPath = revision.getPath();
        final String csId = revision.getCsid();
        return UriBuilder
                .fromUri(baseUri)
                .path("browse")
                .path(repositoryName)
                .path(revisionPath)
                .queryParam("r", csId)
                .build();
    }

    public URI getCommitIconUri(final URI baseUri)
    {
        return UriBuilder
                .fromUri(baseUri)
                .path(FISHEYE_COMMIT_ICON_PATH)
                .build();
    }
}
