package com.atlassian.streams.fisheye.external.activity;

import java.util.Date;
import java.util.TimeZone;

import com.atlassian.fisheye.activity.ExternalActivityItem;
import com.atlassian.fisheye.activity.ExternalActivityItemSearchParams;

import static com.atlassian.fisheye.activity.ExternalActivityItemSearchParams.SearchDirection.TOWARDS_FUTURE;
import static com.atlassian.fisheye.activity.ExternalActivityItemSearchParams.SearchDirection.TOWARDS_PAST;

public class ExternalActivityItemSearchParamsFactoryImpl implements ExternalActivityItemSearchParamsFactory
{
    public ExternalActivityItemSearchParams laterActivityParams(final ExternalActivityItem item)
    {
        return new EmptyExternalActivityItemSearchParams()
        {
            @Override
            public Date getMaxDate()
            {
                return new Date();
            }

            @Override
            public int getMaxItems()
            {
                return 2;
            }

            @Override
            public Date getMinDate()
            {
                return item.getDate();
            }

            @Override
            public SearchDirection getSearchDirection()
            {
                return TOWARDS_FUTURE;
            }
        };
    }
    
    public ExternalActivityItemSearchParams earlierActivityParams(final ExternalActivityItem item)
    {
        return new EmptyExternalActivityItemSearchParams()
        {
            @Override
            public Date getMaxDate()
            {
                return item.getDate();
            }

            @Override
            public int getMaxItems()
            {
                return 2;
            }

            @Override
            public Date getMinDate()
            {
                return new Date(0L);
            }

            @Override
            public SearchDirection getSearchDirection()
            {
                return TOWARDS_PAST;
            }
        };
    }

    private static class EmptyExternalActivityItemSearchParams implements ExternalActivityItemSearchParams
    {
        public Iterable<String> getCommitters()
        {
            return null;
        }

        public Date getMaxDate()
        {
            return null;
        }

        public int getMaxItems()
        {
            return 0;
        }

        public Date getMinDate()
        {
            return null;
        }

        public Integer getProjectId()
        {
            return null;
        }

        public String getRepFilter()
        {
            return null;
        }

        public SearchDirection getSearchDirection()
        {
            return null;
        }

        public TimeZone getTz()
        {
            return null;
        }

        public String getUserFilter()
        {
            return null;
        }
    }
}
