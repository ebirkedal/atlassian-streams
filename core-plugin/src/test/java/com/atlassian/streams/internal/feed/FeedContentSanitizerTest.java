package com.atlassian.streams.internal.feed;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hamcrest.text.IsEmptyString;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.owasp.validator.html.Policy;
import org.owasp.validator.html.PolicyException;

import com.atlassian.streams.api.FeedContentSanitizer;

public class FeedContentSanitizerTest
{
    private FeedContentSanitizer sanitizer;

    private final static Pattern HREF_PATTERN = Pattern.compile("href=\"(.*)\"");

    @Before
    public void createSanitizer()
    {
        sanitizer = new FeedContentSanitizerImpl();
    }

    @Test
    public void assertThatPlainTextIsLeftAsIs()
    {
        String result = sanitizer.sanitize("Hello World!");
        assertThat(result, equalTo("Hello World!"));
    }

    @Test
    public void assertThatHtmlWithSafeTagsIsLeftAsIs()
    {
        String result = sanitizer.sanitize("Hello <i>World</i>!");
        assertThat(result, equalTo("Hello <i>World</i>!"));
    }

    @Test
    public void assertThatUnsafeAttributesAreRemovedFromSafeTags()
    {
        String result = sanitizer.sanitize("Hello <i onclick='alert(\"pwned!\")'>World</i>!");
        assertThat(result, equalTo("Hello <i>World</i>!"));
    }

    @Test
    public void assertThatUnsafeTagsAreRemoved()
    {
        String result = sanitizer.sanitize("Hello <i>World</i><script>alert(\"pwned!\");</script>!");
        assertThat(result, equalTo("Hello <i>World</i>!"));
    }

    @Test
    public void assertThatStyleTagsAreRemoved()
    {
        String result = sanitizer.sanitize("Hello <i>World</i><style>#main{display:none;}</style>!");
        assertThat(result, equalTo("Hello <i>World</i>!"));
    }

    @Test @Ignore("STRM-2043 Currently (with mvn and antisamy config as they are) leads to a java.lang.NoClassDefFoundError: org.w3c.css.sac.DocumentHandler when trying to parse the style attribute contents.")
    public void assertThatStyleAttributesAreRemoved()
    {
        String result = sanitizer.sanitize("Hello <i style='display:none;'>World</i>!");
        assertThat(result, equalTo("Hello <i>World</i>!"));
    }

    @Test
    public void assertThatOnloadAttributeIsRemovedFromImgTags()
    {
        String result = sanitizer.sanitize("Hello World!<img src='hello-world.png' onload='alert(\"bwahahahaha\")'>");
        assertThat(result, not(containsString("onload")));
    }

    @Test
    public void assertThatInvalidOnSiteAhrefAttributeRemoved()
    {
        for (String ahref : provideValidAhrefs())
        {
            assertThat( getHrefAttributeOrNull(sanitizer.sanitize("<a href=\"" + ahref + "\"></a>")), not(IsEmptyString.isEmptyOrNullString()));
        }
    }

    @Test
    public void assertThatValidOnSiteAhrefAttributeNotRemoved() throws PolicyException
    {
        for (String ahref : provideInvalidAhrefs())
        {
            assertThat(getHrefAttributeOrNull(sanitizer.sanitize("<a href=\"" + ahref + "\"></a>")), IsEmptyString.isEmptyOrNullString());
        }
    }

    // let's test just the regular expression without the sanity process
    @Test
    public void assertJustOnsiteRegularExpressionShouldMatch () throws PolicyException
    {
        Pattern onsiteURLPattern =  getOnsiteRegularExpression();
        for (String ahref : provideValidAhrefs())
        {
            assertTrue(onsiteURLPattern + " should match " + ahref, onsiteURLPattern.matcher(ahref).matches());
        }
    }

    @Test
    public void assertJustOnsiteRegularExpressionShouldNotMatch () throws PolicyException
    {
        Pattern onsiteURLPattern =  getOnsiteRegularExpression();
        for (String ahref : provideInvalidAhrefs())
        {
            assertFalse(onsiteURLPattern + " should NOT match " + ahref, onsiteURLPattern.matcher(ahref).matches());
        }
        // can't place this URL(s) in #provideInvalidAhrefs as the sanitizer is configured
        // to allow absolute URLs (for some protocols) in the <a href />
        // so #assertThatInvalidOnSiteAhrefAttributeRemoved will fail
        for (String ahref : new String [] {" http://example.com", "http://example.com", "       ftp://example.com/root", "smtp://user:host@example"})
        {
            assertFalse(onsiteURLPattern + " should NOT match " + ahref, onsiteURLPattern.matcher(ahref).matches());
        }
    }

    private Pattern getOnsiteRegularExpression() throws PolicyException
    {
        URL policyUrl = getClass().getClassLoader().getResource(FeedContentSanitizerImpl.ANTISAMY_POLICY_FILE);
        Policy policy = Policy.getInstance(policyUrl);

        return policy.getRegularExpression("onsiteURL").getPattern();
    }

    private String [] provideValidAhrefs()
    {
        return new String [] {
                "/streams/browse/ONE-2?focusedCommentId=10010&amp;page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-10010",
                "/should/go",
                "should/go",
                "/should/go?x=y#/some/widget",
                "should/go?x=y#/some/widget"
        };
    }

    private String [] provideInvalidAhrefs()
    {
        return new String [] {
                "callto:12445668",
                "malicious:hacker/readme.exe",
                "//example.com/some.spy",
                "<script>alert(3);</script>",
                " javascript://x/%0aalert(0)",
                " //example.com",
                "ms-help:123",
                "\u00A0//asdf"
        };
    }

    private String getHrefAttributeOrNull(String element)
    {
        Matcher matcher = HREF_PATTERN.matcher(element);
        if (matcher.find())
        {
            return matcher.group(1);
        }
        return null;
    }
}
