package com.atlassian.streams.bamboo;

import java.net.URI;

import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.bamboo.builder.BambooStreamsEntryBuilder;
import com.atlassian.streams.bamboo.builder.ResultsSummaryStreamsEntryBuilder;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.StreamsUriBuilder;

import org.joda.time.DateTime;

import static com.google.common.base.Preconditions.checkNotNull;

public class BambooEntryFactoryImpl implements BambooEntryFactory
{
    public static final String BAMBOO_APPLICATION_TYPE = "com.atlassian.bamboo";

    private final ResultsSummaryStreamsEntryBuilder resultsSummaryBuilder;
    private final StreamsI18nResolver i18nResolver;

    public BambooEntryFactoryImpl(final ResultsSummaryStreamsEntryBuilder resultsSummaryBuilder, StreamsI18nResolver i18nResolver)
    {
        this.resultsSummaryBuilder = checkNotNull(resultsSummaryBuilder, "resultsSummaryBuilder");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    public StreamsEntry buildEntry(final URI baseUri, final ResultsSummary buildSummary)
    {
        return build(baseUri, resultsSummaryBuilder, buildSummary, new DateTime(buildSummary.getBuildCompletedDate()));
    }

    private <T> StreamsEntry build(final URI baseUri, BambooStreamsEntryBuilder<T> builder, T t, DateTime dateTime)
    {
        StreamsUriBuilder uriBuilder = new StreamsUriBuilder();
        URI uri = builder.buildId(baseUri, uriBuilder, t);
        
        return new StreamsEntry(StreamsEntry.params()
                .id(uri)
                .postedDate(dateTime)
                .applicationType(BAMBOO_APPLICATION_TYPE)
                .alternateLinkUri(builder.buildUri(baseUri, t))
                .categories(builder.buildCategory(t))
                .addLinks(builder.buildLinks(baseUri, t))
                .authors(builder.getAuthors(baseUri, t))
                .addActivityObject(builder.buildActivityObject(baseUri, t))
                .verb(builder.buildVerb(t))
                .target(builder.buildTarget(baseUri, t))
                .renderer(builder.getRenderer(baseUri, t))
                .baseUri(baseUri), i18nResolver);
    }
}
