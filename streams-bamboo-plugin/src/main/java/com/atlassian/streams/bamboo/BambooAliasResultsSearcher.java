package com.atlassian.streams.bamboo;

import static com.atlassian.bamboo.index.buildresult.BuildResultsSummaryDocument.FIELD_AUTHORS;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.search.Query;

import com.atlassian.bamboo.author.ExtendedAuthor;
import com.atlassian.bamboo.author.ExtendedAuthorManager;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.search.IndexedBuildResultsSearcher;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.bamboo.utils.collection.PartialList;
import com.atlassian.bonnie.LuceneUtils;
import com.atlassian.user.User;

import com.google.common.collect.ImmutableSet;

/**
 * Search for build results or repository aliases for a given username.
 *
 */

public class BambooAliasResultsSearcher
{
    private final IndexedBuildResultsSearcher indexedBuildResultsSearcher;
    private final ExtendedAuthorManager extendedAuthorManager;
    private final BambooUserManager userManager;

    public BambooAliasResultsSearcher(IndexedBuildResultsSearcher indexedBuildResultsSearcher,
        ExtendedAuthorManager extendedAuthorManager, BambooUserManager userManager)
    {
        super();
        this.indexedBuildResultsSearcher = checkNotNull(indexedBuildResultsSearcher, "indexedBuildResultsSearcher");
        this.extendedAuthorManager = checkNotNull(extendedAuthorManager, "extendedAuthorManager");
        this.userManager = checkNotNull(userManager, "userManager");
    }

    /**
     * Takes an iterable of usernames and retrieves all account aliases linked to those users. The list includes the username
     * for implicit mapping of usernames and repository aliases.
     * 
     * Then uses the list of aliases to construct a Lucene query to retrieve all relevant build results.
     * 
     * @param userNames
     * @param maxResults
     * @return
     */

    public PartialList<ResultsSummary> getResultsByUserNames(Iterable<String> userNames, int maxResults)
    {
        ImmutableSet<String> allLinkedAuthors = this.getLinkedAuthorsByUserNames(userNames);
        Query usersQuery = LuceneUtils.buildSingleFieldMultiValueTermQuery(FIELD_AUTHORS, allLinkedAuthors, false);
        // STRM-1537: This search method will return both plan and job summaries, but the provider currently only
        // reports job results
        return indexedBuildResultsSearcher.search(usersQuery, maxResults);
    }
    
    /**
     * Returns all the repository aliases linked to a given username. The list returned includes the username of the user for
     * implicit mapping.
     * 
     * @param userNames
     * @return
     */

    public ImmutableSet<String> getLinkedAuthorsByUserNames(Iterable<String> userNames)
    {
        ImmutableSet.Builder<String> builder = new ImmutableSet.Builder<String>();
        for (String name : userNames)
        {
            User user = this.userManager.getUser(name);
            builder.add(name);
            if (null != user)
            {
                List<ExtendedAuthor> authors = this.extendedAuthorManager.getLinkedAuthorForUser(user);
                for (ExtendedAuthor author : authors)
                {
                    builder.add(author.getName());
                }
            }
        }
        return builder.build();
    }
}
