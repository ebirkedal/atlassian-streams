package com.atlassian.streams.bamboo;

import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.ActivityVerbs.VerbFactory;

import static com.atlassian.streams.api.ActivityVerbs.ATLASSIAN_IRI_BASE;
import static com.atlassian.streams.api.ActivityVerbs.newVerbFactory;

public class BambooActivityVerbs
{
    public static final String BAMBOO_IRI_BASE = ATLASSIAN_IRI_BASE + "bamboo/"; 
    private static final VerbFactory bambooVerbs = newVerbFactory(BAMBOO_IRI_BASE);
    
    public static ActivityVerb fail()
    {
        return bambooVerbs.newVerb("fail");
    }
    
    public static ActivityVerb pass()
    {
        return bambooVerbs.newVerb("pass");
    }
}
