package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;

import static com.atlassian.streams.api.common.uri.Uris.encode;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.option;
import static com.google.common.base.Preconditions.checkNotNull;

public class BambooUserProfileAccessor implements UserProfileAccessor
{
    private final BambooUserManager userManager;
    private final ApplicationProperties applicationProperties;
    private final StreamsI18nResolver i18nResolver;

    public BambooUserProfileAccessor(final BambooUserManager userManager, final ApplicationProperties applicationProperties, final StreamsI18nResolver i18nResolver)
    {
        this.userManager = checkNotNull(userManager, "userManager");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.i18nResolver = i18nResolver;
    }

    private URI getUserProfileUri(final URI baseUri, final String username)
    {
        return URI.create(baseUri.toASCIIString() + "/browse/user/" + encode(username));
    }

    @Override
    public UserProfile getAnonymousUserProfile(final URI baseUri)
    {
        return new UserProfile.Builder(i18nResolver.getText("streams.bamboo.authors.unknown.username"))
                .fullName(i18nResolver.getText("streams.bamboo.authors.unknown.fullname"))
                .build();
    }

    @Override
    public UserProfile getUserProfile(final URI baseUri, String username)
    {
        if (username == null)
        {
            return getAnonymousUserProfile(baseUri);
        }

        final BambooUser user = userManager.getBambooUser(username);
        if (user != null)
        {
            return new UserProfile.Builder(username)
                .fullName(user.getFullName())
                .email(option(user.getEmail()))
                .profilePageUri(option(getUserProfileUri(baseUri, username)))
                .build();
        }
        else
        {
            return new UserProfile.Builder(username)
                .fullName(username)
                .build();
        }
    }

    @Override
    public UserProfile getUserProfile(final String username)
    {
        return getUserProfile(URI.create(applicationProperties.getBaseUrl()), username);
    }

    @Override
    public UserProfile getAnonymousUserProfile()
    {
        return getAnonymousUserProfile(URI.create(applicationProperties.getBaseUrl()));
    }

}
