package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.bamboo.builder.ResultsSummaryStreamsEntryBuilder.StreamsTriggerReason;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.net.URI;
import java.util.Map;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.renderer.Renderers.render;
import static com.google.common.base.Preconditions.checkNotNull;

public class BambooRendererFactory
{
    private final TemplateRenderer templateRenderer;
    private final StreamsEntryRendererFactory rendererFactory;
    private final UriProvider uriProvider;

    public BambooRendererFactory(final TemplateRenderer templateRenderer,
            final StreamsEntryRendererFactory rendererFactory,
            final UriProvider uriProvider)
    {
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
        this.rendererFactory = checkNotNull(rendererFactory, "rendererFactory");
        this.uriProvider = checkNotNull(uriProvider, "uriProvider");
    }

    public Renderer newRenderer(URI baseUri, ResultsSummary buildSummary, StreamsTriggerReason triggerReason)
    {
        return new ResultsSummaryRenderer(baseUri, buildSummary, triggerReason);
    }

    private class ResultsSummaryRenderer implements Renderer
    {
        private final ResultsSummary buildSummary;
        private final StreamsTriggerReason triggerReason;
        private final Function<StreamsEntry, Html> titleRenderer;
        private final URI baseUri;

        public ResultsSummaryRenderer(URI baseUri, ResultsSummary buildSummary, StreamsTriggerReason triggerReason)
        {
            this.buildSummary = checkNotNull(buildSummary, "buildSummary");
            this.triggerReason = checkNotNull(triggerReason, "triggerReason");
            this.baseUri = checkNotNull(baseUri, "baseUri");

            String key = "streams.item.bamboo.build." + (buildSummary.isSuccessful() ? "success" : "fail");
            titleRenderer = rendererFactory.newTitleRenderer(key);
        }

        @Override
        public Option<Html> renderContentAsHtml(StreamsEntry entry)
        {

            final Map<String, Object> context = Maps.newHashMap();

            context.put("entry", entry);
            context.put("buildSummary", buildSummary);
            context.put("summaryCommentsSize", buildSummary.getComments().size());
            context.put("authorsRenderer", rendererFactory.newAuthorsRenderer());
            context.put("trigger", triggerReason.toString().toLowerCase());
            context.put("durationDescriptionDefined", buildSummary.getDuration() > 0);
            context.put("buildSummaryUri", uriProvider.getBuildResultUri(this.baseUri, buildSummary));

            return some(new Html(render(templateRenderer, "build-content.vm", ImmutableMap.copyOf(context))));
        }

        @Override
        public Option<Html> renderSummaryAsHtml(StreamsEntry entry)
        {
            return none();
        }

        @Override
        public Html renderTitleAsHtml(StreamsEntry entry)
        {
            return titleRenderer.apply(entry);
        }
    }
}
