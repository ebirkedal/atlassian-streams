package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.streams.spi.StreamsKeyProvider.StreamsKey;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

public class ProjectKeys
{
    private final ProjectManager projectManager;

    public ProjectKeys(ProjectManager projectManager)
    {
        this.projectManager = checkNotNull(projectManager, "projectManager");
    }

    public Iterable<StreamsKey> get()
    {
        return ImmutableList.<StreamsKey>builder().addAll(projectKeys()).build();
    }
    
    private Iterable<StreamsKey> projectKeys()
    {
        return transform(projectManager.getAllProjects(), ToStreamsKey.INSTANCE);
    }

    enum ToStreamsKey implements Function<Project, StreamsKey>
    {
        INSTANCE;

        public StreamsKey apply(Project p)
        {
            return new StreamsKey(p.getKey(), p.getName());
        }
    }
}
