package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.streams.spi.StreamsValidator;

import static com.google.common.base.Preconditions.checkNotNull;

public class BambooStreamsValidator implements StreamsValidator
{
    private final ProjectManager projectManager;

    public BambooStreamsValidator(ProjectManager projectManager)
    {
        this.projectManager = checkNotNull(projectManager, "projectManager");
    }

    public boolean isValidKey(String key)
    {
        return projectManager.isExistingProjectKey(key);
    }
}
