package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.streams.api.StreamsEntry;

import java.net.URI;

public interface BambooEntryFactory
{
    /**
     * Convert the given build summary to an {@code Entry}
     *
     * @param baseUri the baseUri to use for links in the entries.
     * @param buildSummary the build summary to convert into an entry.
     * @return The converted entry.
     */
    StreamsEntry buildEntry(URI baseUri, ResultsSummary buildSummary);
}
