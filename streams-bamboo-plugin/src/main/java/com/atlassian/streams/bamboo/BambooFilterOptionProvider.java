package com.atlassian.streams.bamboo;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.ActivityOptions;
import com.atlassian.streams.spi.StreamsFilterOption;
import com.atlassian.streams.spi.StreamsFilterOptionProvider;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.bamboo.BambooActivityObjectTypes.job;
import static com.atlassian.streams.bamboo.BambooActivityVerbs.fail;
import static com.atlassian.streams.bamboo.BambooActivityVerbs.pass;
import static com.google.common.collect.Iterables.transform;

public class BambooFilterOptionProvider implements StreamsFilterOptionProvider
{
    public static final Iterable<Pair<ActivityObjectType, ActivityVerb>> activities = ImmutableList.of(
            pair(job(), pass()),
            pair(job(), fail()));
    
    private final Function<Pair<ActivityObjectType, ActivityVerb>, ActivityOption> toActivityOption;
    
    public BambooFilterOptionProvider(I18nResolver i18nResolver)
    {
        this.toActivityOption = ActivityOptions.toActivityOption(i18nResolver, "streams.filter.bamboo");
    }
    
    public Iterable<StreamsFilterOption> getFilterOptions()
    {
        return ImmutableList.of();
    }

    public Iterable<ActivityOption> getActivities()
    {
        return transform(activities, toActivityOption());
    }

    private Function<Pair<ActivityObjectType, ActivityVerb>, ActivityOption> toActivityOption()
    {
        return toActivityOption;
    }
}
