package com.atlassian.streams.bamboo;

import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;

import com.google.common.collect.ImmutableList;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.bamboo.BambooActivityObjectTypes.project;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.google.common.base.Preconditions.checkNotNull;

public class BambooEntityAssociationProvider implements StreamsEntityAssociationProvider
{
    private static final String PROJECT_URL_PREFIX = "/browse/";

    /**
     * Regex to parse project URIs.
     *
     * Group 1: project key, e.g. STRM
     * Group 2: request parameters and hash
     */
    private static final Pattern PROJECT_PATTERN = Pattern.compile("([^\\?#]+)(.*)");

    private final ApplicationProperties applicationProperties;
    private final BambooPermissionManager permissionManager;
    private final ProjectManager projectManager;
    
    public BambooEntityAssociationProvider(ApplicationProperties applicationProperties,
                                           BambooPermissionManager permissionManager,
                                           ProjectManager projectManager)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.permissionManager = checkNotNull(permissionManager, "permissionManager");
        this.projectManager = checkNotNull(projectManager, "projectManager");
    }

    @Override
    public Iterable<EntityIdentifier> getEntityIdentifiers(URI target)
    {
        String targetStr = target.toString();
        if (target.isAbsolute())
        {
            //the target URI must reference an entity on this server
            if (!targetStr.startsWith(applicationProperties.getBaseUrl() + PROJECT_URL_PREFIX))
            {
                return ImmutableList.of();
            }
            return matchEntities(targetStr.substring(applicationProperties.getBaseUrl().length() + PROJECT_URL_PREFIX.length()));
        }
        else
        {
            return matchEntities(targetStr);
        }
    }

    @Override
    public Option<URI> getEntityURI(EntityIdentifier identifier)
    {
        if (identifier.getType().equals(project().iri()))
        {
            return some(URI.create(applicationProperties.getBaseUrl() + PROJECT_URL_PREFIX + identifier.getValue()));
        }
        return none();
    }

    @Override
    public Option<String> getFilterKey(EntityIdentifier identifier)
    {
        if (identifier.getType().equals(project().iri()))
        {
            return some(PROJECT_KEY);
        }
        return none();
    }
    
    @Override
    public Option<Boolean> getCurrentUserViewPermission(EntityIdentifier identifier)
    {
        return getCurrentUserPermission(identifier, BambooPermission.READ);
    }

    @Override
    public Option<Boolean> getCurrentUserEditPermission(EntityIdentifier identifier)
    {
        return getCurrentUserPermission(identifier, BambooPermission.WRITE);
    }

    private Option<Boolean> getCurrentUserPermission(EntityIdentifier identifier, BambooPermission permission)
    {
        if (identifier.getType().equals(project().iri()))
        {
            final Project project = projectManager.getProjectByKey(identifier.getValue());
            if (project != null)
            {
                return some(permissionManager.hasPermission(permission, project, null));
            }
        }
        return none();
    }

    private Iterable<EntityIdentifier> matchEntities(String input)
    {
        Matcher matcher = PROJECT_PATTERN.matcher(input);
        //the target URI is of an unknown format
        if (!matcher.matches())
        {
            return ImmutableList.of();
        }

        String projectKey = matcher.group(1);
        Project project = projectManager.getProjectByKey(projectKey);
        //bamboo does not implement project-specific permissions
        if (project != null)
        {
            URI canonicalUri = URI.create(applicationProperties.getBaseUrl() + PROJECT_URL_PREFIX + projectKey);
            return ImmutableList.of(new EntityIdentifier(project().iri(),
                                                         projectKey,
                                                         canonicalUri));
        }
        else
        {
            return ImmutableList.of();
        }
    }
}