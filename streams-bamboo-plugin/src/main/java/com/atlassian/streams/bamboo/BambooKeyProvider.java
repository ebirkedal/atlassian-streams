package com.atlassian.streams.bamboo;

import com.atlassian.streams.spi.StreamsKeyProvider;

import static com.google.common.base.Preconditions.checkNotNull;

public class BambooKeyProvider implements StreamsKeyProvider
{
    private final ProjectKeys projectKeys;

    public BambooKeyProvider(ProjectKeys projectKeys)
    {
        this.projectKeys = checkNotNull(projectKeys, "projectKeys");
    }
    
    public Iterable<StreamsKey> getKeys()
    {
        return projectKeys.get();
    }
}
