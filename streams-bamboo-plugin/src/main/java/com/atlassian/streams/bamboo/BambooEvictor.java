package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.core.BambooObject;
import com.atlassian.bamboo.persistence3.PluginHibernateSessionFactory;
import com.atlassian.streams.spi.Evictor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Evicts {@link BambooObject} instances from Bamboo's {@link Session}.
 */
public class BambooEvictor implements Evictor<BambooObject>
{
    private static final Logger log = LoggerFactory.getLogger(BambooEvictor.class);
    private final PluginHibernateSessionFactory sessionFactory;

    public BambooEvictor(PluginHibernateSessionFactory sessionFactory)
    {
        this.sessionFactory = checkNotNull(sessionFactory, "sessionFactory");
    }

    /**
     * Evicts the given instance from the current {@link Session}.
     *
     * @param entity the instance to evict.
     */
    @Override
    public Void apply(BambooObject entity)
    {
        try
        {
            sessionFactory.getSession().evict(entity);
        }
        catch (HibernateException e)
        {
            log.warn("Failed to evict the entity from the session", e);
        }
        return null;
    }
}
