function JAtom(data) {
    this._parse(data);
};

JAtom.prototype = {
    
    _parse: function(data) {
    
        var thisFeed = this;
        var xml = thisFeed._getXml(data);
        var channel = jQuery('feed', xml).eq(0);
        this.version = '1.0';
        this.title = jQuery(channel).find('title:first').text();
        this.link = jQuery(channel).find('link:first').attr('href');
        this.description = jQuery(channel).find('subtitle:first').text();
        this.language = jQuery(channel).attr('xml:lang');
        this.updated = jQuery(channel).find('updated:first').text();
        
        this.items = new Array();
        
        var feed = this;
        
        jQuery('entry', xml).each( function() {
        
            var item = new JFeedItem();
            
            item.title = jQuery(this).find('title').eq(0).text();
            jQuery(this).find('link').each(function()
            {
                var rel = jQuery(this).attr("rel");
                var href = jQuery(this).attr("href");
                if (rel == "alternate")
                {
                    item.link = href;
                }
                else if (rel == "http://streams.atlassian.com/syndication/reply-to")
                {
                    item.replyTo = href;
                }
                else if (rel == "http://streams.atlassian.com/syndication/icon")
                {
                    item.icon = href;
                }
            });
            item.summary = jQuery(this).find('summary').eq(0).text();
            item.content = jQuery(this).find('content').eq(0).text();
            item.updated = jQuery(this).find('updated').eq(0).text();
            item.id = jQuery(this).find('id').eq(0).text();
            item.category = jQuery(this).find('category:first').attr("term");
            jQuery(this).find("author").each( function() {
                item.authors.push(thisFeed._parsePerson(this));
            });
            feed.items.push(item);
        });
    },
    _parsePerson: function(xml)
    {
        var person = new JFeedPerson();
        person.name = jQuery(xml).find('name').eq(0).text();
        person.email = jQuery(xml).find('email').eq(0).text();
        person.uri = jQuery(xml).find('uri').eq(0).text();
        person.username = jQuery(xml).find('username').eq(0).text();
        return person;
    },
    _getContentValue: function(content, name)
    {
        var element = content.find(name + ":first");
        if (element)
        {
            return element.text();
        }
        else
        {
            return "";
        }
    },
    _getXml: function(input) {
        if (typeof input == "string") {
            try {
                // We ask IE to return a string to get around it not liking the 'application/atom+xml' MIME type
                var xml = new ActiveXObject("Microsoft.XMLDOM");
                xml.async = false;
                xml.loadXML(input);
                return xml;
            } catch (e) {
                AJS.log('Failed to create an xml object from string: ' + e);
                return input;
            }
        } else {
            // Other browsers will already return an xml object
            return input;
        }
    }
};

function JFeedItem() {
    this.authors = new Array();
};
JFeedItem.prototype = {
    title: '',
    link: '',
    summary: '',
    content: '',
    updated: '',
    id: '',
    category: '',
    inReplyTo: '',
    replyTo: '',
    icon: ''
};

function JFeedPerson() {};
JFeedPerson.prototype = {
    name: '',
    email: '',
    uri: '',
    username: ''
};