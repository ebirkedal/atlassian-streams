package com.atlassian.streams.bamboo;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.atlassian.bamboo.author.Author;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.core.BambooObject;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.utils.collection.PartialList;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.HasAlternateLinkUri;
import com.atlassian.streams.api.StreamsEntry.HasApplicationType;
import com.atlassian.streams.api.StreamsEntry.HasAuthors;
import com.atlassian.streams.api.StreamsEntry.HasId;
import com.atlassian.streams.api.StreamsEntry.HasPostedDate;
import com.atlassian.streams.api.StreamsEntry.HasRenderer;
import com.atlassian.streams.api.StreamsEntry.HasVerb;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.Evictor;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;

import org.hamcrest.Matcher;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooStreamsActivityProviderTest
{
    @Mock BambooEntryFactory entryFactory;
    @Mock ResultsSummaryManager resultsSummaryManager;
    @Mock ProjectManager projectManager;
    @Mock ActivityRequest activityRequest;
    @Mock Multimap<String, Pair<Operator, Iterable<String>>> providerFilters;
    @Mock Multimap<String, Pair<Operator, Iterable<String>>> standardFilters;
    @Mock Project project;
    @Mock I18nResolver i18nResolver;
    @Mock Evictor<BambooObject> bambooEvictor;
    @Mock BambooAliasResultsSearcher authorSearch;
    BambooStreamsActivityProvider streamsActivityProvider;
    List<String> userNames = Collections.singletonList("username");
    ImmutableSet<String> aliases = ImmutableSet.of("alias");

    @Before
    public void createStreamsActivityProvider()
    {
        prepareActivityAndHttpRequest();
        prepareProject();
        prepareI18nResolver();
        prepareAuthorSearch();
        prepareEntryFactory();
        streamsActivityProvider = new BambooStreamsActivityProvider(resultsSummaryManager,
                                                                    projectManager,
                                                                    entryFactory,
                                                                    authorSearch,
                                                                    i18nResolver,
                                                                    bambooEvictor);
    }

    public void prepareEntryFactory()
    {
        @SuppressWarnings("rawtypes") StreamsEntry.Parameters params = newEntryParams();
        @SuppressWarnings("unchecked") StreamsEntry entry = new StreamsEntry(params, i18nResolver);
        when(entryFactory.buildEntry((URI) any(), (ResultsSummary) any())).thenReturn(entry);
    }

    public void prepareActivityAndHttpRequest()
    {
        when(activityRequest.getMaxResults()).thenReturn(100);
        when(activityRequest.getProviderFilters()).thenReturn(providerFilters);
        when(activityRequest.getStandardFilters()).thenReturn(standardFilters);
    }

    public void prepareAuthorOnlyRequest()
    {
        when(standardFilters.size()).thenReturn(1);
        when(providerFilters.isEmpty()).thenReturn(true);

        Collection<Pair<Operator, Iterable<String>>> authorFilters = new ArrayList<Pair<Operator, Iterable<String>>>();
        authorFilters.add(Pair.pair(Operator.IS, (Iterable<String>) userNames));
        when(standardFilters.get(USER.getKey())).thenReturn(authorFilters);
    }

    public void prepareProject()
    {
        when(projectManager.getProjectByKey("STRM")).thenReturn(project);
        when(project.getName()).thenReturn("Streams");
    }

    public void prepareI18nResolver()
    {
        when(i18nResolver.getText("streams.bamboo.emptyprojectlist.name")).thenReturn("all projects");
        when(i18nResolver.getText("streams.bamboo.conjunction.and")).thenReturn("and");
        when(i18nResolver.getText("streams.bamboo.title", "Streams")).thenReturn("Activity Stream for Streams");
        when(i18nResolver.getText("streams.bamboo.title", "all projects")).thenReturn("Activity Stream for all projects");
        when(i18nResolver.getText("streams.bamboo.subtitle")).thenReturn("Activity for Bamboo");
    }

    public void prepareAuthorSearch()
    {
        List<ResultsSummary> innerResults = new ArrayList<ResultsSummary>();
        innerResults.add(getResultsSummaryWithAuthor("username"));
        innerResults.add(getResultsSummaryWithAuthor("alias"));
        PartialList<ResultsSummary> results = new PartialList<ResultsSummary>(2, innerResults);
        when(authorSearch.getResultsByUserNames(argThat(containsString("username")), anyInt())).thenReturn(results);
        when(authorSearch.getLinkedAuthorsByUserNames(argThat(containsString("username")))).thenReturn((ImmutableSet<String>) aliases);
    }

    private StreamsEntry.Parameters<HasId, HasPostedDate, HasAlternateLinkUri, HasApplicationType, HasRenderer, HasVerb, HasAuthors> newEntryParams()
    {
        return StreamsEntry.params()
                           .id(URI.create("http://example.com"))
                           .postedDate(new DateTime())
                           .alternateLinkUri(URI.create("http://example.com"))
                           .applicationType("test")
                           .authors(ImmutableNonEmptyList.of(new UserProfile.Builder("someone").fullName("Some One")
                                                                                               .build()))
                           .addActivityObject(new ActivityObject(ActivityObject.params()
                                                                               .id("activity")
                                                                               .title(some("Some activity"))
                                                                               .alternateLinkUri(URI.create("http://example.com"))
                                                                               .activityObjectType(comment())))
                           .verb(post()).renderer(newRenderer("title", "content"))
                           .baseUri(URI.create("http://example.com"));
    }

    private Renderer newRenderer(String title, String content)
    {
        return newRenderer(title, some(content), none(String.class));
    }

    private Renderer newRenderer(String title, Option<String> content, Option<String> summary)
    {
        Renderer renderer = mock(Renderer.class);
        when(renderer.renderTitleAsHtml(any(StreamsEntry.class))).thenReturn(new Html(title));
        when(renderer.renderContentAsHtml(any(StreamsEntry.class))).thenReturn(content.map(html()));
        when(renderer.renderSummaryAsHtml(any(StreamsEntry.class))).thenReturn(summary.map(html()));
        return renderer;
    }

    private ResultsSummary getResultsSummaryWithAuthor(String authorName)
    {
        Author author = mock(Author.class);
        when(author.getName()).thenReturn(authorName);
        ResultsSummary results = mock(ResultsSummary.class);
        when(results.getUniqueAuthors()).thenReturn(ImmutableSet.of(author));
        Job job = mock(Job.class);
        when(results.getPlan()).thenReturn(job);
        when(results.getBuildKey()).thenReturn("BUILD-KEY");
        when(results.getChangesListSummary()).thenReturn("Bug fixing");
        return results;
    }

    private Matcher<Iterable<String>> containsString(final String value)
    {
        return new ArgumentMatcher<Iterable<String>>()
        {

            @Override
            public boolean matches(Object argument)
            {
                @SuppressWarnings("unchecked") Iterable<String> arg = (Iterable<String>) argument;
                for (String val : arg)
                {
                    if (val.equals(value))
                    {
                        return true;
                    }
                }
                return false;
            }

        };
    }

    @Test
    public void verifyThatSearchIsCalledWithUserName() throws Exception
    {
        prepareAuthorOnlyRequest();

        streamsActivityProvider.getActivityFeed(activityRequest).call();
        verify(authorSearch, atLeastOnce()).getResultsByUserNames(argThat(containsString("username")), eq(100));
    }

    @Test
    public void assertThatSearchByAuthorsReturnsEvents() throws Exception
    {

        prepareAuthorOnlyRequest();

        CancellableTask<StreamsFeed> task = streamsActivityProvider.getActivityFeed(activityRequest);
        StreamsFeed feed = task.call();
        Iterable<StreamsEntry> entries = feed.getEntries();
        assertEquals(2, Iterables.size(entries));
    }

    @Test(expected = CancelledException.class)
    public void assertThatCancelledTaskThrowsExeption() throws Exception
    {
        CancellableTask<StreamsFeed> task = streamsActivityProvider.getActivityFeed(activityRequest);
        task.cancel();
        task.call();
    }

    @Test
    public void verifyThatActivityFeedFindsBuildByProjectIfProjectKeysAreNotEmpty() throws Exception
    {
        Iterable<String> filterValues = ImmutableList.of("STRM");
        Collection<Pair<Operator, Iterable<String>>> filterPairs = ImmutableList.of(pair(IS, filterValues));
        when(standardFilters.get(PROJECT_KEY)).thenReturn(filterPairs);

        streamsActivityProvider.getActivityFeed(activityRequest).call();
        verify(projectManager, atLeastOnce()).getProjectByKey("STRM");
    }

    @Test
    public void assertThatFeedTitleIsCorrectWhenNoProjectIsDefined() throws Exception
    {
        StreamsFeed feed = streamsActivityProvider.getActivityFeed(activityRequest).call();
        assertThat(feed.getTitle(), is(equalTo("Activity Stream for all projects")));
    }

    @Test
    public void assertThatFeedTitleIsCorrectWhenAProjectIsDefined() throws Exception
    {
        Iterable<String> filterValues = ImmutableList.of("STRM");
        Collection<Pair<Operator, Iterable<String>>> filterPairs = ImmutableList.of(pair(IS, filterValues));
        when(standardFilters.get(PROJECT_KEY)).thenReturn(filterPairs);

        StreamsFeed feed = streamsActivityProvider.getActivityFeed(activityRequest).call();
        assertThat(feed.getTitle(), is(equalTo("Activity Stream for Streams")));
    }

    @Test
    public void assertThatFeedSubtitleIsCorrect() throws Exception
    {
        StreamsFeed feed = streamsActivityProvider.getActivityFeed(activityRequest).call();
        assertThat(feed.getSubtitle(), is(equalTo(some("Activity for Bamboo"))));
    }
}
