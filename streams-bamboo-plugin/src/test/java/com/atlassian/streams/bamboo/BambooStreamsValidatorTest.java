package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.user.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooStreamsValidatorTest
{
    @Mock ProjectManager projectManager;
    @Mock BambooUserManager userManager;
    @Mock User user;

    BambooStreamsValidator streamsValidator;

    @Before
    public void createStreamsValidator()
    {
        streamsValidator = new BambooStreamsValidator(projectManager);
    }

    @Test
    public void assertThatNonExistentProjectReturnsIsValidKeyFalse()
    {
        when(projectManager.isExistingProjectKey(anyString())).thenReturn(false);
        assertFalse(streamsValidator.isValidKey("nonexistentproject"));
    }

    @Test
    public void assertThatExistentProjectReturnsIsValidKeyTrue()
    {
        when(projectManager.isExistingProjectKey("myproject")).thenReturn(true);
        assertTrue(streamsValidator.isValidKey("myproject"));
    }
}
