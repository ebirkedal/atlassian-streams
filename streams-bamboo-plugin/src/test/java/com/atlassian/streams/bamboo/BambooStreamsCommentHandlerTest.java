package com.atlassian.streams.bamboo;

import java.net.URI;

import com.atlassian.bamboo.comment.CommentManager;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.spi.StreamsCommentHandler;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNAUTHORIZED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooStreamsCommentHandlerTest
{
    private static final String BUILD_RESULT_KEY = "ONE-ONE10-1";

    private static final String USERNAME = "user";
    public static final URI BASE_URI = URI.create("http://localhost/streams");

    @Mock
    ResultsSummaryManager resultsSummaryManager;
    @Mock CommentManager commentManager;
    @Mock BambooUserManager bambooUserManager;
    @Mock UserManager userManager;
    @Mock ApplicationProperties applicationProperties;
    @Mock BambooUser bambooUser;
    @Mock ResultsSummary resultsSummary;

    PlanResultKey planResultKey;
    BambooStreamsCommentHandler commentHandler;

    @Before
    public void createBambooStreamsCommentHandler()
    {
        planResultKey = PlanKeys.getPlanResultKey(BUILD_RESULT_KEY);
        when(applicationProperties.getBaseUrl()).thenReturn("http://localhost/streams");
        when(userManager.getRemoteUsername()).thenReturn(USERNAME);
        when(bambooUserManager.getBambooUser(USERNAME)).thenReturn(bambooUser);
        when(resultsSummaryManager.getResultsSummary(planResultKey)).thenReturn(resultsSummary);

        commentHandler = new BambooStreamsCommentHandler(resultsSummaryManager, commentManager, bambooUserManager,
            userManager, applicationProperties);
    }

    @Test
    public void assertThatCommentHandlerReturnsExpectedUri()
    {
        Either<StreamsCommentHandler.PostReplyError, URI> postReplyResult = commentHandler.postReply(BASE_URI,
                ImmutableList.of(BUILD_RESULT_KEY), "comment");
        assertTrue(postReplyResult.isRight());
        assertThat(postReplyResult.right().get().toASCIIString(), is(equalTo("http://localhost/streams/browse/ONE-ONE10-1")));
    }

    @Test
    public void assertThatInvalidUserReturnsPostReplyError()
    {
        when(userManager.getRemoteUsername()).thenReturn("invaliduser");
        Either<StreamsCommentHandler.PostReplyError, URI> postReplyResult = commentHandler.postReply(BASE_URI,
                ImmutableList.of(BUILD_RESULT_KEY), "comment");
        assertTrue(postReplyResult.isLeft());
        assertThat(postReplyResult.left().get().getType(), is(equalTo(UNAUTHORIZED)));
    }

    @Test
    public void assertThatNoLoggedInUserReturnsPostReplyError()
    {
        when(userManager.getRemoteUsername()).thenReturn(null);
        Either<StreamsCommentHandler.PostReplyError, URI> postReplyResult = commentHandler.postReply(BASE_URI,
                ImmutableList.of(BUILD_RESULT_KEY), "comment");
        assertTrue(postReplyResult.isLeft());
        assertThat(postReplyResult.left().get().getType(), is(equalTo(UNAUTHORIZED)));
    }
}
