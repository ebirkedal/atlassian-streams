package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.testing.AbstractUserProfileAccessorTestSuite;
import com.atlassian.user.EntityException;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooUserProfileAccessorTest extends AbstractUserProfileAccessorTestSuite
{
    @Mock BambooUserManager userManager;
    @Mock StreamsI18nResolver i18nResolver;

    @Before
    public void createUserProfileBuilder()
    {
        userProfileAccessor = new BambooUserProfileAccessor(userManager, getApplicationProperties(), i18nResolver);
    }

    @Before
    public void prepareUserManager() throws EntityException
    {
        BambooUser user = mock(BambooUser.class);
        when(user.getName()).thenReturn("user");
        when(user.getFullName()).thenReturn("User");
        when(user.getEmail()).thenReturn("u@c.com");
        when(userManager.getBambooUser("user")).thenReturn(user);

        BambooUser userWithSpaceInUsername = mock(BambooUser.class);
        when(userWithSpaceInUsername.getName()).thenReturn("user 2");
        when(userWithSpaceInUsername.getFullName()).thenReturn("User 2");
        when(userWithSpaceInUsername.getEmail()).thenReturn("u2@c.com");
        when(userManager.getBambooUser("user 2")).thenReturn(userWithSpaceInUsername);

        BambooUser userWithFunnyCharactersInName = mock(BambooUser.class);
        when(userWithFunnyCharactersInName.getName()).thenReturn("user3");
        when(userWithFunnyCharactersInName.getFullName()).thenReturn("User <3&'>");
        when(userWithFunnyCharactersInName.getEmail()).thenReturn("u3@c.com");
        when(userManager.getBambooUser("user3")).thenReturn(userWithFunnyCharactersInName);
    }

    @Override
    protected String getProfilePathTemplate()
    {
        return "/browse/user/{username}";
    }

    @Override
    protected String getProfilePicturePathTemplate()
    {
        return null;
    }
}
