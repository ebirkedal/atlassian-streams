package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.author.ExtendedAuthor;
import com.atlassian.bamboo.author.ExtendedAuthorManager;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.search.IndexedBuildResultsSearcher;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.bamboo.utils.collection.PartialList;
import com.atlassian.user.User;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.apache.lucene.search.Query;
import org.hamcrest.Matcher;
import org.hamcrest.object.HasToString;
import org.junit.Before;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooAliasResultsSearcherTest
{
    BambooAliasResultsSearcher authorSearch;
    @Mock ExtendedAuthorManager extendedAuthorManager;
    @Mock IndexedBuildResultsSearcher indexedBuildResultsSearcher;
    @Mock BambooUserManager userManager;
    @Mock User user;

    @Before
    public void createAuthorSearch()
    {
        prepareExtendedAuthorManager();
        prepareIndexedBuildResultsSearcher();
        prepareUserManager();
        this.authorSearch = new BambooAliasResultsSearcher(indexedBuildResultsSearcher,
                                                           extendedAuthorManager,
                                                           userManager);
    }

    private void prepareExtendedAuthorManager()
    {
        ExtendedAuthor extAuthor = mock(ExtendedAuthor.class);
        when(extAuthor.getName()).thenReturn("alias");
        when(extendedAuthorManager.getLinkedAuthorForUser(user)).thenReturn(ImmutableList.of(extAuthor));
    }

    private void prepareIndexedBuildResultsSearcher()
    {
        ImmutableList<ResultsSummary> resultsInner = ImmutableList.of();
    	PartialList<ResultsSummary> results= new PartialList<ResultsSummary>(0,resultsInner);
        when(indexedBuildResultsSearcher.search((Query) any(), anyInt())).thenReturn(results);
    }

    private void prepareUserManager()
    {
        when(userManager.getUser("username")).thenReturn(user);
    }

    @Test
    public void verifyThatUserNameIsInQuery()
    {
        authorSearch.getResultsByUserNames(ImmutableSet.of("username"), 15);
        verify(indexedBuildResultsSearcher, atLeastOnce()).search(argThat(containsString("username")), anyInt());
    }

    @Test
    public void verifyThatAliasesAreInQuery()
    {
        authorSearch.getResultsByUserNames(ImmutableSet.of("username"), 15);
        verify(indexedBuildResultsSearcher, atLeastOnce()).search(argThat(containsString("alias")), anyInt());
    }

    @Test
    public void verifyThatMaxResultsArePassed()
    {
        authorSearch.getResultsByUserNames(ImmutableSet.of("username"), 15);
        verify(indexedBuildResultsSearcher, atLeastOnce()).search((Query) any(), eq(15));
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static Matcher<Query> containsString(String s)
    {
        return new HasToString(JUnitMatchers.containsString(s));
    }
}
