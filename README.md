# Activity Streams

## Description

An activity stream is a list of events, displayed to a user in an attractive and interactive
interface. They are usually recent activities by the current user and other people using the same
social site(s) or system(s).

An example is the [Activity Stream
gadget](https://confluence.atlassian.com/display/JIRA/Adding+the+Activity+Stream+Gadget) used on
JIRA dashboards.

## Atlassian Developer?

### Internal Documentation

[Development and maintenance documentation](https://ecosystem.atlassian.net/wiki/display/STRM/Activity+Streams+Developers+Guide)

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/STRM)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/STRM).

### Documentation

[Activity Streams Documentation](https://developer.atlassian.com/display/DOCS/Activity+Streams)

