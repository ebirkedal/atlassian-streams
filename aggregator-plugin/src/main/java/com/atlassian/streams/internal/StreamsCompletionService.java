package com.atlassian.streams.internal;

import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.Nullable;

import com.atlassian.failurecache.failures.ExponentialBackOffFailureCache;
import com.atlassian.failurecache.failures.FailureCache;
import com.atlassian.failurecache.util.date.Clock;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.internal.ActivityProvider.Error;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.util.concurrent.ResettableLazyReference;
import com.atlassian.util.concurrent.Timeout;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import static com.atlassian.streams.api.common.Either.getLefts;
import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.internal.ActivityProvider.Error.other;
import static com.atlassian.streams.internal.ActivityProvider.Error.timeout;
import static com.atlassian.util.concurrent.Assertions.notNull;
import static com.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static com.atlassian.util.concurrent.Timeout.getNanosTimeout;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Suppliers.memoize;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;

/**
 * A service which allows multiple {@link Callable jobs} to be submitted and returns an {@link Iterable} over the return
 * values.  The jobs are run in a {@link ExecutorService pool} in the background.  When iterating over the
 * return values, the first value available will be returned.  If no values are available, the thread will block until
 * a value becomes available.
 */
public final class StreamsCompletionService implements InitializingBean, DisposableBean
{
    private static final Logger logger = LoggerFactory.getLogger(StreamsCompletionService.class);

    // Ignore failure cache results in dev mode with this property set. Used for diagnosis of failing individual streams.
    private static final boolean IGNORE_FAILURE_CACHE = Sys.inDevMode() && Boolean.getBoolean("com.atlassian.streams.aggregator.ignore.failure.cache");

    private final ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory;
    private final PluginEventManager pluginEventManager;
    private final ResettableLazyReference<Execution> async = new ResettableLazyReference<Execution>()
    {
        @Override
        protected Execution create() throws Exception
        {
            return new Execution(threadLocalDelegateExecutorFactory, failureCache);
        }
    };
    private final FailureCache<ActivityProvider> failureCache;

    StreamsCompletionService(final ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory,
                             final PluginEventManager pluginEventManager, final Clock clock)
    {
        this.threadLocalDelegateExecutorFactory = checkNotNull(threadLocalDelegateExecutorFactory, "threadLocalDelegateExecutorFactory");
        this.pluginEventManager = checkNotNull(pluginEventManager, "pluginEventManager");
        this.failureCache = new ExponentialBackOffFailureCache<ActivityProvider>(clock);
    }

    public Predicate<ActivityProvider> reachable()
    {
        return not(new Predicate<ActivityProvider>()
        {
            public boolean apply(ActivityProvider activityProvider)
            {
                return !IGNORE_FAILURE_CACHE && failureCache.isFailing(activityProvider);
            }
        });
    }

    /**
     * Given some {@link Callable jobs} execute them in parallel and return an Iterable that
     * will block on the first call to {@link Iterator#next()} and return the first available result.
     * <p>
     * Note that the client may block forever if the job becomes stuck (there is no timeout support).
     * Use the overloaded method if timeout support is desired.
     *
     * @param <T> the result type
     * @param callables jobs that return a pair of the original ActivityProvider and the results
     * @return an {@link Iterable} of the results, may block on the first calls to {@link Iterator#next()} to await results becoming available.
     */
    public <T> Iterable<Either<Error, T>> execute(final Iterable<? extends ActivityProviderCallable<Either<Error, T>>> callables)
    {
        if (isEmpty(callables))
        {
            return ImmutableSet.of();
        }

        Iterable<Either<Error, T>> results = async.get().invokeAll(callables);

        registerFailures(results);

        return results;
    }

    /**
     * Given some {@link Callable jobs} execute them in parallel and return an Iterable that
     * will block on the first call to {@link Iterator#next()} and return the first available result.
     *
     * @param <T> the result type
     * @param callables jobs that return a pair of the original ActivityProvider and the results
     * @param time the maximum amount of time to allow all {@link Callable}s to complete
     * @param unit the unit of time in which to measure
     * @return an {@link Iterable} of the results, may block on the first calls to {@link Iterator#next()} to await results becoming available.
     */
    public <T> Iterable<Either<Error, T>> execute(final Iterable<? extends ActivityProviderCallable<Either<Error, T>>> callables, final long time, final TimeUnit unit)
    {
        if (isEmpty(callables))
        {
            return ImmutableSet.of();
        }

        Iterable<Either<Error, T>> results = async.get().invokeAll(callables, time, unit);

        registerFailures(results);

        return results;
    }

    private <T> void registerFailures(final Iterable<Either<Error, T>> results)
    {
        for (Error error : getLefts(results))
        {
            // Don't log failure cache errors for credentials required or unauthorized - these are per-user errors.
            if (error.getActivityProvider().isDefined() && isNotUserSpecificError(error))
            {
                logger.warn("Registering failure for stream provider {} due to error {}", error.getActivityProvider().get().getName(), error);
                failureCache.registerFailure(error.getActivityProvider().get());
            }
        }
    }

    private boolean isNotUserSpecificError(final Error errors)
    {
        final Error.Type type = errors.getType();
        return !(type.equals(Error.Type.CREDENTIALS_REQUIRED) || type.equals(Error.Type.UNAUTHORIZED));
    }

    private void resetCompletionService()
    {
        if (async.isInitialized())
        {
            async.get().close();
        }
        async.reset();
    }

    public synchronized void afterPropertiesSet()
    {
        resetCompletionService();
        pluginEventManager.register(this);
    }

    public synchronized void destroy()
    {
        resetCompletionService();
        pluginEventManager.unregister(this);
    }

    @PluginEventListener
    public void onShutdown(final PluginFrameworkShutdownEvent event)
    {
        // Because disabled() is only called when the module is disabled by a user, and not when the whole plugin
        // system goes down (on JIRA data import for example), this event listener method is needed to shut the
        // executor down to prevent thread and memory leaks.
        resetCompletionService();
    }

    /**
     * Has a cached thread pool executor service with a thread factory that
     * names threads StreamsCompletionService:n as well as a completer for running the jobs
     */
    static class Execution
    {
        private static final int MAX_POOL_SIZE =  Integer.getInteger("streams.completion.service.pool.max", 32);

        private final ExecutorService executorService;
        private final Completer completer;
        private final FailureCache failureCache;

        Execution(final ThreadLocalDelegateExecutorFactory factory, final FailureCache failureCache)
        {
            this.failureCache = failureCache;
            executorService = factory.createExecutorService(
                    newLimitedCachedThreadPool(namedThreadFactory("StreamsCompletionService:"), MAX_POOL_SIZE));
            completer =
                    new Completer(executorService, cancellingCompletionServiceFactory(failureCache));

        }

        private ExecutorService newLimitedCachedThreadPool(final ThreadFactory threadFactory, final int limit)
        {
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(limit, limit,
                    60L, TimeUnit.SECONDS,
                    new LinkedBlockingQueue<Runnable>(),
                    threadFactory);
            threadPoolExecutor.allowCoreThreadTimeOut(true);
            return threadPoolExecutor;
        }

        <T> Iterable<Either<Error, T>> invokeAll(final Iterable<? extends ActivityProviderCallable<Either<Error, T>>> jobs)
        {
            return completer.invokeAll(jobs);
        }

        <T> Iterable<Either<Error, T>> invokeAll(final Iterable<? extends ActivityProviderCallable<Either<Error, T>>> jobs, long time, TimeUnit unit)
        {
            return completer.invokeAll(jobs, time, unit);
        }

        void close()
        {
            executorService.shutdownNow();
        }

        private static Completer.ExecutorCompletionServiceFactory cancellingCompletionServiceFactory(FailureCache failureCache)
        {
            return new CancellingCompletionServiceFactory(failureCache);
        }

        private static class CancellingCompletionServiceFactory implements Completer.ExecutorCompletionServiceFactory
        {
            private final FailureCache failureCache;

            private CancellingCompletionServiceFactory(final FailureCache failureCache) {
                this.failureCache = failureCache;
            }

            @Override
            public <T> Function<Executor, CompletionService<Either<Error, T>>> create()
            {
                return new Function<Executor, CompletionService<Either<Error, T>>>()
                {
                    @Override
                    public CompletionService<Either<Error, T>> apply(Executor e)
                    {
                        return new CancellingCompletionService<Either<Error, T>>(e, failureCache);
                    }
                };
            }
        }

        private static final class CancellingCompletionService<T> implements CompletionService<T>
        {
            private final CompletionService<T> delegate;
            private final FailureCache failureCache;
            private final ConcurrentHashMap<Future<T>, Future<T>> originalFutureToWrappedFuture;

            CancellingCompletionService(Executor executor, FailureCache failureCache)
            {
                this.delegate = new ExecutorCompletionService<T>(executor);
                this.failureCache = failureCache;
                this.originalFutureToWrappedFuture = new ConcurrentHashMap<Future<T>, Future<T>>();
            }

            @Override
            public Future<T> poll()
            {
                return wrappedFuture(delegate.poll());
            }

            @Override
            public Future<T> poll(long timeout, TimeUnit unit) throws InterruptedException
            {
                return wrappedFuture(delegate.poll(timeout, unit));
            }

            @Override
            public Future<T> submit(final Callable<T> task)
            {
                logger.debug("Submitting task stream provider {}", ((ActivityProviderCallable) task).getActivityProvider().getName());

                final Future<T> f = delegate.submit(task);
                Future<T> wrappedFuture = new Future<T>()
                {
                    volatile boolean cancelled = false;

                    @Override
                    public boolean cancel(boolean mayInterrupt)
                    {
                        if (task instanceof CancellableTask)
                        {
                            CancellableTask.Result r = ((CancellableTask<T>) task).cancel();
                            switch (r)
                            {
                                case CANCELLED:
                                    cancelled = true;
                                    break;
                                case CANNOT_CANCEL:
                                    cancelled = false;
                                    break;
                                case INTERRUPT:
                                    cancelled = f.cancel(mayInterrupt);
                                    break;
                                default:
                                    throw new IllegalStateException("Unknown result type '" + r + "' returned from CancellableTask.cancel");
                            }
                        }
                        else
                        {
                            cancelled = f.cancel(mayInterrupt);
                        }

                        logger.warn("Registering failure for stream provider {} due to cancellation (timeout)", ((ActivityProviderCallable) task).getActivityProvider().getName());
                        failureCache.registerFailure(((ActivityProviderCallable) task).getActivityProvider());

                        originalFutureToWrappedFuture.remove(f);

                        return cancelled;
                    }

                    @Override
                    public T get() throws InterruptedException, ExecutionException
                    {
                        logger.debug("Attempting get from stream provider {}", ((ActivityProviderCallable) task).getActivityProvider().getName());
                        T value = f.get();
                        failureCache.registerSuccess(((ActivityProviderCallable) task).getActivityProvider());
                        return value;
                    }

                    @Override
                    public T get(long t, TimeUnit u) throws InterruptedException, ExecutionException, TimeoutException
                    {
                        logger.debug("Attempting get from stream provider {} with timeout {} {}", new Object[] { ((ActivityProviderCallable) task).getActivityProvider().getName(), String.valueOf(t), u.toString() });
                        T value = f.get(t, u);
                        failureCache.registerSuccess(((ActivityProviderCallable) task).getActivityProvider());
                        return value;
                    }

                    @Override
                    public boolean isCancelled()
                    {
                        return cancelled;
                    }

                    @Override
                    public boolean isDone()
                    {
                        return cancelled || f.isDone();
                    }
                };

                originalFutureToWrappedFuture.put(f, wrappedFuture);
                return wrappedFuture;
            }

            @Override
            public Future<T> submit(Runnable task, T result)
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public Future<T> take() throws InterruptedException
            {
                return wrappedFuture(delegate.take());
            }

            private Future<T> wrappedFuture(Future<T> future)
            {
                if (future == null) {
                    return future;
                }
                return originalFutureToWrappedFuture.remove(future);
            }
        }
    }

    private static final class Completer
    {
        private final Executor executor;
        private final ExecutorCompletionServiceFactory completionServiceFactory;

        Completer(final Executor executor, final ExecutorCompletionServiceFactory completionServiceFactory)
        {
            this.executor = notNull("executor", executor);
            this.completionServiceFactory = notNull("completionServiceFactory", completionServiceFactory);
        }

        public <T> Iterable<Either<Error, T>> invokeAll(final Iterable<? extends ActivityProviderCallable<Either<Error, T>>> callables, final long time, final TimeUnit unit)
        {
            return invokeAll(callables, getNanosTimeout(time, unit));
        }

        public <T> Iterable<Either<Error, T>> invokeAll(Iterable<? extends ActivityProviderCallable<Either<Error, T>>> jobs)
        {
            return invokeAll(jobs, null);
        }

        private <T> Iterable<Either<Error, T>> invokeAll(Iterable<? extends ActivityProviderCallable<Either<Error, T>>> callables, @Nullable Timeout nanosTimeout)
        {
            // we must copy the resulting Iterable<Supplier> so
            // each iteration doesn't resubmit the jobs
            final Iterable<Supplier<Either<Error, T>>> lazyAsyncSuppliers = copyOf(transform(callables, new CompletionFunction<T>(completionServiceFactory.<T>create().apply(executor), nanosTimeout)));
            return transform(lazyAsyncSuppliers, Completer.<Either<Error, T>>fromSupplier());
        }

        /**
         * Extension point if a custom CompletionService is required
         */
        public interface ExecutorCompletionServiceFactory
        {
            <T> Function<Executor, CompletionService<Either<Error, T>>> create();
        }

        private static class CompletionFunction<T>
                implements Function<ActivityProviderCallable<Either<Error, T>>, Supplier<Either<Error, T>>>
        {
            private final CompletionService<Either<Error, T>> completionService;

            private Timeout nanosTimeout;

            CompletionFunction(final CompletionService<Either<Error, T>> completionService, @Nullable Timeout nanosTimeout)
            {
                this.completionService = completionService;
                this.nanosTimeout = nanosTimeout;
            }

            public Supplier<Either<Error, T>> apply(final ActivityProviderCallable<Either<Error, T>> task)
            {
                final Future<Either<Error, T>> future = completionService.submit(task);
                return memoize(new Supplier<Either<Error, T>>()
                {
                    @Override
                    public Either<Error, T> get()
                    {

                        try
                        {
                            if (nanosTimeout == null)
                            {
                                return future.get();
                            }
                            else
                            {
                                return future.get(nanosTimeout.getTime(), nanosTimeout.getUnit());
                            }
                        }
                        catch (InterruptedException e)
                        {
                            logger.debug("Handling a non-timeout exception", e);
                            return left(timeout(task.getActivityProvider()));
                        }
                        catch (ExecutionException e)
                        {
                            logger.debug("Handling a non-timeout exception", e);
                            return left(other(task.getActivityProvider()));
                        }
                        catch (TimeoutException e)
                        {
                            logger.debug("Handling a timeout", e.getMessage());
                            //This is a special case were we timed out waiting on task but it is still running
                            // so we have to cancel it
                            future.cancel(true);
                            return left(timeout(task.getActivityProvider()));
                        }
                    }
                });
            }
        }

        static <T> Function<Supplier<? extends T>, T> fromSupplier()
        {
            return new Function<Supplier<? extends T>, T>()
            {
                public T apply(final Supplier<? extends T> supplier)
                {
                    return supplier.get();
                }
            };
        }
    }
}
