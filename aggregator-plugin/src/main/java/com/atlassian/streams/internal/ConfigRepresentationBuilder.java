package com.atlassian.streams.internal;

import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.internal.ActivityProvider.Error;
import com.atlassian.streams.internal.rest.representations.ConfigPreferencesRepresentation;
import com.atlassian.streams.internal.rest.representations.FilterOptionRepresentation;
import com.atlassian.streams.internal.rest.representations.ProviderFilterRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsKeysRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsKeysRepresentation.StreamsKeyEntry;
import com.atlassian.streams.spi.FormatPreferenceProvider;
import com.atlassian.streams.spi.StandardStreamsFilterOption;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import static com.atlassian.streams.api.common.Either.getRights;
import static com.atlassian.streams.internal.ActivityProviders.localOnly;
import static com.atlassian.streams.internal.rest.representations.FilterOptionRepresentation.toFilterOptionEntry;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.STANDARD_FILTERS_PROVIDER_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.projectKeys;
import static com.google.common.base.Functions.compose;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Maps.transformValues;
import static com.google.common.collect.Multimaps.index;
import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.leftPad;

public final class ConfigRepresentationBuilder
{
    private final ActivityProviders activityProviders;
    private final StreamsCompletionService completionService;
    private final I18nResolver i18nResolver;
    private final ApplicationProperties applicationProperties;
    private final FormatPreferenceProvider preferenceProvider;

    ConfigRepresentationBuilder(ActivityProviders activityProviders,
            StreamsCompletionService completionService,
            @Qualifier("streamsI18nResolver") I18nResolver i18nResolver,
            ApplicationProperties applicationProperties,
            FormatPreferenceProvider formatPreferenceProvider)
    {
        this.activityProviders = checkNotNull(activityProviders, "activityProviders");
        this.completionService = checkNotNull(completionService, "completionService");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.preferenceProvider = checkNotNull(formatPreferenceProvider, "preferenceProvider");
    }

    public StreamsConfigRepresentation getConfigRepresentation(boolean local)
    {
        Iterable<ActivityProvider> localProviders = activityProviders.get(localOnly(true), completionService.reachable());
        Iterable<ActivityProvider> applicationProviders = activityProviders.get(localOnly(local), completionService.reachable());
        Map<String, Integer> providerCount = getApplicationTypeCount(concat(localProviders, applicationProviders));

        return new StreamsConfigRepresentation(
            ImmutableList.<ProviderFilterRepresentation>builder()
                .add(getStandardFilterOptions(localProviders, applicationProviders))
                .addAll(ProviderFilterOrdering.prioritizing(transform(localProviders, getName())).sortedCopy(
                    getProviderFilters(applicationProviders, providerCount)))
                .build());
    }

    private Map<String, Integer> getApplicationTypeCount(Iterable<ActivityProvider> providers)
    {
        Map<String, Integer> providerMap = new HashMap<String, Integer>();
        for (ActivityProvider provider : providers)
        {
            int count = 1;
            if (providerMap.containsKey(provider.getType()))
            {
                count = providerMap.get(provider.getType()) + 1;
            }
            providerMap.put(provider.getType(), count);
        }

        return ImmutableMap.copyOf(providerMap);
    }

    public ConfigPreferencesRepresentation getConfigPreferencesRepresentation()
    {
        return new ConfigPreferencesRepresentation(preferenceProvider.getDateFormatPreference(),
                preferenceProvider.getTimeFormatPreference(),
                preferenceProvider.getDateTimeFormatPreference(),
                getUtcOffsetString(preferenceProvider.getUserTimeZone()),
                preferenceProvider.getDateRelativizePreference());
    }

    private String getUtcOffsetString(DateTimeZone timeZone)
    {
        int offset = timeZone.getOffset(new Date().getTime());
        int hour = Math.abs(offset / (60 * 60 * 1000));
        int minute = Math.abs(offset / (60*1000)) % 60;

        return new StringBuilder()
            .append(offset > 0 ? "+" : "-")
            .append(leftPad(String.valueOf(hour), 2, "0"))
            .append(leftPad(String.valueOf(minute), 2, "0"))
            .toString();
    }

    private Function<ActivityProvider, String> getName()
    {
        return GetName.INSTANCE;
    }

    private enum GetName implements Function<ActivityProvider, String>
    {
        INSTANCE;
        public String apply(ActivityProvider ap)
        {
            return ap.getName();
        }
    }

    private ProviderFilterRepresentation getStandardFilterOptions(Iterable<ActivityProvider> providers, Iterable<ActivityProvider> applicationProviders)
    {
        ByFilterConditions byFilterConditions = new ByFilterConditions(Iterables.concat(providers, applicationProviders));
        return new ProviderFilterRepresentation(
            STANDARD_FILTERS_PROVIDER_KEY,
            "",
            "",
            ImmutableList.<FilterOptionRepresentation>builder()
                .addAll(transform(filter(asList(StandardStreamsFilterOption.values()), byFilterConditions), toFilterOptionEntry(i18nResolver)))
                .add(newProjectOptionEntry(providers))
                .build(),
            null);
    }

    /**
     * {@code Predicate} to allow for excluding specific filter options from appearing in specific products
     */
    private class ByFilterConditions implements Predicate<StandardStreamsFilterOption>
    {
        boolean containsJira = false;

        public ByFilterConditions(Iterable<ActivityProvider> providers)
        {
            for (ActivityProvider provider : providers)
            {
                if (provider instanceof AppLinksActivityProvider)
                {
                    AppLinksActivityProvider applinksProvider = (AppLinksActivityProvider)provider;
                    if (applinksProvider.getApplink().getType() instanceof JiraApplicationType)
                    {
                        containsJira = true;
                        break;
                    }
                }
                else
                {
                    if (applicationProperties.getDisplayName().toLowerCase().equals("jira"))
                    {
                        containsJira = true;
                        break;
                    }
                }
            }
        }

        public boolean apply(StandardStreamsFilterOption filterOption)
        {
            // only show the JIRA issue key option if JIRA is one of the providers
            return !filterOption.equals(StandardStreamsFilterOption.ISSUE_KEY) || containsJira;
        }
    }

    private FilterOptionRepresentation newProjectOptionEntry(Iterable<ActivityProvider> providers)
    {
        return new FilterOptionRepresentation(i18nResolver,
            projectKeys(transformValues(index(concat(
                transform(providers, compose(extractKeyEntries, getRepresentation))), extractKey).asMap(),
                                        compose(extractLabel, firstKeyEntry)),
                        applicationProperties.getDisplayName()));
    }

    private static final Function<ActivityProvider, StreamsKeysRepresentation> getRepresentation = new Function<ActivityProvider, StreamsKeysRepresentation>()
    {
        public StreamsKeysRepresentation apply(ActivityProvider provider)
        {
            return provider.getKeys();
        }
    };

    private static final Function<StreamsKeysRepresentation, Iterable<StreamsKeyEntry>> extractKeyEntries = new Function<StreamsKeysRepresentation, Iterable<StreamsKeyEntry>>()
    {
        public Iterable<StreamsKeyEntry> apply(StreamsKeysRepresentation keysRep)
        {
            return keysRep.getKeys();
        }
    };

    private static final Function<Iterable<StreamsKeyEntry>, StreamsKeyEntry> firstKeyEntry = new Function<Iterable<StreamsKeyEntry>, StreamsKeyEntry>()
    {
        public StreamsKeyEntry apply(Iterable<StreamsKeyEntry> keyEntries)
        {
            return get(keyEntries, 0);
        }
    };

    private static final Function<StreamsKeyEntry, String> extractKey = new Function<StreamsKeyEntry, String>()
    {
        public String apply(StreamsKeyEntry keyEntry)
        {
            return keyEntry.getKey();
        }
    };

    private static final Function<StreamsKeyEntry, String> extractLabel = new Function<StreamsKeyEntry, String>()
    {
        public String apply(StreamsKeyEntry keyEntry)
        {
            return keyEntry.getLabel();
        }
    };

    private Collection<ProviderFilterRepresentation> getProviderFilters(Iterable<ActivityProvider> providers,
            Map<String, Integer> providerCount)
    {
        Iterable<ActivityProviderCallable<Either<Error, Iterable<ProviderFilterRepresentation>>>> callables = transform(providers, toFiltersCallable(providerCount));
        //ignore any timeouts or other exceptions
        return ImmutableList.copyOf(concat(getRights(completionService.execute(callables))));
    }

    private final Function<ActivityProvider, ActivityProviderCallable<Either<Error, Iterable<ProviderFilterRepresentation>>>> toFiltersCallable(final Map<String, Integer> providerCount)
    {
        return new Function<ActivityProvider, ActivityProviderCallable<Either<Error, Iterable<ProviderFilterRepresentation>>>>()
        {
            public ActivityProviderCallable<Either<Error, Iterable<ProviderFilterRepresentation>>> apply(final ActivityProvider provider)
            {
                return new ActivityProviderCallable<Either<Error, Iterable<ProviderFilterRepresentation>>>()
                {
                    public Either<Error, Iterable<ProviderFilterRepresentation>> call() throws Exception
                    {
                        boolean addApplinkName = providerCount.containsKey(provider.getType()) && providerCount.get(provider.getType()) > 1;
                        return provider.getFilters(addApplinkName);
                    }

                    @Override
                    public ActivityProvider getActivityProvider()
                    {
                        return provider;
                    }
                };
            }
        };
    }
}
