package com.atlassian.streams.internal;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.streams.spi.ActivityProviderModuleDescriptor;
import com.atlassian.streams.spi.SessionManager;
import com.atlassian.streams.spi.StreamsI18nResolver;

import com.google.common.base.Function;
import com.google.common.base.Supplier;

import org.springframework.beans.factory.annotation.Qualifier;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

class LocalActivityProviders implements Supplier<Iterable<ActivityProvider>>
{
    private final PluginAccessor pluginAccessor;
    private final StreamsI18nResolver i18nResolver;
    private final TransactionTemplate transactionTemplate;
    private final SessionManager sessionManager;
    private final ApplicationProperties applicationProperties;
    
    public LocalActivityProviders(PluginAccessor pluginAccessor,
            StreamsI18nResolver i18nResolver,
            @Qualifier("sessionManager") SessionManager sessionManager,
            TransactionTemplate transactionTemplate,
            ApplicationProperties applicationProperties)
    {
        this.pluginAccessor = checkNotNull(pluginAccessor, "pluginAccessor");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.sessionManager = checkNotNull(sessionManager, "sessionManager");
        this.transactionTemplate = checkNotNull(transactionTemplate, "transactionTemplate");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    public Iterable<ActivityProvider> get()
    {
        return transform(
            pluginAccessor.getEnabledModuleDescriptorsByClass(ActivityProviderModuleDescriptor.class),
            toActivityProvider()
        );
    }

    private Function<ActivityProviderModuleDescriptor, ActivityProvider> toActivityProvider()
    {
        return toActivityProviderFunction;
    }

    private final Function<ActivityProviderModuleDescriptor, ActivityProvider> toActivityProviderFunction = new Function<ActivityProviderModuleDescriptor, ActivityProvider>()
    {
        public ActivityProvider apply(ActivityProviderModuleDescriptor descriptor)
        {
            return new LocalActivityProvider(descriptor, sessionManager, transactionTemplate, i18nResolver, applicationProperties);
        }
    };
}
