package com.atlassian.streams.internal.atom.abdera;

import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;

import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.internal.atom.abdera.StreamsAbdera.AtomParsedFeedEntry;
import com.atlassian.streams.internal.atom.abdera.StreamsAbdera.AtomParsedFeedHeader;
import com.atlassian.streams.internal.feed.FeedEntry;
import com.atlassian.streams.internal.feed.FeedHeader;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.internal.feed.FeedParser;

import com.google.common.base.Function;

import org.apache.abdera.Abdera;
import org.apache.abdera.model.Element;
import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.apache.abdera.parser.Parser;
import org.joda.time.DateTime;

import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.google.common.collect.Iterables.transform;

/**
 * Implementation of {@link FeedParser} that parses an Atom XML feed using Abdera.
 * It does not fully translate entries, since currently they are always output in the same
 * format; instead it just wraps the internal Abdera entry objects which will then be
 * reused by {@link AbderaAtomFeedRenderer}.
 * 
 * If we move away from using this format for internal communication and only want to
 * be able to generate Atom output for external readers, this class can be removed.
 */
public class AbderaAtomFeedParser implements FeedParser
{
    private final Abdera abdera = StreamsAbdera.getAbdera();
    
    @Override
    public FeedModel readFeed(Reader reader) throws IOException, ParseException
    {
        final Parser parser = abdera.getParser();
        
        // Parses using UTF-8 by default
        Feed parsedFeed;
        try
        {
            parsedFeed = parser.<Feed>parse(reader).getRoot();
        }
        catch (org.apache.abdera.parser.ParseException e)
        {
            throw new ParseException(e.getMessage(), 0);
        }
        
        Uri feedUri;
        if (parsedFeed.getSelfLink() != null)
        {
            feedUri = Uri.parse(parsedFeed.getSelfLink().getHref().toASCIIString());
        }
        else
        {
            feedUri = Uri.parse(parsedFeed.getId().toASCIIString());
        }
        
        FeedModel.Builder builder = FeedModel.builder(feedUri)
                .title(option(parsedFeed.getTitle()))
                .subtitle(option(parsedFeed.getSubtitle()))
                .addEntries(transform(parsedFeed.getEntries(), abderaEntryToFeedEntry))
                .addHeaders(transform(parsedFeed.getExtensions(), abderaExtensionToFeedHeader))
                .encodedContent(parsedFeed);
        
        if (parsedFeed.getUpdated() != null)
        {
            builder.updated(some(new DateTime(parsedFeed.getUpdated())));
        }
        
        return builder.build();
    }

    private Function<Entry, FeedEntry> abderaEntryToFeedEntry = new Function<Entry, FeedEntry>()
    {
        public FeedEntry apply(Entry from)
        {
            return new AtomParsedFeedEntry(from);
        }  
    };
    
    private Function<Element, FeedHeader> abderaExtensionToFeedHeader = new Function<Element, FeedHeader>()
    {
        public FeedHeader apply(Element from)
        {
            return new AtomParsedFeedHeader(from);
        }
    };
    
}
