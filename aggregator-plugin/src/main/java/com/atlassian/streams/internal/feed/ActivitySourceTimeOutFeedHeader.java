package com.atlassian.streams.internal.feed;


import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nonnull;

/**
 * A {@link FeedHeader} that indicates which sources have timed out.
 * A link is also provided to reload the feed with a larger timeout.
 */
public class ActivitySourceTimeOutFeedHeader implements FeedHeader
{
    private final String sourceName;

    public ActivitySourceTimeOutFeedHeader(@Nonnull String sourceName)
    {
        this.sourceName = checkNotNull(sourceName);
    }

    @Nonnull public String getSourceName()
    {
        return sourceName;
    }
}
