package com.atlassian.streams.internal.rest.representations;

import java.util.Collection;
import java.util.Map;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.spi.StreamsFilterOption;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.google.common.collect.Collections2.transform;
import static org.apache.commons.lang.StringUtils.isNotBlank;

public class FilterOptionRepresentation
{
    @JsonProperty private final String key;
    @JsonProperty private final String helpText;
    @JsonProperty private final String name;
    @JsonProperty private final Collection<FilterOptionOperatorRepresentation> operators;
    @JsonProperty private final String type;
    @JsonProperty private final boolean unique;
    @JsonProperty private final Map<String, String> values;

    @JsonCreator
    public FilterOptionRepresentation(
            @JsonProperty("key") String key,
            @JsonProperty("helpText") String helpText,
            @JsonProperty("name") String name,
            @JsonProperty("operators") Collection<FilterOptionOperatorRepresentation> operators,
            @JsonProperty("type") String type,
            @JsonProperty("unique") boolean unique,
            @JsonProperty("values") Map<String, String> values)
    {
        this.key = key;
        this.helpText = helpText;
        this.name = name;
        this.operators = ImmutableList.copyOf(operators);
        this.type = type;
        this.unique = unique;
        this.values = ImmutableMap.copyOf(values);
    }

    public FilterOptionRepresentation(I18nResolver i18nResolver, StreamsFilterOption filterOption)
    {
        this.key = filterOption.getKey();
        if (isNotBlank(filterOption.getHelpTextI18nKey()))
        {
            this.helpText = i18nResolver.getText(filterOption.getHelpTextI18nKey());
        }
        else
        {
            this.helpText = null;
        }
        if (isNotBlank(filterOption.getI18nKey()))
        {
            this.name = i18nResolver.getText(filterOption.getI18nKey());
        }
        else
        {
            this.name = filterOption.getDisplayName();
        }
        this.operators = transform(ImmutableList.copyOf(filterOption.getFilterType().getOperators()), toOperatorRepresentation(i18nResolver));
        this.type = filterOption.getFilterType().getType();
        this.unique = filterOption.isUnique();
        if (filterOption.getValues() != null)
        {
            this.values = ImmutableMap.copyOf(filterOption.getValues());
        }
        else
        {
            this.values = null;
        }
    }

    public String getKey()
    {
        return key;
    }

    public String getHelpText()
    {
        return helpText;
    }

    public String getName()
    {
        return name;
    }

    public Collection<FilterOptionOperatorRepresentation> getOperators()
    {
        return operators;
    }

    public String getType()
    {
        return type;
    }

    public boolean isUnique()
    {
        return unique;
    }

    public Map<String, String> getValues()
    {
        return values;
    }
    
    public String toString()
    {
        return getKey();
    }

    enum ToKey implements Function<Operator, String>
    {
        INSTANCE;

        public String apply(Operator operator)
        {
            return operator.getKey();
        }
    }

    static Function<Operator, FilterOptionOperatorRepresentation> toOperatorRepresentation(final I18nResolver i18nResolver)
    {
        return new Function<Operator, FilterOptionOperatorRepresentation>()
        {
            public FilterOptionOperatorRepresentation apply(Operator operator)
            {
                return new FilterOptionOperatorRepresentation(operator.getKey(), i18nResolver.getText(operator.getI18nKey()));
            }
        };
    }

    public static Function<StreamsFilterOption, FilterOptionRepresentation> toFilterOptionEntry(final I18nResolver i18nResolver)
    {
        return new Function<StreamsFilterOption, FilterOptionRepresentation>()
        {
            public FilterOptionRepresentation apply(StreamsFilterOption filterOption)
            {
                return new FilterOptionRepresentation(i18nResolver, filterOption);
            }
        };
    }

    public static class FilterOptionOperatorRepresentation
    {
        @JsonProperty private final String key;
        @JsonProperty private final String name;

        @JsonCreator
        public FilterOptionOperatorRepresentation(
                @JsonProperty("key") String key,
                @JsonProperty("name") String name)
        {
            this.key = key;
            this.name = name;
        }

        public String getKey()
        {
            return key;
        }

        public String getName()
        {
            return name;
        }
    }
}
