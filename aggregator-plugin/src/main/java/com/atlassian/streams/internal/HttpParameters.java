package com.atlassian.streams.internal;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.api.common.Fold;
import com.atlassian.streams.api.common.Function2;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Functions.parseInt;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.internal.ActivityProviders.matches;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.STANDARD_FILTERS_PROVIDER_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.UPDATE_DATE;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.transform;

/**
 * Simple class to encapsulate HTTP parameters. This allows for convenient
 * access to http parameters without resorting to parsing low level data
 * structures.
 *
 * @since v5.3.12
 */
public final class HttpParameters
{
    static final String LOCAL_KEY = new String("local");
    public static final String RELATIVE_LINKS_KEY = new String("relativeLinks");
    public static final String TIMEOUT_TEST = new String("timeout.test");
    public static final String PARAM_TITLE = new String("title");
    public static final String PARAM_MODULE = new String("module");
    public static final String MAX_RESULTS = new String("maxResults");

    private static final String LEGACY_MIN_DATE = new String("minDate");
    private static final String LEGACY_MAX_DATE = new String("maxDate");
    private static final String LEGACY_ITEM_KEY = new String("itemKey");

    @VisibleForTesting
    static final String LEGACY_FILTER = new String("filter");

    @VisibleForTesting
    static final String LEGACY_AUTHOR = new String("filterUser");

    private final ImmutableMultimap<String, String> map;

    public static HttpParameters parameters(final HttpServletRequest request)
    {
        return new HttpParameters(buildParams(request));
    }

    static ImmutableMultimap<String, String> buildParams(final HttpServletRequest request)
    {
        ImmutableMultimap.Builder<String, String> builder = ImmutableMultimap.builder();
        for (Map.Entry<String, String[]> entry : getParameterMap(request).entrySet())
        {
            builder.putAll(entry.getKey(), entry.getValue());
        }
        return builder.build();
    }

    @VisibleForTesting
    public static HttpParameters parameters(Multimap<String, String> params)
    {
        ImmutableMultimap.Builder<String, String> builder = ImmutableMultimap.builder();
        ImmutableMultimap<String, String> immutable = builder.putAll(params).build();
        return new HttpParameters(immutable);
    }

    @SuppressWarnings("unchecked")
    private static Map<String, String[]> getParameterMap(HttpServletRequest request)
    {
        return request.getParameterMap();
    }

    private HttpParameters(ImmutableMultimap<String, String> map)
    {
        this.map = map;
    }

    public boolean useAcceptLanguage()
    {
        return parseBooleanParameter(map, ActivityRequest.USE_ACCEPT_LANG_KEY, false);
    }

    public Predicate<ActivityProvider> fetchLocalOnly()
    {
        return ActivityProviders.localOnly(parseBooleanParameter(map, LOCAL_KEY, false));
    }

    private static boolean parseBooleanParameter(Multimap<String, String> parameters, String key, boolean defVal)
    {
        return parameters.containsKey(key) ? Boolean.parseBoolean(get(parameters.get(key), 0)) : defVal;
    }

    public Option<String> getTitle()
    {
        return Option.option(map.containsKey(PARAM_TITLE) ? get(map.get(PARAM_TITLE), 0) : null);
    }

    @VisibleForTesting
    static Iterable<String> getSelectedProviders(Multimap<String, String> parameters)
    {
        return Arrays.asList(get(parameters.get(ActivityRequest.PROVIDERS_KEY), 0).split(" "));
    }

    public Predicate<ActivityProvider> isSelectedProvider()
    {
        if (!map.containsKey(ActivityRequest.PROVIDERS_KEY))
        {
            return Predicates.alwaysTrue();
        }
        else
        {
            return ActivityProviders.selectedProvider(getSelectedProviders(map));
        }
    }

    public Predicate<ActivityProvider> module()
    {
        if (!map.containsKey(PARAM_MODULE))
        {
            return Predicates.<ActivityProvider>alwaysTrue();
        }
        else
        {
            return ActivityProviders.module(get(map.get(PARAM_MODULE), 0));
        }
    }

    public boolean isTimeoutTest()
    {
        return map.containsEntry(TIMEOUT_TEST, "true");
    }

    public int parseMaxResults(int defaultValue)
    {
        return parseParamAsInt(MAX_RESULTS, map).getOrElse(defaultValue);
    }

    static private Option<Integer> parseParamAsInt(String parameter, Multimap<String, String> parameters)
    {
        return parameters.containsKey(parameter) ?
                parseInt().apply(get(parameters.get(parameter), 0)).right().toOption() :
                none(Integer.class);
    }

    private static Option<String> getProviderKey(Multimap<String, String> parameters, ActivityProvider provider)
    {
        try
        {
            return some(find(parameters.entries(),
                    com.atlassian.streams.internal.Predicates.
                            <String, String>whereMapEntryKey(matches(provider))).getKey());
        }
        catch (NoSuchElementException e)
        {
            return none();
        }
    }

    public Option<String> getProviderKey(final ActivityProvider provider)
    {
        return getProviderKey(map, provider);
    }


    private static Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> parseStandardFilters(Multimap<String, String> parameters)
    {
        Iterable<String> standardFilters = parameters.get(STANDARD_FILTERS_PROVIDER_KEY);
        return ImmutableMultimap.<String, Pair<StreamsFilterType.Operator, Iterable<String>>>builder().
                putAll(getLegacyParametersAsFilters(parameters)).
                putAll(parseFilters(standardFilters)).
                build();
    }

    private static Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> parseFilters(Iterable<String> filters)
    {
        return Fold.foldl(filters, ImmutableMultimap.<String, Pair<StreamsFilterType.Operator, Iterable<String>>>builder(), FilterParser.INSTANCE).build();
    }

    public Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> parseStandardFilters()
    {
        return parseStandardFilters(map);
    }

    /**
     * Calculates the appropriate context URL to use based on the given HTTP Parameters.
     * @param applicationProperties The application properties to get reasonable defaults.
     * @param contextUri The contextUri from the request.
     * @return the appropriate URI to use as a base for all URLs returned by the request.
     */
    public URI calculateContextUrl(final ApplicationProperties applicationProperties, final String contextUri)
    {
        if (parseBooleanParameter(map, RELATIVE_LINKS_KEY, false) && contextUri != null)
        {
            return URI.create(contextUri);
        }
        else
        {
            // Use full canonical base URL unless we use relative links.
            return URI.create(applicationProperties.getBaseUrl());
        }
    }

    public Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>>
    getProviderFilter(final Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> providerFilters,
            final ActivityProvider provider)
    {
        final Option<String> providerKey = getProviderKey(provider);
        return providerKey.fold(
                new Supplier<Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>>>()
                {
                    @Override
                    public Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> get()
                    {
                        return providerFilters;
                    }
                },
                new Function<String, Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>>>()
                {
                    @Override
                    public Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>>
                    apply(@Nullable final String s)
                    {
                        return parseFilters(map.get(s));
                    }
                });
    }

    public Collection<String> getProviders()
    {
        return map.get(ActivityRequest.PROVIDERS_KEY);
    }

    private enum FilterParser implements Function2<String, ImmutableMultimap.Builder<String, Pair<StreamsFilterType.Operator, Iterable<String>>>, ImmutableMultimap.Builder<String, Pair<StreamsFilterType.Operator, Iterable<String>>>>
    {
        INSTANCE;

        public ImmutableMultimap.Builder<String, Pair<StreamsFilterType.Operator, Iterable<String>>> apply(String filter,
                ImmutableMultimap.Builder<String, Pair<StreamsFilterType.Operator, Iterable<String>>> builder)
        {
            String[] keyOpValues = filter.split(" ", 3);
            if (keyOpValues.length == 3)
            {
                String key = keyOpValues[0];
                StreamsFilterType.Operator op = StreamsFilterType.Operator.valueOf(keyOpValues[1]);
                Iterable<String> values = ImmutableList.copyOf(keyOpValues[2].split(" "));

                return builder.put(key, pair(op, transform(values, unescapeValue)));
            }

            return builder;
        }
    }

    private static final Function<String, String> unescapeValue = new Function<String, String>()
    {
        public String apply(String from)
        {
            // this is the reverse of the escaping defined in handleSpecialCases() in activity-streams-parent.js
            return from.replaceAll("(?<!\\\\)\\_", " ").replace("\\_", "_");
        }
    };

    private static Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> getLegacyParametersAsFilters(Multimap<String, String> parameters)
    {
        return ImmutableMultimap.<String, Pair<StreamsFilterType.Operator, Iterable<String>>>builder().
                putAll(getLegacyParameterAsFilter(LEGACY_AUTHOR, USER.getKey(), IS, parameters)).
                putAll(getLegacyParameterAsFilter(PROJECT_KEY, PROJECT_KEY, IS, parameters)).
                putAll(getLegacyIssueKeyParametersAsFilter(parameters)).
                putAll(getLegacyDateParametersAsFilter(parameters)).
                build();
    }

    private static Multimap<? extends String, ? extends Pair<StreamsFilterType.Operator, Iterable<String>>> getLegacyIssueKeyParametersAsFilter(
            Multimap<String, String> parameters)
    {
        if (parameters.containsKey(LEGACY_FILTER))
        {
            return ImmutableMultimap.of(ISSUE_KEY.getKey(), Pair.<StreamsFilterType.Operator, Iterable<String>>pair(IS, parameters.get(LEGACY_FILTER)));
        }
        if (parameters.containsKey(LEGACY_ITEM_KEY))
        {
            return ImmutableMultimap.of(ISSUE_KEY.getKey(), Pair.<StreamsFilterType.Operator, Iterable<String>>pair(IS, parameters.get(LEGACY_ITEM_KEY)));
        }
        return ImmutableMultimap.of();
    }

    private static Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> getLegacyParameterAsFilter(String legacyKey,
            String newKey,
            StreamsFilterType.Operator op,
            Multimap<String, String> parameters)
    {
        if (parameters.containsKey(legacyKey))
        {
            return ImmutableMultimap.of(newKey, Pair.<StreamsFilterType.Operator, Iterable<String>>pair(op, parameters.get(legacyKey)));
        }
        else
        {
            return ImmutableMultimap.of();
        }
    }

    private static Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> getLegacyDateParametersAsFilter(Multimap<String, String> parameters)
    {
        ImmutableMultimap.Builder<String, Pair<StreamsFilterType.Operator, Iterable<String>>> builder = ImmutableMultimap.builder();
        if (parameters.containsKey(LEGACY_MIN_DATE))
        {
            builder.put(UPDATE_DATE.getKey(), Pair.<StreamsFilterType.Operator, Iterable<String>>pair(AFTER, parameters.get(LEGACY_MIN_DATE)));
        }
        if (parameters.containsKey(LEGACY_MAX_DATE))
        {
            builder.put(UPDATE_DATE.getKey(), Pair.<StreamsFilterType.Operator, Iterable<String>>pair(BEFORE, parameters.get(LEGACY_MAX_DATE)));
        }
        return builder.build();
    }
}
