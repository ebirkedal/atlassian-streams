package com.atlassian.streams.internal.rest.resources;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.internal.ProjectKeyValidator;
import com.atlassian.streams.internal.rest.representations.ValidationErrorCollectionRepresentation;
import com.atlassian.streams.internal.rest.representations.ValidationErrorCollectionRepresentation.ValidationErrorEntry;
import com.atlassian.streams.spi.StreamsKeyProvider;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

import static com.atlassian.streams.internal.rest.MediaTypes.STREAMS_JSON;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Collections2.filter;
import static com.google.common.collect.Iterables.isEmpty;
import static java.util.Arrays.asList;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang.StringUtils.isNotEmpty;
import static org.apache.commons.lang.math.NumberUtils.isNumber;

@Path("/validate")
@AnonymousAllowed
public class StreamsValidationResource
{
    private static final CacheControl NO_CACHE = new CacheControl();

    static
    {
        NO_CACHE.setNoStore(true);
        NO_CACHE.setNoCache(true);
    }

    private static final String PREF_TITLE = "title";
    private static final String PREF_KEYS = "keys";
    private static final String PREF_USERNAME = "username";
    private static final String PREF_NUMOFENTRIES = "numofentries";
    
    private final ProjectKeyValidator validator;
    private final UserManager userManager;

    public StreamsValidationResource(ProjectKeyValidator validator, UserManager userManager)
    {
        this.validator = checkNotNull(validator, "validator");
        this.userManager = checkNotNull(userManager, "userManager");
    }

    /**
     * Ensures all parameters are valid
     */
    @GET
    @Produces(STREAMS_JSON)
    public Response validate(@QueryParam(PREF_TITLE) String title,
            @QueryParam(PREF_KEYS) String keys,
            @QueryParam(PREF_USERNAME) String usernames,
            @QueryParam(PREF_NUMOFENTRIES) String numOfEntriesString,
            @QueryParam("local") boolean local)
    {
        ImmutableList.Builder<ValidationErrorEntry> errorBuilder = ImmutableList.builder();

        if (isBlank(title))
        {
            errorBuilder.add(new ValidationErrorEntry(PREF_TITLE, "gadget.activity.stream.error.pref.title"));
        }

        //validate only if we don't select 'all projects'.  If all projects is selected, the rest of the projects
        //will be ignored anyway.
        if (isNotBlank(keys) && !keys.contains(StreamsKeyProvider.ALL_PROJECTS_KEY) && hasInvalidKey(keys, local))
        {
            errorBuilder.add(new ValidationErrorEntry(PREF_KEYS, "gadget.activity.stream.error.pref.keys"));
        }

        if (isNotEmpty(usernames))
        {
            Collection<String> invalidUsernames = filter(asList(usernames.split(",")), not(validUsername));

            if (!isEmpty(invalidUsernames))
            {
                errorBuilder.add(new ValidationErrorEntry(PREF_USERNAME, "gadget.activity.stream.error.pref.username",
                        invalidUsernames));
            }
        }

        if (isBlank(numOfEntriesString))
        {
            errorBuilder.add(new ValidationErrorEntry(PREF_NUMOFENTRIES, "gadget.activity.stream.error.pref.numofentries.required"));
        }
        else if (!isValidNumber(numOfEntriesString))
        {
            errorBuilder.add(new ValidationErrorEntry(PREF_NUMOFENTRIES, "gadget.activity.stream.error.pref.numofentries.number"));
        }

        final Collection<ValidationErrorEntry> errorCollection = errorBuilder.build();

        if (isEmpty(errorCollection))
        {
            return Response.ok().cacheControl(NO_CACHE).build();
        }
        else
        {
            return Response.status(SC_BAD_REQUEST)
                    .entity(new ValidationErrorCollectionRepresentation(errorCollection))
                    .cacheControl(NO_CACHE)
                    .build();
        }
    }

    private final Predicate<String> validUsername = new Predicate<String>()
    {
        public boolean apply(String username)
        {
            return isValidUsername(username);
        }
    };

    private boolean hasInvalidKey(String keys, boolean local)
    {
        return !validator.allKeysAreValid(asList(keys.split(",")), local);
    }

    private boolean isValidUsername(String username)
    {
        return userManager.resolve(username) != null;
    }

    private boolean isValidNumber(String number)
    {
        if (isNumber(number))
        {
            final Long numberOfEntries = Long.valueOf(number);
            return (numberOfEntries > 0 && numberOfEntries <= 100);
        }
        return false;
    }
}
