package com.atlassian.streams.internal.atom.abdera;

import java.net.URI;

import javax.xml.namespace.QName;

import org.apache.abdera.factory.Factory;
import org.apache.abdera.model.Element;
import org.apache.abdera.model.ExtensibleElementWrapper;

import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_APPLICATION_ID;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_APPLICATION_NAME;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_APPLICATION_URI;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_AUTHORISATION_URI;
import static com.google.common.base.Preconditions.checkNotNull;

public class AuthorisationMessage extends ExtensibleElementWrapper
{
    public AuthorisationMessage(Element internal)
    {
        super(internal);
    }

    public AuthorisationMessage(Factory factory, QName qname)
    {
        super(factory, qname);
    }
    
    /**
     * Link URI for initiating the OAuth dance. If this is omitted, then no action is needed,
     * i.e. this is just a confirmation message.
     */
    public void setAuthorisationUri(URI uri)
    {
        addSimpleExtension(ATLASSIAN_AUTHORISATION_URI, checkNotNull(uri, "authorisationUri").toString());
    }
    
    public void setApplicationId(String applicationId)
    {
        addSimpleExtension(ATLASSIAN_APPLICATION_ID, checkNotNull(applicationId, "applicationId"));
    }
    
    public void setApplicationName(String applicationName)
    {
        addSimpleExtension(ATLASSIAN_APPLICATION_NAME, checkNotNull(applicationName, "applicationName"));
    }
    
    public void setApplicationUri(URI uri)
    {
        addSimpleExtension(ATLASSIAN_APPLICATION_URI, checkNotNull(uri, "applicationUri").toString());
    }
}
