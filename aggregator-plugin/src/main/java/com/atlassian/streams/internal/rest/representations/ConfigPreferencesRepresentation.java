package com.atlassian.streams.internal.rest.representations;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents the current set of Streams configuration preferences, such as date/time formats.
 */
public class ConfigPreferencesRepresentation
{
    @JsonProperty final String dateFormat;
    @JsonProperty final String timeFormat;
    @JsonProperty final String dateTimeFormat;
    @JsonProperty final String timeZone;
    @JsonProperty final boolean dateRelativize;


    @JsonCreator
    public ConfigPreferencesRepresentation(@JsonProperty("dateFormat") String dateFormat,
                                           @JsonProperty("timeFormat") String timeFormat,
                                           @JsonProperty("dateTimeFormat") String dateTimeFormat,
                                           @JsonProperty("timeZone") String timeZone,
                                           @JsonProperty("dateRelativize") boolean dateRelativize)
    {
        this.dateFormat = checkNotNull(dateFormat, "dateFormat");
        this.timeFormat = checkNotNull(timeFormat, "timeFormat");
        this.dateTimeFormat = checkNotNull(dateTimeFormat, "dateTimeFormat");
        this.timeZone = checkNotNull(timeZone, "timeZone");
        this.dateRelativize = checkNotNull(dateRelativize, "dateRelativize");
    }

    public String getDateFormat()
    {
        return dateFormat;
    }

    public String getTimeFormat()
    {
        return timeFormat;
    }

    public String getDateTimeFormat()
    {
        return dateTimeFormat;
    }

    public String getTimeZone()
    {
        return timeZone;
    }

    public boolean isDateRelativize()
    {
        return dateRelativize;
    }

}
