package com.atlassian.streams.internal;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.internal.ActivityProvider.Error;
import com.atlassian.streams.internal.feed.FeedAggregator;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.spi.CancellableTask;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import javax.annotation.Nullable;

import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.internal.ActivityProvider.Error.other;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public final class FeedBuilder
{
    private final Logger log = LoggerFactory.getLogger(FeedBuilder.class);

    private final ActivityProviders activityProviders;
    private final FeedAggregator aggregator;
    private final StreamsCompletionService completionService;
    private final ApplicationProperties applicationProperties;

    public FeedBuilder(ActivityProviders activityProviders,
                       FeedAggregator aggregator,
                       StreamsCompletionService completionService,
                       ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
        this.activityProviders = checkNotNull(activityProviders, "activityProviders");
        this.aggregator = checkNotNull(aggregator, "aggregator");
        this.completionService = checkNotNull(completionService, "completionService");
    }

    public FeedModel getFeed(Uri self, String contextPath, HttpParameters parameters, String requestLanguages)
    {
        Iterable<ActivityProvider> providers = activityProviders.get(ImmutableSet.of(
                parameters.module(), parameters.fetchLocalOnly(), parameters.isSelectedProvider()));
        final ImmutableSet<ActivityProvider> banned = ImmutableSet.copyOf(filter(providers, not(completionService.reachable())));
        final Iterable<ActivityProvider> notBannedProviders = filter(providers, not(in(banned)));

        final Iterable<ActivityProviderCancellableTask<Either<Error, FeedModel>>> callables = transform(notBannedProviders,
                toFeedCallable(pair(self, parameters),
                        parameters.calculateContextUrl(applicationProperties, contextPath), requestLanguages));

        final Iterable<Either<Error, FeedModel>> results;
        if (Sys.inDevMode() && !parameters.isTimeoutTest())
        {
            results = completionService.execute(callables);
        }
        else
        {
            results = completionService.execute(callables, ActivityRequestImpl.DEFAULT_TIMEOUT, MILLISECONDS);
        }
        return aggregator.aggregate(concat(results, transform(banned, toLeftBanned())), self,
                parameters.parseMaxResults(ActivityRequestImpl.DEFAULT_MAX_RESULTS), parameters.getTitle());
    }

    private final Function<ActivityProvider, ActivityProviderCancellableTask<Either<Error, FeedModel>>> toFeedCallable(
            Pair<Uri, HttpParameters> feedParameters,
            URI baseUri,
            String requestLanguages)
    {
        return new ToFeedCallable(feedParameters, baseUri, requestLanguages);
    }

    private final class ToFeedCallable implements Function<ActivityProvider, ActivityProviderCancellableTask<Either<Error, FeedModel>>>
    {
        private final Pair<Uri, HttpParameters> feedParameters;
        @SuppressWarnings("unused") private final String requestLanguages;
        private final URI contextUri;

        private ToFeedCallable(Pair<Uri, HttpParameters> feedParameters, URI contextUri, String requestLanguages)
        {
            this.feedParameters = feedParameters;
            this.contextUri = contextUri;
            this.requestLanguages = requestLanguages;
        }

        public ActivityProviderCancellableTask<Either<Error, FeedModel>> apply(final ActivityProvider provider)
        {
            ActivityRequestImpl.Builder builder = ActivityRequestImpl.builder(feedParameters.first()).contextUri(contextUri);
            // Set the Accept-Language header for only the remote (AppLinked) activity requests.
            // We don't want to override the request-language setting for the aggregating plugin.
            if (feedParameters.second().useAcceptLanguage())
            {
                // STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126.
                // builder.requestLanguages(requestLanguages);
            }
            ActivityRequestImpl request = builder.build(feedParameters.second(), provider);
            final CancellableTask<Either<Error, FeedModel>> task = provider.getActivityFeed(request);

            return new ActivityProviderCancellableTask<Either<Error, FeedModel>>()
            {
                public Either<Error, FeedModel> call() throws Exception
                {
                    try
                    {
                        return task.call();
                    }
                    catch (NoMatchingRemoteKeysException nmrke)
                    {
                        log.info("No keys from " + provider.getName() + " matched " + Joiner.on(",").join(nmrke.getKeys()), nmrke);
                        // In this case, we don't want this to be added to the failing list.
                        return left(other());
                    }
                    catch (Exception e)
                    {
                        log.error("Error fetching feed", e);
                        // Not sure on this one. We may get an error because of bad JQL for example, which shouldn't cause it to be added to failing list.
                        return left(other());
                    }
                }

                @Override
                public Result cancel()
                {
                    return task.cancel();
                }

                @Override
                public ActivityProvider getActivityProvider()
                {
                    return provider;
                }
            };
        }
    }

    private Function<ActivityProvider, Either<Error, FeedModel>> toLeftBanned()
    {
        return new Function<ActivityProvider, Either<Error, FeedModel>>()
        {
            @Override
            public Either<Error, FeedModel> apply(@Nullable final ActivityProvider activityProvider)
            {
                return Either.left(Error.banned(activityProvider));
            }
        };
    }

}
