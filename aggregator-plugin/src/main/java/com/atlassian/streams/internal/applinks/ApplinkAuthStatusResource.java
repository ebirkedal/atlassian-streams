package com.atlassian.streams.internal.applinks;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * This REST resource will eventually be moved into the Applinks REST API.  It was originally
 * meant to be used by the front-end logic in applinks.js, but that code no longer needs to
 * use Ajax polling.
 */
@Path("/applinks/status")
public class ApplinkAuthStatusResource
{
    private static final CacheControl NO_CACHE = new CacheControl();
    static
    {
        NO_CACHE.setNoStore(true);
        NO_CACHE.setNoCache(true);
    }
    
    private final ApplicationLinkService appLinkService;
    private final ApplicationLinkServiceExtensions appLinkServiceExtensions;

    public ApplinkAuthStatusResource(ApplicationLinkService appLinkService,
                                     ApplicationLinkServiceExtensions appLinkServiceExtensions)
    {
        this.appLinkService = checkNotNull(appLinkService, "appLinkService");
        this.appLinkServiceExtensions = checkNotNull(appLinkServiceExtensions, "appLinkServiceExtensions");
    }
    
    @GET
    @Path("/{applinkId}")
    @Produces(APPLICATION_JSON)
    /**
     * Tests whether an applink has been authorised.  The response has no content, just an
     * HTTP 200 (OK) status if the link is authorised, or 403 (Forbidden) if it's not.
     */
    public Response getApplinkAvailability(@PathParam("applinkId") String applinkId)
    {
        ApplicationLink appLink;
        ResponseBuilder response;
        
        try
        {
            appLink = appLinkService.getApplicationLink(new ApplicationId(applinkId));
        }
        catch (TypeNotInstalledException e)
        {
            appLink = null;
        }
        if (appLink == null)
        {
            // bad applink ID - return HTTP 404
            response = Response.status(Response.Status.NOT_FOUND);
        }
        else
        {
            if (appLinkServiceExtensions.isAuthorised(appLink))
            {
                // authorized - return HTTP 200
                response = Response.ok();
            }
            else
            {
                // not authorized (yet) - return HTTP 403
                response = Response.status(Response.Status.FORBIDDEN);
            }
        }
        return response.cacheControl(NO_CACHE).build();
    }
}
