package com.atlassian.streams.internal;

import com.atlassian.streams.api.StreamsException;

public final class NoSuchModuleException extends StreamsException
{
    private final String key;
    
    public NoSuchModuleException(String key)
    {
        this.key = key;
    }
    
    public String getKey()
    {
        return key;
    }
}
