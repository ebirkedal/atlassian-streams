package com.atlassian.streams.internal.rest.representations;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * A Jackson representation of i18n translation key-value pairs.
 */
public class I18nTranslations
{
    @JsonProperty private final Map<String, String> translations;

    @JsonCreator
    public I18nTranslations(@JsonProperty("translations") Map<String, String> translations)
    {
        this.translations = ImmutableMap.copyOf(translations);
    }

    public Map<String, String> getTranslations()
    {
        return translations;
    }
}
