package com.atlassian.streams.internal.rest.resources;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.internal.rest.representations.I18nTranslations;
import com.atlassian.streams.spi.StreamsLocaleProvider;

import org.springframework.beans.factory.annotation.Qualifier;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.toArray;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * Resource for accessing i18n values from javascript.
 */
@Path("/i18n")
@AnonymousAllowed
public class I18nResource
{
    private final I18nResolver i18nResolver;
    private final StreamsLocaleProvider localeProvider;

    public I18nResource(@Qualifier("streamsI18nResolver") I18nResolver i18nResolver, StreamsLocaleProvider localeProvider)
    {
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.localeProvider = checkNotNull(localeProvider, "localeProvider");
    }

    @GET
    @Path("key/{key}")
    @Produces(TEXT_PLAIN)
    public Response getTranslation(@PathParam("key") String key, @QueryParam("parameters") List<String> parameters)
    {
        final String text;
        
        if (parameters != null)
        {
            Serializable[] serializables = toArray(parameters, Serializable.class);
            text = i18nResolver.getText(key, serializables);
        }
        else
        {
            text = i18nResolver.getText(key);
        }
        
        return Response.ok(text).type(TEXT_PLAIN).build();
    }

    @GET
    @Path("prefix/{prefix}")
    @Produces(APPLICATION_JSON)
    public Response getTranslations(@PathParam("prefix") String prefix)
    {
        I18nTranslations translations = new I18nTranslations(i18nResolver.getAllTranslationsForPrefix(prefix, localeProvider.getUserLocale()));
        return Response.ok(translations).type(APPLICATION_JSON).build();
    }
}
