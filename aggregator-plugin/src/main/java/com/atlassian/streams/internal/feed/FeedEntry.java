package com.atlassian.streams.internal.feed;

import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.common.Option;

import com.google.common.base.Function;

import org.joda.time.DateTime;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.none;

/**
 * An activity entry in a {@link FeedModel}. For locally generated feeds, this is
 * just a wrapper for a {@link StreamsEntry}. For remote feeds, it holds an object
 * that was parsed by the {@link FeedRenderer} (e.g. an Abdera Entry) and provides
 * access to only the properties of that object that are required for aggregation
 * (like the entry date), since there's no need to fully translate the parsed
 * entry back into a StreamsEntry.
 */
public abstract class FeedEntry
{
    private final Option<FeedModel> sourceFeed;
    
    /**
     * Returns the StreamsEntry if this entry was generated locally; or, if it was
     * parsed from a remote feed, converts the parsed content into a StreamsEntry.
     */
    public abstract StreamsEntry getStreamsEntry();
    
    /**
     * Returns the entry's posted date.  This is the only property we need to look
     * at when we're aggregating a feed.  Getting its value for an entry from a
     * remote feed does not require fully parsing the entry.
     */
    public abstract DateTime getEntryDate();
    
    /**
     * Returns the FeedRepresentation that this entry was originally from, if it
     * has been copied to an aggregated feed.  This is used by the Atom FeedEncoding
     * to determine whether to include a "source" element in the entry.
     */
    public Option<FeedModel> getSourceFeed()
    {
        return sourceFeed;
    }
    
    /**
     * Returns a new FeedEntry that is copied from an existing one, with the addition
     * of a source feed property.
     */
    public abstract FeedEntry toAggregatedEntry(Option<FeedModel> sourceFeed);
    
    protected FeedEntry()
    {
        this(none(FeedModel.class));
    }
    
    protected FeedEntry(Option<FeedModel> sourceFeed)
    {
        this.sourceFeed = sourceFeed;
    }
    
    /**
     * Creates a FeedEntry that is a wrapper for a StreamsEntry.  All lazily generated
     * properties of the StreamsEntry will be accessed and copied at this point so it
     * is safe to use it outside of the current application context.
     */
    public static FeedEntry fromStreamsEntry(StreamsEntry streamsEntry)
    {
        return new LocalEntryWrapper(streamsEntry.toStaticEntry(), none(FeedModel.class));
    }

    public static final Function<StreamsEntry, FeedEntry> fromStreamsEntry() {
        return new Function<StreamsEntry, FeedEntry>() {

            public FeedEntry apply(StreamsEntry from)
            {
                return FeedEntry.fromStreamsEntry(from);
            }
        };
    }
    
    private static class LocalEntryWrapper extends FeedEntry
    {
        private final StreamsEntry streamsEntry;
        
        LocalEntryWrapper(StreamsEntry streamsEntry, Option<FeedModel> sourceFeed)
        {
            super(sourceFeed);
            this.streamsEntry = streamsEntry;
        }
        
        public StreamsEntry getStreamsEntry()
        {
            return streamsEntry;
        }
        
        public DateTime getEntryDate()
        {
            return streamsEntry.getPostedDate();
        }
        
        public FeedEntry toAggregatedEntry(Option<FeedModel> sourceFeed)
        {
            return new LocalEntryWrapper(streamsEntry, sourceFeed);
        }
    }
}
