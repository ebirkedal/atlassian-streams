/*
 This is a phenomenally bad idea.  This breaks _all_ future jsonp requests.  However, json requests are currently broken
 inside any gadget in Confluence 4.0.  And since we're making a json request in Streams and no jsonp requests, this seemed
 like a reasonable temporary fix.  For more information, see STRM-1454.

 If Confluence is upgraded to a version of AG that does not have this bug (AG-1365), this should absolutely be removed.

 If Streams is ever de-gadgetized, this should absolutely be removed (and presumably a different/better workaround found)
 so that we don't break all jsonp requests for the entire application.
 */
(function() {
    AJS.$.ajaxSetup({
        jsonp: null,
        jsonpCallback: null
    });
})();