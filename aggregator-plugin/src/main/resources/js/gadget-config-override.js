var ActivityStreams = ActivityStreams || {};

/**
 * "Hijacks" the built-in gadget configuration UI by overriding the gadget.showConfig() function for the specified gadget
 *
 * @method hijackGadgetConfig
 * @paran {Gadget} gadget The gadget object we want to override
 */
ActivityStreams.hijackGadgetConfig = function(gadget) {
    var gadgetId = gadget.getPrefs().getModuleId();

    /**
     * shows configuration
     *
     * @method showConfig
     */
    gadget.showConfig = function () {
        AJS.$('#' + gadgetId).trigger('showConfiguration.streams')
    };
};