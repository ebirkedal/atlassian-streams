package it.com.atlassian.streams.applinks;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.AppLinksTestRunner;
import com.atlassian.streams.testing.AppLinksTests;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS_OAUTH;

@RunWith(ConfluenceApplinksTest.Runner.class)
@TestGroups({APPLINKS, APPLINKS_OAUTH})
public class ConfluenceApplinksTest extends AppLinksTests
{
    public static final class Runner extends AppLinksTestRunner
    {
        public Runner(Class<?> klass) throws InitializationError
        {
            super(klass, "confluence");
        }
    }

    @Override
    @Ignore("Confluence won't be applinked to itself")
    public void assertThatConfluenceActivityIsInFeed() {}

    @Override
    @Ignore("Confluence won't have applinks filters for itself")
    public void assertThatFilterResourceHasAppLinksProviderFiltersFromConfluence() {}

    @Test @Override
    public void assertThatFilterProvidersAreSorted()
    {
        assertThatFilterProvidersAreSortedWithLocalProvidersFirst("Confluence", "Third Parties");
    }

    @Test @Override @Ignore
    public void assertThatUrlProxyResourceIsWhitelistedToAccessConfluence()
    {
    }

    @Test
    public void assertThatUrlProxyResourceIsNotWhitelistedToAccessConfluence()
    {
        assertThat(restTester.getProxiedResponse("http://localhost:1990/streams/").getStatus(),
                   equalTo(HttpServletResponse.SC_FORBIDDEN));
    }
}
