package it.com.atlassian.streams.confluence;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.Confluence.CONFLUENCE_PROVIDER;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withAuthorElement;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/spaces.zip")
public class ConfluenceSpacesTest
{
    // this should be reviewed whenever the version of Confluence used in the tests is upgraded,
    // as any upgrade tasks run during the startup may generate additional activity entries.
    int maxResults = 140;

    @Inject static FeedClient feedClient;

    @Test
    public void assertThatCreatedSpaceAppearsInFeed()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), maxResults(maxResults));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("added space"), containsString("My New Space")))));
    }

    @Test
    public void assertThatCreatedPersonalSpaceAppearsInFeed()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), maxResults(maxResults));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("created their"), containsString("personal space")))));
    }

    @Test
    public void assertThatEditedSpaceAppearsInFeed()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), maxResults(maxResults));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("edited space"), containsString("Demonstration Space")))));
    }

    @Test
    public void assertThatEditedPersonalSpaceAppearsInFeed()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), maxResults(maxResults));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("edited their"), containsString("personal space")))));
    }
    
    @Test
    public void assertThatCreatedSpaceThatWasAlsoEditedAppearsInFeed()
    {
        // STRM-1640
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), maxResults(maxResults));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("added space"), containsString("Demonstration Space")))));
    }
    
    @Test
    public void assertThatCreatedPersonalSpaceThatWasAlsoEditedAppearsInFeed()
    {
        // STRM-1640
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), maxResults(maxResults));
        assertThat(feed.getEntries(), hasEntry(
                withAuthorElement(equalTo("admin")),
                withTitle(allOf(containsString("edited their"), containsString("personal space")))));
        assertThat(feed.getEntries(), hasEntry(
                withAuthorElement(equalTo("admin")),
                withTitle(allOf(containsString("created their"), containsString("personal space")))));
    }
}
