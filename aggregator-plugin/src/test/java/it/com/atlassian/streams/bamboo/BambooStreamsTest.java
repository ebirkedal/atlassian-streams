package it.com.atlassian.streams.bamboo;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.testing.Bamboo.Data.ProjectAAResult;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.PreferenceMapBuilder;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;
import com.sun.jersey.api.client.ClientResponse;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.bamboo.BambooActivityObjectTypes.job;
import static com.atlassian.streams.bamboo.BambooActivityVerbs.fail;
import static com.atlassian.streams.bamboo.BambooActivityVerbs.pass;
import static com.atlassian.streams.testing.Bamboo.BAMBOO_PROVIDER;
import static com.atlassian.streams.testing.Bamboo.Data.allBuildEntries;
import static com.atlassian.streams.testing.Bamboo.Data.buildEntriesForProjectAA;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.PreferenceMapBuilder.builder;
import static com.atlassian.streams.testing.StreamsTestGroups.BAMBOO;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasBambooActivityFilterOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasBambooFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasStandardFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withFilterProviderName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionKey;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionValueKeys;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntriesInAnyOrder;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtlassianApplicationElement;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtomGeneratorElement;
import static com.atlassian.streams.testing.matchers.Matchers.withActivityObjectElement;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withType;
import static com.atlassian.streams.testing.matchers.Matchers.withVerbElement;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(BAMBOO)
public class BambooStreamsTest
{
    @Inject static FeedClient client;
    @Inject static RestTester restTester;

    @Test
    public void assertThatFeedUpdatedDateIsSameAsFirstEntryPublishedDate()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getUpdated(), is(equalTo(feed.getEntries().get(0).getPublished())));
    }

    @Test
    public void assertThatInitialBuildEntryHasCorrectTriggerReasonInDescription()
    {
        Feed feed = client.get(maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(allOf(
                withTitle(containsString("AP-YET-JOB1-1")),
                withContent(containsString("Initial clean build")))));
    }

    @Test
    public void assertThatBuildUpdatedEntryHasCorrectTriggerReasonInDescription()
    {
        Feed feed = client.get(maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(allOf(
            withTitle(containsString("AP-YET-JOB1-2")),
            withContent(allOf(
                containsString("Updated by"),
                containsString("testadmin"))))));
    }

    @Test
    public void assertThatDependencyBuildEntryHasCorrectTriggerReasonInDescription()
    {
        Feed feed = client.get(maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(allOf(
            withTitle(containsString("AP-YET-JOB1-3")),
            withContent(allOf(
                containsString("Dependant of"),
                containsString("AA-SLAP-57"))))));
    }

    @Test
    public void assertThatScheduledBuildEntryHasCorrectTriggerReasonInDescription()
    {
        Feed feed = client.get(maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(allOf(
            withTitle(containsString("AP-YET-JOB1-4")),
            withContent(containsString("Scheduled build")))));
    }

    @Test
    public void assertThatChildDependencyBuildEntryHasCorrectTriggerReasonInDescription()
    {
        Feed feed = client.get(maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(allOf(
            withTitle(containsString("AP-YET-JOB1-5")),
            withContent(allOf(
                containsString("Triggered by child"),
                containsString("AA-SLAP-56"))))));
    }

    @Test
    public void assertThatManualBuildEntryHasCorrectTriggerReasonInDescription()
    {
        Feed feed = client.get(maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(allOf(
            withTitle(containsString("AP-YET-JOB1-6")),
            withContent(allOf(
                containsString("Manual build by"),
                containsString("Admin"))))));
    }

    @Test
    public void assertThatActivityStreamTitleIsForAllProjects()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getTitle(), is(equalTo("Activity Stream for all projects")));
    }

    @Test
    public void assertThatActivityStreamContainsAtomGeneratorModule()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), allEntries(haveAtomGeneratorElement(containsString(client.getBaseUrl()))));
    }

    @Test
    public void assertThatAllActivityStreamEntriesContainsAtlassianApplicationElement()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), allEntries(haveAtlassianApplicationElement(containsString("com.atlassian.bamboo"))));
    }

    @Test
    public void assertThatStreamsAllEvents()
    {
        Feed feed = client.get(maxResults(100),provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntriesInAnyOrder(allBuildEntries()));
    }

    @Test
    public void assertThatStreamWithMaxOfTwentyResultsContainsLastTwentyBuilds()
    {
        Feed feed = client.get(maxResults(20),provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(buildEntriesForProjectAA(ProjectAAResult.NUM_BUILDS - 19)));
    }

    @Test
    public void assertThatFailedBuildHasFailVerb()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("AA-SLAP-JOB1-55"), containsString("failed"))),
                withVerbElement(equalTo(fail().iri().toASCIIString()))));
    }

    @Test
    public void assertThatSuccessfulBuildHasPassVerb()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("AA-SLAP-JOB1-56"), containsString("was successful"))),
                withVerbElement(equalTo(pass().iri().toASCIIString()))));
    }

    @Test
    public void assertThatFailedBuildHasFailedTests()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(containsString("AA-SLAP-JOB1-55")),
                withContent(containsString("251 of 5,049 tests failed"))));
    }

    @Test
    public void assertThatSuccessfulBuildHasPassedTests()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(containsString("AA-SLAP-JOB1-56")),
                withContent(containsString("5,049 tests passed"))));
    }

    @Test
    public void assertThatSomeBuildHasNoTestsFound()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(containsString("AA-SLAP-JOB1-51")),
                withContent(containsString("no tests found"))));
    }

    @Test
    public void assertThatBuildHasActivityObjectWithTypeJob()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(containsString("AA-SLAP-JOB1-56")),
                withActivityObjectElement(withType(equalTo(job().iri().toASCIIString())))));
    }

    @Test
    public void assertThatFeedSubtitleIsCorrect()
    {
        Feed feed = client.get(provider(BAMBOO_PROVIDER));
        assertThat(feed.getSubtitle(), equalTo("Activity for Bamboo"));
    }

    @Test
    public void assertThatFilterResourceHasBambooApplicationFilters()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasBambooFilters());
    }

    @Test
    public void assertThatBambooFilterHasBambooAsDisplayName()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasBambooFilters(withFilterProviderName(equalTo("Bamboo"))));
    }

    @Test
    public void assertThatFilterResourceHasKeyFilterNamedProductSpecific()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(withOption(withOptionKey("key"), withOptionName("Plan"))));
    }

    @Test
    public void assertThatFilterResourceHasBambooProjectsAsKeyOptionValues()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(withOption(withOptionKey("key"), withOptionValueKeys("AA", "AP"))));
    }

    @Test
    public void assertThatBambooFilterHasActivityOptions()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasBambooActivityFilterOption());
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsHasInvalidKeyParameter()
    {
        ClientResponse response = restTester.validatePreferences(builder().keys("INVALIDKEY").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithKeysDefined()
    {
        ClientResponse response = restTester.validatePreferences(PreferenceMapBuilder.builder().keys("AA").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameAndKeysDefined()
    {
        ClientResponse response = restTester.validatePreferences(
            PreferenceMapBuilder.builder().usernames("admin").keys("AA").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithMultipleKeysDefined()
    {
        ClientResponse response = restTester.validatePreferences(PreferenceMapBuilder.builder().keys("AA,AP").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameAndMultipleKeysDefined()
    {
        ClientResponse response = restTester.validatePreferences(
            PreferenceMapBuilder.builder().usernames("admin").keys("AA,AP").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }
}
