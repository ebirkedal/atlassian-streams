package it.com.atlassian.streams.applinks.fisheye.external;

import com.gargoylesoftware.htmlunit.html.HtmlElement;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import static org.hamcrest.Matchers.hasItemInArray;

public abstract class HtmlUnitMatchers
{
    public static Matcher<HtmlElement[]> hasElement(Matcher<? super HtmlElement> matcher)
    {
        return hasItemInArray(matcher);
    }

    public static Matcher<HtmlElement> withText(Matcher<? super String> matcher)
    {
        return new WithText(matcher);
    }

    private static final class WithText extends TypeSafeDiagnosingMatcher<HtmlElement>
    {
        private final Matcher<? super String> matcher;

        private WithText(Matcher<? super String> matcher)
        {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(HtmlElement element, Description mismatchDescription)
        {
            if (!matcher.matches(element.asText()))
            {
                mismatchDescription.appendText("text ");
                matcher.describeMismatch(element.asText(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("text is").appendDescriptionOf(matcher);
        }
    }
}
