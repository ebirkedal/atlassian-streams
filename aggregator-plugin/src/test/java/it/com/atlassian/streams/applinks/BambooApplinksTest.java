package it.com.atlassian.streams.applinks;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.AppLinksTestRunner;
import com.atlassian.streams.testing.AppLinksTests;

import org.apache.abdera.model.Feed;
import org.hamcrest.Matchers;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;

import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.user;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS_OAUTH;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

@RunWith(BambooApplinksTest.Runner.class)
@TestGroups({APPLINKS, APPLINKS_OAUTH})
public class BambooApplinksTest extends AppLinksTests
{
    public static final class Runner extends AppLinksTestRunner
    {
        public Runner(Class<?> klass) throws InitializationError
        {
            super(klass, "bamboo");
        }
    }

    @Override
    @Ignore("Bamboo won't be applinked to itself")
    public void assertThatBambooActivityIsInFeed() {}

    @Override
    @Ignore("Bamboo won't have applinks filters for itself")
    public void assertThatFilterResourceHasAppLinksProviderFiltersFromBamboo() {}

    @Test
    public void assertThatBambooEntriesDoesNotDisplayBothUpdatedAndTriggeredUsernamesWhenBuildHasBoth()
    {
        Feed feed = client.getAs("admin", local());
        assertThat(feed.getEntries(), hasEntry(
                withTitle(containsString("ONE-ONE10-JOB1-5")),
                withContent(allOf(not(containsString("Updated by")), containsString("Manual build by")))));
    }

    @Test
    public void assertThatBambooEntriesWithMultipleAuthorsRespectNotUserFilter()
    {
        Feed feed = client.getAs("admin", local(), maxResults(100), user(NOT, "rwallace"));
        assertThat(feed.getEntries(), not(hasEntry(withContent(containsString("rwallace")))));
    }

    @Test @Override
    public void assertThatFilterProvidersAreSorted()
    {
        assertThatFilterProvidersAreSortedWithLocalProvidersFirst("Bamboo", "Third Parties");
    }

    @Test @Override @Ignore
    public void assertThatUrlProxyResourceIsWhitelistedToAccessBamboo()
    {
    }

    @Test
    public void assertThatUrlProxyResourceIsNotWhitelistedToAccessBamboo()
    {
        assertThat(restTester.getProxiedResponse("http://localhost:6990/streams/").getStatus(),
                   equalTo(HttpServletResponse.SC_FORBIDDEN));
    }
}
