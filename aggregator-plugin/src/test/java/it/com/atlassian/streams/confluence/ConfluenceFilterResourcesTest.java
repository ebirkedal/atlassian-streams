package it.com.atlassian.streams.confluence;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.internal.rest.representations.ConfigPreferencesRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasConfluenceActivityFilterOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasConfluenceFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasStandardFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withFilterProviderName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionKey;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionValueKeys;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/base.zip")
public class ConfluenceFilterResourcesTest
{
    @Inject static RestTester restTester;

    @Test
    public void assertThatFilterResourceHasConfluenceApplicationFilters()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasConfluenceFilters());
    }

    @Test
    public void assertThatConfluenceFiltersHaveConfluenceAsDisplayName()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasConfluenceFilters(withFilterProviderName(equalTo("Confluence"))));
    }

    @Test
    public void assertThatFilterResourceHasKeyFilterNamedProductSpecific()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(withOption(withOptionKey("key"), withOptionName("Space"))));
    }

    @Test
    public void assertThatFilterResourceHasConfluenceSpacesAsKeyOptionValues()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(withOption(withOptionKey("key"),
                                                                 withOptionValueKeys("ds", "newspace", "~admin"))));
    }

    @Test
    public void assertThatConfluenceFilterHasActivityOptionValues()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasConfluenceActivityFilterOption());
    }

    @Test
    public void assertThatTheDefaultDateFormatIsCorrect()
    {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getDateFormat(), is(equalTo("MMM dd, yyyy")));
    }

    @Test
    public void assertThatTheDefaultTimeFormatIsCorrect()
    {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeFormat(), is(equalTo("h:mm a")));
    }

    @Test
    public void assertThatTheDefaultDateTimeFormatIsCorrect()
    {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getDateTimeFormat(), is(equalTo("MMM dd, yyyy HH:mm")));
    }
}
