package it.com.atlassian.streams.jira;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.jira.JiraActivityObjectTypes.issue;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.Jira.activities;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.withSummary;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/long-description-and-comment.xml")
public class LongDescriptionCommentTest
{
    @Inject static FeedClient client;

    @Test
    public void assertThatCreatedIssueSummaryContainsReadMoreLinkForLongDescription()
    {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), activities(IS, pair(issue(), post())));
        assertThat(feed.getEntries(), hasEntries(allOf(
                withTitle(allOf(containsString("created"), containsString("ONE-3"))),
                withSummary(allOf(containsString("content-more"), containsString("Read more"))))));
    }

    @Test
    public void assertThatUpdatedIssueSummaryContainsReadMoreLinkForLongDescription()
    {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), activities(IS, pair(issue(), update())));
        assertThat(feed.getEntries(), hasEntries(allOf(
                withTitle(allOf(containsString("updated the Description"), containsString("ONE-3"))),
                withSummary(allOf(containsString("content-more"), containsString("Read more"))))));
    }

    @Test
    public void assertThatIssueCommentSummaryContainsReadMoreLinkForLongComments()
    {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"), activities(IS, pair(comment(), post())));
        assertThat(feed.getEntries(), hasEntries(allOf(
                withTitle(allOf(containsString("commented"), containsString("ONE-3"))),
                withSummary(allOf(containsString("comment-more"), containsString("Read more"))))));
    }
}
