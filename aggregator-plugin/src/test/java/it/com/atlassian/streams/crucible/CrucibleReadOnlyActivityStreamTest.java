package it.com.atlassian.streams.crucible;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.PreferenceMapBuilder;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.sun.jersey.api.client.ClientResponse;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.google.common.collect.Iterables.size;

import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.abandon;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.reopen;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.summarize;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.summarizeAndClose;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.AbstractFeedClient.module;
import static com.atlassian.streams.testing.Crucible.crucibleModule;
import static com.atlassian.streams.testing.Crucible.Data.review1Created;
import static com.atlassian.streams.testing.Crucible.Data.review2Created;
import static com.atlassian.streams.testing.Crucible.Data.review2Started;
import static com.atlassian.streams.testing.Crucible.Data.review3Summarized;
import static com.atlassian.streams.testing.Crucible.Data.review4Closed;
import static com.atlassian.streams.testing.Crucible.Data.review4Summarized;
import static com.atlassian.streams.testing.Crucible.Data.review5Closed;
import static com.atlassian.streams.testing.Crucible.Data.review5Summarized;
import static com.atlassian.streams.testing.Crucible.Data.review6Closed;
import static com.atlassian.streams.testing.Crucible.Data.review6ReOpened;
import static com.atlassian.streams.testing.Crucible.Data.review6Commented;
import static com.atlassian.streams.testing.Crucible.Data.review6CommentThatHasBeenDeleted;
import static com.atlassian.streams.testing.Crucible.Data.review7Commented;
import static com.atlassian.streams.testing.Crucible.Data.review8Abandoned;
import static com.atlassian.streams.testing.Crucible.Data.review9Created;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.PreferenceMapBuilder.builder;
import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasCrucibleActivityFilterOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasCrucibleFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasStandardFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withFilterProviderName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionKey;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionValueKeys;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasAuthor;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.whereRel;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withLink;
import static com.atlassian.streams.testing.matchers.Matchers.withVerbElement;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(FECRU)
public class CrucibleReadOnlyActivityStreamTest
{
    @Inject static FeedClient feedClient;
    @Inject static RestTester restTester;

    @Test
    public void assertThatFeedUpdatedDateIsSameAsFirstEntryPublishedDate()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local());
        assertThat(feed.getUpdated(), is(equalTo(feed.getEntries().get(0).getPublished())));
    }

    @Test
    public void assertThatCrucibleActivityStreamContainsDefaultEntry()
    {
        // Crucible by default has a created review in its test data.  We'll check that this is right.
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), hasEntry(review1Created()));
    }

    @Test
    public void assertThatAuthorsContainProfilePictureUris()
    {
        Feed feed = feedClient.getAs("admin", module("crucible"));
        assertThat(feed.getEntries(), allEntries(hasAuthor(withLink(whereRel(equalTo("photo"))))));
    }

    @Test
    public void assertThatCreatedReviewHasEntryWithCorrectSummary()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), hasEntry(review2Created()));
    }

    @Test
    public void assertThatCreatedReviewHasEntryWithCorrectVerb()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), hasEntry(review2Created(),
                withVerbElement(equalTo(post().iri().toASCIIString()))));
    }

    @Test
    public void assertThatCreatedReviewHasCorrectNumberOfFilesInContent()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local());
        assertThat(feed.getEntries(), hasEntry(review9Created()));
    }

    @Test
    public void assertThatSummarizedReviewHasEntryWithCorrectSummary()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), hasEntry(review3Summarized()));
    }

    @Test
    public void assertThatSummarizedReviewHasEntryWithCorrectVerb()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), hasEntry(review3Summarized(),
                withVerbElement(equalTo(summarize().iri().toASCIIString()))));
    }

    @Test
    public void assertThatClosedReviewHasEntryWithCorrectSummary()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), hasEntry(review4Closed()));
    }

    @Test
    public void assertThatClosedReviewHasEntryWithCorrectVerb()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), hasEntry(review4Closed(),
                withVerbElement(equalTo(summarizeAndClose().iri().toASCIIString()))));
    }

    @Test
    public void assertThatClosedReviewHasEntryButSummarizedDoesNotIfTheyWereExecutedWithinAMinute()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
            hasEntry(review4Closed()),
            not(hasEntry(review4Summarized())))));
    }

    @Test
    public void assertThatClosedReviewHasEntryButSummarizedDoesNotIfTheyWereExecutedConsecutivelyRegardlessOfTime()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), key(IS, "CR"), maxResults(20));
        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
                hasEntry(review5Closed()),
                not(hasEntry(review5Summarized())))));
    }

    @Test
    public void assertThatCreatedReviewHasEntryButStartedDoesNotIfTheyWereExecutedWithinAMinute()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
                hasEntry(review2Created()),
                not(hasEntry(review2Started())))));
    }

    @Test
    public void assertThatSummarizedReviewHasEntryWithReviewSummary()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), hasEntry(review6Closed(),
                                               withContent(containsString("Summarizing this review!"))));
    }

    @Test
    public void assertThatReopenedReviewHasEntryWithCorrectSummary()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local());
        assertThat(feed.getEntries(), hasEntry(review6ReOpened()));
    }

    @Test
    public void assertThatReopenedReviewHasEntryWithCorrectVerb()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local());
        assertThat(feed.getEntries(), hasEntry(review6ReOpened(),
                withVerbElement(equalTo(reopen().iri().toASCIIString()))));
    }

    @Test
    public void assertThatCommentingOnAReviewShowsEntryInFeed()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local());
        assertThat(feed.getEntries(), hasEntry(review7Commented()));
    }

    @Test
    public void assertThatCommentingOnAReviewShowsEntryInFeedWithCorrectVerb()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local());
        assertThat(feed.getEntries(), hasEntry(review7Commented(),
                withVerbElement(equalTo(post().iri().toASCIIString()))));
    }

    @Test
    public void assertThatDeletedCommentDoesNotShowInFeed()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(),local());
        assertThat(feed.getEntries(), not(hasEntry(review6CommentThatHasBeenDeleted())));
    }

    @Test
    public void assertThatCommentDoesNotStripSingleQuotes()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(),local());
        assertThat(feed.getEntries(), hasEntry(review6Commented()));
    }
    
    @Test
    public void assertThatAbandonedReviewHasEntryWithCorrectSummary()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local());
        assertThat(feed.getEntries(), hasEntry(review8Abandoned()));
    }

    @Test
    public void assertThatAbandonedReviewHasEntryWithCorrectVerb()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local());
        assertThat(feed.getEntries(), hasEntry(review8Abandoned(),
                withVerbElement(equalTo(abandon().iri().toASCIIString()))));
    }

    @Test
    public void assertThatFeedWithMaxResultsParameterContainsNoMoreThanMaxResults() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(3), key(IS, "CR"));
        assertThat(size(feed.getEntries()), equalTo(3));
    }

    @Test
    public void assertThatCrucibleFilterIsAvailable()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasCrucibleFilters());
    }

    @Test
    public void assertThatCrucibleFilterHasActivityOptions()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasCrucibleActivityFilterOption());
    }

    @Test
    public void assertThatFilterResourceHasKeyFilterNamedProductSpecific()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(withOption(withOptionKey("key"), withOptionName("Project/Repository"))));
    }

    @Test
    public void assertThatFilterResourceHasCrucibleProjectsAndFisheyeRepositoriesAsKeyOptionValues()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(
            withOption(withOptionKey("key"), withOptionValueKeys("CR", "TST"))));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsHasInvalidKeyParameter()
    {
        ClientResponse response = restTester.validatePreferences(builder().keys("INVALIDKEY").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithKeysDefined()
    {
        ClientResponse response = restTester.validatePreferences(PreferenceMapBuilder.builder().keys("CR").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameAndKeysDefined()
    {
        ClientResponse response = restTester.validatePreferences(
            PreferenceMapBuilder.builder().usernames("admin").keys("CR").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatCrucibleActivityStreamStripsBasicMarkup()
    {
        // Crucible by default has a created review in its test data.  We'll check that this is right.
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), hasEntry(allOf(ImmutableList.<Matcher<? super Entry>>of(
            review1Created(),
            withContent(allOf(
                containsString("A Sample Review"),
                not(containsString("{cs")),
                not(containsString("id=1")),
                not(containsString("rep=TST"))))))));
    }

    @Test
    public void assertThatCrucibleActivityStreamStripsColonAfterChangesetMarkup()
    {
        // Crucible by default has a created review in its test data.  We'll check that this is right.
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxResults(20));
        assertThat(feed.getEntries(), hasEntry(allOf(ImmutableList.<Matcher<? super Entry>>of(
                review1Created(),
                withContent(allOf(
                    not(containsString(": TST-1")),
                    containsString("TST-1 A Sample Review")))))));
    }

    @Test
    public void assertThatCrucibleFilterHasCrucibleAsDisplayName()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasCrucibleFilters(withFilterProviderName(equalTo("Crucible"))));
    }
}
