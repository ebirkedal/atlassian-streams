package it.com.atlassian.streams;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
public class SelfLinkAuthParameterTest
{
    @Inject static FeedClient client;

    @Test
    @TestGroups(excludes=FECRU)
    public void assertThatSelfLinkContainsOsUserAuthParameterWhenLoggedIn()
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getSelfLink().getHref().toASCIIString(), containsString("os_authType=basic"));
    }

    @Test
    @TestGroups(excludes=FECRU)
    public void assertThatSelfLinkDoesNotContainOsUserAuthParameterWhenNotLoggedIn()
    {
        Feed feed = client.get();
        assertThat(feed.getSelfLink().getHref().toASCIIString(), not(containsString("os_authType=basic")));
    }
    
    @Test
    @TestGroups(FECRU)
    public void assertThatSelfLinkContainsFeAuthTokenWhenLoggedIn()
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getSelfLink().getHref().toASCIIString(), containsString("FEAUTH=admin"));
    }

    @Test
    @TestGroups(FECRU)
    public void assertThatSelfLinkDoesNotContainFeAuthTokenWhenNotLoggedIn()
    {
        Feed feed = client.get();
        assertThat(feed.getSelfLink().getHref().toASCIIString(), not(containsString("FEAUTH")));
    }
}
