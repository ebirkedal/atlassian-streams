package it.com.atlassian.streams.jira;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.whereHref;
import static com.atlassian.streams.testing.matchers.Matchers.whereRel;
import static com.atlassian.streams.testing.matchers.Matchers.withLink;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.startsWith;

@RunWith (StreamsUiTesterRunner.class)
@TestGroups (JIRA)
@RestoreOnce ("jira/backups/issue-historymetadata.xml")
public class IssueHistoryMetadataTest
{
    @Inject
    static FeedClient client;

    @Inject
    static ApplicationProperties applicationProperties;

    @Test
    public void testCustomDescriptionSuffix() throws Exception
    {
        // when
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        final Entry entryWithNonExistantDescriptionKey = feed.getEntries().get(3);
        final Entry entryWithNoDescriptionKey = feed.getEntries().get(2);

        // then
        assertEntryText(entryWithNoDescriptionKey, "admin reopened ONE-1 - 6 &lt;b&gt;raw description&lt;/b&gt;");
        assertEntryText(entryWithNonExistantDescriptionKey, "admin changed the Summary of ONE-1 to '6' Fallback Description;");
    }

    @Test
    public void testCustomDescriptionKey() throws Exception
    {
        // when
        final Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        final Entry entryWithLinks = feed.getEntries().get(0);
        final Entry entryWithCustomAvatar = feed.getEntries().get(8);
        final Entry entryWithRawTextCause = feed.getEntries().get(9);

        // then
        assertThat(entryWithLinks,
                withTitle(allOf(
                        containsString("http://localhost:3990/streams/secure/ViewProfile.jspa?name=admin"),
                        containsString("http://localhost/cause"),
                        containsString("http://bitbucket.org"))));
        assertEntryText(entryWithLinks, "Automated transition updated 2 fields of ONE-1 - foo triggered when "
                + "admin created Bitbucket &lt;script&gt;alert(&#39;foo&#39;);&lt;/script&gt;");
        assertCustomAvatar(entryWithLinks);

        assertEntryText(entryWithCustomAvatar, "Automated transition reopened ONE-1 - Issue One triggered when admin created Bitbucket pull request #666");
        assertCustomAvatar(entryWithCustomAvatar);

        assertEntryText(entryWithRawTextCause, "Automated transition resolved ONE-1 - Issue One as 'Fixed' triggered when admin created Bitbucket raw text cause");
        assertCustomAvatar(entryWithRawTextCause);
    }

    @Test
    public void testSubstitutesUserWithActor() throws Exception
    {
        // when
        final Entry entryWithCustomActor = client.getAs("admin", local(), issueKey(IS, "ONE-1")).getEntries().get(5);

        // then
        assertEntryText(entryWithCustomActor, "Automated transition resolved ONE-1 - Issue One as 'Fixed' "
                + "triggered when &lt;b&gt;\u0141ukasz Pater&lt;/b&gt; created Bitbucket &lt;script&gt;alert(&#39;foo&#39;);&lt;/script&gt;");
        assertThat(entryWithCustomActor.getAuthor().getName(), equalTo("<b>\u0141ukasz Pater</b>"));
        assertThat(entryWithCustomActor.getAuthor().getUri().toString(), equalTo("http://localhost/customprofile"));
        assertCustomAvatar(entryWithCustomActor);
    }

    @Test
    public void testRelativeUrlAvatar() throws Exception
    {
        // when
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        final Entry entryWithRelativeAvatarUrl = feed.getEntries().get(2);

        // then
        assertThat(entryWithRelativeAvatarUrl.getAuthor(),
                withLink(allOf(whereHref(startsWith(applicationProperties.getBaseUrl() + "/language-avatars/java_64.png")), whereRel(equalTo("photo")))));
    }

    @Test
    public void testCustomDescriptionKeyWithOriginalActivityMessge() throws Exception
    {
        // when
        final Entry entryWithOriginalMessage = client.getAs("admin", local(), issueKey(IS, "ONE-1")).getEntries().get(7);

        // then
        assertEntryText(entryWithOriginalMessage, "admin resolved ONE-1 - Issue One as 'Fixed' - automated transition triggered by Bitbucket pull request");
    }

    private void assertCustomAvatar(final Entry entryWithCustomActor)
    {
        assertThat(entryWithCustomActor.getAuthor(), withLink(allOf(whereHref(containsString("language-avatars/java_64.png")), whereRel(equalTo("photo")))));
    }

    private void assertEntryText(Entry entry, String expectedText)
    {
        final String text = entry.getTitle().replaceAll("<.+?>", "").replaceAll("\n", "").replaceAll(" +", " ").replaceAll(" $", "");
        assertThat(text, equalTo(expectedText));
    }
}
