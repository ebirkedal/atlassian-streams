package it.com.atlassian.streams.bamboo;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.Bamboo.Data.ProjectAPResult;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BETWEEN;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.bamboo.BambooActivityObjectTypes.job;
import static com.atlassian.streams.bamboo.BambooActivityVerbs.fail;
import static com.atlassian.streams.bamboo.BambooActivityVerbs.pass;
import static com.atlassian.streams.testing.Bamboo.BAMBOO_PROVIDER;
import static com.atlassian.streams.testing.Bamboo.activities;
import static com.atlassian.streams.testing.Bamboo.Data.allBuildEntriesForProjectAA;
import static com.atlassian.streams.testing.Bamboo.Data.allBuildEntriesForProjectAP;
import static com.atlassian.streams.testing.Bamboo.Data.buildEntriesForProjectAP;
import static com.atlassian.streams.testing.Bamboo.Data.buildEntryForProjectAA;
import static com.atlassian.streams.testing.Bamboo.Data.buildEntryForProjectAP;
import static com.atlassian.streams.testing.Bamboo.Data.ProjectAAResult.BUILD_2;
import static com.atlassian.streams.testing.Bamboo.Data.ProjectAAResult.BUILD_41;
import static com.atlassian.streams.testing.Bamboo.Data.ProjectAAResult.BUILD_43;
import static com.atlassian.streams.testing.Bamboo.Data.ProjectAAResult.BUILD_56;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.FeedClient.updateDate;
import static com.atlassian.streams.testing.FeedClient.user;
import static com.atlassian.streams.testing.StreamsTestGroups.BAMBOO;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withVerbElement;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(BAMBOO)
public class BambooFiltersActivityStreamTest
{
    @Inject static FeedClient client;
    @Inject static RestTester restTester;

    @Test
    public void assertThatFeedWithFilterIsVerbParameterOnlyContainsEntriesMatchingThatVerb() throws Exception
    {
        Feed feed = client.getAs("admin", activities(IS, pair(job(), pass())), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), allEntries(withVerbElement(equalTo(pass().iri().toASCIIString()))));
    }

    @Test
    public void assertThatFeedWithFilterNotVerbParameterOnlyContainsEntriesNotMatchingThatVerb() throws Exception
    {
        Feed feed = client.getAs("admin", activities(NOT, pair(job(), pass())), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), allEntries(not(withVerbElement(equalTo(pass().iri().toASCIIString())))));
    }

    @Test
    public void assertThatFeedWithMultipleFilterIsVerbParametersOnlyContainsEntriesMatchingEitherVerb() throws Exception
    {
        Feed feed = client.getAs("admin", activities(IS, pair(job(), pass()), pair(job(), fail())), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), allEntries(anyOf(
            withVerbElement(equalTo(pass().iri().toASCIIString())),
            withVerbElement(equalTo(fail().iri().toASCIIString()))
        )));
    }

    @Test
    public void assertThatFeedWithIssueKeyContainsFindsBuildAssociatedWithJiraKey() throws Exception
    {
        Feed feed = client.get(issueKey(IS, "JST-16332"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(buildEntryForProjectAA(55)));
    }

    @Test
    public void assertThatFeedWithIssueKeyDoesNotContainFilterDoesNotFindBuildAssociatedWithJiraKey() throws Exception
    {
        Feed feed = client.get(issueKey(NOT, "JST-16332"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), not(hasEntries(buildEntryForProjectAA(55))));
    }

    @Test
    public void assertThatFeedWithMultipleIssueKeysFindsBuildsAssociatedWithJiraKeys() throws Exception
    {
        Feed feed = client.get(issueKey(IS, "JST-16257", "JST-16332"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(buildEntryForProjectAA(55), buildEntryForProjectAA(54)));
    }

    @Test
    public void assertThatStreamTitleIsForProjectIfFilterIsDefined()
    {
        Feed feed = client.get(key(IS, "AA"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getTitle(), is(equalTo("Activity Stream for Atlassian Anarchy")));
    }

    @Test
    public void assertThatStreamTitleShowsMultipleProjectNameIfMultipleKeyIsDefined()
    {
        Feed feed = client.get(key(IS, "AA", "AP"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getTitle(), is(equalTo("Activity Stream for Atlassian Anarchy and Another Project")));
    }

    @Test
    public void assertThatUsingNottedProjectKeyFindsNoBuildEntriesForProjectAA()
    {
        Feed feed = client.get(key(NOT, "AA"), maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), allOf(not(hasEntries(allBuildEntriesForProjectAA())),
                                            hasEntries(allBuildEntriesForProjectAP())));
    }

    @Test
    public void assertThatUsingProjectKeysFindsAllBuildEntriesForProjectAA()
    {
        Feed feed = client.get(key(IS, "AA"), maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(allBuildEntriesForProjectAA()));
    }

    @Test
    public void assertThatUsingProjectKeysFindsAllBuildEntriesForProjectAP()
    {
        Feed feed = client.get(key(IS, "AP"), maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(allBuildEntriesForProjectAP()));
    }

    @Test
    public void assertThatUsingProjectKeyAndUserFilterFindsAllBuildEntriesForProjectAPAndUserTestAdmin()
    {
        Feed feed = client.get(key(IS, "AP"), user(IS, "testadmin"), provider(BAMBOO_PROVIDER));
        // returns all the builds except the last (which is not triggered by testadmin)
        assertThat(feed.getEntries(), hasEntries(allBuildEntriesForProjectAP()));
    }
    
    @Test
    public void assertThatUsingProjectKeyAndBambooUserNameFindsAllBuildEntriesForProjectAPAndUserTestAdmin()
    {
        //Repository alias mapping added to test data: admin -> testadmin
        Feed feed = client.get(key(IS, "AP"), user(IS, "admin"), provider(BAMBOO_PROVIDER));
        // returns all the builds except the last (which is not triggered by testadmin)
        assertThat(feed.getEntries(), hasEntries(allBuildEntriesForProjectAP()));
    }

    @Test
    public void assertThatUsingNottedUserFilterDoesNotReturnBuildEntryByUser()
    {
        Feed feed = client.get(user(NOT, "testadmin"), provider(BAMBOO_PROVIDER));
        // the first AA build is the only build without code changes by testadmin
        assertThat(feed.getEntries(), hasEntries(buildEntryForProjectAA(1)));
    }

    @Test
    public void assertThatUsingFilterUserWithInvalidUsernameReturnsZeroEntries()
    {
        Feed feed = client.get(key(IS, "invaliduser"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries().size(), is(equalTo(0)));
    }

    @Test
    public void assertThatFeedWithMinDateContainsEntriesAfterTheMinDate() throws Exception
    {
        Feed feed = client.get(updateDate(AFTER, BUILD_56.minusSeconds(1)), provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(
                buildEntryForProjectAA(57),
                buildEntryForProjectAA(56)));
    }

    @Test
    public void assertThatFeedWithMaxDateContainsEntriesBeforeTheMaxDate() throws Exception
    {
        Feed feed = client.get(updateDate(BEFORE, BUILD_2.plusSeconds(1)), provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(
                buildEntryForProjectAP(2),
                buildEntryForProjectAA(2),
                buildEntryForProjectAP(1),
                buildEntryForProjectAA(1)));
    }

    @Test
    public void assertThatFeedWithMaxDateAndProjectKeyContainsEntriesBeforeTheMaxDateForProject() throws Exception
    {
        Feed feed = client.get(key(IS, "AP"), updateDate(BEFORE, BUILD_2.plusSeconds(1)), provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(
                buildEntryForProjectAP(2),
                buildEntryForProjectAP(1)));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateAndProjectKeyContainsEntriesWithinTheDateRangeForProject() throws Exception
    {
        Feed feed = client.get(key(IS, "AA"),
            updateDate(AFTER, BUILD_2.minusSeconds(1)),
            updateDate(BEFORE, BUILD_2.plusSeconds(1)),
            provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(buildEntryForProjectAA(2)));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateContainsEntriesWithinTheDateRange() throws Exception
    {
        Feed feed = client.get(
            updateDate(AFTER, BUILD_41.minusSeconds(1)),
            updateDate(BEFORE, BUILD_43.plusSeconds(1)),
            provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(
                buildEntryForProjectAA(43),
                buildEntryForProjectAA(42),
                buildEntryForProjectAA(41)));
    }

    @Test
    public void assertThatFeedWithDateRangeContainsEntriesWithinTheDateRange() throws Exception
    {
        Feed feed = client.get(
            updateDate(BETWEEN, BUILD_41.minusSeconds(1), BUILD_43.plusSeconds(1)),
            provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(
                buildEntryForProjectAA(43),
                buildEntryForProjectAA(42),
                buildEntryForProjectAA(41)));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateAndMaxResultsContainsMaximumAmountOfEntriesWithinTheDateRange() throws Exception
    {
        Feed feed = client.get(maxResults(2),
            updateDate(AFTER, BUILD_41.minusSeconds(1)),
            updateDate(BEFORE, BUILD_43.plusSeconds(1)),
            provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(
                buildEntryForProjectAA(43),
                buildEntryForProjectAA(42)));
    }

    @Test
    public void assertThatStreamWithMaxOfThreeAndProjectAPResultsContainsLastFiveBuildsOfProjectAP()
    {
        Feed feed = client.get(key(IS, "AP"), maxResults(3), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(buildEntriesForProjectAP(ProjectAPResult.NUM_BUILDS - 2)));
    }

    @Test
    public void assertThatStreamFilteredByUserDoesNotHaveFormatBraces()
    {
        Feed feed = client.get(user(IS, "rwallace"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), not(hasEntry(
                withTitle(anyOf(containsString("{"),
                                containsString("}"))))));

    }
}
