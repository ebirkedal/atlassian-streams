package it.com.atlassian.streams.crucible;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.AbstractFeedClient.Parameter;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import org.apache.abdera.ext.thread.InReplyTo;
import org.apache.abdera.i18n.iri.IRI;
import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.crucible.CrucibleActivityObjectTypes.review;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.start;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.Crucible.Data.CR_1_CREATE;
import static com.atlassian.streams.testing.Crucible.Data.CR_3_CREATE;
import static com.atlassian.streams.testing.Crucible.Data.CR_4_CLOSE;
import static com.atlassian.streams.testing.Crucible.Data.CR_4_CREATE;
import static com.atlassian.streams.testing.Crucible.Data.CR_7_COMMENT;
import static com.atlassian.streams.testing.Crucible.Data.CR_7_CREATE;
import static com.atlassian.streams.testing.Crucible.Data.CR_7_THREAD_COMMENT;
import static com.atlassian.streams.testing.Crucible.Data.allCrucibleDefaultProjectEntries;
import static com.atlassian.streams.testing.Crucible.Data.allCrucibleEntries;
import static com.atlassian.streams.testing.Crucible.Data.review1Created;
import static com.atlassian.streams.testing.Crucible.Data.review2Created;
import static com.atlassian.streams.testing.Crucible.Data.review3Created;
import static com.atlassian.streams.testing.Crucible.Data.review3CreatedOnly;
import static com.atlassian.streams.testing.Crucible.Data.review3Summarized;
import static com.atlassian.streams.testing.Crucible.Data.review4Closed;
import static com.atlassian.streams.testing.Crucible.Data.review4Created;
import static com.atlassian.streams.testing.Crucible.Data.review7CommentCommented;
import static com.atlassian.streams.testing.Crucible.Data.review7Commented;
import static com.atlassian.streams.testing.Crucible.Data.review7Created;
import static com.atlassian.streams.testing.Crucible.Data.review8Abandoned;
import static com.atlassian.streams.testing.Crucible.Data.review8Created;
import static com.atlassian.streams.testing.Crucible.Data.review9Created;
import static com.atlassian.streams.testing.Crucible.Data.reviewStrm1Created;
import static com.atlassian.streams.testing.Crucible.activities;
import static com.atlassian.streams.testing.Crucible.crucibleModule;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.updateDate;
import static com.atlassian.streams.testing.FeedClient.user;
import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.hasNoEntries;
import static com.atlassian.streams.testing.matchers.Matchers.withActivityObjectElement;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withType;
import static com.atlassian.streams.testing.matchers.Matchers.withVerbElement;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.isEmpty;
import static org.apache.abdera.ext.thread.ThreadConstants.IN_REPLY_TO;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(FECRU)
public class CrucibleFiltersActivityStreamTest
{
    @Inject static FeedClient feedClient;

    @Test
    public void assertThatFeedWithFilterIsStartOrCreateReviewActivityContainsOnlyEntriesWithStartCreateOrBothInTitle()
    {
        Feed feed = feedClient.getAs(
                "admin", crucibleModule(), activities(IS, pair(review(), start()), pair(review(), post())));
        assertThat(feed.getEntries(), allEntries(
                withTitle(anyOf(containsString("created review"), containsString("started review")))));
    }

    /**
     * Verifies that, in this scenario, CR-2's "created and started" event is displayed as "started".
     */
    @Test
    public void assertThatFeedWithFilterIsStartReviewActivityContainsOnlyEntriesWithStartInTitleIncludingFromCreateAndStartedEntry()
    {
        Feed feed = feedClient.getAs(
                "admin", crucibleModule(), activities(IS, pair(review(), start())));
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("started review"))));
    }

    /**
     * Verifies that, in this scenario, CR-2's "created and started" event is displayed as "created".
     */
    @Test
    public void assertThatFeedWithFilterIsCreateReviewActivityContainsOnlyEntriesWithCreateInTitleIncludingFromCreateAndStartedEntry()
    {
        Feed feed = feedClient.getAs(
                "admin", crucibleModule(), activities(IS, pair(review(), post())));
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("created review"))));
    }

    @Test
    public void assertThatFeedWithFilterIsUserParameterOnlyContainsEntriesMatchingThatUser() throws Exception
    {
        Feed feed = feedClient.getAs("admin", user(IS, "admin"), crucibleModule());
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("admin"))));
    }

    @Test
    public void assertThatFeedWithFilterNotUserParameterOnlyContainsEntriesNotMatchingThatUser() throws Exception
    {
        Feed feed = feedClient.getAs("admin", user(NOT, "admin"), crucibleModule());
        assertThat(feed.getEntries(), allEntries(not(withTitle(containsString("admin")))));
    }

    @Test
    public void assertThatFeedWithFilterNotCreateReviewActivityOnlyContainsEntriesNotMatchingThatActivity() throws Exception
    {
        Feed feed = feedClient.getAs("admin", activities(NOT, pair(review(), post())), crucibleModule());
        assertThat(feed.getEntries(), allEntries(not(allOf(
                withVerbElement(equalTo(post().iri().toASCIIString())),
                withActivityObjectElement(withType(equalTo(review().iri().toASCIIString())))))));
    }

    @Test
    public void assertThatFeedWithKeyParameterOnlyContainsEntriesWithThatKey() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), key(IS, "CR"), maxResults(20));
        assertThat(feed.getEntries(), hasEntries(allCrucibleDefaultProjectEntries()));
    }

    @Test
    public void assertThatFeedWithNotKeyParameterContainsNoEntriesWithThatKey() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), key(NOT, "CR"));
        assertThat(feed.getEntries(), hasEntries(reviewStrm1Created()));
    }

    @Test
    public void assertThatFeedWithInvalidKeyParameterContainsNoEntries() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), key(IS, "ABC"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFeedWithIssueKeyParameterOnlyContainsEntriesWithThatFilterKey() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), issueKey(IS, "TST-1"));
        assertThat(feed.getEntries(), hasEntries(review1Created()));
    }

    @Test
    public void assertThatFeedWithNonExistentIssueKeyParameterContainsNoEntries() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), issueKey(IS, "RandomText"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFeedWithNotIssueKeyFilterDoesNotContainEntriesWithThatIssueKey()
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), issueKey(NOT, "TST-1"));
        assertThat(feed.getEntries(), not(hasEntry(review1Created())));
    }

    @Test
    public void assertThatFeedWithFilterUserParameterOnlyContainsEntriesMatchingThatUser() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), user(IS, "admin"), maxResults(20));
        assertThat(feed.getEntries(), hasEntries(allCrucibleEntries()));
    }

    @Test
    @Ignore("Uncomment when review comment in CR-FE-4471 regarding invalid username has been applied")
    public void assertThatFeedWithInvalidUserParameterContainsNoEntries() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), user(IS, "someuser"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFeedWithMinDateOnlyContainsEntriesAfterTheMinDate() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), updateDate(AFTER, CR_7_CREATE.minusSeconds(1)));
        assertThat(feed.getEntries(), hasEntries(ImmutableList.<Matcher<? super Entry>>of(
                reviewStrm1Created(),
                review9Created(),
                review8Abandoned(),
                review8Created(),
                review7CommentCommented(),
                review7Commented(),
                review7Created())));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxResultsOnlyContainsEntriesAfterTheMinDateAndNoMoreThanMaxResults() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(),
                updateDate(AFTER, CR_7_CREATE.minusSeconds(1)),
                maxResults(1));
        assertThat(feed.getEntries(), hasEntries(reviewStrm1Created()));
    }

    @Test
    public void assertThatFeedWithMaxDateOnlyContainsEntriesBeforeTheMaxDate() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), updateDate(BEFORE, CR_3_CREATE.plusSeconds(1)));
        assertThat(feed.getEntries(), hasEntries(review3CreatedOnly(),
                review2Created(),
                review1Created()));
    }

    @Test
    public void assertThatFeedWithMaxDateAndMaxResultsOnlyContainsEntriesBeforeTheMaxDateAndNoMoreThanMaxResults() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(),
                updateDate(BEFORE, CR_3_CREATE.plusSeconds(1)),
                maxResults(2));
        assertThat(feed.getEntries(), hasEntries(review3CreatedOnly(), review2Created()));
    }

    @Test
    public void assertThatFeedWithDateRangeOnlyContainsEntriesAfterTheMinDateAndBeforeTheMaxDate() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(),
                updateDate(AFTER, CR_4_CREATE.minusSeconds(1)),
                updateDate(BEFORE, CR_4_CLOSE.plusSeconds(1)));
        assertThat(feed.getEntries(), hasEntries(review4Closed(), review4Created()));
    }

    @Test
    public void assertThatFeedWithDateRangeAndMaxResultsOnlyContainsEntriesAfterTheMinDateAndBeforeTheMaxDateAndNoMoreThanMaxResults() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(),
                updateDate(AFTER, CR_1_CREATE.minusSeconds(1)),
                updateDate(BEFORE, CR_4_CLOSE.plusSeconds(1)),
                maxResults(4));
        assertThat(feed.getEntries(), hasEntries(review4Closed(),
                review4Created(),
                review3Summarized(),
                review3Created()));
    }

    @Test
    public void assertThatFeedCommentEntryHasInReplyToUriSimilarToReviewUri() throws Exception
    {
        Entry cr7Create = getFirstEntry(crucibleModule(),
                local(),
                updateDate(AFTER, CR_7_CREATE.minusSeconds(1)),
                updateDate(BEFORE, CR_7_CREATE.plusSeconds(1)));
        Entry cr7Comment = getFirstEntry(crucibleModule(),
                local(),
                updateDate(AFTER, CR_7_COMMENT.minusSeconds(1)),
                updateDate(BEFORE, CR_7_COMMENT.plusSeconds(1)));

        assertThat(getInReplyToRef(cr7Comment), is(equalTo(cr7Create.getId())));
    }

    @Test
    public void assertThatFeedThreadedCommentEntryHasInReplyToUriSimilarToCommentUri() throws Exception
    {
        Entry cr7Comment = getFirstEntry(crucibleModule(),
                local(),
                updateDate(AFTER, CR_7_COMMENT.minusSeconds(1)),
                updateDate(BEFORE, CR_7_COMMENT.plusSeconds(1)));
        Entry cr7ThreadedComment = getFirstEntry(crucibleModule(),
                local(),
                updateDate(AFTER, CR_7_THREAD_COMMENT.minusSeconds(1)),
                updateDate(BEFORE, CR_7_THREAD_COMMENT.plusSeconds(1)));

        assertThat(getInReplyToRef(cr7ThreadedComment), is(equalTo(cr7Comment.getId())));
    }

    private Entry getFirstEntry(Parameter... params)
    {
        Feed feed = feedClient.getAs("admin", params);
        if (feed != null && !isEmpty(feed.getEntries()))
        {
            return get(feed.getEntries(), 0);
        }
        return null;
    }

    private IRI getInReplyToRef(Entry entry)
    {
        InReplyTo inReplyTo = entry.getExtension(IN_REPLY_TO);
        if (inReplyTo != null && inReplyTo.getRef() != null)
        {
            return inReplyTo.getRef();
        }
        return null;
    }
}
