package it.com.atlassian.streams.jira;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/edit-multiple-issue-fields-no-comment.xml")
public class EditMultipleIssueFieldsTest
{
    @Inject static FeedClient client;

    @Test
    public void assertThatFeedContainsMultipleUpdateEntryWhenMultipleFieldsAreUpdated() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("updated 6 fields of"), containsString("ONE-1"))),
            withContent(allOf(containsString("Changed the Summary to 'new summary'"),
                    containsString("Changed the Issue Type to 'Improvement'"),
                    containsString("Updated the Environment"),
                    containsString("Changed the Priority to 'Blocker'"),
                    containsString("Updated the Description"),
                    containsString("Changed the Due Date")))));
    }

    @Test
    public void assertThatFeedEntryContainsFormattedDueDate() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("updated 6 fields of"), containsString("ONE-1"))),
            withContent(allOf(containsString("Changed the Summary to 'new summary'"),
                    containsString("Changed the Due Date to '28/Oct/10'")))));
    }

    @Test
    public void assertThatFeedContainsDoesNotContainWorkflowUpdate() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), not(hasEntry(withContent(containsString("Workflow")))));
    }

    @Test
    @Restore("jira/backups/edit-multiple-issue-fields-with-comment.xml")
    public void assertThatFeedContainsMultipleUpdateEntryWithCommentWhenMultipleFieldsAreUpdatedWithComment() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("updated 6 fields of"), containsString("ONE-1"))),
            withContent(allOf(ImmutableList.<Matcher<? super String>>of(
                    containsString("new comment"),
                    containsString("Changed the Summary to 'new summary'"),
                    containsString("Changed the Issue Type to 'Improvement'"),
                    containsString("Updated the Environment"),
                    containsString("Changed the Priority to 'Blocker'"),
                    containsString("Updated the Description"),
                    containsString("Changed the Due Date to '28/Oct/10'"))))));
    }

}
