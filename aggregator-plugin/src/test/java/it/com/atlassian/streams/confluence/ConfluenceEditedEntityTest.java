package it.com.atlassian.streams.confluence;

import java.io.IOException;
import java.net.URISyntaxException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.Confluence.CONFLUENCE_PROVIDER;
import static com.atlassian.streams.testing.Confluence.activities;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/edited-entities.zip")
public class ConfluenceEditedEntityTest
{
    private static final String EDITED_PAGE_TITLE = "Testing for updated page content";
    private static final String EDITED_BLOG_TITLE = "Testing updating blog post";

    @Inject static FeedClient feedClient;

    @Test
    public void assertThatEditedPageHasValidSummary()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("admin"), containsString(EDITED_PAGE_TITLE))),
                withContent(containsString("view change"))));
    }

    @Test
    public void assertThatEditedBlogPostHasValidSummary()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), hasEntry(allOf(
            withTitle(allOf(containsString("admin"),
                            containsString("edited blog"),
                            containsString(EDITED_BLOG_TITLE))),
            withContent(containsString("view change")))));
    }

    @Test
    public void assertThatStatusUpdatesShowUpInActivityStream()
    {
        // retrieve activity streams
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("admin"), containsString("updated their status"))),
                withContent(allOf(
                    containsString("is browsing"),
                    containsString("href=\"http://www.google.com\""),
                    containsString("class=\"external-link\""),
                    containsString("rel=\"nofollow\""),
                    containsString("http://www.google.com</a>")))));
    }

    @Test
    public void assertThatRenamingPageHasRenameContentAndViewChange() throws IOException, URISyntaxException
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());

        assertThat(feed.getEntries(), hasEntry(allOf(
                withTitle(allOf(
                    containsString("admin"),
                    containsString("edited"),
                    containsString("My Renamed Page"))),
                withContent(allOf(
                    containsString("&quot;My Page&quot; renamed to &quot;My Renamed Page&quot;"),
                    containsString("view change"))))));
    }

    /**
     * STRM-1649 Edited comments were not appearing properly in the feed.
     * However, since we don't support showing multiple revisions of comments, we display them
     * the same as newly added (e.g. not edited) comments.
     */
    @Test
    public void assertThatEditedCommentAppearsInFeed()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER),  activities(IS, pair(comment(), post())));
        assertThat(feed.getEntries(), hasEntry(allOf(
                withTitle(allOf(
                    containsString("admin"),
                    containsString("commented on"),
                    containsString("My First Blog"))),
                withContent(allOf(
                    containsString("<blockquote>"),
                    containsString("Awesome blog!"),
                    containsString("</blockquote>"))))));
    }
}
