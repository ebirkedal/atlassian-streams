package it.com.atlassian.streams.bamboo;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.Bamboo.Data.ProjectAPResult;
import com.atlassian.streams.testing.LegacyFeedClient;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.Bamboo.BAMBOO_PROVIDER;
import static com.atlassian.streams.testing.Bamboo.Data.allBuildEntriesForProjectAA;
import static com.atlassian.streams.testing.Bamboo.Data.allBuildEntriesForProjectAP;
import static com.atlassian.streams.testing.Bamboo.Data.buildEntriesForProjectAP;
import static com.atlassian.streams.testing.Bamboo.Data.buildEntryForProjectAA;
import static com.atlassian.streams.testing.Bamboo.Data.buildEntryForProjectAP;
import static com.atlassian.streams.testing.Bamboo.Data.ProjectAAResult.BUILD_2;
import static com.atlassian.streams.testing.Bamboo.Data.ProjectAAResult.BUILD_41;
import static com.atlassian.streams.testing.Bamboo.Data.ProjectAAResult.BUILD_43;
import static com.atlassian.streams.testing.Bamboo.Data.ProjectAAResult.BUILD_56;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.LegacyFeedClient.filter;
import static com.atlassian.streams.testing.LegacyFeedClient.key;
import static com.atlassian.streams.testing.LegacyFeedClient.maxDate;
import static com.atlassian.streams.testing.LegacyFeedClient.minDate;
import static com.atlassian.streams.testing.LegacyFeedClient.user;
import static com.atlassian.streams.testing.StreamsTestGroups.BAMBOO;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(BAMBOO)
public class BambooLegacyFiltersActivityStreamTest
{
    @Inject static LegacyFeedClient client;
    @Inject static RestTester restTester;

    @Test
    public void assertThatFeedWithFilterContainsFindsBuildAssociatedWithJiraKey() throws Exception
    {
        Feed feed = client.get(filter("JST-16332"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(buildEntryForProjectAA(55)));
    }

    @Test
    public void assertThatFeedWithMultipleFilterContainsFindsBuildsAssociatedWithJiraKeys() throws Exception
    {
        Feed feed = client.get(filter("JST-16257"), filter("JST-16332"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(buildEntryForProjectAA(55), buildEntryForProjectAA(54)));
    }

    @Test
    public void assertThatStreamTitleIsForProjectIfFilterIsDefined()
    {
        Feed feed = client.get(key("AA"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getTitle(), is(equalTo("Activity Stream for Atlassian Anarchy")));
    }

    @Test
    public void assertThatStreamTitleShowsMultipleProjectNameIfMultipleKeyIsDefined()
    {
        Feed feed = client.get(key("AA"), key("AP"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getTitle(), is(equalTo("Activity Stream for Atlassian Anarchy and Another Project")));
    }

    @Test
    public void assertThatUsingProjectKeysFindsAllBuildEntriesForProjectAA()
    {
        Feed feed = client.get(key("AA"), maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(allBuildEntriesForProjectAA()));
    }

    @Test
    public void assertThatUsingProjectKeysFindsAllBuildEntriesForProjectAP()
    {
        Feed feed = client.get(key("AP"), maxResults(100), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(allBuildEntriesForProjectAP()));
    }

    @Test
    public void assertThatUsingProjectKeyAndUserFilterFindsAllBuildEntriesForProjectAPAndUserTestAdmin()
    {
        Feed feed = client.get(key("AP"), user("testadmin"), provider(BAMBOO_PROVIDER));
        // returns all the builds except the last (which is not triggered by testadmin)
        assertThat(feed.getEntries(), hasEntries(allBuildEntriesForProjectAP()));
    }

    @Test
    public void assertThatUsingFilterUserWithInvalidUsernameReturnsZeroEntries()
    {
        Feed feed = client.get(key("invaliduser"), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries().size(), is(equalTo(0)));
    }

    @Test
    public void assertThatFeedWithMinDateContainsEntriesAfterTheMinDate() throws Exception
    {
        Feed feed = client.get(minDate(BUILD_56.minusSeconds(1)), provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(
                buildEntryForProjectAA(57),
                buildEntryForProjectAA(56)));
    }

    @Test
    public void assertThatFeedWithMaxDateContainsEntriesBeforeTheMaxDate() throws Exception
    {
        Feed feed = client.get(maxResults(100), maxDate(BUILD_2.plusSeconds(1)), provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(
                buildEntryForProjectAP(2),
                buildEntryForProjectAA(2),
                buildEntryForProjectAP(1),
                buildEntryForProjectAA(1)));
    }

    @Test
    public void assertThatFeedWithMaxDateAndProjectKeyContainsEntriesBeforeTheMaxDateForProject() throws Exception
    {
        Feed feed = client.get(key("AP"), maxDate(BUILD_2.plusSeconds(1)), provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(
                buildEntryForProjectAP(2),
                buildEntryForProjectAP(1)));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateAndProjectKeyContainsEntriesWithinTheDateRangeForProject() throws Exception
    {
        Feed feed = client.get(key("AA"), maxResults(100),
            minDate(BUILD_2.minusSeconds(1)),
            maxDate(BUILD_2.plusSeconds(1)),
            provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(buildEntryForProjectAA(2)));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxDateContainsEntriesWithinTheDateRange() throws Exception
    {
        Feed feed = client.get(
            minDate(BUILD_41.minusSeconds(1)),
            maxDate(BUILD_43.plusSeconds(1)),
            provider(BAMBOO_PROVIDER));

        assertThat(feed.getEntries(), hasEntries(
                buildEntryForProjectAA(43),
                buildEntryForProjectAA(42),
                buildEntryForProjectAA(41)));
    }

    @Test
    public void assertThatStreamWithMaxOfThreeAndProjectAPResultsContainsLastFiveBuildsOfProjectAP()
    {
        Feed feed = client.get(key("AP"), maxResults(3), provider(BAMBOO_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(buildEntriesForProjectAP(ProjectAPResult.NUM_BUILDS - 2)));
    }
}
