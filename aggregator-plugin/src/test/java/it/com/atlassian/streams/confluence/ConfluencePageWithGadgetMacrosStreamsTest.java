package it.com.atlassian.streams.confluence;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.Confluence.CONFLUENCE_PROVIDER;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/page-with-gadget-macros.zip")
public class ConfluencePageWithGadgetMacrosStreamsTest
{
    private static final String STREAM_WITHIN_STREAM_PAGE_TITLE = "Stream within a Stream";
    private static final String PAGE_WITH_GADGET_MACROS_TITLE = "Page with Gadget Macros";

    @Inject static FeedClient feedClient;

    @Test
    public void assertThatActivityStreamsWithinAnActivityStreamIsStripped()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), hasEntry(allOf(
                withTitle(allOf(containsString("admin"),
                        containsString("added"),
                        containsString(STREAM_WITHIN_STREAM_PAGE_TITLE))),
                withContent(not(containsString("activitystream-gadget.xml"))))));
    }

    @Test
    public void assertThatActivityStreamsWithinAnActivityStreamDoesNotStripOtherContents()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), hasEntry(allOf(
                withTitle(allOf(containsString("admin"),
                        containsString("added"),
                        containsString(STREAM_WITHIN_STREAM_PAGE_TITLE))),
                withContent(allOf(containsString("activity streams should not show up!"),
                                  containsString("ARE YOU SURE?"),
                                  containsString("I am!!"),
                                  containsString("yeah!!"),
                                  containsString("ok, good!"))))));
    }

    @Test
    public void assertThatConfluencePageGadgetDoesNotShowUpInActivityStreams()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), hasEntry(allOf(
                withTitle(allOf(containsString("admin"),
                        containsString("added"),
                        containsString(PAGE_WITH_GADGET_MACROS_TITLE))),
                withContent(not(containsString("confluence-page-gadget.xml"))))));
    }

    @Test
    public void assertThatConfluenceQuickNavGadgetDoesNotShowUpInActivityStreams()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), hasEntry(allOf(
                withTitle(allOf(containsString("admin"),
                        containsString("added"),
                        containsString(PAGE_WITH_GADGET_MACROS_TITLE))),
                withContent(not(containsString("gadget-search.xml"))))));
    }

    @Test
    public void assertThatConfluenceNewsGadgetDoesNotShowUpInActivityStreams()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local());
        assertThat(feed.getEntries(), hasEntry(allOf(
                withTitle(allOf(containsString("admin"),
                        containsString("added"),
                        containsString(PAGE_WITH_GADGET_MACROS_TITLE))),
                withContent(not(containsString("confluence-news-gadget.xml"))))));
    }
}
