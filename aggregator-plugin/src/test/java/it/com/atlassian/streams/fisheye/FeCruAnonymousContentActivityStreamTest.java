package it.com.atlassian.streams.fisheye;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.Crucible.Data.review8Created;
import static com.atlassian.streams.testing.Crucible.Data.reviewStrm1Created;
import static com.atlassian.streams.testing.FishEye.Data.changeSet5;
import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(FECRU)
public class FeCruAnonymousContentActivityStreamTest
{
    @Inject static FeedClient feedClient;

    @Test
    public void assertThatAnonymousCrucibleContentAppearsInFeed()
    {
        //CR-STRM project is public
        assertThat(feedClient.get().getEntries(), hasEntry(reviewStrm1Created()));
    }

    @Test
    public void assertThatPrivateCrucibleContentDoesNotAppearsInFeed()
    {
        //CR project is private
        assertThat(feedClient.get().getEntries(), not(hasEntry(review8Created())));
    }

    @Test
    public void assertThatAnonymousFishEyeContentAppearsInFeed()
    {
        //TST repo has been made public by default
        assertThat(feedClient.get().getEntries(), hasEntry(changeSet5()));
    }
}
