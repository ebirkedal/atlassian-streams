package it.com.atlassian.streams.crucible;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.LegacyFeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.Crucible.Data.CR_3_CREATE;
import static com.atlassian.streams.testing.Crucible.Data.CR_4_CLOSE;
import static com.atlassian.streams.testing.Crucible.Data.CR_4_CREATE;
import static com.atlassian.streams.testing.Crucible.Data.CR_7_CREATE;
import static com.atlassian.streams.testing.Crucible.Data.allCrucibleDefaultProjectEntries;
import static com.atlassian.streams.testing.Crucible.Data.allCrucibleEntries;
import static com.atlassian.streams.testing.Crucible.Data.review1Created;
import static com.atlassian.streams.testing.Crucible.Data.review2Created;
import static com.atlassian.streams.testing.Crucible.Data.review3CreatedOnly;
import static com.atlassian.streams.testing.Crucible.Data.review4Closed;
import static com.atlassian.streams.testing.Crucible.Data.review4Created;
import static com.atlassian.streams.testing.Crucible.Data.review7CommentCommented;
import static com.atlassian.streams.testing.Crucible.Data.review7Commented;
import static com.atlassian.streams.testing.Crucible.Data.review7Created;
import static com.atlassian.streams.testing.Crucible.Data.review8Abandoned;
import static com.atlassian.streams.testing.Crucible.Data.review8Created;
import static com.atlassian.streams.testing.Crucible.Data.review9Created;
import static com.atlassian.streams.testing.Crucible.Data.reviewStrm1Created;
import static com.atlassian.streams.testing.Crucible.crucibleModule;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.LegacyFeedClient.filter;
import static com.atlassian.streams.testing.LegacyFeedClient.itemKey;
import static com.atlassian.streams.testing.LegacyFeedClient.key;
import static com.atlassian.streams.testing.LegacyFeedClient.maxDate;
import static com.atlassian.streams.testing.LegacyFeedClient.minDate;
import static com.atlassian.streams.testing.LegacyFeedClient.user;
import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasNoEntries;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(FECRU)
public class CrucibleLegacyFiltersActivityStreamTest
{
    @Inject static LegacyFeedClient feedClient;

    @Test
    public void assertThatFeedWithFilterIsUserParameterOnlyContainsEntriesMatchingThatUser() throws Exception
    {
        Feed feed = feedClient.getAs("admin", user("admin"), crucibleModule());
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("admin"))));
    }

    @Test
    public void assertThatFeedWithKeyParameterOnlyContainsEntriesWithThatKey() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), key("CR"), maxResults(20));
        assertThat(feed.getEntries(), hasEntries(allCrucibleDefaultProjectEntries()));
    }

    @Test
    public void assertThatFeedWithInvalidKeyParameterContainsNoEntries() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), key("ABC"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFeedWithItemKeyParameterOnlyContainsEntriesWithThatItemKey() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), itemKey("Sample"));
        assertThat(feed.getEntries(), hasEntries(review1Created()));
    }

    @Test
    public void assertThatFeedWithInvalidItemKeyParameterContainsNoEntries() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), itemKey("ABC-1"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFeedWithFilterKeyParameterOnlyContainsEntriesWithThatFilterKey() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), filter("Sample"));
        assertThat(feed.getEntries(), hasEntries(review1Created()));
    }

    @Test
    public void assertThatFeedWithNonExistentFilterKeyParameterContainsNoEntries() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), filter("RandomText"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFeedWithFilterUserParameterOnlyContainsEntriesMatchingThatUser() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), user("admin"), maxResults(20));
        assertThat(feed.getEntries(), hasEntries(allCrucibleEntries()));
    }

    @Test
    @Ignore("Uncomment when review comment in CR-FE-4471 regarding invalid username has been applied")
    public void assertThatFeedWithInvalidUserParameterContainsNoEntries() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), user("someuser"));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFeedWithMinDateOnlyContainsEntriesAfterTheMinDate() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), minDate(CR_7_CREATE.minusSeconds(1)));
        assertThat(feed.getEntries(), hasEntries(ImmutableList.<Matcher<? super Entry>>of(reviewStrm1Created(),
                review9Created(),
                review8Abandoned(),
                review8Created(),
                review7CommentCommented(),
                review7Commented(),
                review7Created())));
    }

    @Test
    public void assertThatFeedWithMinDateAndMaxResultsOnlyContainsEntriesAfterTheMinDateAndNoMoreThanMaxResults() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(),
                minDate(CR_7_CREATE.minusSeconds(1)),
                maxResults(1));
        assertThat(feed.getEntries(), hasEntries(reviewStrm1Created()));
    }

    @Test
    public void assertThatFeedWithMaxDateOnlyContainsEntriesBeforeTheMaxDate() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(), maxDate(CR_3_CREATE.plusSeconds(1)));
        assertThat(feed.getEntries(), hasEntries(review3CreatedOnly(), review2Created(), review1Created()));
    }

    @Test
    public void assertThatFeedWithDateRangeOnlyContainsEntriesAfterTheMinDateAndBeforeTheMaxDate() throws Exception
    {
        Feed feed = feedClient.getAs("admin", crucibleModule(), local(),
                minDate(CR_4_CREATE.minusSeconds(1)),
                maxDate(CR_4_CLOSE.plusSeconds(1)));
        assertThat(feed.getEntries(), hasEntries(review4Closed(), review4Created()));
    }
}
