package it.com.atlassian.streams.jira;

import java.io.IOException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.jira.JiraStreamsActivityProvider.ISSUE_VOTE_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.REPLY_TO_LINK_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.WATCH_LINK_REL;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.haveLink;
import static com.atlassian.streams.testing.matchers.Matchers.whereRel;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withHtmlTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withLink;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/remote-issue-linked.xml")
public class RemoteIssueLinkedTest
{
    @Inject static FeedClient client;
    private final static String LINK_TARGET_URL = "http://www.google.com";

    @Test
    public void assertThatCreatedRemoteIssueLinkHasCorrectTitle() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withHtmlTitle(allOf(
                containsString("admin"),
                containsString("created a link from"),
                containsString("ONE-1 - Issue One"),
                containsString("to"),
                containsString("Google Search"),
                containsString("(Web Link)"),
                containsString(LINK_TARGET_URL)))));
    }

    @Test
    public void assertThatUpdatedRemoteIssueLinkHasCorrectTitle() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withHtmlTitle(allOf(
                containsString("admin"),
                containsString("updated a link from"),
                containsString("ONE-1 - Issue One"),
                containsString("to"),
                containsString("Google Search"),
                containsString("(Web Link)"),
                containsString(LINK_TARGET_URL)))));
    }

    @Test
    public void assertThatRemoteIssueLinkWithCommentShowsComment() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(
                containsString("admin"),
                containsString("created a link from"),
                containsString("ONE-1 - Issue One"),
                containsString("to"),
                containsString("Example"),
                containsString("(Web Link)"))),
            withContent(containsString("Created example link"))));
    }

    @Test
    public void assertThatRemoteIssueLinksHaveReplyToLink() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(
            withTitle(anyOf(
                containsString("created a link from"),
                containsString("updated a link from"))),
            haveLink(whereRel(is(equalTo(REPLY_TO_LINK_REL))))));
    }

    @Test
    public void assertThatRemoteIssueLinksHaveWatchLink() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(
            withTitle(anyOf(
                containsString("created a link from"),
                containsString("updated a link from"))),
            haveLink(whereRel(is(equalTo(WATCH_LINK_REL))))));
    }

    @Test
    public void assertThatRemoteIssueLinksDoNotHaveVoteLink() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(
            withTitle(anyOf(
                containsString("created a link from"),
                containsString("updated a link from"))),
            not(withLink(whereRel(is(equalTo(ISSUE_VOTE_REL)))))));
    }

    @Test
    public void assertThatRemoteIssueLinksProperlyEscapesRemoteLinkTitle() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(
                containsString("admin"),
                containsString("created a link from"),
                containsString("ONE-2 - I like creating bugs."),
                containsString("to"),
                containsString("&lt;script&gt;alert(&#39;XSS&#39;)&lt;/script&gt;"),
                containsString("(Web Link)")))));
    }

    @Test
    public void assertThatRemoteIssueLinksProperlyEscapesRemoteLinkApplicationName() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(
                        containsString("admin"),
                        containsString("created a link from"),
                        containsString("ONE-1 - Issue One"),
                        containsString("to"),
                        containsString("DEMO-1"),
                        containsString("(&lt;script&gt;alert(&quot;XSS in AppLink Application Name&quot;);&lt;/script&gt;)")))));
    }
}
