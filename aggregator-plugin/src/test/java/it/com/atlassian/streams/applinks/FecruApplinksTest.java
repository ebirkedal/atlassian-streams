package it.com.atlassian.streams.applinks;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.testing.AppLinksTestRunner;
import com.atlassian.streams.testing.AppLinksTests;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;

import static org.hamcrest.Matchers.equalTo;

import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS_OAUTH;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@RunWith(FecruApplinksTest.Runner.class)
@TestGroups({APPLINKS, APPLINKS_OAUTH})
public class FecruApplinksTest extends AppLinksTests
{
    public static final class Runner extends AppLinksTestRunner
    {
        public Runner(Class<?> klass) throws InitializationError
        {
            super(klass, "fecru");
        }
    }

    @Override
    @Ignore("Fisheye won't be applinked to itself")
    public void assertThatFisheyeActivityIsInFeed() {}

    @Override
    @Ignore("Crucible won't be applinked to itself")
    public void assertThatCrucibleActivityIsInFeed() {}

    @Override
    @Test @Ignore
    public void assertThatOnlyEntriesFromCrucibleProjectCrOneAreInFeedWhenOneKeyIsSpecified()
    {
        fail("Making a request to FECRU for anything where key=ONE only includes the repository entries because" +
        		"crucible and fisheye aren't actually linked in any meaningful way");
    }

    @Override
    @Ignore("Fisheye won't have applinks filters for itself")
    public void assertThatFilterResourceHasAppLinksProviderFiltersFromFisheye() {}

    @Override
    @Ignore
    public void assertThatNoEntriesFromCrucibleProjectCrOneAreInFeedWhenOneKeyIsFiltered() {}

    @Override
    @Ignore("Crucible won't have applinks filters for itself")
    public void assertThatFilterResourceHasAppLinksProviderFiltersFromCrucible() {}

    // STRM-1349 : Make sure there's no self linked provider in crucible
    @Test
    public void assertThatFecruFilterResourceDoesNotHaveProviderFilterFromCrucible()
    {
        assertThat(restTester.getStreamsConfigRepresentation(), not(hasSelfLinkedCrucibleFilters()));
    }

    @Test @Override
    public void assertThatFilterProvidersAreSorted()
    {
        assertThatFilterProvidersAreSortedWithLocalProvidersFirst("Crucible", "FishEye", "Third Parties");
    }
    
    @Test @Override @Ignore
    public void assertThatUrlProxyResourceIsWhitelistedToAccessFeCru()
    {
    }

    @Test
    public void assertThatUrlProxyResourceIsNotWhitelistedToAccessFeCru()
    {
        assertThat(restTester.getProxiedResponse("http://localhost:3990/streams/").getStatus(),
                   equalTo(HttpServletResponse.SC_FORBIDDEN));
    }
}
