package com.atlassian.streams.testing;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.internal.rest.representations.JsonProvider;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import org.apache.abdera.model.Feed;
import org.joda.time.DateTime;

public abstract class AbstractFeedClient
{
    protected final Client client;
    private final ApplicationProperties applicationProperties;

    public AbstractFeedClient(ApplicationProperties applicationProperties)
    {
        DefaultClientConfig config = new DefaultClientConfig();
        config.getClasses().add(JsonProvider.class);
        this.client = Client.create(config);
        this.applicationProperties = applicationProperties;
    }

    protected abstract String getPath();

    public String getBaseUrl()
    {
        return applicationProperties.getBaseUrl();
    }

    public URI getUri(Parameter... params)
    {
        UriBuilder uriBuilder = UriBuilder.fromUri(applicationProperties.getBaseUrl() + getPath());
        for (Parameter param : params)
        {
            uriBuilder.queryParam(param.name, param.value);
        }
        return uriBuilder.build();
    }

    public <T> T get(Class<T> type, Parameter... params)
    {
        return client.resource(getUri(params)).get(type);
    }

    public Feed get(Parameter... params)
    {
        return get(Feed.class, params);
    }

    public Feed getAs(String userName, Parameter... params)
    {
        return getAs(userName, Feed.class, params);
    }

    public <T> T getAs(String userName, final Class<T> type, final Parameter... params)
    {
        return doAs(userName, new Supplier<T>() { public T get() {
            return AbstractFeedClient.this.get(type, params);
        }});
    }

    public Feed getAs(String userName, final String requestLanguages, final Parameter... params)
    {
        ClientFilter acceptLanguage = new ClientFilter()
        {
            @Override
            public ClientResponse handle(ClientRequest cr) throws ClientHandlerException
            {
                cr.getHeaders().putSingle("Accept-Language", requestLanguages);
                return getNext().handle(cr);
            }
        };

        try
        {
            client.addFilter(acceptLanguage);
            return getAs(userName, params);
        }
        finally
        {
            client.removeFilter(acceptLanguage);
        }
    }

    public void destroy()
    {
        client.destroy();
    }

    protected enum DateTimeToString implements Function<DateTime, String>
    {
        INSTANCE;

        public String apply(DateTime d)
        {
            if (d != null)
            {
                return Long.toString(d.getMillis());
            }
            return "";
        }
    }

    public static Parameter param(String name, String value)
    {
        return new Parameter(name, value);
    }

    public static Parameter local()
    {
        return new Parameter("local", "true");
    }

    public static Parameter module(String name)
    {
        return new Parameter("module", name);
    }

    public static Parameter asc()
    {
        return param("asc", "true");
    }

    public static final class Parameter
    {
        private final String name;
        private final String value;

        private Parameter(String name, String value)
        {
            this.name = name;
            this.value = value;
        }
    }

    protected <T> T doAs(String userName, Supplier<T> supplier)
    {
        ClientFilter auth = new HTTPBasicAuthFilter(userName, userName);
        client.addFilter(auth);
        try
        {
            return supplier.get();
        }
        finally
        {
            client.removeFilter(auth);
        }
    }
}
