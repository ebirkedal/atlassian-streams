package com.atlassian.streams.testing;

import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.integrationtesting.runner.CompositeTestRunner;
import com.atlassian.integrationtesting.runner.TestGroupRunner;
import com.atlassian.integrationtesting.ui.UiTester;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.testing.StreamsUiTesterRunner.StreamsModule.Testers;

import com.google.common.base.Function;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import org.junit.runners.model.InitializationError;

public class AppLinksTestRunner extends CompositeTestRunner
{
    public AppLinksTestRunner(Class<?> klass, String app) throws InitializationError
    {
        super(klass, compose(app));
    }

    public static Composer compose(String app)
    {
        Injector injector = Guice.createInjector(new AppLinksModule(app));
        return CompositeTestRunner.compose().
            from(TestGroupRunner.compose()).
            beforeTestClass(new InjectStatics(injector)).
            afterTestClass(new DestroyFeedClient(injector));
    }
    
    private static final class InjectStatics implements Function<BeforeTestClass, Void>
    {
        private final Injector injector;

        public InjectStatics(Injector injector)
        {
            this.injector = injector;
        }

        public Void apply(BeforeTestClass test)
        {
            injector.createChildInjector(new StaticInjectionModule(test.testClass.getJavaClass()));
            return null;
        }
        
        private static final class StaticInjectionModule extends AbstractModule
        {
            private final Class<?> testClass;

            public StaticInjectionModule(Class<?> testClass)
            {
                this.testClass = testClass;
            }

            @Override
            protected void configure()
            {
                requestStaticInjection(testClass);
            }
        }
    }

    static final class AppLinksModule extends AbstractModule
    {
        private final String app;

        public AppLinksModule(String app)
        {
            this.app = app;
        }

        @Override
        protected void configure() {}

        @Provides @Singleton ApplicationProperties getApplicationProperties()
        {
            String baseUrl = System.getProperty("baseurl." + app + "-applinks");
            if (baseUrl == null)
            {
                baseUrl = System.getProperty("baseurl." + app + "-applinks-oauth");
            }
            return new ApplicationPropertiesImpl(baseUrl);
        }
        
        @Provides @Singleton UiTester getUiTester(ApplicationProperties applicationProperties)
        {
            return Testers.valueOf(app.toUpperCase()).get(applicationProperties);
        }

        @Provides @Singleton RestTester getRestTester(ApplicationProperties applicationProperties)
        {
            return new RestTester(applicationProperties);
        }
    }
}
