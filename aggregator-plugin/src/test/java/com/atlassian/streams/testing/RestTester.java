package com.atlassian.streams.testing;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.internal.rest.representations.I18nTranslations;
import com.atlassian.streams.internal.rest.representations.JsonProvider;
import com.atlassian.streams.internal.rest.representations.ConfigPreferencesRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.internal.rest.resources.ConfigurationPreferencesResource;
import com.atlassian.streams.internal.rest.resources.I18nResource;
import com.atlassian.streams.internal.rest.resources.StreamsConfigResource;
import com.atlassian.streams.internal.rest.resources.StreamsValidationResource;
import com.atlassian.streams.internal.rest.resources.UrlProxyResource;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.uri.UriBuilderImpl;

import static com.atlassian.streams.internal.rest.MediaTypes.STREAMS_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

public class RestTester
{
    private static final String DEFAULT_USERNAME = "admin";
    private static final String DEFAULT_PASSWORD = "admin";

    private final Client client;
    private final ApplicationProperties applicationProperties;

    public RestTester(ApplicationProperties applicationProperties)
    {
        this(applicationProperties, true);
    }

    public RestTester(ApplicationProperties applicationProperties, boolean login)
    {
        this(applicationProperties, login, DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }

    public RestTester(ApplicationProperties applicationProperties, String username, String password)
    {
        this(applicationProperties, true, username, password);
    }

    private RestTester(ApplicationProperties applicationProperties, boolean login, String username, String password)
    {
        this.applicationProperties = applicationProperties;
        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(JsonProvider.class);
        client = Client.create(config);
        if (login)
        {
            client.addFilter(new HTTPBasicAuthFilter(username, password));
        }
        client.setFollowRedirects(false);
    }

    public void destroy()
    {
        client.destroy();
    }

    public StreamsConfigRepresentation getStreamsConfigRepresentation()
    {
        WebResource configResource = client.resource(newBaseUriBuilder().path(StreamsConfigResource.class).build());
        return configResource.accept(STREAMS_JSON).type(STREAMS_JSON).get(StreamsConfigRepresentation.class);
    }

    public ConfigPreferencesRepresentation getConfigPreferencesRepresentation()
    {
        WebResource configResource = client.resource(newBaseUriBuilder().path(ConfigurationPreferencesResource.class).build());
        return configResource.accept(STREAMS_JSON).type(STREAMS_JSON).get(ConfigPreferencesRepresentation.class);
    }

    public String getI18nValue(String key, String... parameters)
    {
        UriBuilder uriBuilder = newBaseUriBuilder().path(I18nResource.class).path("key").path(key);
        if (parameters.length > 0)
        {
            uriBuilder.queryParam("parameters", new Object[] { parameters });
        }
        WebResource resource = client.resource(uriBuilder.build());
        return resource.accept(TEXT_PLAIN).type(TEXT_PLAIN).get(String.class);
    }

    public I18nTranslations getI18nValues(String prefix)
    {
        WebResource resource = client.resource(newBaseUriBuilder().path(I18nResource.class).path("prefix").path(prefix).build());
        return resource.accept(APPLICATION_JSON).type(APPLICATION_JSON).get(I18nTranslations.class);
    }

    public ClientResponse getProxiedResponse(String url)
    {
        WebResource proxyResource = client.resource(newBaseUriBuilder().path(UrlProxyResource.class).queryParam("url", url).build());
        return proxyResource.get(ClientResponse.class);
    }

    public ClientResponse validatePreferences(MultivaluedMap<String, String> preferences)
    {
        WebResource resource = client.resource(newBaseUriBuilder().path(StreamsValidationResource.class).build());
        return resource.queryParams(preferences).type(STREAMS_JSON).get(ClientResponse.class);
    }

    protected UriBuilder newBaseUriBuilder()
    {
        return new UriBuilderImpl().replacePath(applicationProperties.getBaseUrl()).path("/rest/activity-stream/1.0");
    }
}
