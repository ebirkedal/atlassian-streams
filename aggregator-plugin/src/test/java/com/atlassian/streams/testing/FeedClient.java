package com.atlassian.streams.testing;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import org.joda.time.DateTime;

import static com.atlassian.streams.spi.StandardStreamsFilterOption.ACTIVITY_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ACTIVITY_OBJECT_VERB_SEPARATOR;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.STANDARD_FILTERS_PROVIDER_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.UPDATE_DATE;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;
/**
 * Fetches Activity Stream feed from the application.
 */
@Singleton
public class FeedClient extends AbstractFeedClient
{
    @Inject
    public FeedClient(ApplicationProperties applicationProperties)
    {
        super(applicationProperties);
    }

    @Override
    protected String getPath()
    {
        return "/activity";
    }

    public static Parameter maxResults(int max)
    {
        return param("maxResults", Integer.toString(max));
    }

    public static Parameter timeout(int timeout)
    {
        return param("timeout", Integer.toString(timeout));
    }

    public static Parameter provider(String... values)
    {
        return param("providers", Joiner.on("+").join(values));
    }

    public static Parameter key(Operator op, String... values)
    {
        return standardFilter(PROJECT_KEY, op, values);
    }

    public static Parameter updateDate(Operator op, DateTime... values)
    {
        return standardFilter(UPDATE_DATE.getKey(), op, transform(asList(values), DateTimeToString.INSTANCE));
    }

    public static Parameter user(Operator op, String... values)
    {
        return standardFilter(USER.getKey(), op, values);
    }

    public static Parameter issueKey(Operator op, String... values)
    {
        return standardFilter(ISSUE_KEY.getKey(), op, values);
    }

    public static Parameter activities(String providerKey, Operator op,
            Iterable<Pair<ActivityObjectType, ActivityVerb>> activities)
    {
        return providerFilter(providerKey, ACTIVITY_KEY, op, transform(activities, toActivityKeys()));
    }

    private static Function<Pair<ActivityObjectType, ActivityVerb>, String> toActivityKeys()
    {
        return new Function<Pair<ActivityObjectType,ActivityVerb>, String>()
        {
            public String apply(Pair<ActivityObjectType, ActivityVerb> a)
            {
                return a.first().key() + ACTIVITY_OBJECT_VERB_SEPARATOR + a.second().key();
            }
        };
    }

    private static Parameter standardFilter(String filterKey, Operator op, String... values)
    {
        return standardFilter(filterKey, op, ImmutableList.copyOf(values));
    }

    private static Parameter standardFilter(String filterKey, Operator op, Iterable<String> values)
    {
        return providerFilter(STANDARD_FILTERS_PROVIDER_KEY, filterKey, op, values);
    }

    public static Parameter providerFilter(String providerKey, String filterKey, Operator op, String... values)
    {
        return providerFilter(providerKey, filterKey, op, ImmutableList.copyOf(values));
    }

    public static Parameter providerFilter(String providerKey, String filterKey, Operator op, Iterable<String> values)
    {
        return param(providerKey, filterKey + " " + op + " " + Joiner.on(' ').join(values));
    }
}
