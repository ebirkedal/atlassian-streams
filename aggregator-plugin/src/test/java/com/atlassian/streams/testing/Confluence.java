package com.atlassian.streams.testing;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.testing.AbstractFeedClient.Parameter;

import com.google.common.collect.ImmutableList;

import org.joda.time.DateTime;
import org.joda.time.Months;

import static com.atlassian.streams.confluence.ConfluenceStreamsActivityProvider.PROVIDER_KEY;

public class Confluence
{
    public static final String CONFLUENCE_PROVIDER = "wiki";

    public static Parameter activities(Operator op, Iterable<Pair<ActivityObjectType, ActivityVerb>> activities)
    {
        return FeedClient.activities(PROVIDER_KEY, op, activities);
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb>... activities)
    {
        return activities(op, ImmutableList.copyOf(activities));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> activity)
    {
        return activities(op, ImmutableList.of(activity));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> a1, Pair<ActivityObjectType, ActivityVerb> a2)
    {
        return activities(op, ImmutableList.of(a1, a2));
    }

    /**
     * Timestamp for "My First Blog" 
     */
    public static final DateTime MY_FIRST_BLOG = new DateTime()
        .withYear(2010).withMonthOfYear(Months.ELEVEN.getMonths()).withDayOfMonth(11)
        .withHourOfDay(14).withMinuteOfHour(28).withSecondOfMinute(26).withMillisOfSecond(0);

    //2011-06-29T21:42:52.165Z
    public static final DateTime STRM_769_ADD_PAGE = new DateTime()
        .withYear(2011).withMonthOfYear(Months.SIX.getMonths()).withDayOfMonth(29)
        .withHourOfDay(14).withMinuteOfHour(42).withSecondOfMinute(52).withMillisOfSecond(165);

    //2011-06-29T21:43:03.043Z
    public static final DateTime STRM_769_EDIT_PAGE = new DateTime()
        .withYear(2011).withMonthOfYear(Months.SIX.getMonths()).withDayOfMonth(29)
        .withHourOfDay(14).withMinuteOfHour(43).withSecondOfMinute(3).withMillisOfSecond(43);

    // 2011-06-29T21:43:10.273Z
    public static final DateTime STRM_975_PAGE = new DateTime()
        .withYear(2011).withMonthOfYear(Months.SIX.getMonths()).withDayOfMonth(29)
        .withHourOfDay(14).withMinuteOfHour(43).withSecondOfMinute(10).withMillisOfSecond(273);

    // 2011-06-29T21:43:18.605Z
    public static final DateTime STRM_1426_PAGE = new DateTime()
        .withYear(2011).withMonthOfYear(Months.SIX.getMonths()).withDayOfMonth(29)
        .withHourOfDay(14).withMinuteOfHour(43).withSecondOfMinute(18).withMillisOfSecond(605);

    // 2011-06-29T21:43:26.253Z
    public static final DateTime STRM_1486_BLOG = new DateTime()
        .withYear(2011).withMonthOfYear(Months.SIX.getMonths()).withDayOfMonth(29)
        .withHourOfDay(14).withMinuteOfHour(43).withSecondOfMinute(26).withMillisOfSecond(253);

    // 2011-06-29T21:43:33.492Z
    public static final DateTime STRM_930_PAGE = new DateTime()
        .withYear(2011).withMonthOfYear(Months.SIX.getMonths()).withDayOfMonth(29)
        .withHourOfDay(14).withMinuteOfHour(43).withSecondOfMinute(33).withMillisOfSecond(492);

    // 2011-06-29T21:43:45.303Z
    public static final DateTime MIN_DATE_PAGE = new DateTime()
        .withYear(2011).withMonthOfYear(Months.SIX.getMonths()).withDayOfMonth(29)
        .withHourOfDay(14).withMinuteOfHour(43).withSecondOfMinute(45).withMillisOfSecond(303);

    // 2011-06-29T21:43:52.004Z
    public static final DateTime MAX_DATE_PAGE = new DateTime()
        .withYear(2011).withMonthOfYear(Months.SIX.getMonths()).withDayOfMonth(29)
        .withHourOfDay(14).withMinuteOfHour(43).withSecondOfMinute(52).withMillisOfSecond(4);
}
