package com.atlassian.streams.testing;

import java.util.List;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.testing.AbstractFeedClient.Parameter;

import com.google.common.collect.ImmutableList;

import org.apache.abdera.model.Entry;
import org.hamcrest.Matcher;
import org.joda.time.DateTime;
import org.joda.time.Months;

import static com.atlassian.streams.crucible.CrucibleStreamsActivityProvider.PROVIDER_KEY;
import static com.atlassian.streams.testing.AbstractFeedClient.module;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.joda.time.DateTimeZone.UTC;

public class Crucible
{
    public static final String CRUCIBLE_PROVIDER = "reviews";

    public static final class Data
    {
        // CR-1 was created 2009-10-20T04:27:25Z
        public static final DateTime CR_1_CREATE = new DateTime()
            .withYear(2009).withMonthOfYear(Months.TEN.getMonths()).withDayOfMonth(20)
            .withHourOfDay(4).withMinuteOfHour(27).withSecondOfMinute(25).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        // CR-1 was was started 2009-10-20T04:28:28Z
        public static final DateTime CR_1_START = new DateTime()
            .withYear(2009).withMonthOfYear(Months.TEN.getMonths()).withDayOfMonth(20)
            .withHourOfDay(4).withMinuteOfHour(28).withSecondOfMinute(28).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        // CR-3 was created 2010-11-13T00:27:47Z
        public static final DateTime CR_3_CREATE = new DateTime()
            .withYear(2010).withMonthOfYear(Months.ELEVEN.getMonths()).withDayOfMonth(13)
            .withHourOfDay(0).withMinuteOfHour(27).withSecondOfMinute(47).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        // CR-4 was created 2010-11-13T00:28:09Z
        public static final DateTime CR_4_CREATE = new DateTime()
            .withYear(2010).withMonthOfYear(Months.ELEVEN.getMonths()).withDayOfMonth(13)
            .withHourOfDay(0).withMinuteOfHour(28).withSecondOfMinute(9).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        // CR-4 was closed 2010-11-13T00:28:30Z
        public static final DateTime CR_4_CLOSE = new DateTime()
            .withYear(2010).withMonthOfYear(Months.ELEVEN.getMonths()).withDayOfMonth(13)
            .withHourOfDay(0).withMinuteOfHour(28).withSecondOfMinute(30).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        // CR-7 was created 2010-11-13T00:30:26Z
        public static final DateTime CR_7_CREATE = new DateTime()
            .withYear(2010).withMonthOfYear(Months.ELEVEN.getMonths()).withDayOfMonth(13)
            .withHourOfDay(0).withMinuteOfHour(30).withSecondOfMinute(26).withMillisOfSecond(425)
            .withZoneRetainFields(UTC);

        // CR-7 was commented 2010-11-13T00:30:44Z
        public static final DateTime CR_7_COMMENT = new DateTime()
            .withYear(2010).withMonthOfYear(Months.ELEVEN.getMonths()).withDayOfMonth(13)
            .withHourOfDay(0).withMinuteOfHour(30).withSecondOfMinute(44).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        // CR-7 comment was commented 2010-12-03T19:01:38Z
        public static final DateTime CR_7_THREAD_COMMENT = new DateTime()
            .withYear(2010).withMonthOfYear(Months.TWELVE.getMonths()).withDayOfMonth(03)
            .withHourOfDay(19).withMinuteOfHour(1).withSecondOfMinute(38).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        // CR-8 was created 2010-12-08T19:04:11Z
        public static final DateTime CR_8_CREATE = new DateTime()
            .withYear(2010).withMonthOfYear(Months.TWELVE.getMonths()).withDayOfMonth(8)
            .withHourOfDay(19).withMinuteOfHour(4).withSecondOfMinute(11).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        // CR-8 was abandoned 2010-12-08T19:04:32Z
        public static final DateTime CR_8_ABANDON = new DateTime()
            .withYear(2010).withMonthOfYear(Months.TWELVE.getMonths()).withDayOfMonth(8)
            .withHourOfDay(19).withMinuteOfHour(4).withSecondOfMinute(32).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        // CR-9 was created 2010-12-08T19:04:36Z
        public static final DateTime CR_9_CREATE = new DateTime()
            .withYear(2010).withMonthOfYear(Months.TWELVE.getMonths()).withDayOfMonth(8)
            .withHourOfDay(19).withMinuteOfHour(4).withSecondOfMinute(36).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        // CR-9 was started 2010-12-08T19:08:41Z
        public static final DateTime CR_9_START = new DateTime()
            .withYear(2010).withMonthOfYear(Months.TWELVE.getMonths()).withDayOfMonth(8)
            .withHourOfDay(19).withMinuteOfHour(8).withSecondOfMinute(41).withMillisOfSecond(0)
            .withZoneRetainFields(UTC);

        public static List<Matcher<? super Entry>> allCrucibleEntries()
        {
            return ImmutableList.<Matcher<? super Entry>>builder()
                .add(reviewStrm1Created())
                .addAll(allCrucibleDefaultProjectEntries())
                .build();
        }
        
        public static List<Matcher<? super Entry>> allCrucibleDefaultProjectEntries()
        {
            return ImmutableList.<Matcher<? super Entry>>of(
                    review9Created(),
                    review8Abandoned(),
                    review8Created(),
                    review7CommentCommented(),
                    review7Commented(),
                    review7Created(),
                    review6Commented(),
                    review6ReOpened(),
                    review6Closed(),
                    review6Created(),
                    review5Closed(),
                    review5Created(),
                    review4Closed(),
                    review4Created(),
                    review3Summarized(),
                    review3Created(),
                    review2Created(),
                    review1Created());
        }

        public static Matcher<? super Entry> reviewStrm1Created()
        {
            return withTitle(allOf(
                containsString("A. D. Ministrator"),
                containsString("created and started review"),
                containsString("CR-STRM-1")
            ));
        }

        public static Matcher<? super Entry> review1Created()
        {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created and started review"),
                    containsString("CR-1"),
                    containsString("A Sample Review")));
        }

        public static Matcher<? super Entry> review2Created()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created and started review"),
                    containsString("CR-2"))))
                .and(withContent(allOf(
                    containsString("Default Project"),
                    containsString("Files: 0"),
                    containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review2Started()
        {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("started review"),
                    not(containsString("created")),
                    containsString("CR-2")));
        }

        public static Matcher<? super Entry> review3Created()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created and started review"),
                    containsString("CR-3"))))
                .and(withContent(allOf(
                    containsString("Default Project"),
                    containsString("Files: 0"),
                    containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review3CreatedOnly()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created review"),
                    containsString("CR-3"))))
                .and(withContent(allOf(
                    containsString("Default Project"),
                    containsString("Files: 0"),
                    containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review3Summarized()
        {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("summarised review"),
                    containsString("CR-3")));
        }

        public static Matcher<? super Entry> review4Created()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created and started review"),
                    containsString("CR-4"))))
                .and(withContent(allOf(
                    containsString("Default Project"),
                    containsString("Files: 0"),
                    containsString("Reviewers: None"))));
        }

        public static Matcher<Entry> review4Summarized()
        {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("summarised review"),
                    containsString("CR-4")));
        }

        public static Matcher<? super Entry> review4Closed()
        {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("summarised and closed review"),
                    containsString("CR-4")));
        }

        public static Matcher<? super Entry> review5Created()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created and started review"),
                    containsString("CR-5"))))
                .and(withContent(allOf(
                    containsString("Default Project"),
                    containsString("Files: 0"),
                    containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review5Summarized()
        {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("summarised review"),
                    containsString("CR-5")));
        }

        public static Matcher<? super Entry> review5Closed()
        {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("summarised and closed review"),
                    containsString("CR-5")));
        }

        public static Matcher<? super Entry> review6Created()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created and started review"),
                    containsString("CR-6"))))
                .and(withContent(allOf(
                    containsString("Default Project"),
                    containsString("Files: 0"),
                    containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review6Closed()
        {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("summarised and closed review"),
                    containsString("CR-6")));
        }

        public static Matcher<? super Entry> review6ReOpened()
        {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("reopened review"),
                    containsString("CR-6")));
        }

        public static Matcher<? super Entry> review6Commented()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("commented on review"),
                    containsString("CR-6"))))
                .and(withContent(containsString("We're commenting on this review")));
        }

        public static Matcher<? super Entry> review6CommentThatHasBeenDeleted()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("commented on review"),
                    containsString("CR-6"))))
                .and(withContent(containsString("comment to be deleted")));
        }
        
        public static Matcher<? super Entry> review7Created()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created and started review"),
                    containsString("CR-7"))))
                .and(withContent(allOf(
                    containsString("Default Project"),
                    containsString("Files: 0"),
                    containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review7Commented()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("commented on review"),
                    containsString("CR-7"))))
                .and(withContent(containsString("this is my comment")));
        }

        public static Matcher<? super Entry> review7CommentCommented()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("commented on review"),
                    containsString("CR-7"))))
                .and(withContent(containsString("reply to a comment")));
        }

        public static Matcher<? super Entry> review8Created()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created review"),
                    containsString("CR-8"))))
                .and(withContent(allOf(
                    containsString("Default Project"),
                    containsString("Files: 0"),
                    containsString("Reviewers: None"))));
        }

        public static Matcher<? super Entry> review8Abandoned()
        {
            return withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("abandoned review"),
                    containsString("CR-8")));
        }

        public static Matcher<? super Entry> review9Created()
        {
            return org.hamcrest.Matchers.<Entry>both(withTitle(allOf(
                    containsString("A. D. Ministrator"),
                    containsString("created and started review"),
                    containsString("CR-9"),
                    containsString("wow, that's a lot of files.."))))
                .and(withContent(allOf(
                    containsString("Default Project"),
                    containsString("Files: 9"),
                    containsString("Reviewers: None"))));
        }

    }

    public static Parameter crucibleModule()
    {
        return module(PROVIDER_KEY);
    }

    public static Parameter activities(Operator op, Iterable<Pair<ActivityObjectType, ActivityVerb>> activities)
    {
        return FeedClient.activities(PROVIDER_KEY, op, activities);
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb>... activities)
    {
        return activities(op, ImmutableList.copyOf(activities));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> activity)
    {
        return activities(op, ImmutableList.of(activity));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> a1, Pair<ActivityObjectType, ActivityVerb> a2)
    {
        return activities(op, ImmutableList.of(a1, a2));
    }
}
