package com.atlassian.streams.testing;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import com.atlassian.integrationtesting.ui.UiTester;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.jira.JiraFilterOptionProvider;
import com.atlassian.streams.testing.AbstractFeedClient.Parameter;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlFileInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

import org.apache.abdera.model.Entry;
import org.hamcrest.Matcher;
import org.hamcrest.core.CombinableMatcher;
import org.joda.time.DateTime;
import org.joda.time.Months;

import be.roam.hue.doj.Doj;

import static com.atlassian.streams.jira.JiraFilterOptionProvider.PROJECT_CATEGORY;
import static com.atlassian.streams.jira.JiraStreamsActivityProvider.PROVIDER_KEY;
import static com.atlassian.streams.testing.FeedClient.providerFilter;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.CombinableMatcher.CombinableBothMatcher;

public class Jira
{
    public static final String JIRA_PROVIDER = "issues";
    private static final String BROWSE_ISSUE_URL = "/browse/";
    private static final String EDIT_ISSUE_URL = "/streams/secure/EditIssue";

    public static void attachFile(UiTester uiTester, String issueKey, File file, String comment)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        // resolve attachment path using base url, then remove base url. we do this because the href in the link
        // will contain the context and to use uiTester.gotoPage we need to remove it
        String attachFilePath = URI.create(uiTester.getBaseUrl()).resolve(uiTester.elementById("attach-file").attribute("href"))
            .toASCIIString().substring(uiTester.getBaseUrl().length() + 1);
        uiTester.gotoPage(attachFilePath);

        ((HtmlFileInput) uiTester.currentPage().get("input").withAttribute("name", "tempFilename").firstElement()).setValueAttribute(file.getAbsolutePath());
        uiTester.waitForAsyncEventsToComplete();
        uiTester.elementById("comment").value(comment);
        uiTester.clickElementWithId("attach-file-submit");
    }

    public static void deleteAttachment(UiTester uiTester, String issueKey, String fileName)
    {
        uiTester.clickElementWithId("manage-attachment-link");
        try
        {
            Doj row = Doj.on(find(asList(uiTester.currentPage().get("issue-attachments-table td").allElements()), contentIs(fileName))).parent("tr");
            HtmlPage page = (HtmlPage) row.getByClass("icon-delete").click();
            Doj.on(page).getById("delete_submit").click();
        }
        catch (ClassCastException e)
        {
            throw new RuntimeException("Failed to delete attachment", e);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to delete attachment", e);
        }
    }

    public static void addComment(UiTester uiTester, String issueKey, String comment)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        try
        {
            uiTester.currentPage().get("a").withTextMatching("Comment").click();
            uiTester.elementById("comment").value(comment);
            uiTester.elementById("issue-comment-add-submit").click();
            uiTester.waitForAsyncEventsToComplete();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed add a comment on " + issueKey, e);
        }
    }

    public static void editIssue(UiTester uiTester, String issueKey, String summary, IssueType issueType,
            Priority priority, String dueDate, String environment, String description,
            File attachment, String comment)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        try
        {
            HtmlPage editPage = (HtmlPage) uiTester.currentPage().getById("editIssue").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(editPage.getUrl().getPath(), EDIT_ISSUE_URL);
            changeSummaryValue(editPage, summary);
            changeIssueTypeValue(editPage, issueType);
            changePriorityValue(editPage, priority);
            changeDueDateValue(editPage, dueDate);
            changeEnvironmentValue(editPage, environment);
            changeDescriptionValue(editPage, description);
            addAttachment(editPage, attachment);
            changeCommentValue(editPage, comment);

            Page newPage = Doj.on(editPage).getById("issue-edit-submit").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(newPage.getUrl().getPath(), BROWSE_ISSUE_URL + issueKey);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to edit issue " + issueKey, e);
        }
    }

    public static void editIssueSummary(UiTester uiTester, String issueKey, String summary)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        try
        {
            HtmlPage editPage = (HtmlPage) uiTester.currentPage().getById("editIssue").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(editPage.getUrl().getPath(), EDIT_ISSUE_URL);
            changeSummaryValue(editPage, summary);

            Page newPage = Doj.on(editPage).getById("issue-edit-submit").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(newPage.getUrl().getPath(), BROWSE_ISSUE_URL + issueKey);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to edit issue summary of " + issueKey, e);
        }
    }

    public static void editIssueType(UiTester uiTester, String issueKey, IssueType issueType)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        try
        {
            HtmlPage editPage = (HtmlPage) uiTester.currentPage().getById("editIssue").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(editPage.getUrl().getPath(), EDIT_ISSUE_URL);
            changeIssueTypeValue(editPage, issueType);

            Page newPage = Doj.on(editPage).getById("issue-edit-submit").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(newPage.getUrl().getPath(), BROWSE_ISSUE_URL + issueKey);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to edit issue type of " + issueKey, e);
        }
    }

    public static void editIssuePriority(UiTester uiTester, String issueKey, Priority priority)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        try
        {
            HtmlPage editPage = (HtmlPage) uiTester.currentPage().getById("editIssue").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(editPage.getUrl().getPath(), EDIT_ISSUE_URL);
            changePriorityValue(editPage, priority);

            Page newPage = Doj.on(editPage).getById("issue-edit-submit").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(newPage.getUrl().getPath(), BROWSE_ISSUE_URL + issueKey);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to edit issue priority of " + issueKey, e);
        }
    }

    public static void editIssueDueDate(UiTester uiTester, String issueKey, String dueDate)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        try
        {
            HtmlPage editPage = (HtmlPage) uiTester.currentPage().getById("editIssue").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(editPage.getUrl().getPath(), EDIT_ISSUE_URL);
            changeDueDateValue(editPage, dueDate);

            Page newPage = Doj.on(editPage).getById("issue-edit-submit").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(newPage.getUrl().getPath(), BROWSE_ISSUE_URL + issueKey);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to edit issue date of " + issueKey, e);
        }
    }

    public static void editIssueAssigneeByUserId(UiTester uiTester, String issueKey, String userId)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        try
        {
            HtmlPage editPage = (HtmlPage) uiTester.currentPage().getById("editIssue").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(editPage.getUrl().getPath(), EDIT_ISSUE_URL);
            changeAssigneeByUserId(editPage, userId);

            Page newPage = Doj.on(editPage).getById("issue-edit-submit").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(newPage.getUrl().getPath(), BROWSE_ISSUE_URL + issueKey);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to edit assignee of " + issueKey, e);
        }
    }

    public static void editIssueReporterByUserId(UiTester uiTester, String issueKey, String userId)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        try
        {
            HtmlPage editPage = (HtmlPage) uiTester.currentPage().getById("editIssue").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(editPage.getUrl().getPath(), EDIT_ISSUE_URL);
            changeReporterByUserId(editPage, userId);

            Page newPage = Doj.on(editPage).getById("issue-edit-submit").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(newPage.getUrl().getPath(), BROWSE_ISSUE_URL + issueKey);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to edit assignee of " + issueKey, e);
        }
    }

    public static void editIssueEnvironment(UiTester uiTester, String issueKey, String environment)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        try
        {
            HtmlPage editPage = (HtmlPage) uiTester.currentPage().getById("editIssue").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(editPage.getUrl().getPath(), EDIT_ISSUE_URL);
            changeEnvironmentValue(editPage, environment);

            Page newPage = Doj.on(editPage).getById("issue-edit-submit").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(newPage.getUrl().getPath(), BROWSE_ISSUE_URL + issueKey);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to edit issue environment of " + issueKey, e);
        }
    }

    public static void editIssueDescription(UiTester uiTester, String issueKey, String description)
    {
        uiTester.gotoPage(BROWSE_ISSUE_URL + issueKey);

        try
        {
            HtmlPage editPage = (HtmlPage) uiTester.currentPage().getById("editIssue").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(editPage.getUrl().getPath(), EDIT_ISSUE_URL);
            changeDescriptionValue(editPage, description);

            Page newPage = Doj.on(editPage).getById("issue-edit-submit").click();
            uiTester.waitForAsyncEventsToComplete();
            checkUrl(newPage.getUrl().getPath(), BROWSE_ISSUE_URL + issueKey);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to edit issue description of " + issueKey, e);
        }
    }

    public static void setUserLanguage(UiTester uiTester, String user, String language)
    {
        uiTester.gotoPage("/secure/UpdateUserPreferences!default.jspa?username=" + user);
        uiTester.elementById("update-user-preferences-locale").value(language);
        uiTester.clickElementWithId("update-user-preferences-submit");
    }

    private static void changeSummaryValue(HtmlPage editPage, String newValue)
    {
        if (newValue != null)
        {
            Doj.on(editPage).getById("summary").value(newValue);
        }
    }

    private static void changeIssueTypeValue(HtmlPage editPage, IssueType issueType) throws IOException
    {
        if (issueType != null)
        {
            HtmlSelect dropDown = (HtmlSelect) Doj.on(editPage).getById("issuetype").firstElement();
            dropDown.getOptionByText(issueType.value()).click();
        }
    }

    private static void changePriorityValue(HtmlPage editPage, Priority priority) throws IOException
    {
        if (priority != null)
        {
            HtmlSelect dropDown = (HtmlSelect) Doj.on(editPage).getById("priority").firstElement();
            dropDown.getOptionByText(priority.value()).click();
        }
    }

    private static void changeDueDateValue(HtmlPage editPage, String dueDate)
    {
        if (dueDate != null)
        {
            Doj.on(editPage).getById("duedate").value(dueDate);
        }
    }

    private static void changeAssigneeByUserId(HtmlPage editPage, String userId) throws IOException
    {
        if (userId != null)
        {
            HtmlSelect dropDown = (HtmlSelect) Doj.on(editPage).getById("assignee").firstElement();
            dropDown.getOptionByValue(userId).click();
        }
    }

    private static void changeReporterByUserId(HtmlPage editPage, String userId) throws IOException
    {
        if (userId != null)
        {
            Doj.on(editPage).getById("reporter").value(userId);
        }
    }

    private static void changeEnvironmentValue(HtmlPage editPage, String environment) throws IOException
    {
        if (environment != null)
        {
            Doj.on(editPage).getById("environment").value(environment);
        }
    }

    private static void changeDescriptionValue(HtmlPage editPage, String description) throws IOException
    {
        if (description != null)
        {
            Doj.on(editPage).getById("description").value(description);
        }
    }

    private static void changeCommentValue(HtmlPage editPage, String comment) throws IOException
    {
        if (comment != null)
        {
            Doj.on(editPage).getById("comment").value(comment);
        }
    }

    private static void addAttachment(HtmlPage editPage, File file)
    {
        if (file != null)
        {
            Doj.on(editPage).getById("attachment_box").value(file.getAbsolutePath());
        }
    }

    private static void checkUrl(String currentUrl, String expectedUrl)
    {
        checkState(currentUrl.startsWith(expectedUrl), "Not at expected url of " + expectedUrl + ", instead at " + currentUrl);
    }

    private static Predicate<HtmlElement> contentIs(final String content)
    {
        return new Predicate<HtmlElement>()
        {
            public boolean apply(HtmlElement e)
            {
                return content.equals(e.getTextContent());
            }
        };
    }

    public static enum IssueType
    {
        BUG("Bug", "1"),
        NEW_FEATURE("New Feature", "2"),
        TASK("Task", "3"),
        IMPROVEMENT("Improvement", "4");

        private final String issueType;
        private final String id;

        IssueType(String issueType, String id)
        {
            this.issueType = issueType;
            this.id = id;
        }

        public String value()
        {
            return issueType;
        }

        public String id()
        {
            return id;
        }
    }

    public static enum Priority
    {
        BLOCKER("Blocker"),
        CRITICAL("Critical"),
        MAJOR("Major"),
        MINOR("Minor"),
        TRIVIAL("Trivial");

        private final String priority;

        Priority(String priority)
        {
            this.priority = priority;
        }

        public String value()
        {
            return priority;
        }
    }

    /**
     * Provides matchers for entries that will appear in the activity stream when started with the
     * jira-streams-test-resources.zip or jira-streams-applinks-test-resources.zip as the home directory or when
     * the streams-jira-data.xml file is restored.
     */
    public static final class Data
    {
        // issue ONE-1 was created at 2009-01-23 15:52:23
        public static final DateTime ONE1_CREATED = new DateTime()
            .withYear(2009).withMonthOfYear(Months.ONE.getMonths()).withDayOfMonth(23)
            .withHourOfDay(15).withMinuteOfHour(52).withSecondOfMinute(23).withMillisOfSecond(0);

        // issue TWO-1 was created at 2009-01-23 15:52:37
        public static final DateTime TWO1_CREATED = new DateTime()
            .withYear(2009).withMonthOfYear(Months.ONE.getMonths()).withDayOfMonth(23)
            .withHourOfDay(15).withMinuteOfHour(52).withSecondOfMinute(37).withMillisOfSecond(0);

        // comment on ONE-1 was made at 2009-01-23 16:08:16
        public static final DateTime ONE1_COMMENTED = new DateTime()
            .withYear(2009).withMonthOfYear(Months.ONE.getMonths()).withDayOfMonth(23)
            .withHourOfDay(16).withMinuteOfHour(8).withSecondOfMinute(16).withMillisOfSecond(0);

        // issue ONE-2 was created at 2009-01-27 11:16:21
        public static final DateTime ONE2_CREATED = new DateTime()
            .withYear(2009).withMonthOfYear(Months.ONE.getMonths()).withDayOfMonth(27)
            .withHourOfDay(11).withMinuteOfHour(16).withSecondOfMinute(21).withMillisOfSecond(0);


        public static Matcher<? super Entry> createdOne2()
        {
            CombinableBothMatcher<String> containsCreated = both(containsString("created"));
            return withTitle(containsCreated.and(containsString("ONE-2")));
        }

        public static Matcher<? super Entry> commentedOne1()
        {
            CombinableBothMatcher<String> containsCommented = both(containsString("commented"));
            CombinableBothMatcher<Entry> titleContainsCommented = both(withTitle(containsCommented
                    .and(containsString("ONE-1"))));
            return titleContainsCommented.and(withContent(containsString("This is a comment.")));
        }

        public static Matcher<? super Entry> createdTwo1()
        {
            CombinableBothMatcher<String> containsCreated = both(containsString("created"));
            return withTitle(containsCreated.and(containsString("TWO-1")));
        }

        public static Matcher<? super Entry> createdOne1()
        {
            CombinableBothMatcher<String> containsCreated = both(containsString("created"));
            return withTitle(containsCreated.and(containsString("ONE-1")));
        }
    }

    public static Parameter issueType(Operator op, IssueType... issueTypes)
    {
        return providerFilter(PROVIDER_KEY, "issue_type", op, transform(asList(issueTypes), IssueTypeToId.INSTANCE));
    }

    public static Parameter projectCategory(Operator op, String... categoryIds)
    {
        return providerFilter(PROVIDER_KEY, PROJECT_CATEGORY, op, categoryIds);
    }

    public static Parameter activities(Operator op, Iterable<Pair<ActivityObjectType, ActivityVerb>> activities)
    {
        return FeedClient.activities(PROVIDER_KEY, op, activities);
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb>... activities)
    {
        return activities(op, ImmutableList.copyOf(activities));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> activity)
    {
        return activities(op, ImmutableList.of(activity));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> a1, Pair<ActivityObjectType, ActivityVerb> a2)
    {
        return activities(op, ImmutableList.of(a1, a2));
    }

    private enum IssueTypeToId implements Function<IssueType, String>
    {
        INSTANCE;

        public String apply(IssueType issueType)
        {
            return issueType.id;
        }
    }
}
