package com.atlassian.streams.internal.rest.resources.whitelist;

import java.net.URI;

import com.atlassian.sal.api.ApplicationProperties;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AppLinksWhitelistTest
{
    private static final String LOCAL_SERVER = "http://localhost:3990/streams";
    private static final String APPLINK_SERVER = "http://path/to/my/server";

    @Mock ApplicationProperties applicationProperties;
    @Mock AppLinksUriSupplier whitelistUris;

    private Whitelist whitelist;

    @Before
    public void setup() throws Exception
    {
        when(applicationProperties.getBaseUrl()).thenReturn(LOCAL_SERVER);
        when(whitelistUris.get()).thenReturn(ImmutableList.of(URI.create(APPLINK_SERVER)));

        whitelist = new AppLinksWhitelist(applicationProperties, whitelistUris);
    }

    @Test
    public void assertThatApplinkedServerIsWhitelisted()
    {
        assertTrue(whitelist.allows(URI.create(APPLINK_SERVER)));
    }

    @Test
    public void assertThatLocalServerIsWhitelisted()
    {
        assertTrue(whitelist.allows(URI.create(LOCAL_SERVER)));
    }

    @Test
    public void assertThatLocalServerUrlProxyResourceIsNotWhitelisted()
    {
        assertFalse(whitelist.allows(URI.create(LOCAL_SERVER + "/rest/activity-stream/1.0/url-proxy")));
    }

    /**
     * If A is applinked to B and B is applinked to C, but A is not applinked to C, ensure that
     * A cannot access C through B's proxy resource.
     */
    @Test
    public void assertThatApplinkedServerUrlProxyResourceIsNotWhitelisted()
    {
        assertFalse(whitelist.allows(URI.create(APPLINK_SERVER + "/rest/activity-stream/1.0/url-proxy?url=http://path/to/another/server")));
    }

    @Test
    public void assertThatUnknownServerIsNotWhitelisted()
    {
        assertFalse(whitelist.allows(URI.create("http://path/to/unknown/server")));
    }
}
