package com.atlassian.streams.internal;

import com.atlassian.streams.api.common.uri.Uri;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ActivityProvidersTest
{
    private static final String BAMBOO_BASEURL = "http://idog.private.atlassian.com:8080/bamboo";
    private static final String FISHEYE_BASEURL = "http://idog.private.atlassian.com";

    @Mock LocalActivityProviders localProviders;
    @Mock AppLinksActivityProviders applinksProviders;

    @Mock AppLinksActivityProvider bamboo;
    @Mock AppLinksActivityProvider fisheye;

    private ActivityProviders providers;

    @Before
    public void setUp()
    {
        when(bamboo.getBaseUrl()).thenReturn(BAMBOO_BASEURL);
        when(fisheye.getBaseUrl()).thenReturn(FISHEYE_BASEURL);
        when(localProviders.get()).thenReturn(ImmutableList.<ActivityProvider>of());
        when(applinksProviders.get()).thenReturn(ImmutableList.<ActivityProvider>of(bamboo, fisheye));
        providers = new ActivityProviders(localProviders, applinksProviders);
    }

    @Test
    public void assertThatLongerBaseUrlIsLinkedToCorrectApplinksActivityProvider() throws Exception
    {
        assertThat(providers.getRemoteProviderForUri(Uri.parse(BAMBOO_BASEURL)).get(), is(equalTo(bamboo)));
    }

    @Test
    public void assertThatShorterBaseUrlIsLinkedToCorrectApplinksActivityProvider() throws Exception
    {
        assertThat(providers.getRemoteProviderForUri(Uri.parse(FISHEYE_BASEURL)).get(), is(equalTo(fisheye)));
    }
}
