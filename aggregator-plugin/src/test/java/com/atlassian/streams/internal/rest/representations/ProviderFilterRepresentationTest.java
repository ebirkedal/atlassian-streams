package com.atlassian.streams.internal.rest.representations;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.spi.StreamsFilterOption;
import com.atlassian.streams.spi.StreamsFilterOptionProvider;

import com.google.common.collect.ImmutableList;
import com.google.inject.internal.ImmutableMap;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.projectKeys;
import static com.atlassian.streams.spi.StreamsFilterOptionProvider.ActivityOption;
import static com.google.common.collect.Iterables.get;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProviderFilterRepresentationTest
{
    @Mock StreamsFilterOptionProvider provider;
    @Mock I18nResolver i18nResolver;

    private final String KEY = "testkey";
    private final String NAME = "testname";
    private final StreamsFilterOption PROJECT_KEYS = projectKeys(ImmutableMap.<String, String>of(), "testproduct");

    private ProviderFilterRepresentation rep;

    @Before
    public void setup()
    {
        when(provider.getActivities()).thenReturn(ImmutableList.<ActivityOption>of());
        when(i18nResolver.getText(anyString())).thenAnswer(new Answer<Object>()
        {
            public Object answer(InvocationOnMock invocation) throws Throwable
            {
                return invocation.getArguments()[0];
            }
        });

        //force each test case to set the representation with the desired filter order
        rep = null;
    }

    @Test
    public void testProjectIsOrderedFirstWhenSuppliedFirstInList()
    {
        build(ImmutableList.of(PROJECT_KEYS, USER, ISSUE_KEY));
        assertThat(get(rep.getOptions(), 0), hasKey(equalTo(PROJECT_KEY)));
    }

    @Test
    public void testProjectIsOrderedFirstWhenSuppliedLastInList()
    {
        build(ImmutableList.of(USER, ISSUE_KEY, PROJECT_KEYS));
        assertThat(get(rep.getOptions(), 0), hasKey(equalTo(PROJECT_KEY)));
    }

    private void build(Iterable<StreamsFilterOption> options)
    {
        when(provider.getFilterOptions()).thenReturn(options);
        rep = new ProviderFilterRepresentation(KEY, NAME, NAME, provider, i18nResolver);
    }

    private static Matcher<FilterOptionRepresentation> hasKey(Matcher<? super String> matcher)
    {
        return new HasKey(matcher);
    }

    private static final class HasKey extends TypeSafeDiagnosingMatcher<FilterOptionRepresentation>
    {
        private final Matcher<? super String> matcher;

        public HasKey(Matcher<? super String> matcher)
        {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(FilterOptionRepresentation rep, Description mismatchDescription)
        {
            if (!matcher.matches(rep.getKey()))
            {
                mismatchDescription.appendText("key ");
                matcher.describeMismatch(rep.getKey(), mismatchDescription);
                return false;
            }

            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("key ").appendDescriptionOf(matcher);
        }
    }
}
