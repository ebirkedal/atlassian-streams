package com.atlassian.streams.internal.rest.resources;

import java.security.Principal;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.internal.ProjectKeyValidator;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StreamsValidationResourceTest
{
    @Mock ProjectKeyValidator validator;
    @Mock UserManager userManager;

    StreamsValidationResource streamsValidationResource;

    @Before
    public void createResource()
    {
        streamsValidationResource = new StreamsValidationResource(validator, userManager);
    }
    
    @Before
    public void prepareUserManager()
    {
        when(userManager.resolve("user")).thenReturn(new Principal()
        {
            public String getName()
            {
                return "user";
            }
        });
    }

    @Test
    public void assertThatValidateReturnsOkWhenOneKeyProviderAcceptsKey()
    {
        when(validator.allKeysAreValid(ImmutableList.of("keyB"), false)).thenReturn(true);
        assertThat(streamsValidationResource.validate("title", "keyB", "user", "1", false).getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatValidateReturnsErrorWhenNoKeyProviderAcceptsKey()
    {
        assertThat(streamsValidationResource.validate("title", "keyB", "user", "1", false).getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatValidateReturnsErrorWhenUserManagerCannotResolveUser()
    {
        assertThat(streamsValidationResource.validate("title", "keyB", "unknownuser", "1", false).getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }
}
