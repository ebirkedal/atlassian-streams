package com.atlassian.streams.internal.servlet;


import javax.servlet.http.HttpServletRequest;

import com.atlassian.sal.api.xsrf.XsrfHeaderValidator;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.streams.internal.PostReplyHandler;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.sal.api.xsrf.XsrfHeaderValidator.TOKEN_HEADER;
import static com.atlassian.streams.internal.PostReplyHandler.NO_CHECK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class StreamsCommentsServletTests
{
    private StreamsCommentsServlet servlet;

    @Mock
    private PostReplyHandler postReplyHandler;
    @Mock
    private XsrfTokenValidator xsrfTokenValidator;
    private XsrfHeaderValidator xsrfHeaderValidator = new XsrfHeaderValidator();
    @Mock
    private HttpServletRequest request;

    @Before
    public void setUp() throws Exception
    {
        when(xsrfTokenValidator.validateFormEncodedToken(request)).thenReturn(false);

        servlet = new StreamsCommentsServlet(postReplyHandler, xsrfTokenValidator, xsrfHeaderValidator);
    }

    @Test
    public void assertThatXsrfCheckingHappenWhenThereIsNoSkipHeader()
    {
        when(xsrfTokenValidator.getXsrfParameterName()).thenReturn("xsrfParam");

        assertThat(servlet.isXsrfSafe(request), is(false));

        // Verify that xsrfTokenValidator is called.
        verify(xsrfTokenValidator).getXsrfParameterName();
    }

    @Test
    public void assertThatXsrfCheckingNotHappenWhenThereIsSkipHeader()
    {
        when(request.getHeader(TOKEN_HEADER)).thenReturn(NO_CHECK);
        assertThat(servlet.isXsrfSafe(request), is(true));

        // Verify that xsrfTokenValidator is not called at all.
        verify(xsrfTokenValidator, never()).getXsrfParameterName();
    }
}
