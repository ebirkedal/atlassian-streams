package com.atlassian.streams.crucible;

import com.atlassian.streams.spi.StreamsKeyProvider;

import static com.google.common.base.Preconditions.checkNotNull;

public class CrucibleKeyProvider implements StreamsKeyProvider
{
    private ReviewKeys reviewKeys;

    public CrucibleKeyProvider(ReviewKeys reviewKeys)
    {
        this.reviewKeys = checkNotNull(reviewKeys, "reviewKeys");
    }

    public Iterable<StreamsKey> getKeys()
    {
        return reviewKeys.get();
    }
}
