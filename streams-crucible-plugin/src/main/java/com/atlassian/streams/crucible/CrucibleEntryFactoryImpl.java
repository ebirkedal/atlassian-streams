package com.atlassian.streams.crucible;

import java.net.URI;
import java.util.Date;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.NeedsAlternateLinkUri;
import com.atlassian.streams.api.StreamsEntry.NeedsApplicationType;
import com.atlassian.streams.api.StreamsEntry.NeedsAuthors;
import com.atlassian.streams.api.StreamsEntry.NeedsId;
import com.atlassian.streams.api.StreamsEntry.NeedsPostedDate;
import com.atlassian.streams.api.StreamsEntry.NeedsRenderer;
import com.atlassian.streams.api.StreamsEntry.NeedsVerb;
import com.atlassian.streams.api.StreamsEntry.Parameters;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.uri.Uris;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.StreamsUriBuilder;
import com.atlassian.streams.spi.UserProfileAccessor;

import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Review;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.crucible.CrucibleActivityObjectTypes.review;
import static com.atlassian.streams.crucible.CrucibleStreamsActivityProvider.PROVIDER_KEY;
import static com.atlassian.streams.crucible.CrucibleStreamsActivityProvider.REVIEW_CLOSE_REL;
import static com.atlassian.streams.crucible.CrucibleStreamsActivityProvider.REVIEW_SUMMARIZE_AND_CLOSE_REL;
import static com.atlassian.streams.spi.ServletPath.COMMENTS;
import static com.atlassian.streams.spi.StreamsActivityProvider.ICON_LINK_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.REPLY_TO_LINK_REL;
import static com.cenqua.crucible.model.managers.LogAction.COMMENT_ADDED;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringUtils.isEmpty;

public class CrucibleEntryFactoryImpl implements CrucibleEntryFactory
{
    private static final Logger log = LoggerFactory.getLogger(CrucibleEntryFactoryImpl.class);
    public static final String CRUCIBLE_APPLICATION_TYPE = "com.atlassian.fisheye";

    private final CrucibleCommentManager commentManager;
    private final UserProfileAccessor userProfileAccessor;
    private final UriProvider uriProvider;
    private final ReviewRendererFactory reviewRendererFactory;
    private final UserManager userManager;
    private final StreamsI18nResolver i18nResolver;
    private final CruciblePermissionAccessor permissionAccessor;
    private final ApplicationProperties applicationProperties;

    public CrucibleEntryFactoryImpl(final CrucibleCommentManager commentManager,
                                    UserProfileAccessor userProfileAccessor,
                                    UriProvider uriProvider,
                                    ReviewRendererFactory reviewRendererFactory,
                                    UserManager userManager,
                                    StreamsI18nResolver i18nResolver,
                                    CruciblePermissionAccessor permissionAccessor,
                                    ApplicationProperties applicationProperties)
    {
        this.commentManager = checkNotNull(commentManager, "commentManager");
        this.userProfileAccessor = checkNotNull(userProfileAccessor, "userProfileAccessor");
        this.uriProvider = checkNotNull(uriProvider, "uriProvider");
        this.reviewRendererFactory = checkNotNull(reviewRendererFactory, "reviewRendererFactory");
        this.userManager = checkNotNull(userManager, "userManager");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.permissionAccessor = checkNotNull(permissionAccessor, "permissionAccessor");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    @Override
    public Option<StreamsEntry> getEntryFromActivityItem(Pair<Pair<StreamsCrucibleActivityItem, ActivityVerb>, ActivityRequest> activityEntry, URI baseUri)
    {
        try
        {
            StreamsCrucibleActivityItem activityItem = activityEntry.first().first();
            ActivityVerb verb = activityEntry.first().second();

            if (COMMENT_ADDED.equals(activityItem.getLogAction()))
            {
                return getEntryFromComment(baseUri, option(commentManager.getById(activityItem.getEntityId())), verb);
            }
            else
            {
                return getEntryFromReview(activityItem, verb, baseUri);
            }
        }
        catch (Exception e)
        {
            log.warn("Error creating streams entry", e);
            return none(StreamsEntry.class);
        }
    }

    private Option<StreamsEntry> getEntryFromReview(StreamsCrucibleActivityItem activityItem, ActivityVerb verb, URI baseUri)
    {
        final Review review = activityItem.getReview();
        final Date changeTimestamp = verb.equals(post()) ? review.getCreateDate() : activityItem.getCreateDate();
        String changeUsername = null;
        try
        {
            changeUsername = activityItem.getUsername();
        }
        catch (NullPointerException e)
        {
            // activityItem.getUsername throws a NullPointerException if user does not exist
        }
        final String action = verb.key();
        final URI uri = uriProvider.getReviewUri(baseUri, review);

        final StreamsUriBuilder uriBuilder = new StreamsUriBuilder().
            setUrl(uri.toASCIIString()).
            setTimestamp(changeTimestamp);

        Parameters<NeedsId, NeedsPostedDate, NeedsAlternateLinkUri, NeedsApplicationType, NeedsRenderer, NeedsVerb, NeedsAuthors> params =
            StreamsEntry.params();
        //add a "summarise and close" or "close" link if the current user's review is in the appropriate state and everyone has completed it
        if (review.isAllReviewersComplete() && isReviewCreatorOrModerator(review))
        {
            if (review.getState().isReviewState())
            {
                params = params.addLink(uriProvider.getRestReviewServiceUri(baseUri, review), REVIEW_SUMMARIZE_AND_CLOSE_REL, none(String.class));
            }
            else if (review.getState().isSummarizeState())
            {
                params = params.addLink(uriProvider.getRestReviewServiceUri(baseUri, review), REVIEW_CLOSE_REL, none(String.class));
            }
        }

        return some(new StreamsEntry(params
                                    .id(uriBuilder.getUri())
                                    .postedDate(new DateTime(changeTimestamp))
                                    .applicationType(CRUCIBLE_APPLICATION_TYPE)
                                    .alternateLinkUri(uri)
                                    .authors(ImmutableNonEmptyList.of(
                                            userProfileAccessor.getUserProfile(baseUri, changeUsername)))
                                    .categories(ImmutableList.of(action))
                                    .addLink(uriProvider.getReviewIconUri(baseUri), ICON_LINK_REL, some(i18nResolver.getText("streams.item.crucible.tooltip")))
                                    .addActivityObject(buildReviewObject(uriBuilder.getUri(), review, baseUri))
                                    .verb(verb)
                                    .renderer(reviewRendererFactory.newRenderer(verb, review, baseUri))
                                    .baseUri(baseUri), i18nResolver));
    }

    protected Option<URI> buildReplyTo(final Comment item)
    {
        if (permissionAccessor.currentUserCanReplyToComment(item))
        {
            return some(URI.create(applicationProperties.getBaseUrl() + COMMENTS.getPath() + '/'
                                   + Uris.encode(PROVIDER_KEY) + '/'
                                   + item.getReviewId() + '/' + item.getId()).normalize());
        }
        else
        {
            return none();
        }
    }

    private boolean isReviewCreatorOrModerator(Review review)
    {
        String username = userManager.getRemoteUsername();
        if (username == null)
        {
            return false;
        }
        boolean isNullAuthor = false;
        if (review.getAuthor() == null)
        {
            log.debug("Review (id:{}) has null author", review.getId());
            isNullAuthor = true;
        }
        boolean isNullModerator = false;
        if (review.getModerator() == null)
        {
            log.debug("Review (id:{}) has null moderator", review.getId());
            isNullModerator = true;
        }
        return (!isNullAuthor && username.equals(review.getAuthor().getUsername()))
               || (!isNullModerator && username.equals(review.getModerator().getUsername()));

    }

    private Option<StreamsEntry> getEntryFromComment(final URI baseUri, Option<Comment> comment, ActivityVerb verb)
    {
        return comment.flatMap(commentEntry(baseUri, verb));
    }

    private Function<Comment, Option<StreamsEntry>> commentEntry(final URI baseUri, final ActivityVerb verb)
    {
        return new Function<Comment, Option<StreamsEntry>>()
        {
            @Override
            public Option<StreamsEntry> apply(Comment comment)
            {
                if (comment.isDeleted())
                {
                    return none();
                }

                final Date changeTimestamp = comment.getUpdatedDate();
                final String changeUsername = comment.getUser().getUsername();
                final URI uri = uriProvider.getCommentUri(baseUri, comment);

                final StreamsUriBuilder uriBuilder = new StreamsUriBuilder().
                    setUrl(uri.toASCIIString()).
                    setTimestamp(changeTimestamp);

                final StreamsUriBuilder inReplyToUriBuilder = new StreamsUriBuilder();
                if (comment.getReplyToComment() != null)
                {
                    inReplyToUriBuilder.setUrl(uriProvider.getCommentUri(baseUri, comment.getReplyToComment()).toASCIIString()).
                        setTimestamp(comment.getReplyToComment().getUpdatedDate());
                }
                else
                {
                    inReplyToUriBuilder.setUrl(uriProvider.getReviewUriAsString(baseUri, comment.getReview())).
                        setTimestamp(comment.getReview().getCreateDate());
                }

                return some(new StreamsEntry(StreamsEntry.params()
                        .id(uriBuilder.getUri())
                        .postedDate(new DateTime(changeTimestamp))
                        .applicationType(CRUCIBLE_APPLICATION_TYPE)
                        .alternateLinkUri(uri)
                        .inReplyTo(some(inReplyToUriBuilder.getUri()))
                        .authors(ImmutableNonEmptyList.of(userProfileAccessor.getUserProfile(baseUri, changeUsername)))
                        .categories(ImmutableList.of("commented"))
                        .addLink(buildReplyTo(comment), REPLY_TO_LINK_REL, none(String.class))
                        .addLink(uriProvider.getReviewIconUri(baseUri), ICON_LINK_REL, some(i18nResolver.getText("streams.item.crucible.tooltip")))
                        .addActivityObject(buildCommentObject(uriBuilder.getUri(), comment, baseUri))
                        .verb(verb)
                        .target(some(buildReviewObject(baseUri, comment.getReview())))
                        .baseUri(baseUri)
                        .renderer(reviewRendererFactory.newCommentRenderer(comment.getMessage(), comment.getDefectRaised())), i18nResolver));
            }
        };
    }

    private Option<String> getName(Review review)
    {
        if (review == null || isEmpty(review.getName()))
        {
            return none();
        }
        else
        {
            return some(review.getName());
        }
    }

    private ActivityObject buildReviewObject(URI baseUri, Review review)
    {
        URI id = new StreamsUriBuilder().
            setUrl(uriProvider.getReviewUri(baseUri, review).toASCIIString()).
            setTimestamp(review.getCreateDate()).
            getUri();
        return buildReviewObject(id, review, baseUri);
    }

    private ActivityObject buildReviewObject(URI id, Review review, URI baseUri)
    {
        return new ActivityObject(ActivityObject.params()
                                      .id(id.toASCIIString())
                                      .activityObjectType(review())
                                      .title(option(review.getPermaId()))
                                      .summary(getName(review))
                                      .alternateLinkUri(uriProvider.getReviewUri(baseUri, review)));
    }

    private ActivityObject buildCommentObject(URI id, Comment comment, URI baseUri)
    {
        return new ActivityObject(ActivityObject.params()
                                      .id(id.toASCIIString())
                                      .title(none(String.class))
                                      .activityObjectType(comment())
                                      .alternateLinkUri(uriProvider.getCommentUri(baseUri, comment)));
    }
}
