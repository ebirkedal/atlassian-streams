package com.atlassian.streams.crucible;

import com.atlassian.fecru.user.User;

import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Review;

public interface CrucibleCommentManager
{
    Comment getById(int commentId);

    Comment createComment(String message, Review review, User user);

    void announceCommentCreation(Review review, Comment newComment);

    /**
     * Determine if a comment can be added to a review
     * 
     * @param review The review that is to be checked if it can be commented on
     * @return true if a comment can be added, false otherwise
     */
    boolean canAddComment(Review review);
}
