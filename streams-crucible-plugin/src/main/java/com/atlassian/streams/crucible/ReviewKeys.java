package com.atlassian.streams.crucible;

import com.atlassian.crucible.spi.data.ProjectData;
import com.atlassian.crucible.spi.services.ProjectService;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.streams.spi.StreamsKeyProvider.StreamsKey;

import com.google.common.base.Function;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Collections2.transform;

public class ReviewKeys
{
    private ProjectService projectService;

    public ReviewKeys()
    {
        this(ComponentLocator.getComponent(ProjectService.class));
    }

    public ReviewKeys(ProjectService projectService)
    {
        this.projectService = checkNotNull(projectService, "projectService");
    }

    public Iterable<StreamsKey> get()
    {
        return transform(projectService.getAllProjects(), projectToStreamsKey);
    }

    private static final Function<ProjectData, StreamsKey> projectToStreamsKey = new Function<ProjectData, StreamsKey>()
    {
        public StreamsKey apply(ProjectData project)
        {
            return new StreamsKey(project.getKey(), project.getName());
        }

    };

}
