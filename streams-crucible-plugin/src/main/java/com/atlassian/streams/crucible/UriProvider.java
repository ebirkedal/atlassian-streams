package com.atlassian.streams.crucible;

import java.net.URI;

import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Project;
import com.cenqua.crucible.model.Review;

public class UriProvider
{
    private static final String REVIEW_ICON_PATH = "/download/resources/com.atlassian.streams.crucible/images/review.gif";

    public UriProvider()
    {
    }

    public URI getReviewUri(final URI baseUri, final Review review)
    {
        return URI.create(getReviewUriAsString(baseUri, review));
    }

    public String getReviewUriAsString(final URI baseUri, final Review review)
    {
        return baseUri.toASCIIString() + "/cru/" + review.getPermaId();
    }

    public URI getCommentUri(final URI baseUri, final Comment comment)
    {
        return URI.create(getReviewUriAsString(baseUri, comment.getReview()) + "#c" + comment.getId());
    }

    public URI getProjectUri(final URI baseUri, final Project project)
    {
        return URI.create(baseUri.toASCIIString() + "/project/" + project.getProjKey());
    }

    public URI getReviewIconUri(URI baseUri)
    {
        return URI.create(baseUri.toASCIIString() + REVIEW_ICON_PATH);
    }

    public URI getRestReviewServiceUri(final URI baseUri, final Review review)
    {
        return URI.create(baseUri.toASCIIString() + "/rest-service/reviews-v1/" + review.getPermaId());
    }
}
