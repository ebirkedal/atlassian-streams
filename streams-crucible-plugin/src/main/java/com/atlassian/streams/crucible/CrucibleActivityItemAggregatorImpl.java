package com.atlassian.streams.crucible;

import java.util.List;

import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.common.Pair;

import com.google.common.collect.ImmutableList;

import org.joda.time.Interval;

import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.close;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.createAndStart;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.start;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.summarize;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.summarizeAndClose;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static org.joda.time.Duration.standardMinutes;

/**
 * Combines related activity item entries under certain conditions
 */
public class CrucibleActivityItemAggregatorImpl implements CrucibleActivityItemAggregator
{
    /**
     * Aggregate related activity item entries under certain conditions into a single entry
     * @param activityItems the activity item list to aggregate
     * @return the modified activity item list with combined entries
     */
    public Iterable<Pair<StreamsCrucibleActivityItem, ActivityVerb>> aggregate(Iterable<Pair<StreamsCrucibleActivityItem, ActivityVerb>> activityItems)
    {
        ImmutableList<Pair<StreamsCrucibleActivityItem, ActivityVerb>> activities = ImmutableList.copyOf(activityItems);
        ImmutableList.Builder<Pair<StreamsCrucibleActivityItem, ActivityVerb>> builder =
                new ImmutableList.Builder<Pair<StreamsCrucibleActivityItem, ActivityVerb>>();

        for (int i = activities.size() - 1; i >= 0; i--)
        {
            Pair<StreamsCrucibleActivityItem, ActivityVerb> firstActivity = activities.get(i);
            if (post().equals(firstActivity.second()))
            {
                builder.add(getAggregatedEntryIfAble(activities, i, start(), createAndStart()));
            }
            else if (start().equals(firstActivity.second()))
            {
                if (!activityWasAggregated(activities, i, post()))
                {
                    builder.add(firstActivity);
                }
            }
            else if (summarize().equals(firstActivity.second()))
            {
                builder.add(getAggregatedEntryIfAble(activities, i, close(), summarizeAndClose()));
            }
            else if (close().equals(firstActivity.second()))
            {
                if (!activityWasAggregated(activities, i, summarize()))
                {
                    builder.add(firstActivity);
                }
            }
            else
            {
                builder.add(firstActivity);
            }
        }

        return builder.build();
    }

    /**
     *
     * @param activities List of activities
     * @param currentIndex the index of the activity item we're currently checking
     * @param verbToAggregate the verb the current activity should be aggregated with
     * @param verbToReplaceWith the verb to replace the current aggregated function with
     * @return the aggregated activity entry if able, the current activity otherwise
     */
    private Pair<StreamsCrucibleActivityItem, ActivityVerb> getAggregatedEntryIfAble(
        List<Pair<StreamsCrucibleActivityItem, ActivityVerb>> activities, int currentIndex, ActivityVerb verbToAggregate,
        ActivityVerb verbToReplaceWith)
    {
        Pair<StreamsCrucibleActivityItem, ActivityVerb> firstActivity = activities.get(currentIndex);

        if (currentIndex == 0)
        {
            return firstActivity;
        }

        // check if next element is verb to aggregate or if there is an activity with the verb to aggregate within a minute
        Pair<StreamsCrucibleActivityItem, ActivityVerb> nextActivity = activities.get(currentIndex - 1);
        if (verbToAggregate.equals(nextActivity.second()) && isSameReview(firstActivity, nextActivity))
        {
            return pair(firstActivity.first(), verbToReplaceWith);
        }
        for (int i = currentIndex - 1; i >= 0; i--)
        {
            Pair<StreamsCrucibleActivityItem, ActivityVerb> secondActivity = activities.get(i);
            if (activityDiffIsLongerThanAMinute(firstActivity, secondActivity))
            {
                // stop checking if the activity is past a minute
                break;
            }
            else if (verbToAggregate.equals(secondActivity.second())
                && isSameReview(firstActivity, secondActivity)
                && !activityDiffIsLongerThanAMinute(firstActivity, secondActivity))
            {
                // return aggregated entry if verbToAggregate matches the activity
                return pair(firstActivity.first(), verbToReplaceWith);
            }
        }

        return firstActivity;
    }

    /**
     * Check if current activity was aggregated before with the specified verb
     * @param activities List of activities
     * @param currentIndex the index of the activity item we're currently checking
     * @param verb the verb the current activity should be aggregated with
     * @return {@code true} if activity was aggregated, {@code false} otherwise
     */
    private boolean activityWasAggregated(List<Pair<StreamsCrucibleActivityItem, ActivityVerb>> activities, int currentIndex,
        ActivityVerb verb)
    {
        Pair<StreamsCrucibleActivityItem, ActivityVerb> firstActivity = activities.get(currentIndex);
        if (currentIndex == activities.size() - 1)
        {
            return false;
        }

        // check if prev element is verb to aggregate or if there is an activity with the verb to aggregate within a minute
        Pair<StreamsCrucibleActivityItem, ActivityVerb> prevActivity = activities.get(currentIndex + 1);
        if (verb.equals(prevActivity.second()) && isSameReview(firstActivity, prevActivity))
        {
            return true;
        }
        for (int i = currentIndex + 1; i < activities.size(); i++)
        {
            Pair<StreamsCrucibleActivityItem, ActivityVerb> secondActivity = activities.get(i);
            if (activityDiffIsLongerThanAMinute(firstActivity, secondActivity))
            {
                // stop checking if the activity is past a minute
                return false;
            }
            else if (verb.equals(secondActivity.second())
                && isSameReview(firstActivity, secondActivity)
                && !activityDiffIsLongerThanAMinute(firstActivity, secondActivity))
            {
                return true;
            }
        }

        return false;
    }

    private boolean isSameReview(Pair<StreamsCrucibleActivityItem, ActivityVerb> firstActivity, Pair<StreamsCrucibleActivityItem, ActivityVerb> secondActivity)
    {
        return firstActivity.first().getReviewId().equals(secondActivity.first().getReviewId());
    }

    private boolean activityDiffIsLongerThanAMinute(Pair<StreamsCrucibleActivityItem, ActivityVerb> firstActivity, Pair<StreamsCrucibleActivityItem, ActivityVerb> secondActivity)
    {
        return activityDiffIsLongerThanAMinute(firstActivity.first(), secondActivity.first());
    }

    private boolean activityDiffIsLongerThanAMinute(StreamsCrucibleActivityItem first, StreamsCrucibleActivityItem second)
    {
        return dateDiffIsLongerThanAMinute(first.getCreateDateTime(), second.getCreateDateTime());
    }

    private boolean dateDiffIsLongerThanAMinute(long first, long second)
    {
        return new Interval(min(first, second), max(first, second)).toDuration().isLongerThan(standardMinutes(1));
    }
}
