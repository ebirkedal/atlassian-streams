package com.atlassian.streams.crucible;

import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.ActivityVerbs.VerbFactory;

import static com.atlassian.streams.api.ActivityVerbs.ATLASSIAN_IRI_BASE;
import static com.atlassian.streams.api.ActivityVerbs.newVerbFactory;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.ActivityVerbs.update;

public final class CrucibleActivityVerbs
{
    private static final String CRUCIBLE_IRI_BASE = ATLASSIAN_IRI_BASE + "crucible/";
    private static final VerbFactory crucibleVerbs = newVerbFactory(CRUCIBLE_IRI_BASE);

    public static ActivityVerb abandon()
    {
        return crucibleVerbs.newVerb("abandon", update());
    }

    public static ActivityVerb close()
    {
        return crucibleVerbs.newVerb("close", update());
    }

    public static ActivityVerb summarizeAndClose()
    {
        return crucibleVerbs.newVerb("summarize-and-close", update());
    }

    public static ActivityVerb complete()
    {
        return crucibleVerbs.newVerb("complete", update());
    }

    public static ActivityVerb reopen()
    {
        return crucibleVerbs.newVerb("reopen", update());
    }

    public static ActivityVerb start()
    {
        return crucibleVerbs.newVerb("start", update());
    }

    public static ActivityVerb createAndStart()
    {
        return crucibleVerbs.newVerb("create-and-start", post());
    }

    public static ActivityVerb summarize()
    {
        return crucibleVerbs.newVerb("summarize", update());
    }

    public static ActivityVerb uncomplete()
    {
        return crucibleVerbs.newVerb("uncomplete", update());
    }
}