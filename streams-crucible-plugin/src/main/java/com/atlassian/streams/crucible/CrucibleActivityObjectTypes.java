package com.atlassian.streams.crucible;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityObjectTypes.TypeFactory;

import static com.atlassian.streams.api.ActivityObjectTypes.ATLASSIAN_IRI_BASE;
import static com.atlassian.streams.api.ActivityObjectTypes.newTypeFactory;

public final class CrucibleActivityObjectTypes
{
    private static final TypeFactory crucibleTypes = newTypeFactory(ATLASSIAN_IRI_BASE);

    public static ActivityObjectType project()
    {
        return crucibleTypes.newType("crucible-project");
    }

    public static ActivityObjectType review()
    {
        return crucibleTypes.newType("review");
    }
}
