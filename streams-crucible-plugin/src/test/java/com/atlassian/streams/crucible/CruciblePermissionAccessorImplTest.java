package com.atlassian.streams.crucible;

import com.atlassian.fecru.user.User;
import com.atlassian.fisheye.spi.TxTemplate;

import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Review;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CruciblePermissionAccessorImplTest
{
    @Mock private TxTemplate txTemplate;
    @Mock private CrucibleCommentManager commentManager;
    @Mock private Comment comment;
    @Mock private Review review;

    private CruciblePermissionAccessorImpl test;

    @Before
    public void setUp() throws Exception
    {
        when(comment.getReview()).thenReturn(review);
        when(comment.getReviewId()).thenReturn(123);
        when(comment.getId()).thenReturn(456);
        test = new CruciblePermissionAccessorImpl(txTemplate, commentManager);
    }

    @Test
    public void testCanReplyToCommentAffirmative()
    {
        when(review.checkWriteAccess(any(User.class))).thenReturn(true);
        when(commentManager.canAddComment(any(Review.class))).thenReturn(true);
        assertTrue("canReplyToComment should return true", test.currentUserCanReplyToComment(comment));
    }

    @Test
    public void testCanReplyToCommentNegativeReview()
    {
        when(review.checkWriteAccess(any(User.class))).thenReturn(true);
        when(commentManager.canAddComment(any(Review.class))).thenReturn(false);
        assertFalse("canReplyToComment should return false b/c the review isn't accepting comments",
                    test.currentUserCanReplyToComment(comment));
    }

    @Test
    public void testCanReplyToCommentNegativeUser()
    {
        when(review.checkWriteAccess(any(User.class))).thenReturn(false);
        when(commentManager.canAddComment(any(Review.class))).thenReturn(true);
        assertFalse("canReplyToComment should return false b/c the user can't comment", test.currentUserCanReplyToComment(
            comment));
    }
}
