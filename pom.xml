<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>public-pom</artifactId>
        <version>3.0.28</version>
    </parent>
    <groupId>com.atlassian.streams</groupId>
    <artifactId>streams-parent</artifactId>
    <version>6.1.0-SNAPSHOT</version>
    <name>Activity Stream Plugin Parent</name>
    <packaging>pom</packaging>
    <description>Lists recent activity in a single project, or in all projects.</description>
    <url>http://confluence.atlassian.com/display/JIRAEXT/Atlassian+Activity+Stream+Plugin</url>

    <ciManagement>
        <system>Bamboo</system>
        <url>https://ecosystem-bamboo.internal.atlassian.com/browse/STRM</url>
    </ciManagement>
    <scm>
        <connection>scm:git:git@bitbucket.org:atlassian/atlassian-streams.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:atlassian/atlassian-streams.git</developerConnection>
        <url>https://bitbucket.org/atlassian/atlassian-streams</url>
      <tag>HEAD</tag>
  </scm>

	<licenses>
		<license>
			<name>BSD License</name>
			<url>https://maven.atlassian.com/public/licenses/license.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

    <modules>
        <module>inline-actions-plugin</module>
        <module>streams-gadget-resources</module>
        <module>bamboo-inline-actions-plugin</module>
        <module>confluence-inline-actions-plugin</module>
        <module>jira-inline-actions-plugin</module>
        <module>fecru-inline-actions-plugin</module>
        <module>streams-api</module>
        <module>spi</module>
        <module>testing</module>
        <module>js-tests-plugin</module>
        <module>streams-bamboo-plugin</module>
        <module>streams-confluence-plugin</module>
        <module>streams-fisheye-plugin</module>
        <module>streams-crucible-plugin</module>
        <module>streams-jira-plugin</module>
        <module>streams-thirdparty-api</module>
        <module>streams-thirdparty-plugin</module>
        <module>aggregator-plugin</module>
        <module>core-plugin</module>
        <module>pageobjects</module>
    </modules>

    <issueManagement>
        <system>jira</system>
        <url>https://ecosystem.atlassian.net/browse/STRM</url>
    </issueManagement>

    <dependencyManagement>
        <dependencies>

            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>platform</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>third-party</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>com.atlassian.user</groupId>
                <artifactId>atlassian-user</artifactId>
                <version>${atlassian.user.version}</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>com.atlassian.bundles</groupId>
                <artifactId>jsr305</artifactId>
                <version>1.0</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>com.atlassian.velocity.htmlsafe</groupId>
                <artifactId>velocity-htmlsafe</artifactId>
                <version>1.0.2</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>com.atlassian.bonnie</groupId>
                <artifactId>atlassian-bonnie</artifactId>
                <version>3.2-beta5</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>commons-collections</groupId>
                <artifactId>commons-collections</artifactId>
                <version>3.2</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>commons-lang</groupId>
                <artifactId>commons-lang</artifactId>
                <version>2.4</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>2.4</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.codehaus.jackson</groupId>
                <artifactId>jackson-jaxrs</artifactId>
                <version>${jackson.version}</version>
            </dependency>
            <dependency>
                <groupId>com.sun.jersey</groupId>
                <artifactId>jersey-core</artifactId>
                <version>1.2</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>javax.xml.bind</groupId>
                <artifactId>jaxb-api</artifactId>
                <version>2.1</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.abdera</groupId>
                <artifactId>abdera-core</artifactId>
                <version>${abdera.version}</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.abdera</groupId>
                <artifactId>abdera-parser</artifactId>
                <version>${abdera.version}</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.abdera</groupId>
                <artifactId>abdera-extensions-main</artifactId>
                <version>${abdera.version}</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.velocity</groupId>
                <artifactId>velocity</artifactId>
                <version>1.5</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.felix</groupId>
                <artifactId>org.osgi.compendium</artifactId>
                <version>1.4.0</version>
                <scope>provided</scope>
                <exclusions>
                    <exclusion>
                        <groupId>org.apache.felix</groupId>
                        <artifactId>org.osgi.foundation</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>org.apache.felix</groupId>
                <artifactId>org.apache.felix.bundlerepository</artifactId>
                <version>1.4.0</version>
                <exclusions>
                    <exclusion>
                        <groupId>org.apache.felix</groupId>
                        <artifactId>org.osgi.core</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.apache.felix</groupId>
                        <artifactId>org.osgi.service.obr</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>net.sf.kxml</groupId>
                        <artifactId>kxml2</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>joda-time</groupId>
                <artifactId>joda-time</artifactId>
                <version>1.6</version>
                <scope>provided</scope>
            </dependency>
            <dependency>
                <groupId>net.sourceforge.nekohtml</groupId>
                <artifactId>nekohtml</artifactId>
                <version>1.9.21</version>
            </dependency>
            <!-- Test dependencies -->
            <dependency>
                <groupId>${project.groupId}</groupId>
                <artifactId>streams-testing</artifactId>
                <version>${project.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit-dep</artifactId>
                <version>4.5</version>
                <scope>test</scope>
                <exclusions>
                    <exclusion>
                        <groupId>org.hamcrest</groupId>
                        <artifactId>hamcrest-core</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-core</artifactId>
                <version>1.10.19</version>
                <scope>test</scope>
                <exclusions>
                    <exclusion>
                        <groupId>org.hamcrest</groupId>
                        <artifactId>hamcrest-core</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest-all</artifactId>
                <version>1.3</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>com.atlassian.integrationtesting</groupId>
                <artifactId>atlassian-integrationtesting-lib</artifactId>
                <version>${ait.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>com.sun.jersey</groupId>
                <artifactId>jersey-client</artifactId>
                <version>1.2</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>com.sun.jersey.contribs</groupId>
                <artifactId>jersey-atom-abdera</artifactId>
                <version>1.2</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.abdera</groupId>
                <artifactId>abdera-extensions-json</artifactId>
                <version>1.1</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>rhino</groupId>
                <artifactId>js</artifactId>
                <version>1.7R1</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>com.atlassian.selenium</groupId>
                <artifactId>atlassian-webdriver-core</artifactId>
                <version>${atlassian.selenium.version}</version>
            </dependency>
            <dependency>
                <groupId>com.atlassian.jira</groupId>
                <artifactId>atlassian-jira-pageobjects</artifactId>
                <version>${jira.version}</version>
                <scope>test</scope>
                <exclusions>
                    <exclusion>
                        <groupId>com.google.collections</groupId>
                        <artifactId>google-collections</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>com.atlassian.selenium</groupId>
                <artifactId>atlassian-webdriver-confluence</artifactId>
                <version>${atlassian.selenium.version}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
    <build>
        <extensions>
            <extension>
                <groupId>org.apache.maven.wagon</groupId>
                <artifactId>wagon-webdav</artifactId>
                <version>1.0-beta-2</version>
            </extension>
        </extensions>
        <plugins>
            <plugin>
                <groupId>com.atlassian.bamboo.maven.sharing</groupId>
                <artifactId>bamboo-artifact-sharing-maven-plugin</artifactId>
                <version>${bamboo.sharing.plugin.version}</version>
                <executions>
                    <execution>
                        <id>sharing</id>
                        <goals>
                            <goal>share</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <id>enforce-guava</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <phase>validate</phase>
                        <configuration>
                            <rules>
                                <bannedDependencies>
                                    <searchTransitive>true</searchTransitive>
                                    <message>Something is depending on google-collections (perhaps transitively), but we use guava</message>
                                    <excludes>
                                        <exclude>com.google.collections:google-collections</exclude>
                                    </excludes>
                                </bannedDependencies>
                            </rules>
                            <fail>true</fail>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>com.atlassian.maven.plugins</groupId>
                    <artifactId>maven-amps-plugin</artifactId>
                    <version>${amps.version}</version>
                    <extensions>true</extensions>
                    <configuration>
                        <skipAllPrompts>true</skipAllPrompts>
                        <systemPropertyVariables>
                            <upm.pac.disable>true</upm.pac.disable>
                            <atlassian.upm.server.data.disable>true</atlassian.upm.server.data.disable>
                        </systemPropertyVariables>
                    </configuration>
                </plugin>
                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <configuration>
                        <skip>${maven.test.unit.skip}</skip>
                        <excludes>
                            <exclude>it/**/*</exclude>
                            <exclude>com/atlassian/streams/test/*</exclude>
                        </excludes>
                        <systemProperties>
                            <property>
                                <name>jsunit.entrypoint</name>
                                <value>${basedir}/src/test/javascript/test.js</value>
                            </property>
                        </systemProperties>
                        <junitArtifactName>junit:junit-dep</junitArtifactName>
                    </configuration>
                    <executions>
                        <execution>
                            <id>integration-test</id>
                            <phase>integration-test</phase>
                            <goals>
                                <goal>test</goal>
                            </goals>
                            <configuration>
                                <skip>${maven.test.it.skip}</skip>
                                <includes>
                                    <include>it/**/*java</include>
                                </includes>
                                <systemProperties>
                                    <property>
                                        <name>baseurl</name>
                                        <value>http://${server}:${httpPort}${contextPath}</value>
                                    </property>
                                </systemProperties>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <properties>
        <failOnMilestoneOrReleaseCandidateDeps>true</failOnMilestoneOrReleaseCandidateDeps>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <jdkLevel>1.7</jdkLevel>
        <amps.version>5.1.5</amps.version>
        <ait.version>1.1.2</ait.version>
        <jirawallboard.version>1.5.7</jirawallboard.version>
        <bamboo.sharing.plugin.version>3.0</bamboo.sharing.plugin.version>

        <rome.version>1.0</rome.version>

        <!-- STRM-1109 : product.target.versions are set so that they'll work in the 'final' target versions of the products
            even if we're still specifying RCs here -->
        <bamboo.version>5.8.1</bamboo.version>
        <bamboo.target.version>5.8.1</bamboo.target.version>

        <fecru.version>3.6.3-20141229134624</fecru.version>

        <jira.version>7.0.0-QR20150609193101</jira.version>

        <confluence.target.version>5.6</confluence.target.version>
        <confluence.version>5.6</confluence.version>

        <refapp.version>3.0.0-m245</refapp.version>

        <atlassian.concurrent.import.version>2.3.0</atlassian.concurrent.import.version>
        <atlassian.user.version>1.9</atlassian.user.version>
        <atlassian.annotations.version>0.8.1</atlassian.annotations.version>

        <server>localhost</server>
        <contextPath>/streams</contextPath>
        <httpPort>3990</httpPort>
        <httpPort.jira.applinks>2990</httpPort.jira.applinks>
        <httpPort.confluence.applinks>1990</httpPort.confluence.applinks>
        <httpPort.bamboo.applinks>6990</httpPort.bamboo.applinks>

        <sal.import.version>3.0.0</sal.import.version>
        <applinks.import.version>4.1.2</applinks.import.version>
        <trustedapps.version>4.0.0</trustedapps.version>
        <app.start.phase>pre-integration-test</app.start.phase>

        <abdera.version>1.1</abdera.version>
        <javax.ws.rs.version>1.0</javax.ws.rs.version>
        <axiom.version>1.2.5</axiom.version>
        <jaxen.version>1.1.1</jaxen.version>
        <woodstox.version>3.2.6</woodstox.version>
        <commons.codec.version>1.4</commons.codec.version>
        <antisamy.version>1.4.4</antisamy.version>

        <!-- dogfood server deploy config -->
        <utac.server>dashboard-test.atlassian.com</utac.server>
        <utac.httpPort>80</utac.httpPort>
        <utac.jira.contextPath>/jira</utac.jira.contextPath>
        <utac.confluence.contextPath>/confluence</utac.confluence.contextPath>

        <atlassian.selenium.version>2.1.0.m3</atlassian.selenium.version>
        <xvfb.enable>true</xvfb.enable>
        <webdriver.browser>firefox-3.5</webdriver.browser>

        <enforcer.fail>false</enforcer.fail>

        <org.w3c.css.version>1.3</org.w3c.css.version>
        <xerces.version>2.9.1</xerces.version>

        <failure.cache.version>0.14</failure.cache.version>
        <failure.cache.import.version>0.14</failure.cache.import.version>

        <platform.version>3.0.0-m090</platform.version>

        <rest.version>3.0.0</rest.version>
        <jackson.version>1.9.1</jackson.version>

    </properties>

    <profiles>
        <profile>
            <id>it</id>
            <modules>
                <module>ui-tests</module>
            </modules>
        </profile>

        <profile>
            <id>ie</id>
            <properties>
                <webdriver.browser>ie</webdriver.browser>
                <xvfb.enable>false</xvfb.enable>
            </properties>
            <modules>
                <module>ui-tests</module>
            </modules>
        </profile>

        <profile>
            <id>win7-firefox</id>
            <properties>
                <webdriver.browser>firefox-3.5</webdriver.browser>
                <xvfb.enable>false</xvfb.enable>
            </properties>
            <modules>
                <module>ui-tests</module>
            </modules>
        </profile>

        <profile>
            <id>deploy</id>
            <build>
                <defaultGoal>deploy</defaultGoal>
            </build>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
        </profile>
        <!-- Disable Javadoc linting on Java 8+ till we fix the docs -->
        <profile>
            <id>javadoc-linting-java8-disable</id>
            <activation>
                <jdk>[1.8,)</jdk>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <configuration>
                            <additionalparam>-Xdoclint:none</additionalparam>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
